﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UpdateTokensOldRepository : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string message = String.Empty;
            SelectPaymentAuthorizationAll(ref message);
            //SelectIndividualPayment(ref message);

	    Response.Write(message);
        }
    }

    public void SelectPaymentAuthorizationAll(ref string message)
    {
        string connectionString = "Data Source=192.168.20.12,1433; Initial Catalog=Repository;Persist Security Info=False;User ID=sa;Password=2000Albatros;Timeout=100";
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = @"Select top(60) pa.*, ct.Name CardTypeName 
                            from dbo.PaymentAuthorization pa 
                            inner join dbo.CardType ct on pa.cardTypeID = ct.id
                            where token is null";

        DataTable dtPaymentAuthorization = new DataTable();

        var merchant = new Merchant();
        merchant = Merchant.IgdInternationlGroup;


        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dtPaymentAuthorization);

            foreach (DataRow dr in dtPaymentAuthorization.Rows)
            {
                PaymentAuthorizationExpanded pa = new PaymentAuthorizationExpanded();
                GetPaymentAuthorizationDataExpanded(dr, ref pa);

                string response = InternetSecureAPI.CheckCreditCardAndGetToken("0", pa.CardholderName, pa.CreditCardNumber, pa.CardType, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), false, DatabaseConnection.GetGatewayID(merchant), DatabaseConnection.GetGatewayPassword(merchant), merchant);


                //Save token for further transaction
                pa.Token = response;

                UpdatePaymentyAuthorization(pa.CardTypeID, pa.Token);
            }
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }

	    message = ex.Message;
        }
    }

    public void UpdatePaymentyAuthorization(int id, string token)
    {
        string connectionString = "Data Source=192.168.20.12,1433; Initial Catalog=Repository;Persist Security Info=False;User ID=sa;Password=2000Albatros;Timeout=100";
        SqlConnection conn = new SqlConnection(connectionString);


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = @"Update dbo.PaymentAuthorization
                            Set token = @Token
                            Where id = @ID";

        cmd.Parameters.Add(new SqlParameter("@ID", id));
        //Updating token with the token value - PCI Compliance
        cmd.Parameters.Add(new SqlParameter("@Token", StringHelpers.Encrypt(token)));

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
    }

    protected void GetPaymentAuthorizationDataExpanded(DataRow dr, ref PaymentAuthorizationExpanded pa)
    {
        pa.CardTypeID = int.Parse(dr["id"].ToString());
        pa.CardholderName = dr["cardholderName"].ToString();
        pa.CreditCardNumber = StringHelpers.Decrypt(dr["creditCardNumber"].ToString());
        pa.CardType = dr["CardTypeName"].ToString();
        pa.ExparationDate = DateTime.Parse(dr["exparationDate"].ToString());
    }

    public void SelectIndividualPayment(ref string message)
    {
        string connectionString = "Data Source=192.168.20.12,1433; Initial Catalog=Repository;Persist Security Info=False;User ID=sa;Password=2000Albatros;Timeout=100"; //DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = @"Select top(10) ip.*, ct.Name CardTypeName 
                            from dbo.IndividualPayment ip 
                            inner join CardType ct on ip.cardTypeID = ct.id
                            where token is null";

        DataTable dtIndividualPayment = new DataTable();
        var merchant = new Merchant();
        merchant = Merchant.IgdInternationlGroup;

        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dtIndividualPayment);

            foreach (DataRow dr in dtIndividualPayment.Rows)
            {
                PaymentAuthorizationExpanded pa = new PaymentAuthorizationExpanded();
                GetPaymentAuthorizationDataExpanded(dr, ref pa);

                string response = InternetSecureAPI.CheckCreditCardAndGetToken("0", pa.CardholderName, pa.CreditCardNumber, pa.CardType, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), false, DatabaseConnection.GetGatewayID(merchant), DatabaseConnection.GetGatewayPassword(merchant), merchant);

                //Save token for further transaction
                pa.Token = response;

                UpdateIndividualPaymentBoa(pa.CardTypeID, pa.Token);
            }
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }

	    message = ex.Message;
        }
    }

    public void UpdateIndividualPaymentBoa(int id, string token)
    {
        string connectionString = "Data Source=192.168.20.12,1433; Initial Catalog=Repository;Persist Security Info=False;User ID=sa;Password=2000Albatros;Timeout=100"; //DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = @"Update IndividualPayment
                            Set token = @Token
                            Where id = @ID";

        cmd.Parameters.Add(new SqlParameter("@ID", id));
        //Updating token with the token - PCI Compliance
        cmd.Parameters.Add(new SqlParameter("@Token", StringHelpers.Encrypt(token)));

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
    }
}