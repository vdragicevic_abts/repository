﻿<%@ WebHandler Language="C#" Class="GroupsNew" %>

using System;
using System.Web;

public class GroupsNew : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        try
        {            
            string cardTypeID = context.Request["cardTypeID"];
            string ccv = context.Request["ccv"];
            string conferenceName = context.Request["conferenceName"];
            string cardnumber = context.Request["cardnumber"];
            string expireMonth = context.Request["expireMonth"];
            string expireYear = context.Request["expireYear"];
            string haID = context.Request["haID"];
            string userID = context.Request["userID"];
            string cardholder = context.Request["cardholder"];
            string billingAddress = context.Request["billingAddress"];
            string phone = context.Request["phone"];
            string city = context.Request["city"];
            string zip = context.Request["zip"];
            string country = context.Request["country"];
            string email = context.Request["email"];
            string state = context.Request["state"];
            
            bool success = false;
            string callback = context.Request["callback"];
            
            string conference = conferenceName.Remove(conferenceName.Length - 4);
            string url = GetConferenceDetails(conference);
            string post = Security_Post(url, "haID=" + haID + "&userID=" + userID);

            if (post == "true")
            {
                success = SendHAToRepository(int.Parse(cardTypeID), ccv, conferenceName, cardnumber, new DateTime(int.Parse(expireYear), int.Parse(expireMonth), 1), int.Parse(haID), int.Parse(userID), cardholder, billingAddress, phone, city, zip, country, email, state);

                context.Response.ContentType = "text/plain";
                context.Response.Write(callback + "({ \"Success\": \"" + success + "\" })");
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write(callback + "({ \"Success\": \"" + success + "\" })");
            }
        }
        catch (Exception ex)
        {
            string callback = context.Request["callback"];
            context.Response.Write(callback + "({ \"Success\": \"" + "false" + "\" })");
        }          
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    private string GetConferenceDetails(string conferenceID)
    {
        string url = String.Empty;
        string connectionString = DatabaseConnection.GetConnectionString();
        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString);
        conn.Open();

        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "Select 'http://' + url + '/' + webPage from Conferences where conferenceID=@conferenceID";
        
        cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ConferenceID", conferenceID));

        try
        {
            url = (string)cmd.ExecuteScalar();
            
            conn.Close();
        }
        catch (Exception ex)
        {
            if (conn.State == System.Data.ConnectionState.Open)
            {
                conn.Close();
            }        
        }

        return url;              
    }

    public bool SendHAToRepository(int cardTypeID, string ccv, string conferenceID, string creditCardNumber, DateTime exparationDate, int housingAgreementID, int userID, string cardholderName, string billingAddress, string phone, string city, string zip, string country, string email, string state)
    {       
        HousingRegistrationAgreementExpanded hra = new HousingRegistrationAgreementExpanded();

        hra.CardTypeID = cardTypeID;
        hra.CCV = ccv;
        hra.ConferenceID = conferenceID;
        hra.CreditCardNumber = creditCardNumber;
        hra.ExparationDate = exparationDate;
        hra.HaID = housingAgreementID;
        hra.UserID = userID;
        hra.CardholderName = cardholderName;
        hra.BillingAddress = billingAddress;
        hra.City = city;
        hra.Country = country;
        hra.Zip = zip;
        hra.Phone = phone;
        hra.Email = email;
        hra.State = state;

        User us = new User();
        us.UserID = "Admin";
        us.Password = "password";

        Users users = new Users();
        users.CurrentUser = us;
       
        return users.SendHAToRepositoryExpandedRQ(hra);
    }

    private string Security_Post(string strPage, string strBuffer)
    {
        //Our postvars
        byte[] buffer = System.Text.Encoding.ASCII.GetBytes(strBuffer);
        //Initialization
        System.Net.HttpWebRequest WebReq = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(strPage);
        //Our method is post, otherwise the buffer (postvars) would be useless
        WebReq.Method = "POST";
        //We use form contentType, for the postvars.
        WebReq.ContentType = "application/x-www-form-urlencoded";
        //The length of the buffer (postvars) is used as contentlength.
        WebReq.ContentLength = buffer.Length;
        //We open a stream for writing the postvars
        System.IO.Stream PostData = WebReq.GetRequestStream();
        //Now we write, and afterwards, we close. Closing is always important!
        PostData.Write(buffer, 0, buffer.Length);
        PostData.Close();
        //Get the response handle, we have no true response yet!
        System.Net.HttpWebResponse WebResp = (System.Net.HttpWebResponse)WebReq.GetResponse();
        
        //Let's show some information about the response
        //Console.WriteLine(WebResp.StatusCode);
        //Console.WriteLine(WebResp.Server);
        
        if (WebResp.StatusCode == System.Net.HttpStatusCode.OK)
        {
            System.IO.Stream Answer = WebResp.GetResponseStream();
            System.IO.StreamReader _Answer = new System.IO.StreamReader(Answer);
            return _Answer.ReadToEnd();
        }
        else
        {
            return "";
        }
        
        //Now, we read the response (the string), and output it.          
        //Console.WriteLine(_Answer.ReadToEnd());
    }   

}