  
USE [Repository]


ALTER TABLE dbo.IndividualPayment
ADD token nvarchar(100) NULL
  , transactionID nvarchar(50) NULL

ALTER TABLE dbo.PaymentAuthorization
ADD amount decimal(18,2) NULL
  , token nvarchar(100) NULL
  , merchant int NULL

GO

/****** Object:  StoredProcedure [dbo].[InsertPaymentAuthorizationExpanded]    Script Date: 11/17/2016 15:47:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vladimir Dragicevic
-- Create date: 11/17/2016
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[InsertPaymentAuthorizationExpanded]
	-- Add the parameters for the stored procedure here
	(@ConferenceID nvarchar(20), @InvoiceID nvarchar(20), @ProposalID nvarchar(20), @EventName nvarchar(50), @Email nvarchar(100), @CardTypeID int, @CreditCardNumber nvarchar(100), @ExparationDate datetime, @CardholderName nvarchar(200), @Preparer nvarchar(100), @Address nvarchar(100), @Phone nvarchar(20), @City nvarchar(20), @Zip nvarchar(20), @Country nvarchar(100), @State nvarchar(50), @PaymentInstructions nvarchar(MAX), @CC nvarchar(4), @DescriptionOfServices nvarchar(MAX), @Token nvarchar(100))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into dbo.PaymentAuthorization(conferenceID, invoiceID, proposalID, eventName, email, cardTypeID, creditCardNumber, exparationDate, cardholderName, preparer, billingAddress, phone, city, zip, country, [state], paymentInstructions, cc, descriptionOfServices, token)
	values (@ConferenceID, @InvoiceID, @ProposalID, @EventName, @Email, @CardTypeID, @CreditCardNumber, @ExparationDate, @CardholderName, @Preparer, @Address, @Phone, @City, @Zip, @Country, @State, @PaymentInstructions, @CC, @DescriptionOfServices, @Token)
END


GO

/****** Object:  StoredProcedure [dbo].[InsertPaymentAuthorizationFirstData]    Script Date: 11/17/2016 15:48:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vladimir Dragicevic
-- Create date: 11/17/2016
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[InsertPaymentAuthorizationFirstData]
	-- Add the parameters for the stored procedure here
	(@ConferenceID nvarchar(20), @InvoiceID nvarchar(20), @ProposalID nvarchar(20), @EventName nvarchar(50), @Email nvarchar(100), @CardTypeID int, @CreditCardNumber nvarchar(100), @ExparationDate datetime, @CardholderName nvarchar(200), @Preparer nvarchar(100), @Address nvarchar(100), @Phone nvarchar(20), @City nvarchar(20), @Zip nvarchar(20), @Country nvarchar(100), @State nvarchar(50), @PaymentInstructions nvarchar(MAX), @CC nvarchar(4), @DescriptionOfServices nvarchar(MAX), @ID int output)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into dbo.PaymentAuthorization(conferenceID, invoiceID, proposalID, eventName, email, cardTypeID, creditCardNumber, exparationDate, cardholderName, preparer, billingAddress, phone, city, zip, country, [state], paymentInstructions, cc, descriptionOfServices)
	values (@ConferenceID, @InvoiceID, @ProposalID, @EventName, @Email, @CardTypeID, @CreditCardNumber, @ExparationDate, @CardholderName, @Preparer, @Address, @Phone, @City, @Zip, @Country, @State, @PaymentInstructions, @CC, @DescriptionOfServices)
	
	Set @ID = @@IDENTITY
END



GO

/****** Object:  StoredProcedure [dbo].[InsertIndividualPayment]    Script Date: 12/22/2016 14:07:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Vladimir Dragicevic
-- Create date: 24/04/2012
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[InsertIndividualPayment]
	-- Add the parameters for the stored procedure here
	(@ConferenceID nvarchar(20), @PaymentDetailCCID int, @CardTypeID int, @CreditCardNumber nvarchar(200), @CCV nvarchar(50), @ExparationDate datetime, @CardHolderName nvarchar(50), @UserID int, @ID int output)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into dbo.IndividualPayment(conferenceID, paymentDetailCCID, cardTypeID, creditCardNumber, ccv, exparationDate, cardHolderName, userID)
	values (@ConferenceID, @PaymentDetailCCID, @CardTypeID, @CreditCardNumber, @CCV, @ExparationDate, @CardHolderName, @UserID)
	
	Set @ID = @@IDENTITY
	
END


GO

  