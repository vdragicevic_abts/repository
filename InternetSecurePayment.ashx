﻿<%@ WebHandler Language="C#" Class="InternetSecurePayment" %>

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Web;

public class InternetSecurePayment : IHttpHandler {

    public void ProcessRequest (HttpContext context) {

        try
        {
            string cardholder = context.Request["cardholder"];
            string address = context.Request["address"];
            string phone = context.Request["phone"];
            string email = context.Request["email"];
            string city = context.Request["city"];
            string postal = context.Request["postal"];
            string country = context.Request["country"];
            string cardType = context.Request["cardType"];
            string expireMonth = context.Request["expireMonth"];
            string expireYear = context.Request["expireYear"];
            string cardnumber = context.Request["cardnumber"];
            string ccv = context.Request["ccv"];
            string amount = context.Request["amountcc"];
            string paymentDetailCCID = String.Empty;
            string conferenceID = String.Empty;
            string userID = context.Request["userID"];
            string callback = context.Request["callback"];
            string currency = context.Request["currency"];
            string test = context.Request["test"];
            string conferenceName = context.Request["conferenceName"];
            bool finalPayment = Boolean.Parse(context.Request["finalPayment"]);
            Merchant merchant = context.Request["merchant"] != null ? (Merchant)Enum.Parse(typeof(Merchant), context.Request["merchant"]) : Merchant.IgdInternationlGroup;

            string Messages = String.Empty;
            
            string token = String.Empty;
            string transactionID = InternetSecureAPI.SendUserInformation2BoaRestService(amount, conferenceName, userID, cardholder, "", address, city, "", postal, country, phone, email, cardnumber, expireMonth, expireYear, ccv, currency, bool.Parse(test), DatabaseConnection.GetGatewayID(merchant), DatabaseConnection.GetGatewayPassword(merchant), ref token, merchant);  
                                    
            SendRegistrationFormToRepository(GetCardTypeID(cardType), ccv, conferenceName, cardnumber, new DateTime(int.Parse("20" + expireYear), int.Parse(expireMonth), 1), int.Parse(userID), int.Parse(userID), cardholder, address, phone, city, postal, country, email, "", transactionID);

            if(finalPayment)
			{
				EmailHelper.SendInternetSecureClientEmailSuccess(email, cardholder.Split(' ')[0], cardholder.Split(' ')[1], transactionID, amount, cardnumber, "noreply@internationalpsychopharmacology.org", "info@internationalpsychopharmacology.org", "International Psychopharmacology Course 2017: Payment Successful", "International Psychopharmacology Course 2017", userID);
				EmailHelper.SendInternetSecureRegistrantEmailSuccess(email, cardholder.Split(' ')[0], cardholder.Split(' ')[1], transactionID, amount, cardnumber, "noreply@internationalpsychopharmacology.org", "info@internationalpsychopharmacology.org", "International Psychopharmacology Course 2017: Your payment is now finalized.", "International Psychopharmacology Course 2017", "<br /><br /> For all other questions or comments, we invite you to email us directly at <a href='mailto:info@internationalpsychopharmacology.org'>info@internationalpsychopharmacology.org</a>.<br/><br/>We look forward to seeing you in Salerno!<br /><br />Best regards,<br />International Psychopharmacology Course 2017");
			}

            context.Response.ContentType = "text/plain";
            context.Response.Write(callback + "({ \"transactionID\": \"" + transactionID + "\" })");
        }
        catch (Exception ex)
        {
            string callback = context.Request["callback"];
            context.Response.Write(callback + "({ \"transactionID\": \"" + "40" + "\" })");
        }
    }

    private int GetCardTypeID(string cardType)
    {
        int cardTypeID = 1;

        switch (cardType)
        {
            case "Visa":
                cardTypeID = 1;
                break;
            case "Master/Euro Card":
                cardTypeID = 2;
                break;
            case "American Express":
                cardTypeID = 3;
                break;
            default:
                cardTypeID = 1;
                break;
        }

        return cardTypeID;
    }

   



    public bool SendRegistrationFormToRepository(int cardTypeID, string ccv, string conferenceID, string creditCardNumber, DateTime exparationDate, int registrationFormID, int userID, string cardholderName, string billingAddress, string phone, string city, string zip, string country, string email, string state, string transactionID)
    {
        RegistrationForm rf = new RegistrationForm();

        rf.CardTypeID = cardTypeID;
        rf.CCV = ccv;
        rf.ConferenceID = conferenceID;
        rf.CreditCardNumber = creditCardNumber;
        rf.ExparationDate = exparationDate;
        rf.RfID = registrationFormID;
        rf.UserID = userID;
        rf.CardholderName = cardholderName;
        rf.Address = billingAddress;
        rf.City = city;
        rf.Country = country;
        rf.Postal = zip;
        rf.Phone = phone;
        rf.Email = email;
        rf.State = transactionID;

        User us = new User();
        us.UserID = "Admin";
        us.Password = "password";

        Users users = new Users();
        users.CurrentUser = us;        
        
        return users.SendRFToRepositoryRQ(rf);
    }



    public bool IsReusable {
        get {
            return false;
        }
    }

}