﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Admin_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
</head>
<body>
    <form id="form1" runat="server">
    <table cellpadding="5" cellspacing="1" border="1" align="center" style="background-color:#ccc;">
        <tr>
            <td style="font-size:13px;">
                User name:
            </td>
            <td>
                <asp:TextBox ID="tbUserName" runat="server" Width="150"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="font-size:13px;">
                Password:
            </td>
            <td>
                <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" Width="150"></asp:TextBox>
            </td>
        </tr>
        <tr align="center">
            <td colspan="2">
                <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" Width="120px" /><br />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
