﻿using Cryptography;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HotelRooms;
using System.Net;

public partial class HotelsAdmin_HotelRoomingList : System.Web.UI.Page
{
    private string _sortDirection;

    public string SortDireaction
    {
        get
        {
            if (ViewState["SortDireaction"] == null)
                return string.Empty;
            else
                return ViewState["SortDireaction"].ToString();
        }
        set
        {
            ViewState["SortDireaction"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["HotelsAdmin"] == null || (String)Session["HotelsAdmin"] == "False")
            {
                Response.Redirect("/2repository/HotelsAdmin/Login.aspx");
            }

            if (((DataTable)Session["User"]).Rows[0]["usertype"].ToString() != "admin")
            {
                btnProcessReservation.Visible = true;
                CheckIfTokenIsValid();
                LoadRoomingList();                
            }
            else
            {
                lblCOnference.Attributes.Add("style", "display:block; margin-bottom:-5px;");
                lblHotel.Attributes.Add("style", "display:block; margin-bottom:-5px; margin-left:5px;");

                btnProcessReservation.Visible = false;
                btnDownloadExcel.Visible = false;
                pnlConference.Visible = true;

                DataTable dtHotelsAndCOnferences = LoadHotelsAndConferences();
                FillDDLConferences(GetAllConferences(dtHotelsAndCOnferences));

                ViewState["HotelsAndConferences"] = dtHotelsAndCOnferences;
            }
           
        }
    }

    private bool CheckIfTokenIsValid()
    {
        if (Session["Token"] != null && StringHelpers.CheckWebServiceAuthKeyValidity(Session["Token"].ToString()))
        {
            btnDownloadExcel.Enabled = true;
            return true;
        }
        else
        {
            btnDownloadExcel.Enabled = false;
            return false;
        }
    }

    private void SynchronizeRoomingList(RepositoryGuestList hotelRoomingList)
    {
        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            try
            {
                foreach (RepositoryGuest rg in hotelRoomingList.Guests)
                { 
                    try
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = conn;
                        cmd.Transaction = trans;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "InsertUpdateGuestList";

                        cmd.Parameters.Add(new SqlParameter("@Conference", rg.Conference));
                        cmd.Parameters.Add(new SqlParameter("@ClientName", rg.ClientName));
                        cmd.Parameters.Add(new SqlParameter("@GroupName", rg.GroupName));
                        cmd.Parameters.Add(new SqlParameter("@HotelCode", rg.HotelCode));
                        cmd.Parameters.Add(new SqlParameter("@HotelName", rg.HotelName));
                        cmd.Parameters.Add(new SqlParameter("@IDGuestList", rg.IDGuestList));
                        cmd.Parameters.Add(new SqlParameter("@GuestNumber", rg.GuestNumber));
                        cmd.Parameters.Add(new SqlParameter("@FirstName", rg.FirstName));
                        cmd.Parameters.Add(new SqlParameter("@LastName", rg.LastName));
                        cmd.Parameters.Add(new SqlParameter("@ArrivalDate", rg.ArrivalDate));
                        cmd.Parameters.Add(new SqlParameter("@DepartureDate", rg.DepartureDate));
                        cmd.Parameters.Add(new SqlParameter("@ContactEmail", rg.ContactEmail));
                        cmd.Parameters.Add(new SqlParameter("@ContactMobilePhone", rg.ContactMobilePhone));
                        cmd.Parameters.Add(new SqlParameter("@ContactOfficePhone", rg.ContactOfficePhone));
                        cmd.Parameters.Add(new SqlParameter("@RoomType", rg.RoomType));
                        cmd.Parameters.Add(new SqlParameter("@Occupancy", rg.Occupancy));
                        cmd.Parameters.Add(new SqlParameter("@SharingGuests", rg.SharingGuests));
                        cmd.Parameters.Add(new SqlParameter("@RoomRate", rg.SellingPrice));

                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {

                    } 
                }

                trans.Commit();
            }
            catch(Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "001", "alert('Update of rooming list Failed!');", true);
                trans.Rollback();
            }
            finally
            {
               
                conn.Close();
                conn.Dispose();
            }
        }
    }

    private void LoadRoomingList()
    {
        DataTable user = (DataTable)Session["User"];

        if(String.IsNullOrEmpty(user.Rows[0]["hotelcode"].ToString()) || String.IsNullOrEmpty(user.Rows[0]["conferenceId"].ToString())) { return; }

        RepositoryGuestList hotelRoomingList = GetRoomingListByHotelCodeAndConference(long.Parse(user.Rows[0]["hotelcode"].ToString()), user.Rows[0]["conferenceId"].ToString(), "9DD873E8-3465-4426-B498-38EA9589090E");

        if (hotelRoomingList.Status == RepositoryGuestListStatus.OK)
        {
            SynchronizeRoomingList(hotelRoomingList);
        }

        PopulateGridView();

        //ViewState["HotelRoomingList"] = dtHotelRoomingList;
    }

    private void LoadRoomingListForAdmin(string conferenceId, string hotelcode)
    {
        DataTable user = (DataTable)Session["User"];

        if (user.Rows[0]["usertype"].ToString() == "admin")
        {

            if (String.IsNullOrEmpty(hotelcode) || String.IsNullOrEmpty(conferenceId)) { return; }

            RepositoryGuestList hotelRoomingList = GetRoomingListByHotelCodeAndConference(long.Parse(hotelcode), conferenceId, "9DD873E8-3465-4426-B498-38EA9589090E");

            if (hotelRoomingList.Status == RepositoryGuestListStatus.OK)
            {
                SynchronizeRoomingList(hotelRoomingList);
            }

            PopulateGridView();
        }
    }

    private void PopulateGridView()
    {
        long hotelCode = 0;
        string conference = "";

        if (!pnlConference.Visible)
        {
            DataTable user = (DataTable)Session["User"];

            hotelCode = long.Parse(user.Rows[0]["hotelcode"].ToString());
            conference = user.Rows[0]["conferenceId"].ToString();
        }
        else
        {
            hotelCode = long.Parse(ddlHotel.SelectedItem.Value);
            conference = ddlConference.SelectedItem.Value;
        }

        DataTable dtRoomingList = new DataTable();

        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand();

        try
        {            
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetGuestList";

            cmd.Parameters.Add(new SqlParameter("@Conference", conference));
            cmd.Parameters.Add(new SqlParameter("@HotelCode", hotelCode));
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            adapter.Fill(dtRoomingList);           

        }
        catch(Exception ex)
        {
            if(conn.State == ConnectionState.Open)
            {
                conn.Close();                
            } 
        }
        finally
        {
            ViewState["HotelRoomingList"] = dtRoomingList;

            cmd.Dispose();

            gvRoomingList.DataSource = null;
            gvRoomingList.DataSource = dtRoomingList;
            gvRoomingList.DataBind();

            if (pnlConference.Visible)
                btnProcessReservation.Visible = true;
        }

        //List<RepositoryGuest> list = roomingList.Guests.Cast<RepositoryGuest>().ToList();        
    }


    private RepositoryGuestList GetRoomingListByHotelCodeAndConference(long hotelCode, string conference, string authKey)
    {
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
		 
        ABTSoluteWebServices hotelRooms = new HotelRooms.ABTSoluteWebServices();

        RepositoryGuestList hotelRoomingList = hotelRooms.GetGuestListForRepository(conference, hotelCode, "", authKey);
        //hotelRooms.GetRoomingListByHotelCodeAndCOnference(hotelCode, conference, authKey);

        return hotelRoomingList;        
    }

    protected void gvRoomingList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            TextBox tb = (TextBox)e.Row.FindControl("tbConfirmationID");
            tb.Text = dr.Row.ItemArray[15].ToString();

            DropDownList ddlStatus = (DropDownList)e.Row.FindControl("ddlStatus");

            switch (dr.Row.ItemArray[16].ToString().Trim())
            {
                case "": ddlStatus.SelectedIndex = 0;
                    break;
                case "Cancelled": ddlStatus.SelectedIndex = 2;
                    break;
                case "Rejected": ddlStatus.SelectedIndex = 1;
                    break;
                default:
                    break;
            }

            //ddlStatus.Text = dr.Row.ItemArray[16].ToString().Length > 0 ? dr.Row.ItemArray[16].ToString() : ddlStatus.Items[0].Text;
        }
    }

    protected void btnDownloadExcel_Click(object sender, EventArgs e)
    {
        //UpdateGridviewChanges();
        //PopulateGridView();

        DataTable dtRoomingList = (DataTable)ViewState["HotelRoomingList"];

        if (CheckIfTokenIsValid())
        {
            GetCreditCardDetailsForRoomingListByEmail(ref dtRoomingList);
            UpdateDownloadedStatus(dtRoomingList);
        }
        else
        {
            dtRoomingList.Columns.Remove("CreditCardNumber");
            dtRoomingList.Columns.Remove("CardHolder");
            dtRoomingList.Columns.Remove("ExpirationDate");
        }

        dtRoomingList.Columns.Remove("IDGuestList");        
        dtRoomingList.Columns.Remove("Conference");
        dtRoomingList.Columns.Remove("HotelCode");
        dtRoomingList.Columns.Remove("Downloaded");

        PopulateGridView();

        DownloadDocument.Excel2(dtRoomingList, "Hotel Rooming List");        
    }

    private void UpdateDownloadedStatus(DataTable dtRoomingList)
    {
        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            try
            {
                foreach (DataRow dr in dtRoomingList.Rows)
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = conn;
                        cmd.Transaction = trans;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "Update GuestList " +
                                          "Set Downloaded = @downloaded " +
                                          "Where id=@id";

                        cmd.Parameters.Add(new SqlParameter("@downloaded", true));
                        cmd.Parameters.Add(new SqlParameter("@id", dr["id"].ToString()));                      
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "001", "alert('Update of rooming list Failed!');", true);
                trans.Rollback();

                if(conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            finally
            {

                conn.Close();
                conn.Dispose();
            }
        }
    }

    private void UpdateGridviewChanges()
    {
        long hotelCode = 0;
        string conference = "";

        if (!pnlConference.Visible)
        {
            DataTable user = (DataTable)Session["User"];
            hotelCode = long.Parse(user.Rows[0]["hotelcode"].ToString());
            conference = user.Rows[0]["conferenceId"].ToString();
        }
        else
        {
            hotelCode = long.Parse(ddlHotel.SelectedItem.Value);
            conference = ddlConference.SelectedItem.Value;
        }

        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            try
            {
                foreach (GridViewRow gvr in gvRoomingList.Rows)
                {
                    if (gvr.RowType == DataControlRowType.DataRow)
                    {
                        DataRowView rowView = (DataRowView)gvr.DataItem;                
                        CheckBox chkProcessed = (CheckBox)gvr.FindControl("chkProcess");

                        if (!chkProcessed.Checked) { continue; }                        
                        
                        HiddenField IDGuestList = (HiddenField)gvr.FindControl("HiddenID");
                        DropDownList ddlStatus = (DropDownList)gvr.FindControl("ddlStatus");
                        TextBox confirmationID = (TextBox)gvr.FindControl("tbConfirmationID");                        

                        if ((ddlStatus.SelectedIndex == 0 && confirmationID.Text.Trim().Length == 0)) { continue; }

                        string guestNumber = gvr.Cells[1].Text;
                        string email = gvr.Cells[9].Text.Replace("&nbsp;", "");

                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = conn;
                        cmd.Transaction = trans;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "UpdateGuestList";

                        cmd.Parameters.Add(new SqlParameter("@HotelCode", hotelCode));
                        cmd.Parameters.Add(new SqlParameter("@IDGuestList", IDGuestList.Value));
                        cmd.Parameters.Add(new SqlParameter("@GuestNumber", guestNumber));
                        cmd.Parameters.Add(new SqlParameter("@ContactEmail", email));
                        cmd.Parameters.Add(new SqlParameter("@ConfirmationID", confirmationID.Text));
                        cmd.Parameters.Add(new SqlParameter("@Status", ddlStatus.SelectedItem.Text));
                        cmd.Parameters.Add(new SqlParameter("@Conference", conference));

                        cmd.ExecuteNonQuery();

                    }
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "001", "alert('Update of rooming list Failed!');", true);
                trans.Rollback();
            }
            finally
            {

                conn.Close();
                conn.Dispose();
            }
        }
    }

    private void GetCreditCardDetailsForRoomingListByEmail(ref DataTable dtRoomingList)
    {
        DataColumn dcCreditCardNumber = new DataColumn("CreditCardNumber", typeof(string));
        DataColumn dcExpirationDate = new DataColumn("ExpirationDate", typeof(DateTime));
        DataColumn dcCardHolder = new DataColumn("CardHolder", typeof(string));
        DataColumn dcAddress = new DataColumn("Address", typeof(string));

        dtRoomingList.Columns.Add(dcCreditCardNumber);
        dtRoomingList.Columns.Add(dcExpirationDate);
        dtRoomingList.Columns.Add(dcCardHolder);
        dtRoomingList.Columns.Add(dcAddress);

        foreach (DataRow dr in dtRoomingList.Rows)
        {

            if (dr["ContactEmail"].ToString().Length > 0 && !Boolean.Parse(dr["Downloaded"].ToString()))
            {
                IndividualPayment ip = GetCreditCardByEmailAndConference(dr["ContactEmail"].ToString(), dr["Conference"].ToString());
                dr["CreditCardNumber"] = StringHelpers.Decrypt(ip.CreditCardNumber);
                dr["ExpirationDate"] = ip.ExparationDate.ToShortDateString();
                dr["CardHolder"] = ip.CardHolderName;
                dr["Address"] = "N/A";
            }
        }
    }

    private IndividualPayment GetCreditCardByEmailAndConference(string email, string conference)
    {
        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);       

        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Select top 1 creditCardNumber, exparationDate, cardHolderName, cardTypeID, timestamp " +
                          "From IndividualPayment " +
                          "Where email=@email and conferenceID=@conferenceID";


        cmd.Parameters.Add(new SqlParameter("@conferenceID", conference.Substring(5) + conference.Substring(0, 4)));
        cmd.Parameters.Add(new SqlParameter("@email", email));        

        DataTable dtIndividualPayment = new DataTable();
        IndividualPayment ip = new IndividualPayment();

        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dtIndividualPayment);

            foreach (DataRow dr in dtIndividualPayment.Rows)
            {                
                ip.CardTypeID = int.Parse(dr["cardTypeID"].ToString());                
                ip.CreditCardNumber = dr["creditCardNumber"].ToString();
                ip.ExparationDate = DateTime.Parse(dr["exparationDate"].ToString());                
                ip.CardHolderName = dr["cardHolderName"].ToString();                
            }

            return ip;
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }

            return ip;
        }
    }

    protected void gvRoomingList_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dtRoomingList = (DataTable)ViewState["HotelRoomingList"];

        SetSortDirection(SortDireaction);

        if (dtRoomingList != null)
        {
            //Sort the data.
            dtRoomingList.DefaultView.Sort = e.SortExpression + " " + _sortDirection;
            gvRoomingList.DataSource = dtRoomingList;
            gvRoomingList.DataBind();
            SortDireaction = _sortDirection;
            int columnIndex = 0;
            foreach (DataControlFieldHeaderCell headerCell in gvRoomingList.HeaderRow.Cells)
            {
                if (headerCell.ContainingField.SortExpression == e.SortExpression)
                {
                    columnIndex = gvRoomingList.HeaderRow.Cells.GetCellIndex(headerCell);
                }
            }

            //gvRoomingList.HeaderRow.Cells[columnIndex].Controls.Add(sortImage);
        }

    }

    protected void SetSortDirection(string sortDirection)
    {
        if (sortDirection == "ASC")
        {
            _sortDirection = "DESC";
            //sortImage.ImageUrl = "view_sort_ascending.png";

        }
        else
        {
            _sortDirection = "ASC";
            //sortImage.ImageUrl = "view_sort_descending.png";
        }
    }


    protected void btnProcessReservation_Click(object sender, EventArgs e)
    {
        UpdateGridviewChanges();

        PopulateGridView();
    }

    protected void FillDDLConferences(List<string> dtConferences)
    {
        if (dtConferences == null) { return; }

        ddlConference.Items.Clear();

        ListItem liSelect = new ListItem("--Select--");
        ddlConference.Items.Add(liSelect);

        foreach (var item in dtConferences)
        {
            ListItem li = new ListItem(item);
            ddlConference.Items.Add(li);
        }

        ddlConference.DataBind();
    }

    protected void FillDDLHotels(List<Hotel> hotels)
    {
        if (hotels == null) { return; }

        ddlHotel.Items.Clear();

        ListItem liSelect = new ListItem("--Select--");
        ddlHotel.Items.Add(liSelect);

        foreach (Hotel h in hotels)
        {
            ListItem li = new ListItem();
            li.Text = h.Name;
            li.Value = h.Code.ToString();
            ddlHotel.Items.Add(li);
        }

        ddlConference.DataBind();
    }

    protected List<string> GetAllConferences(DataTable dtConferences)
    {
        var conferences = (from row in dtConferences.AsEnumerable()
                           select row.Field<string>("conferenceID")).Distinct().ToList<string>();

        if (conferences.Count > 0)
        {
            return conferences;
        }

        return null;
    }


    protected List<Hotel> GetHotelsForConference(string conference, DataTable dtConferences)
    {
        List<Hotel> hotelList = new List<Hotel>();

        var hotels = (from row in dtConferences.AsEnumerable()
                      where row.Field<string>("conferenceID") == conference
                      select row).ToList();

        foreach (var hotel in hotels)
        {
            hotelList.Add(new Hotel()
            {
                Name = hotel[0].ToString(),
                Code = int.Parse(hotel[2].ToString())
            });
        }

        if (hotelList.Count > 0)
            return hotelList;

        return null;
    }

    protected DataTable LoadHotelsAndConferences()
    {
        DataSet dsHotelsAndConfereces = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("..\\") + "\\" + "App_Data\\Hotels.xml");

        dsHotelsAndConfereces.ReadXml(reader);

        if (dsHotelsAndConfereces.Tables.Count > 0)
        {
            return dsHotelsAndConfereces.Tables[0];
        }

        return null;
    }

    protected class Hotel
    {
        public int Code { get; set; }
        public string Name { get; set; }
    }

    protected void ddlConference_SelectedIndexChanged(object sender, EventArgs e)
    {
        string conference = this.ddlConference.SelectedItem.Text;

        if (conference == "--Select--")
        {
            ddlHotelDiv.Visible = false;
            btnProcessReservation.Visible = false;
            gvRoomingList.DataSource = null;
            gvRoomingList.DataBind();

            return;
        }

        DataTable dtHotelAndConferences = (DataTable)ViewState["HotelsAndConferences"];

        List<Hotel> hotels = GetHotelsForConference(conference, dtHotelAndConferences);

        FillDDLHotels(hotels);

        ddlHotelDiv.Visible = true;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LoadRoomingListForAdmin(ddlConference.SelectedItem.Text, ddlHotel.SelectedItem.Value);
    }
}

public class RepositoryGuestExpanded : RepositoryGuest
{
    public string ConfirmationID { get; set; }

    public Status GuestStatus { get; set; }

    public string CreditCardNumber { get; set; }

    public DateTime ExpirationDate { get; set; }

    public string CardHolderName { get; set; }

    public string Address { get; set; }
}

public enum Status
{
   None,
   Processed,
   Rejected
}

