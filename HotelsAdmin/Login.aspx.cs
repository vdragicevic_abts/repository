﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Xml;
using System.Web.SessionState;
using System.IO;

public partial class Admin_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((string)Session["HotelsAdmin"] == "True")
        {
            Response.Redirect("/2repository/HotelsAdmin/HotelAdminMain.aspx");
        }

        if (!IsPostBack)
        {
            //string tokenEncrypted = StringHelpers.Encrypt("ECE186FB-07F2-46DD-82E9-47645EDAAA88");
            //string token = StringHelpers.GetWebServiceAuthKey();
        }
    }

    public string RemoveSpecialCharacters(string str)
    {
        return Regex.Replace(str, "[^a-zA-Z0-9_.@]+", "", RegexOptions.Compiled);
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string username = this.tbUserName.Text.Trim().Replace(" ", "");
        string password = this.tbPassword.Text.Trim().Replace(" ", "");

        username = RemoveSpecialCharacters(username);

        XmlDocument doc = new XmlDocument();
        doc.Load(Server.MapPath("..\\") + "\\" + "App_Data\\HotelUsers.xml");

        XmlNode node = doc.SelectSingleNode("//user[./password/text()='" + StringHelpers.Encrypt(password) + "' and ./username/text()='" + username + "']");
     
        try
        {          
            if (node !=null)
            {
		        using (StreamWriter sw = File.AppendText(Server.MapPath("/HotelsAdmin/Login.txt")))
                //using (StreamWriter sw = File.AppendText("C:\\Windows\\System32\\winevt\\Logs\\PaymentLog.txt"))
                {
                    sw.WriteLine(username + ", " + System.DateTime.Now.ToString() + ", Logged in");
                }


                XmlNodeReader xnr = new XmlNodeReader(node);
                DataSet user = new DataSet("User");
                user.ReadXml(xnr);                

                Session["User"] = user.Tables[0];
                Session["HotelsAdmin"] = "True";               
                Session["Authentication"] = StringHelpers.DecodeTo64(username + "&" + password);
                Session["username"] = username;

                if (Request.QueryString["t"] != null)
                {
                    string token = StringHelpers.Decrypt(Request.QueryString["t"]);
                    Session["Token"] = token;
                }

                Response.Redirect("/2repository/HotelsAdmin/HotelAdminMain.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "001", "alert('Incorrect Login!!!!');", true);    
            }
        }
        catch (Exception ex)
        {          
            Session["ERROR"] = ex.Message;
        }       
    }
}
