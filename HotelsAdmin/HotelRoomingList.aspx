﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelsAdmin/HotelAdmin.master" AutoEventWireup="true" CodeFile="HotelRoomingList.aspx.cs" Inherits="HotelsAdmin_HotelRoomingList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdminPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function CheckProcess(lnk) {
            var row = lnk.parentNode.parentNode;
            var chkProcessId = row.cells[0].children[0].firstChild.id;
            var confirmationID = row.cells[7].childNodes[1].value;
            var status = row.cells[8].childNodes[1].value;

            //if (row.cells[0].children[0].firstChild.checked) {
            //    $(lnk).closest('tr').css('background-color', '');
            //}
            //else {
            //    $(lnk).closest('tr').css('background-color', '#F7F7DE');
            //}

            if (status == 0 && confirmationID == '') {
                $('#' + chkProcessId).attr('checked', false);
                alert('Please enter ConfirmationID or select Status!');
            } 
        }
    </script>

    <asp:Panel ID="pnlConference" runat="server" Visible="false" Style="padding: 10px; padding-bottom: 0px; color:black;">
        <fieldset style="padding: 10px;">
            <div style="float:left;">
                <asp:Label ID="lblCOnference" runat="server" Text="Conference: "></asp:Label><br />
                <asp:DropDownList ID="ddlConference" runat="server" Width="200" AutoPostBack="true" OnSelectedIndexChanged="ddlConference_SelectedIndexChanged"></asp:DropDownList>                
            </div>
            <div style="float:left" id="ddlHotelDiv" class="floatLeft" visible="false" runat="server">
                <asp:Label ID="lblHotel" runat="server" Text="Hotel Name: "></asp:Label><br />
                <asp:DropDownList ID="ddlHotel" runat="server" Width="200" style="margin-left:5px;"></asp:DropDownList>                
                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
            </div>            
        </fieldset>
    </asp:Panel>
    <div style="padding-left: 10px; padding-top: 10px;">
        <asp:Button ID="btnDownloadExcel" Height="30" Width="140" runat="server" Text="Download to Excel" OnClick="btnDownloadExcel_Click"
            OnClientClick="setTimeout(function () {
                             window.location.reload(1);
                           }, 3000);" />
        <asp:Button ID="btnProcessReservation" Height="30" Width="140" runat="server" Text="Process Reservation" OnClick="btnProcessReservation_Click" />
    </div>
    <div style="padding: 10px;">
        <asp:GridView ID="gvRoomingList" runat="server" AutoGenerateColumns="False" AllowSorting="True" CellPadding="4" ForeColor="Black" GridLines="Vertical" OnRowDataBound="gvRoomingList_RowDataBound" OnSorting="gvRoomingList_Sorting" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="Select" HeaderStyle-BackColor="#305087" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkProcess" runat="server" Enabled="true" OnChange="CheckProcess(this);" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="GuestNumber" SortExpression="GuestNumber" HeaderText="Guest Number" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="First Name" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087" ForeColor="White"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087" ForeColor="White"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="ArrivalDate" SortExpression="ArrivalDate" HeaderText="Arrival Date" DataFormatString="{0:yyyy-MM-dd}" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="DepartureDate" SortExpression="DepartureDate" HeaderText="Departure Date" DataFormatString="{0:yyyy-MM-dd}" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="HotelName" HeaderText="Hotel Name" HeaderStyle-ForeColor="white" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087" ForeColor="White"></HeaderStyle>
                </asp:BoundField>
                <%--<asp:BoundField DataField="RoomOccupancy" HeaderText="Room Occupancy" />--%>
                <asp:TemplateField HeaderText="ConfirmationID" SortExpression="ConfirmationID" HeaderStyle-BackColor="#305087">
                    <ItemTemplate>
                        <asp:TextBox ID="tbConfirmationID" runat="server" CssClass="confirmationHighlight" Width="100px"></asp:TextBox>
                    </ItemTemplate>

                    <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status" HeaderStyle-BackColor="#305087" SortExpression="Status">
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Selected="True" Value="0">--</asp:ListItem>
                            <asp:ListItem Value="1">Rejected</asp:ListItem>
                            <asp:ListItem Value="2">Cancelled</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>

                    <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:TemplateField>
                <asp:BoundField DataField="ContactEmail" SortExpression="ContactEmail" HeaderText="Email" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="RoomType" SortExpression="RoomType" HeaderText="Room Type" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Occupancy" SortExpression="Occupancy" HeaderText="Occupancy" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="SharingGuests" SortExpression="SharingGuests" HeaderText="Sharing" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="ModifiedState" SortExpression="ModifiedState" HeaderText="State" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Timestamp" SortExpression="Timestamp" HeaderText="Modified" DataFormatString="{0:yyyy-MM-dd HH:mm:ss}" HeaderStyle-BackColor="#305087">
                    <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HiddenField ID="HiddenID" runat="server" Value='<%#Eval("IDGuestList") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Downloaded" HeaderStyle-BackColor="#305087" SortExpression="Downloaded" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkDownloaded" runat="server" Enabled="false" Checked='<%#Eval("Downloaded") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:BoundField DataField="Downloaded" SortExpression="Downloaded" HeaderText="Downloaded" HeaderStyle-BackColor="#305087" >
                <HeaderStyle BackColor="#305087"></HeaderStyle>
                </asp:BoundField>--%>
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <HeaderStyle BackColor="#6B696B" Height="30" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle BackColor="#F7F7DE" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FBFBF2" />
            <SortedAscendingHeaderStyle BackColor="#848384" />
            <SortedDescendingCellStyle BackColor="#EAEAD3" />
            <SortedDescendingHeaderStyle BackColor="#575357" />
        </asp:GridView>
    </div>
</asp:Content>

