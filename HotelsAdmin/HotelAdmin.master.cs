﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class HotelAdmin_HotelAdmin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["HotelsAdmin"] == null || (String)Session["HotelsAdmin"] == "False")
        {
            Response.Redirect("/2repository/HotelsAdmin/Login.aspx");
        }
    }
    
    protected void logout_Click(object sender, EventArgs e)
    {
        Session["HotelsAdmin"] = null;

        //using (StreamWriter sw = File.AppendText("C:\\Windows\\System32\\winevt\\Logs\\PaymentLog.txt"))
        //   {
        //       sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", Logged out");
        //   }

        Session["User"] = null;
        Session["username"] = null;
        Session["Token"] = null;

        Response.Redirect("/2repository/HotelsAdmin/Login.aspx");
    }
}
