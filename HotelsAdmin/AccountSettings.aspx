﻿<%@ Page Language="C#" MasterPageFile="~/HotelsAdmin/HotelAdmin.master" AutoEventWireup="true" CodeFile="AccountSettings.aspx.cs" Inherits="HotelAdmin_AccountSettings" Title="Account Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .floatLeft {
            float: left;
            margin-right: 15px;
        }
    </style>
    <script type="text/javascript">
        function copy() {
            var textarea = document.getElementById("ctl00_ContentPlaceHolder1_txtUrlLink");
            textarea.select();
            document.execCommand("copy");
        }
    </script>

    <div style="width: 600px; min-height: 610px; float: left; margin-top: 10px; margin-left: 40px; font-size: 12px; color: Black;">

        <p style="font-size: 16px; color: #990033; font-weight: bold; margin-bottom: 20px;">
            Update account
        </p>

        <asp:Panel ID="pnlConference" runat="server" Visible="false" Style="padding:10px; padding-bottom:0px;">
            <fieldset style="padding:10px;">
                <div class="floatLeft">
                    <asp:Label ID="lblCOnference" runat="server" Text="Conference: "></asp:Label><br />
                    <asp:DropDownList ID="ddlConference" runat="server" Width="200" AutoPostBack="true" OnSelectedIndexChanged="ddlConference_SelectedIndexChanged"></asp:DropDownList>
                    <br />
                </div>
                <div id="ddlHotelDiv" class="floatLeft" visible="false" runat="server">
                    <asp:Label ID="lblHotel" runat="server" Text="Hotel Name: "></asp:Label><br />
                    <asp:DropDownList ID="ddlHotel" runat="server" Width="200"></asp:DropDownList>
                    <br />
                </div>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="Register" runat="server" Visible="true" Style="padding: 5px;">
            <fieldset  style="padding:10px;">
                <div class="floatLeft">
                    <asp:Label ID="lblConferenceId" runat="server" Text="Conference Name: "></asp:Label><br />
                    <asp:TextBox ID="tbConferenceId" runat="server" ReadOnly="true" BackColor="Gainsboro"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfvConferenceId" runat="server" ErrorMessage="*Enter conference name!" Text="*" ControlToValidate="tbConferenceId" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <br />
                </div>
                <div class="floatLeft">
                    <asp:Label ID="lblCompanyName" runat="server" Text="Hotel Name: "></asp:Label><br />
                    <asp:TextBox ID="tbCompanyName" runat="server" ReadOnly="true" BackColor="Gainsboro"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfvCompanyName" runat="server" ErrorMessage="*Enter company name!" Text="*" ControlToValidate="tbCompanyName" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <br />
                </div>
                <div class="floatLeft">
                    <asp:Label ID="lblCompanyCode" runat="server" Text="Hotel Code: "></asp:Label><br />
                    <asp:TextBox ID="tbCompanyCode" runat="server" ReadOnly="true" BackColor="Gainsboro"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfvCompanyCode" runat="server" ErrorMessage="*Enter company code!" Text="*" ControlToValidate="tbCompanyCode" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <br />
                </div>
                <div style="clear: both"></div>
                <div class="floatLeft">
                    <asp:Label ID="lblFirstName" runat="server" Text="First Name: "></asp:Label><br />
                    <asp:TextBox ID="tbFirstName" runat="server" ReadOnly="true" BackColor="Gainsboro"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfvFirstName" runat="server" ErrorMessage="*Enter first name!" Text="*" ControlToValidate="tbFirstname" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <br />
                </div>
                <div class="floatLeft">
                    <asp:Label ID="lblLastName" runat="server" Text="Last/Family Name:"></asp:Label><br />
                    <asp:TextBox ID="tbLastName" runat="server" ReadOnly="true" BackColor="Gainsboro"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfvLastName" runat="server" ErrorMessage="*Enter last name!" Text="*" ControlToValidate="tbLastName" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <br />
                </div>
                <div class="floatLeft">
                    <asp:Label ID="lblAddress" runat="server" Text="Street Address: "></asp:Label><br />
                    <asp:TextBox ID="tbAddress" runat="server" ReadOnly="true" BackColor="Gainsboro"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfvAddress" runat="server" ErrorMessage="*Enter address!" Text="*" ControlToValidate="tbAddress" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <br />
                </div>
                <br />
                <div class="floatLeft">
                    <asp:Label ID="lblCity" runat="server" Text="City: "></asp:Label><br />
                    <asp:TextBox ID="tbCity" runat="server" ReadOnly="true" BackColor="Gainsboro"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfvCity" runat="server" ErrorMessage="*Enter city!" Text="*" ControlToValidate="tbCity" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <br />
                </div>
                <div class="floatLeft">
                    <asp:Label ID="lblZip" runat="server" Text="Zip/Postal Code: "></asp:Label><br />
                    <asp:TextBox ID="tbZip" runat="server" ReadOnly="true" BackColor="Gainsboro"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfvZip" runat="server" ErrorMessage="*Enter zip code!" Text="*" ControlToValidate="tbZip" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <br />
                </div>
                <br />
                <br />
                <br />
                <div class="floatLeft">
                    <asp:Label ID="lblCountry" runat="server" Text="Country: "></asp:Label><br />
                    <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" BackColor="Gainsboro" Width="310">
                    </asp:DropDownList><asp:Label ID="lblState" runat="server" Visible="false" Text="State: "><br /></asp:Label>
                    <asp:DropDownList ID="ddlState" runat="server" Visible="false"></asp:DropDownList>
                </div>
                <br />
                <br />
                <br />
                <br />
                <br /> 
                <br /><br />
                <div class="floatLeft">
                    <asp:Label ID="lblEmail" runat="server" Text="E-mail address:"></asp:Label><br />
                    <asp:TextBox ID="tbEmail" runat="server" ReadOnly="true" BackColor="Gainsboro"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="*Enter email!" Text="*" ControlToValidate="tbEmail" Display="Dynamic"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="revEmail" runat="server" ErrorMessage="*Not a valid email!!" Text="*" ControlToValidate="tbEmail" Display="Dynamic"
                            ValidationExpression="^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$"></asp:RegularExpressionValidator>
                </div>
                <div class="floatLeft">
                    <asp:Label ID="lblPhone" runat="server" Text="Phone number: "></asp:Label><br />
                    <asp:TextBox ID="tbPhone" runat="server" ReadOnly="true" BackColor="Gainsboro"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ErrorMessage="*Enter phone" Text="*" ControlToValidate="tbPhone" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <br />
                </div>

                <div class="floatLeft">
                    <asp:Label ID="lblUserName" runat="server" Text="username: "></asp:Label><br />
                    <asp:TextBox ID="tbUserName" runat="server" ReadOnly="true" BackColor="Gainsboro"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfvUserName" runat="server" ErrorMessage="*Enter username!" Text="*" ControlToValidate="tbUserName" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <br />
                </div>
                <div class="floatLeft">
                    <asp:Label ID="lbPassword" runat="server" Text="Password: "></asp:Label><br />
                    <asp:TextBox ID="tbPassword" runat="server" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfvPassword" runat="server" ErrorMessage="*Enter password" Text="*" ControlToValidate="tbPassword" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <br />
                </div>
                <div class="floatLeft">
                    <asp:Label ID="lblUserType" runat="server" Text="User Type: "></asp:Label><br />
                    <asp:DropDownList runat="server" ID="ddlUserType" Width="145" Enabled="false" style="color:black;">
                        <asp:ListItem Text="--Select--" Value ="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Hotel User" Value="user"></asp:ListItem>
                        <asp:ListItem Text="Admin User" Value="admin"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <div class="floatLeft" style="margin-top: 7px;">
                    <asp:Button ID="Submit" Text="Submit" runat="server" OnClick="Submit_Click"
                        Enabled="true" />
                </div>
            </fieldset>
        </asp:Panel>

        <div style="clear: both"></div>
        <br />
        <br />

        <asp:Panel ID="pnHotelUserLink" runat="server" Style="padding: 10px; padding-left: 0px; padding-right: 0px;" Visible="false">
            <fieldset style="padding:10px;">
                <div>
                    <div>Url Link:</div>
                    <textarea type="text" id="txtUrlLink" runat="server" style="width: 550px;"></textarea>
                </div>
                <div>
                    <asp:Button ID="btnGenerateUrl" runat="server" Text="Generate URL" OnClick="btnGenerateUrl_Click" />
                    <input type="button" id="btnCopy" style="padding-left: 5px;" onclick="copy();" value="Copy" />
                </div>
            </fieldset>
        </asp:Panel>
    </div>
</asp:Content>

