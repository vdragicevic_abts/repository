﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using System.Linq;
using System.Collections.Generic;

public partial class HotelAdmin_AccountSettings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["HotelsAdmin"] == null || (String)Session["HotelsAdmin"] == "False")
            {
                Response.Redirect("/2repository/HotelsAdmin/Login.aspx");
            }

            pnHotelUserLink.Visible = false;

            FillDDLCountries();
            FillDDLStates();

            GetAccountInformation();

            DataTable dtHotelsAndCOnferences = LoadHotelsAndConferences();            
            FillDDLConferences(GetAllConferences(dtHotelsAndCOnferences));

            ViewState["HotelsAndConferences"] = dtHotelsAndCOnferences;
        }
    }

    protected void FillDDLConferences(List<string> dtConferences)
    {
        if(dtConferences == null) { return; }

        ddlConference.Items.Clear();

        ListItem liSelect = new ListItem("--Select--");
        ddlConference.Items.Add(liSelect);

        foreach (var item in dtConferences)
        {
            ListItem li = new ListItem(item);
            ddlConference.Items.Add(li);
        }         
        
        ddlConference.DataBind();        
    }

    protected void FillDDLHotels(List<Hotel> hotels)
    {
        if(hotels == null) { return; }

        ddlHotel.Items.Clear();

        ListItem liSelect = new ListItem("--Select--");
        ddlHotel.Items.Add(liSelect);

        foreach(Hotel h in hotels)
        {
            ListItem li = new ListItem();
            li.Text = h.Name;
            li.Value = h.Code.ToString();
            ddlHotel.Items.Add(li);
        }

        ddlConference.DataBind();
    }

    protected List<string> GetAllConferences(DataTable dtConferences)
    {
        var conferences = (from row in dtConferences.AsEnumerable()
                           select row.Field<string>("conferenceID")).Distinct().ToList<string>();

        if (conferences.Count > 0)
        {
            return conferences;
        }

        return null;
    }


    protected List<Hotel> GetHotelsForConference(string conference, DataTable dtConferences)
    {
        List<Hotel> hotelList = new List<Hotel>();

        var hotels = (from row in dtConferences.AsEnumerable()
                      where row.Field<string>("conferenceID") == conference
                      select row).ToList();

        foreach(var hotel in hotels)
        {
            hotelList.Add(new Hotel()
            {
                Name = hotel[0].ToString(),
                Code = int.Parse(hotel[2].ToString())
            });
        }

        if (hotelList.Count > 0)
            return hotelList;

        return null; 
    }

    protected DataTable LoadHotelsAndConferences()
    {
        DataSet dsHotelsAndConfereces = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("..\\") + "\\" + "App_Data\\Hotels.xml");

        dsHotelsAndConfereces.ReadXml(reader); 
        
        if(dsHotelsAndConfereces.Tables.Count > 0)
        {
            return dsHotelsAndConfereces.Tables[0];
        }

        return null;
    }

    protected void FillDDLCountries()
    {       
        DataSet dsCountries = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("..\\") + "\\" + "App_Data\\Countries.xml");

        dsCountries.ReadXml(reader);
      
        ListItem item = new ListItem("Select", "-1");

        ddlCountry.DataTextField = "Country";
        ddlCountry.DataValueField = "id";
        ddlCountry.DataSource = dsCountries.Tables[0];
        ddlCountry.DataBind();

        ddlCountry.Items.Insert(0, item);

    }

    protected void FillDDLStates()
    {
        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("..\\") + "\\" + "App_Data\\States.xml");
        
        DataSet dsStates = new DataSet();

        dsStates.ReadXml(reader);
    
        ListItem item = new ListItem("Select", "-1");
 
        ddlState.DataTextField = "Name";
        ddlState.DataValueField = "IDState";
        ddlState.DataSource = dsStates.Tables[0];
        ddlState.DataBind();

        ddlState.Items.Insert(0, item);

    }

    protected void GetAccountInformation()
    {
        string username_password = StringHelpers.DecodeFrom64(Session["Authentication"].ToString());

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath("..\\") + "\\" + "App_Data\\HotelUsers.xml");

        DataSet dsUsers = new DataSet("Users");
        dsUsers.ReadXml(reader);

        foreach (DataRow dr in dsUsers.Tables[0].Rows)
        {


            if (dr["username"].ToString() == username_password.Split('&')[0] && dr["password"].ToString() == StringHelpers.Encrypt(username_password.Split('&')[1]))
            {
                this.tbConferenceId.Text = dr["ConferenceId"].ToString().Trim();
                this.tbCompanyName.Text = dr["hotel"].ToString().Trim();
                this.tbCompanyCode.Text = dr["hotelcode"].ToString().Trim();
                this.tbFirstName.Text = dr["firstname"].ToString();
                this.tbLastName.Text = dr["lastname"].ToString();
                this.tbEmail.Text = dr["email"].ToString();
                this.tbPhone.Text = dr["phone"].ToString();
                this.ddlCountry.SelectedValue = dr["country"].ToString();
                this.tbAddress.Text = dr["address"].ToString();
                this.tbZip.Text = dr["zip"].ToString();
                this.tbCity.Text = dr["city"].ToString();
                this.ddlState.SelectedValue = String.IsNullOrEmpty(dr["state"].ToString()) ? "-1" : dr["state"].ToString();
                this.tbUserName.Text = dr["username"].ToString();
                this.ddlUserType.SelectedValue = dr["usertype"].ToString();
                
            }
        }

        reader.Close();
        reader.Dispose();

    }

    protected void Submit_Click(object sender, EventArgs e)
    {      
        string hotelName = this.tbCompanyName.Text.Trim();
        string hotelCode = this.tbCompanyCode.Text.Trim();
        string firstName = this.tbFirstName.Text.Trim();
        string lastName = this.tbLastName.Text.Trim();
        string eMail = this.tbEmail.Text.Trim();
        string username = this.tbUserName.Text.Trim();
        string password = this.tbPassword.Text.Trim();
        string phone = this.tbPhone.Text.Trim();
        string country = this.ddlCountry.SelectedValue;
        string address = this.tbAddress.Text;
        string zip = this.tbZip.Text.Trim();
        string city = this.tbCity.Text.Trim();
        string id = String.Empty;
        string state = this.ddlState.Visible ? this.ddlState.SelectedValue : "";
        string usertype = this.ddlUserType.SelectedItem.Value;

        XmlDocument doc = new XmlDocument();

        doc.Load(Server.MapPath("..\\") + "\\" + "App_Data\\HotelUsers.xml");

        int _id = int.Parse(doc.DocumentElement.LastChild.Attributes.Item(1).Value) + 1;

        string _conferenceId = doc.DocumentElement.LastChild.Attributes.Item(0).Value; 

        foreach (XmlNode node in doc.ChildNodes)
        {
            if(node.ChildNodes.Count == 0)
            {
                continue;
            }

            foreach(XmlNode node1 in node.ChildNodes)
            {
                if(node1["email"].FirstChild == null)
                {
                    continue;
                }

                if (node1["email"].FirstChild.Value == tbEmail.Text)
                {                    
                    node.RemoveChild(node1);                    
                }

            }          
        }


        doc.Save(Server.MapPath("..\\") + "\\" + "App_Data\\HotelUsers.xml");           

        XmlElement _user = doc.CreateElement("user");        
        XmlElement _firstname = doc.CreateElement("firstname");
        XmlElement _lastname = doc.CreateElement("lastname");
        XmlElement _email = doc.CreateElement("email");
        XmlElement _userName = doc.CreateElement("username");
        XmlElement _password = doc.CreateElement("password");
        XmlElement _phone = doc.CreateElement("phone");
        XmlElement _country = doc.CreateElement("country");
        XmlElement _address = doc.CreateElement("address");
        XmlElement _city = doc.CreateElement("city");
        XmlElement _hotelname = doc.CreateElement("hotel");
        XmlElement _state = doc.CreateElement("state");
        XmlElement _zip = doc.CreateElement("zip");
        XmlElement _hotelcode = doc.CreateElement("hotelcode");
        XmlElement _usertype = doc.CreateElement("usertype");

        XmlAttribute _userid = doc.CreateAttribute("userid");
        _userid.Value = _id.ToString();

        XmlAttribute _conferenceid = doc.CreateAttribute("conferenceid");
        _conferenceid.Value = _conferenceId;

        XmlText _firstnametext = doc.CreateTextNode(firstName);
        XmlText _lastnametext = doc.CreateTextNode(lastName);
        XmlText _emailtext = doc.CreateTextNode(eMail);
        XmlText _usernametext = doc.CreateTextNode(username);
        XmlText _passwordtext = doc.CreateTextNode(StringHelpers.Encrypt(password));
        XmlText _phonetext = doc.CreateTextNode(phone);
        XmlText _countrytext = doc.CreateTextNode(country);
        XmlText _addresstext = doc.CreateTextNode(address);
        XmlText _ziptext = doc.CreateTextNode(zip);
        XmlText _citytext = doc.CreateTextNode(city);
        XmlText _hotelnametext = doc.CreateTextNode(hotelName);
        XmlText _statetext = doc.CreateTextNode(state);
        XmlText _hotelcodetext = doc.CreateTextNode(hotelCode);
        XmlText _usertypetext = doc.CreateTextNode(usertype);

        _user.Attributes.Append(_userid);
        _user.Attributes.Append(_conferenceid);
        _user.AppendChild(_firstname);
        _user.AppendChild(_lastname);
        _user.AppendChild(_email);
        _user.AppendChild(_userName);
        _user.AppendChild(_password);
        _user.AppendChild(_phone);
        _user.AppendChild(_country);
        _user.AppendChild(_address);
        _user.AppendChild(_city);
        _user.AppendChild(_hotelname);
        _user.AppendChild(_state);
        _user.AppendChild(_zip);
        _user.AppendChild(_hotelcode);
        _user.AppendChild(_usertype);

        _firstname.AppendChild(_firstnametext);
        _lastname.AppendChild(_lastnametext);
        _email.AppendChild(_emailtext);
        _userName.AppendChild(_usernametext);
        _password.AppendChild(_passwordtext);
        _phone.AppendChild(_phonetext);
        _country.AppendChild(_countrytext);
        _address.AppendChild(_addresstext);
        _city.AppendChild(_citytext);
        _hotelname.AppendChild(_hotelnametext);
        _state.AppendChild(_statetext);
        _zip.AppendChild(_ziptext);
        _hotelcode.AppendChild(_hotelcodetext);
        _usertype.AppendChild(_usertypetext);


        Session["Authentication"] = StringHelpers.DecodeTo64(username + "&" + password);


        doc.DocumentElement.AppendChild(_user);
        doc.Save(Server.MapPath("..\\") + "\\" + "App_Data\\HotelUsers.xml");

        using (StreamWriter sw = File.AppendText(Server.MapPath("/HotelsAdmin/Login.txt")))	
        {
            sw.WriteLine(username + ", " + System.DateTime.Now.ToString() + ", " + "Created Account (" + username + ")");
        }

        GetAccountInformation();        
    }

    protected void ClearControls()
    {
        this.tbCompanyName.Text = "";
        this.tbFirstName.Text = "";
        this.tbLastName.Text = "";
        this.tbEmail.Text = "";
        this.tbUserName.Text = "";
        this.tbPassword.Text = "";
        this.tbPhone.Text = "";
        this.ddlCountry.SelectedIndex = 0;
        this.tbAddress.Text = "";
        this.tbCity.Text = "";
        this.tbZip.Text = "";
    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCountry.SelectedValue == "226")
        {
            this.ddlState.Visible = true;
            this.lblState.Visible = true;
        }
        else
        {
            this.ddlState.Visible = false;
            this.lblState.Visible = false;
        }
    }

    protected void btnGenerateUrl_Click(object sender, EventArgs e)
    {
        string generatedToken = GetWebServiceAuthKey();

        string url = ConfigurationManager.AppSettings["HotelUserUrl"].ToString();

        this.txtUrlLink.InnerText = url + generatedToken;
    } 
    
    private string GetWebServiceAuthKey()
    {
        string guid = Guid.NewGuid().ToString();

        string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection conn = new SqlConnection(connectionString);

        try
        {
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "InsertWebServiceAuthKey";
            cmd.Parameters.AddWithValue("@AuthKey", guid);
            cmd.ExecuteNonQuery();

            conn.Close();

            return StringHelpers.Encrypt(guid);
        }
        catch (Exception ex)
        {
            Session["ERROR"] = ex.Message;

            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }

            return "400";
        }
    }

    protected void ddlConference_SelectedIndexChanged(object sender, EventArgs e)
    {
        string conference = this.ddlConference.SelectedItem.Text;

        if(conference == "--Select--")
        {
            ddlHotelDiv.Visible = false;
            return;
        }

        DataTable dtHotelAndConferences = (DataTable)ViewState["HotelsAndConferences"];

        List<Hotel> hotels = GetHotelsForConference(conference, dtHotelAndConferences);

        FillDDLHotels(hotels);

        ddlHotelDiv.Visible = true;
    }

    public class Hotel
    {
        public int Code { get; set; }
        public string Name { get; set; }
    }
}

   

