﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;

public partial class RepositoryIndividualPaymentReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e) 
    {
        if (Session["Admin"] == null)
        {
            Response.Redirect("./2repository/Login.aspx");
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable dtAuthorizationPayment = LoadIndividualPaymanets();
        
        if (dtAuthorizationPayment.Rows.Count > 0)
        {
            UpdateIndividualDepositBookingPaymentsFromABTSolute(ref dtAuthorizationPayment);

            gvAuthorizationPayment.DataSource = dtAuthorizationPayment;
            gvAuthorizationPayment.DataBind();

            ViewState["dtAuthorizationPayment"] = dtAuthorizationPayment;
        }
    }

    protected DataTable LoadIndividualPaymanets()
    {
        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = conn;
        command.CommandType = CommandType.Text;

        var sql = @"SELECT [id]
                          ,[conferenceID]
                          ,[paymentDetailCCID]
                          ,[cardTypeID]
                          ,[creditCardNumber]
                          ,[ccv]
                          ,[exparationDate]
                          ,[cardHolderName]
                          ,[userID]
                          ,[timestamp]
                          ,[token]
	                      ,Case cardTypeID when 1 then 'Visa' when 2 then 'MasterCard' when 3 then 'American Express' end CardType
                          ,[transactionID]
                          ,[email]
                          ,[address]
                          ,[depositAmount]
                          ,[finalBalanceTransactionID]
                          ,[finalBalanceAmount]
	                      ,ISNULL([merchant], 0) merchant
                          ,[bookingNumber]
                          ,SUBSTRING([conferenceID], 0, LEN([conferenceID]) - 3) + SUBSTRING([conferenceID], LEN([conferenceID]) - 1, 2) Conference
                      FROM [dbo].[IndividualPayment]
                      Where 1=1";

        String dateFrom = String.Empty;
        String dateTo = String.Empty;
        String ccNumber = String.Empty;
        String cardHolerName = String.Empty;

        if (this.tbDateFrom.Text.Length > 0 && tbDateTo.Text.Length > 0)
        {
            dateFrom = DateTime.Parse(this.tbDateFrom.Text).ToShortDateString();
            dateTo = DateTime.Parse(this.tbDateTo.Text).AddDays(1).ToShortDateString();

            sql += " AND timestamp >= @DateFrom and timestamp <= @DateTo";

            command.Parameters.Add(new SqlParameter("@DateFrom", dateFrom));
            command.Parameters.Add(new SqlParameter("@DateTo", dateTo));

        }

        if (tbCCNumber.Text.Length > 0)
        {
            ccNumber = tbCCNumber.Text;

            sql += " AND cc = @CC";

            command.Parameters.Add(new SqlParameter("@CC", ccNumber));
        }


        if (tbCardHolderName.Text.Length > 0)
        {
            cardHolerName = tbCardHolderName.Text;

            sql += " AND cardholderName like '%' + @CardHolerName + '%'";

            command.Parameters.Add(new SqlParameter("@CardHolerName", cardHolerName));
        }


        if (ddlMerchantSearch.SelectedValue != "0")
        {
            int merchant = int.Parse(ddlMerchantSearch.SelectedValue);
            sql += " AND merchant = @Merchant";
            command.Parameters.Add(new SqlParameter("@Merchant", merchant));
        }

        command.CommandText = sql;

        SqlDataAdapter sda = new SqlDataAdapter(command);

        DataTable dtAuthorizationPayment = new DataTable();

        try
        {
            sda.Fill(dtAuthorizationPayment);

            return dtAuthorizationPayment;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private void UpdateIndividualDepositBookingPaymentsFromABTSolute(ref DataTable dtAuthorizationPayment)
    {
        string transactionNumbers = String.Empty;

        foreach(DataRow dr in dtAuthorizationPayment.Rows)
        {
            if (dr["DepositAmount"].ToString().Length == 0)
            {
                transactionNumbers += "'" + dr["transactionID"].ToString() + "'" + ",";
            }
        }

        transactionNumbers = transactionNumbers.Substring(0, transactionNumbers.Length - 1);
		
		ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        HotelRooms.ABTSoluteWebServices abtsService = new HotelRooms.ABTSoluteWebServices();

        DataTable dtDepositBookingPayments =  abtsService.GetAllIndividualDepositBookingPaymentsByTransactionID(transactionNumbers, "9DD873E8-3465-4426-B498-38EA9589090E");

        if(UpdateIndividualPaymentWithABTSoluteDepositTransactions(dtDepositBookingPayments))
        {
            dtAuthorizationPayment = new DataTable();
            dtAuthorizationPayment = LoadIndividualPaymanets();
        }
    }

    private bool UpdateIndividualPaymentWithABTSoluteDepositTransactions(DataTable dtDepositBookingPayments)
    {
        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);

        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            try
            {
                foreach (DataRow dr in dtDepositBookingPayments.Rows)
                {
                    if (dr["FinalPaid"].ToString() == "Yes")
                    {
                        continue;
                    }

                    SqlCommand command = new SqlCommand(@"Update IndividualPayment
                                                          Set depositAmount = @DepositAmount, finalBalanceAmount = @FinalBalanceAmount, bookingNumber =  @BookingNumber
                                                          Where transactionID = @TransactionID", conn, trans);
                    command.CommandType = CommandType.Text;

                    command.Parameters.Clear();

                    command.Parameters.Add(new SqlParameter("@TransactionID", dr["DepositTransactionNumber"].ToString()));
                    command.Parameters.Add(new SqlParameter("@DepositAmount", dr["DepositAmount"].ToString()));
                    command.Parameters.Add(new SqlParameter("@FinalBalanceAmount", dr["FinalBalanceAmount"].ToString()));
                    command.Parameters.Add(new SqlParameter("@BookingNumber", dr["bookingNumber"].ToString()));

                    command.ExecuteNonQuery();
                }

                trans.Commit();                  
            }
            catch(Exception ex)
            {
                trans.Rollback();
                return false;
            }
            finally
            {
                conn.Close();
                conn.Dispose();                 
            }            
        }

        return true;
    }

    private void UpdateIndividualFinalBalancePayment(int id, string finalBalanceAmount, string finalBalanceTransactionID, string merchant)
    {
        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);

        try
        {
            conn.Open();

            SqlCommand command = new SqlCommand(@"Update IndividualPayment
                                                  Set finalBalanceAmount = @FinalBalanceAmount, finalBalanceTransactionID =  @FinalBalanceTransactionID, merchant = @Merchant
                                                  Where id = @ID", conn);

            command.CommandType = CommandType.Text;

            command.Parameters.Clear();

            command.Parameters.Add(new SqlParameter("@FinalBalanceTransactionID", finalBalanceTransactionID));            
            command.Parameters.Add(new SqlParameter("@FinalBalanceAmount", finalBalanceAmount));
            command.Parameters.Add(new SqlParameter("@Merchant", merchant));
            command.Parameters.Add(new SqlParameter("@ID", id));

            command.ExecuteNonQuery();
        }
        catch(Exception ex)
        {
        }
        finally
        {
            conn.Close();
            conn.Dispose();
        }
    }

    protected void gvAuthorizationPayment_RowDataBound(object sender, GridViewRowEventArgs e)
    {                   
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string authentication = StringHelpers.DecodeFrom64(Session["Authentication"].ToString());

            ((TextBox)e.Row.Cells[7].Controls[1]).ReadOnly = true;
            ((TextBox)e.Row.Cells[7].Controls[1]).BackColor = Color.Gainsboro;
            ((TextBox)e.Row.Cells[7].Controls[1]).BorderStyle = BorderStyle.Groove;
            ((TextBox)e.Row.Cells[7].Controls[1]).ForeColor = Color.DimGray;

            if (!authentication.Contains("akostka") && !authentication.Contains("jbrescia"))
            {
                e.Row.Cells[2].Text = e.Row.Cells[2].Text.Length == 16 ? "XXXXXXXXXXXX-" + e.Row.Cells[2].Text.Substring(12) : "XXXXXXXXXXX-" + e.Row.Cells[2].Text.Substring(11);

                e.Row.Cells[3].Text = "XX/XX/XXXX";
            }

            if (e.Row.Cells[8].Text.Trim().Length == 0)
            {
                e.Row.Cells[8].Text = "N/A";               
            }         
           
            var t = ((TextBox)e.Row.Cells[7].Controls[1]).Text.Trim();

            if (t.Length == 0)
            {
                ((TextBox)e.Row.Cells[7].Controls[1]).Text = "0.00";
            }

            if(((TextBox)e.Row.Cells[9].Controls[1]).Text.Length == 0)
            {
                ((TextBox)e.Row.Cells[9].Controls[1]).Text = "0.00";
            }

            if(e.Row.Cells[8].Text != "N/A")
            {
                ((TextBox)e.Row.Cells[9].Controls[1]).Enabled = false;
                ((DropDownList)e.Row.Cells[10].Controls[1]).Enabled = false;
                ((CheckBox)e.Row.Cells[11].Controls[1]).Enabled = false;
            }
        }
    }

    protected void btnCharge_Click(object sender, EventArgs e)
    { 
		ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		
        var us = new User();
        us.UserID = "Admin";
        us.Password = "password";
        var usr = new Users();
        usr.CurrentUser = us;
        string message = "'";

        foreach (GridViewRow gvr in gvAuthorizationPayment.Rows)
        {
            if (gvr.RowType == DataControlRowType.DataRow)
            {
                if (((CheckBox) gvr.Cells[11].Controls[1]).Checked)
                {

                    string merchantID = ((DropDownList) gvr.Cells[10].Controls[1]).SelectedValue;

                    Merchant merchant = (Merchant)Enum.Parse(typeof (Merchant), merchantID);

                    var itemIndex = gvr.DataItemIndex;

                    int id = (int)gvAuthorizationPayment.DataKeys[itemIndex].Value;

                    var dtAuthPayment =  (DataTable)ViewState["dtAuthorizationPayment"];
                    DataRow dr = dtAuthPayment.Rows[itemIndex];
                    var pa = new PaymentAuthorizationFirstData();
                    pa.ProductPrice = ((TextBox) gvr.Cells[9].Controls[1]).Text;
                    GetPaymentAuthorizationFirstData(ref pa, dr, merchant);                     

                    string token = String.Empty;
                    var transactionID = String.Empty;                                     

                    if (pa.Token.Length > 0)
                    {
                        transactionID = InternetSecureAPI.SendUserInformation2BoaRestServiceTokenVersion(pa.ProductDescription, pa.ProposalID, pa.ProductPrice, pa.CardHolderName, pa.Token, pa.CardType, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), false, pa.ExactID, pa.Password, merchant);
                    }
                    else
                    {
                        transactionID = InternetSecureAPI.SendUserInformation2BoaRestService(pa.ProductPrice, pa.ProductCode, pa.ProductDescription, pa.CardHolderName, pa.Province, pa.Address, pa.City, "", pa.Postal, pa.Country, pa.Phone, pa.Email, pa.CreditCardNumber, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), pa.CCV, pa.Currency, pa.Test, pa.ExactID, pa.Password, ref token, merchant);  
                    }                  
                    
                    token = pa.Token.Length > 0 ? pa.Token : token;

                    if (transactionID.Length > 3)
                    {                        
                        message += "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Status: " + "Sucessfull\\n";

                        UpdateIndividualFinalBalancePayment(id, pa.ProductPrice, transactionID, merchantID.ToString());                    

                        using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
                        {
                            //Save Transaction to Log
                            sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "Sucessfully Charged: " + "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Merchant: " + merchant.ToString());
                        }
                    }
                    else
                    {
                        string res = String.Empty;

                        try
                        {
                            var firstData = ((InternetSecureAPI.FirstData)Int64.Parse(transactionID));
                            string result = firstData.ToString();
                            res = new string((result.SelectMany((c, i) => i != 0 && char.IsUpper(c) && !char.IsUpper(result[i - 1]) ? new char[] { ' ', c } : new char[] { c })).ToArray());
                        }
                        catch (Exception)
                        {

                            res = "Invalid Transaction";
                        }                     
                        
                        message += "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", " + ", Status: " + res + "\\n";

                        UpdateIndividualFinalBalancePayment(id, pa.ProductPrice, transactionID, merchant.ToString());                        

                        using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
                        {
                            //Save Transaction to Log
                            sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "Not Sucessfully Charged: " + "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Merchant: " + merchant.ToString());
                        }
                    }
                }
            }
        }

        message = message + "'";

        ClientScript.RegisterStartupScript(this.GetType(), "000", "alert("+ message + ");", true);

        btnSearch_Click(this, null);
    }

    protected void GetPaymentAuthorizationFirstData(ref PaymentAuthorizationFirstData pa, DataRow dr, Merchant merchant)
    {
        pa.PaymentDetailCCID = int.Parse(dr["id"].ToString());
        pa.CardTypeID = int.Parse(dr["cardTypeID"].ToString());
        pa.CreditCardNumber = dr["creditCardNumber"].ToString().Length > 16 ? StringHelpers.Decrypt(dr["creditCardNumber"].ToString()) : dr["creditCardNumber"].ToString();
        pa.Email = dr["email"].ToString();
        pa.EventName = dr["conferenceID"].ToString();
        pa.ExparationDate = DateTime.Parse(dr["exparationDate"].ToString());
        pa.InvoiceID = dr["paymentDetailCCID"].ToString();
        pa.ProposalID = dr["bookingNumber"].ToString();
        pa.CardHolderName = dr["cardholderName"].ToString();
        //pa.Preparer = dr["preparer"].ToString();
        //pa.City = dr["city"].ToString();
        //pa.Postal = dr["zip"].ToString();
        pa.Address = dr["address"].ToString();
        //pa.Phone = dr["phone"].ToString();
        //pa.Country = dr["country"].ToString();
        //pa.Province = dr["state"].ToString();
        //pa.PaymentInstructions = dr["paymentInstructions"].ToString();
        pa.Test = false;
        pa.CCV = ""; //this.tbCVV.Text;
        pa.ExactID = DatabaseConnection.GetGatewayID(merchant);
        pa.Password = DatabaseConnection.GetGatewayPassword(merchant);
        //pa.ProductDescription = dr["proposalID"].ToString() + dr["eventName"].ToString() + dr["invoiceID"].ToString();
        pa.Currency = "USD";
        //pa.DescriptionOfServices = dr["descriptionOfServices"].ToString();
        pa.ConferenceID = dr["conferenceID"].ToString();
        pa.Token = dr["Token"].ToString().Length > 0 ? dr["Token"].ToString() : "";
        pa.CardType = dr["CardType"].ToString();
        pa.DCC = false;
        pa.ProductDescription = dr["paymentDetailCCID"].ToString() + dr["Conference"].ToString() + dr["userID"].ToString();
        //pa.RateResponseID = dr["rateResponseId"].ToString();

    }
}