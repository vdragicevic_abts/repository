function Validate(frm) {

    var len = frm.elements.length;
    var message = '';

    for (var i = 0; i < len; i++) {

        if (frm.elements[i].attributes["display"] != undefined && frm.elements[i].attributes["display"].nodeValue == "true") {

            if ((frm.elements[i].attributes["message"] != undefined) && (frm.elements[i].tagName == "INPUT")) {

                if (frm.elements[i].value == "") {

                    frm.elements[i].style.backgroundColor = '#FFF9D1';
                    message += frm.elements[i].attributes["message"].nodeValue + '\n'
                }
                else {

                    if (frm.elements[i].attributes["regularexpression"] != undefined) {

                        if (frm.elements[i].value.search(frm.elements[i].attributes["regularexpression"].nodeValue) == -1) {

                            frm.elements[i].style.backgroundColor = '#FFF9D1';
                            message += frm.elements[i].attributes["regmessage"].nodeValue + '\n';

                        }
                        else {

                            frm.elements[i].style.backgroundColor = 'white';
                        }

                    }
                    else {

                        frm.elements[i].style.backgroundColor = 'white';
                    }
                }
            }

            if ((frm.elements[i].attributes["message"] != undefined) && (frm.elements[i].tagName == "SELECT")) {

                frm.elements[i].style.backgroundColor = 'white';
                
                if (frm.elements[i].options[frm.elements[i].options.selectedIndex].value == '-1') {
                    
                    frm.elements[i].style.backgroundColor = '#FFF9D1';
                    message += frm.elements[i].attributes["message"].nodeValue + '\n';

                }
            }
        }
    }    

    if (message.length != '') {

        alert(message);
        return false;
    }
    else {
        return true;
    }
}

function ValidationGroup(enable) {

    if (enable == 'true') {

        document.getElementById('ctl00_ContentPlaceHolder1_tbGroupName').setAttribute("display", 'true');
        document.getElementById('ctl00_ContentPlaceHolder1_tbAgencyName').setAttribute("display", 'true');
        document.getElementById('ctl00_ContentPlaceHolder1_tbPhoneGroup').setAttribute("display", 'true');
        document.getElementById('ctl00_ContentPlaceHolder1_ddlCompanyType').setAttribute("display", 'true');     
    }
    else {

        document.getElementById('ctl00_ContentPlaceHolder1_tbGroupName').removeAttribute("display", 0);
        document.getElementById('ctl00_ContentPlaceHolder1_tbGroupName').style.backgroundColor = 'white';

        document.getElementById('ctl00_ContentPlaceHolder1_tbAgencyName').removeAttribute("display", 0);
        document.getElementById('ctl00_ContentPlaceHolder1_tbAgencyName').style.backgroundColor = 'white';

        document.getElementById('ctl00_ContentPlaceHolder1_tbPhoneGroup').removeAttribute("display", 0);
        document.getElementById('ctl00_ContentPlaceHolder1_tbPhoneGroup').style.backgroundColor = 'white';

        document.getElementById('ctl00_ContentPlaceHolder1_ddlCompanyType').removeAttribute("display", 0);
        document.getElementById('ctl00_ContentPlaceHolder1_ddlCompanyType').style.backgroundColor = 'white';            
    }
}

function ValidationIndividual(enable) {

    if (enable == 'true') {

        document.getElementById('ctl00_ContentPlaceHolder1_tbCompanyName').setAttribute("display", 'true');
        document.getElementById('ctl00_ContentPlaceHolder1_tbTitle').setAttribute("display", 'true');
        document.getElementById('ctl00_ContentPlaceHolder1_tbPhoneIndividual').setAttribute("display", 'true');
        //document.getElementById('ctl00_ContentPlaceHolder1_ddlPhysician').setAttribute("display", 'true');
        
    }
    else {

        document.getElementById('ctl00_ContentPlaceHolder1_tbCompanyName').removeAttribute("display", 0);
        document.getElementById('ctl00_ContentPlaceHolder1_tbCompanyName').style.backgroundColor = 'white';

        document.getElementById('ctl00_ContentPlaceHolder1_tbTitle').removeAttribute("display", 0);
        document.getElementById('ctl00_ContentPlaceHolder1_tbTitle').style.backgroundColor = 'white';

        document.getElementById('ctl00_ContentPlaceHolder1_tbPhoneIndividual').removeAttribute("display", 0);
        document.getElementById('ctl00_ContentPlaceHolder1_tbPhoneIndividual').style.backgroundColor = 'white';

        //document.getElementById('ctl00_ContentPlaceHolder1_ddlPhysician').removeAttribute("display", 0);
        //document.getElementById('ctl00_ContentPlaceHolder1_ddlPhysician').style.backgroundColor = 'white';
    }    
}

function validateCountry() {

    if (document.getElementById('ctl00_ContentPlaceHolder1_ddlCountries').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlCountries').options.selectedIndex].value == '226') {

        document.getElementById('ctl00_ContentPlaceHolder1_tbState').setAttribute("display", 'true');
    }
    else {

        document.getElementById('ctl00_ContentPlaceHolder1_tbState').removeAttribute("display", 0);
        document.getElementById('ctl00_ContentPlaceHolder1_tbState').style.backgroundColor = 'white';
    }
}

function ComparePasswords() {

    if (document.getElementById('ctl00_ContentPlaceHolder1_tbPassword').value != document.getElementById('ctl00_ContentPlaceHolder1_tbRetypePassword').value) {

        alert('Passwords do not match! Please reenter.');
        document.getElementById('ctl00_ContentPlaceHolder1_tbRetypePassword').focus();
        document.getElementById('ctl00_ContentPlaceHolder1_tbRetypePassword').select();
        document.getElementById('ctl00_ContentPlaceHolder1_tbRetypePassword').style.backgroundColor = '#FFF9D1';
        return false;
    }
    else {

        document.getElementById('ctl00_ContentPlaceHolder1_tbRetypePassword').style.backgroundColor = 'white';
        return true;
    }
}

function ValidationCreditCardInformation(enable) {

    if (enable == 'true') {

        document.getElementById('ddlCardType').setAttribute("display", 'true');
        document.getElementById('tbCardNumber').setAttribute("display", 'true');
        document.getElementById('tbCCV').setAttribute("display", 'true');
        document.getElementById('tbCardHolderName').setAttribute("display", 'true'); 
    }
    else {

        document.getElementById('ddlCardType').removeAttribute("display", 'false');
        document.getElementById('ddlCardType').style.backgroundColor = 'white';

        document.getElementById('tbCardNumber').removeAttribute("display", 0);
        document.getElementById('tbCardNumber').style.backgroundColor = 'white';

        document.getElementById('tbCCV').removeAttribute("display", 0);
        document.getElementById('tbCCV').style.backgroundColor = 'white';

        document.getElementById('tbCardHolderName').removeAttribute("display", 0);
        document.getElementById('tbCardHolderName').style.backgroundColor = 'white';
    }
}

function ValidationAtendee(enable) {

    if (enable == 'true') {

        document.getElementById('travellingWith').style.visibility = "visible";
        document.getElementById('ctl00_ContentPlaceHolder1_ddlAtendee').setAttribute("display", 'true');
        document.getElementById('ctl00_ContentPlaceHolder1_tbAtendee').setAttribute("display", 'true');
    }
    else {

        document.getElementById('travellingWith').style.visibility = "hidden";
        document.getElementById('ctl00_ContentPlaceHolder1_ddlAtendee').removeAttribute("display", 0);
        document.getElementById('ctl00_ContentPlaceHolder1_ddlAtendee').style.backgroundColor = 'white';

        document.getElementById('ctl00_ContentPlaceHolder1_tbAtendee').removeAttribute("display", 0);
        document.getElementById('ctl00_ContentPlaceHolder1_tbAtendee').style.backgroundColor = 'white';    
    }

}
