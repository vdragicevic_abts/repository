﻿function Validation(frm) {

    var len = frm.elements.length;
    var message = '';

    for (var i = 0; i < len; i++) {

        if (frm.elements[i].attributes["display"] != undefined && frm.elements[i].attributes["display"].nodeValue == "true") {

            if ((frm.elements[i].attributes["message"] != undefined) && (frm.elements[i].tagName == "INPUT")) {

                if (frm.elements[i].value == "") {

                    frm.elements[i].style.backgroundColor = '#FFF9D1';
                    message += frm.elements[i].attributes["message"].nodeValue + '\n'
                }
                else {

                    if (frm.elements[i].attributes["regularexpression"] != undefined) {

                        if (frm.elements[i].value.search(frm.elements[i].attributes["regularexpression"].nodeValue) == -1) {

                            frm.elements[i].style.backgroundColor = '#FFF9D1';
                            message += frm.elements[i].attributes["regmessage"].nodeValue + '\n';

                        }
                        else {

                            frm.elements[i].style.backgroundColor = 'white';
                        }

                    }
                    else {

                        frm.elements[i].style.backgroundColor = 'white';
                    }
                }
            }

            if ((frm.elements[i].attributes["message"] != undefined) && (frm.elements[i].tagName == "SELECT")) {

                frm.elements[i].style.backgroundColor = 'white';

                if (frm.elements[i].id == 'ddlHotelsNew' && frm.elements[i].options[frm.elements[i].options.selectedIndex].value == '0') {

                    frm.elements[i].style.backgroundColor = '#FFF9D1';
                    message += frm.elements[i].attributes["message"].nodeValue + '\n';
                }

                if (frm.elements[i].id == 'ddlHotelsNew' && frm.elements[i].options[frm.elements[i].options.selectedIndex].text.search("Sold") > 0) {

                    frm.elements[i].style.backgroundColor = '#FFF9D1';
                    message += "*Hotel sold out! \n";
                }

                if (frm.elements[i].id != 'ddlHotelsNew' && frm.elements[i].options[frm.elements[i].options.selectedIndex].value == '-1') {

                    frm.elements[i].style.backgroundColor = '#FFF9D1';
                    message += frm.elements[i].attributes["message"].nodeValue + '\n';
                }
            }
        }
    }

    if (message.length != '') {

        alert(message);
        return false;
    }
    else {        
        return true;       
    }
}

function paymentValidation(enable) {

    if (enable == 'true') {

        document.getElementById('cardholder').setAttribute("display", 'true');
        document.getElementById('tbBillingAddress').setAttribute("display", 'true');
        document.getElementById('tbCity').setAttribute("display", 'true');
        document.getElementById('tbZipCode').setAttribute("display", 'true');
        document.getElementById('tbPhone').setAttribute("display", 'true');
        document.getElementById('tbExpateMonth').setAttribute("display", 'true');
        document.getElementById('tbExpateYear').setAttribute("display", 'true');
        document.getElementById('cardnumber').setAttribute("display", 'true');
        document.getElementById('cvv').setAttribute("display", 'true');
        document.getElementById('tbEmail').setAttribute("display", 'true');

    }
    else {

        document.getElementById('cardholder').removeAttribute("display", 0);
        document.getElementById('cardholder').style.backgroundColor = 'white';

        document.getElementById('tbBillingAddress').removeAttribute("display", 0);
        document.getElementById('tbBillingAddress').style.backgroundColor = 'white';

        document.getElementById('tbCity').removeAttribute("display", 0);
        document.getElementById('tbCity').style.backgroundColor = 'white';

        document.getElementById('tbZipCode').removeAttribute("display", 0);
        document.getElementById('tbZipCode').style.backgroundColor = 'white';

        document.getElementById('tbPhone').removeAttribute("display", 0);
        document.getElementById('tbPhone').style.backgroundColor = 'white';

        document.getElementById('tbExpateMonth').removeAttribute("display", 0);
        document.getElementById('tbExpateMonth').style.backgroundColor = 'white';

        document.getElementById('tbExpateYear').removeAttribute("display", 0);
        document.getElementById('tbExpateYear').style.backgroundColor = 'white';

        document.getElementById('cardnumber').removeAttribute("display", 0);
        document.getElementById('cardnumber').style.backgroundColor = 'white';

        document.getElementById('cvv').removeAttribute("display", 0);
        document.getElementById('cvv').style.backgroundColor = 'white';

        document.getElementById('tbEmail').removeAttribute("display", 0);
        document.getElementById('tbEmail').style.backgroundColor = 'white';

    }

}