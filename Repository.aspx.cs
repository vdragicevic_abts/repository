﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class Repository : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin"] == null)
        {
            Response.Redirect("./2repository/Login.aspx");
        }

        if (!IsPostBack)
        {

        }
        if (!IsPostBack)
        {
            FillConferences();
            FillDDLCardTypes();
        }
    }

    protected void FillConferences()
    {
       Users usr = new Users();
       DataTable dtConferences = usr.GetConferencesRS();
       
        try
        {
            if (dtConferences.Rows.Count > 0)
            {

                ddlConference.DataTextField = "conferenceID";
                ddlConference.DataValueField = "conferenceID";
                ddlConference.DataSource = dtConferences;

                ddlConference.DataBind();

                ListItem li = new ListItem("Select", "-1");

                ddlConference.Items.Insert(0, li);
            }

        }
        catch (Exception ex)
        {
          
        }                                           
    }

    protected void btnGet_Click(object sender, EventArgs e)
    {                
        HousingRegistrationAgreement hra = HousingAgreementHelper.SelectHousingAgreement(ddlConference.SelectedValue, int.Parse(tbHAID.Text), int.Parse(tbUserID.Text));

        Log.InsertLog4Groups(ddlConference.SelectedValue, int.Parse(tbHAID.Text), int.Parse(tbUserID.Text), "SendHAToRepositoryRS");

        if (hra.CreditCardNumber != null)
        {
            this.tbCreditCardNumber.Text = StringHelpers.Decrypt(hra.CreditCardNumber);
            this.ddlCreditCard.SelectedValue = hra.CardTypeID.ToString();
            this.tbExparationMonth.Text = hra.ExparationDate.Month.ToString();
            this.tbExparationYear.Text = hra.ExparationDate.Year.ToString();
        }
    }    

    protected void FillDDLCardTypes()
    {
        Users usr = new Users();
        DataTable dtCardTypes = usr.GetVisaTypesRS();

        if (dtCardTypes.Rows.Count > 0)
        {
            ddlCreditCard.DataTextField = "Name";
            ddlCreditCard.DataValueField = "id";
            ddlCreditCard.DataSource = dtCardTypes;
            ddlCreditCard.DataBind();

            ListItem li = new ListItem("None", "-1");
            ddlCreditCard.Items.Insert(0, li);
        }
    }

}