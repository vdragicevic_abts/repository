﻿<%@ WebHandler Language="C#" Class="IndividualsBoa" %>

using System;
using System.Web;
using System.Net;

public class IndividualsBoa : IHttpHandler {

    public void ProcessRequest (HttpContext context) {

        ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        try
        {
            string cardholder = context.Request["cardholder"];
            string address = context.Request["address"];
            string phone = context.Request["phone"];
            string email = context.Request["email"];
            string city = context.Request["city"];
            string postal = context.Request["postal"];
            string country = context.Request["country"];
			string state = context.Request["state"] != null ? context.Request["state"] : "";
            string cardType = context.Request["cardType"];
            string cardTypeID = context.Request["cardTypeID"];
            string expireMonth = context.Request["expireMonth"];
            string expireYear = context.Request["expireYear"];
            string cardnumber = context.Request["cardnumber"];
            string ccv = context.Request["ccv"];
            string amount = context.Request["amountcc"];
            string code = context.Request["code"];
            string exactID = context.Request["gatewayID"];
            string password = context.Request["gatewayPassword"];
            string paymentDetailCCID = String.Empty;
            string conferenceID = String.Empty;
            string userID = String.Empty;
            string callback = context.Request["callback"];
            string currency = context.Request["currency"];
            string test = context.Request["test"];
            string conferenceName = context.Request["conferenceName"];
            string guestName = context.Request["guestName"];
            string websiteBookingID = context.Request["websiteBookingID"];

			// Set Merchant terminal sent from website or default one
            Merchant merchant = context.Request["merchant"] != null ? (Merchant)Enum.Parse(typeof(Merchant), context.Request["merchant"]) : Merchant.IgdInternationlGroup;

            // Check Merchant from ABTSolute
            Merchant? merchantTerminal = new Users().GetMerchantIDFromABTSolute(conferenceName);

            if(merchantTerminal != null)
            {
                merchant = (Merchant)merchantTerminal;
            }
			
			
			guestName = context.Request["guestName"] != null ? context.Request["guestName"].ToString() : "";

            ParseCode(code, ref paymentDetailCCID, ref conferenceID, ref userID);

            string transactionID = SendPayPalPayment2Repository(int.Parse(paymentDetailCCID), int.Parse(userID), int.Parse(cardTypeID), cardType, cardnumber, ccv, cardholder, new DateTime(int.Parse("20" + expireYear), int.Parse(expireMonth), 1), conferenceID, address, city, country, email, phone, postal, "Individual Payment ", amount, conferenceID + userID + ", " + cardholder + ", " + email, exactID, password, conferenceName, Boolean.Parse(test), currency, merchant, guestName, websiteBookingID, code, state);

            context.Response.ContentType = "application/javascript; charset=utf-8";
            context.Response.Write(callback + "({ \"transactionID\": \"" + transactionID + "\" })");
        }
        catch (Exception ex)
        {
            string callback = context.Request["callback"];
			context.Response.ContentType = "application/javascript; charset=utf-8";
            context.Response.Write(callback + "({ \"transactionID\": \"" + "40" + "\" })");
        }
    }

    protected void ParseCode(string _code, ref string paymentDetailCCID, ref string conferenceID, ref string userID)
    {
        string code = _code.Trim();

        char[] c = code.ToCharArray();

        paymentDetailCCID = String.Empty;
        conferenceID = String.Empty;
        userID = String.Empty;

        bool firstNumAfterLetter = false;
        int yearCount = 0;

        foreach (char chr in c)
        {
            if (char.IsDigit(chr))
            {
                if (yearCount == 2)
                {
                    firstNumAfterLetter = false;
                    userID += chr;
                }

                if (firstNumAfterLetter)
                {
                    conferenceID += chr;
                    yearCount += 1;
                }
                else
                {
                    if (yearCount == 0)
                    {
                        paymentDetailCCID += chr;
                    }
                }
            }

            if (char.IsLetter(chr))
            {
                conferenceID += chr;
                firstNumAfterLetter = true;
            }
        }
    }


    protected string SendPayPalPayment2Repository(int id, int userID, int cardTypeID, string cardType, string cardNumber, string ccv, string cardHolder, DateTime exparationDate, string conferenceID, string address, string city, string country, string email, string phone, string postal, string productDescription, string price, string province, string exactID, string password, string conferenceName, bool test, string currency, Merchant merchant, string guestName, string bookingNumber, string code, string stateName)
    {

        string fullName = string.Empty;
        string userEmail = string.Empty;
        string transactionID = string.Empty;

        User us = new User();
        us.UserID = "Admin";
        us.Password = "password";

        Users users = new Users();
        users.CurrentUser = us;

        IndividualPaymentInternetSecure ip = new IndividualPaymentInternetSecure();

        ip.CardHolderName = cardHolder;
        ip.CardTypeID = cardTypeID;
        ip.CardType = cardType;
        ip.CCV = ccv;
        ip.ConferenceID = conferenceName; //conferenceID;
        ip.CreditCardNumber = cardNumber;
        ip.ExparationDate = exparationDate;
        ip.PaymentDetailCCID = id;
        ip.UserID = userID;
        ip.Address = address;
        ip.City = city;
        ip.Country = country;
        ip.Email = email;
        ip.Phone = phone;
        ip.Postal = postal;
        ip.ProductCode = id.ToString();
        ip.ProductDescription = productDescription;
        ip.ProductPrice = price;
        //ip.Province = id.ToString() + conferenceID + userID.ToString() + ", " + cardHolder + ", " + email;
        ip.Province = id.ToString() + conferenceID + userID.ToString();
        ip.ExactID = DatabaseConnection.GetGatewayID(merchant);
        ip.Password = DatabaseConnection.GetGatewayPassword(merchant);
        ip.Currency = currency;
        ip.Test = test;
        ip.MerchantAccount = merchant;
        ip.GuestName = guestName;
        ip.Code = code;
        ip.WebsiteBookingID = bookingNumber;
        ip.State = province;
		ip.StateName = stateName;

        try
        {
            transactionID = users.SendIPToRepositoryBOARQ(ip);
        }
        catch (Exception ex)
        {

        }

        return transactionID;
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}