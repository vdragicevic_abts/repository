﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using HotelRooms;
using System.Net;
using System.IO;

public partial class pages_PaymentAuthorizationFirstData : System.Web.UI.Page
{
    public string DbaName
    {
        get { return (string)ViewState["DBAName"]; }
        set { ViewState["DBAName"] = value; }
    }

    public int IDGuestList
    {
        get { return (int)ViewState["IDGuestList"]; }
        set { ViewState["IDGuestList"] = value; }
    }

    public int IDServiceItem
    {
        get { return (int)ViewState["IDServiceItem"]; }
        set { ViewState["IDServiceItem"] = value; }
    }

    public long HotelCode
    {
        get { return (long)ViewState["HotelCode"]; }
        set { ViewState["HotelCode"] = value; }
    }

    public Merchant MerchantAccount
    {
        get { return (Merchant)ViewState["MerchantAccount"]; }
        set { ViewState["MerchantAccount"] = value; }
    }

    public string GuestName
    {
        get { return (string)ViewState["GuestName"]; }
        set { ViewState["GuestName"] = value; }
    }

    public string Code
    {
        get { return (string)ViewState["Code"]; }
        set { ViewState["Code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        if (!IsPostBack)
        {
            IDServiceItem = 0;
            IDGuestList = 0;
            HotelCode = 0;
            MerchantAccount = Merchant.IgdInternationlGroup;
            GuestName = "";
            Code = "";
        }

        //string test = "ProposalID=232&InvoiceNumber=3232&Email=dvladh@gmail.com&EventName=ASBMR2012";
        //test = HttpUtility.UrlEncode(StringHelpers.DecodeTo64(test));

        if (Request.QueryString.Count > 0)
        {
            string str = Request.QueryString.Get(0);
            str = HttpUtility.UrlDecode(str);

            string d = String.Empty;

            try
            {
                d = StringHelpers.DecodeFrom64(str);
            }
            catch (Exception)
            {
                Response.Write("<script type='text/javascript'>alert('There is a problem with this link. The URL may have been broken or cut off in the email. Please copy and paste the entire link address (URL) into your browser`s address bar and press Enter.');</script>");
                Response.End();
            }

            string[] query = d.Split('&');

            foreach (string s in query)
            {
                string[] q = s.Split('=');

                switch (q[0])
                {
                    case "ProposalID":
                        this.tbProposalID.Text = q[1];
                        break;
                    case "InvoiceNumber":
                        this.tbInvoiceNumber.Text = q[1];
                        break;
                    case "Email":
                        this.tbEmail.Text = q[1];
                        break;
                    case "EventName":
                        this.tbEventName.Text = q[1]; //this.imgLogo.ImageUrl = "../repository/Visuals/img/" + q[1] + ".jpg";
                        break;
                    //this.imgLogo.ImageUrl = "../repository/Visuals/img/" + q[1] + ".jpg";
                    case "CompanyLogo":
                        DbaName = q[1]; this.lblLogo.Text = q[1];
                        break;
                    case "CSM":
                        this.hfCMS.Value = q[1];
                        break;
                    case "Preparer":
                        this.hfPreparer.Value = q[1];
                        break;
                    case "Amount":
                        this.tbPrice.Value = Decimal.Parse(q[1].Replace("$", "").Trim()).ToString("F");
                        break;
                    case "Note":
                        this.txtDescriptionOfServices.Value = q[1];
                        break;
                    case "IDGuestList":
                        IDGuestList = int.Parse(q[1]);
                        break;
                    case "IDServiceItem":
                        IDServiceItem = int.Parse(q[1]);
                        break;
                    case "HotelCode":
                        HotelCode = long.Parse(q[1]);
                        break;
                    case "Merchant":
                        MerchantAccount = (Merchant)Enum.Parse(typeof(Merchant), q[1]);
                        break;
                    case "GuestName":
                        GuestName = q[1];
                        break;
                    case "Code":
                        Code = q[1];
                        break;
                }
            }


            //Set Privacy Policy link depending on DBA name
            linkPrivacyPolicy.Attributes["onClick"] = lblLogo.Text.ToLower().Contains("cmr") ? "window.open('" + ConfigurationManager.AppSettings["PrivacyPolicyCMR"] + "','targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=800, height=1000'); return false;" : "window.open('" + ConfigurationManager.AppSettings["PrivacyPolicyIGD"] + "','targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=800, height=1000'); return false;";
        }

        if (!IsPostBack)
        {
            FillDDLCardTypes();
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        SavePaymentAuthorization();
    }


    protected void FillDDLCardTypes()
    {
        Users usr = new Users();
        DataTable dtCardTypes = usr.GetVisaTypesRS();

        if (dtCardTypes.Rows.Count > 0)
        {
            ddlCardType.DataTextField = "Name";
            ddlCardType.DataValueField = "id";
            ddlCardType.DataSource = dtCardTypes;
            ddlCardType.DataBind();

            ListItem li = new ListItem("None", "-1");
            ddlCardType.Items.Insert(0, li);

            //ddlCardType.Items.FindByText("American Express").Enabled = false;

        }
    }

    protected void SavePaymentAuthorization()
    {
        var us = new User();
        us.UserID = "Admin";
        us.Password = "password";

        var usr = new Users();
        usr.CurrentUser = us;
		
		Merchant? merchantTerminal = new Users().GetMerchantIDFromABTSolute(tbEventName.Text);

        if (merchantTerminal == null)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "002", "alert('Merchant account not found!')", true);
            return;
        }
        else
        {
            MerchantAccount = (Merchant)merchantTerminal;
        }

        var pa = new PaymentAuthorizationFirstData();
        GetPaymentAuthorizationFirstData(ref pa);
		
		MerchantAccount = (MerchantAccount.ToString().Contains("PP") && pa.CardType.ToLower().Contains("american express")) ? (Merchant)Enum.Parse(typeof(Merchant), MerchantAccount.ToString() + "_Amex") : MerchantAccount;

        pa.MerchantAccount = MerchantAccount;
		
		string transactionID = usr.SendPAToRepositoryFirstDataRQ(pa);

        /* var firstData = ((InternetSecureAPI.FirstData)Int64.Parse(transactionID));

        string result = firstData.ToString();

        var res = new string((result.SelectMany((c, i) => i != 0 && char.IsUpper(c) && !char.IsUpper(result[i - 1]) ? new char[] { ' ', c } : new char[] { c })).ToArray()); */

        string res = String.Empty;
        string result = String.Empty;

        if (transactionID.Length <= 3)
        {
            try
            {
                var firstData = ((InternetSecureAPI.FirstData)Int64.Parse(transactionID));

                result = firstData.ToString();
            }
            catch (Exception ex)
            {
                result = transactionID;
            }

            res = new string((result.SelectMany((c, i) => i != 0 && char.IsUpper(c) && !char.IsUpper(result[i - 1]) ? new char[] { ' ', c } : new char[] { c })).ToArray());
        }
		
		// Planet Payment - It is an error if it contains 'Error' in message 
        if(pa.MerchantAccount.ToString().Contains("PP"))
        {
            res = transactionID;
        }

        string information = (transactionID.Length > 3 && !transactionID.Contains("Error")) ? "Payment authorization successfully submitted!\\nYour transactionID is " + transactionID + ".\\nYou will receive an email confirming payment." : "Payment authorization not successfully submitted! " + res;

        //ClientScript.RegisterStartupScript(this.GetType(), "001", "alert('" + information  + "')", true);

        if (transactionID.Length > 3 && !transactionID.Contains("Error"))
        {
            if (pa.GuestListID != 0)
            {
                //Send Proposal final balance payment to ABTSolute
                var aws = new ABTSoluteWebServices();

                if (IDServiceItem != 0)
                {
                    //Save proposal request to Log
                    Log.InsertLog4Individuals(pa.EventName, pa.PaymentDetailCCID, " Request for Proposal: Conference: " + pa.EventName + " ServiceItemID: " + pa.ServiceItemID + " Cardholder: " + pa.CardHolderName + " Date: " + System.DateTime.Now + " IDGuestList: " + pa.GuestListID);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    IndividualSavingResult isr = aws.AddIndividualToProposal(pa.EventName, pa.GuestListID, pa.ServiceItemID, "", "", "", "", System.DateTime.Now, System.DateTime.Now, pa.Email, "", transactionID, Decimal.Parse(pa.ProductPrice), IndividualPaymentType.FinalDeposit, "9DD873E8-3465-4426-B498-38EA9589090E");

                    //Save Proposal response to Log
                    Log.InsertLog4Individuals(pa.EventName, pa.PaymentDetailCCID, " Response for Proposal : Conference: " + pa.EventName + " GuestListID: " + pa.GuestListID + " ServiceItemID: " + pa.ServiceItemID + " Status: " + isr.IndividualSavingStatus + " Error: " + isr.ErrorMessage);
                }
                else
                {
                    //Save proposal request to Log
                    //Log.InsertLog4Individuals(pa.EventName, pa.PaymentDetailCCID, " Request for Proposal: Conference: " + pa.EventName + " HotelCode: " + pa.HotelCode + " Cardholder: " + pa.CardHolderName + " Date: " + System.DateTime.Now + " IDGuestList: " + pa.GuestListID);

                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    //IndividualSavingResult isr = aws.AddIndividualToProposalByHotelCode(pa.EventName, pa.GuestListID, pa.HotelCode, 0, "", "", "", "", System.DateTime.Now, System.DateTime.Now, "", transactionID, Decimal.Parse(pa.ProductPrice), IndividualPaymentType.FinalDeposit, "9DD873E8-3465-4426-B498-38EA9589090E");

                    //Save Proposal response to Log
                    //Log.InsertLog4Individuals(pa.EventName, pa.PaymentDetailCCID, " Response for Proposal : Conference: " + pa.EventName + " GuestListID: " + pa.GuestListID + " HotelCode: " + pa.HotelCode + " Status: " + isr.IndividualSavingStatus + " Error: " + isr.ErrorMessage);

                }
            }

            EmailHelper.SendClientEmailFirstData(pa.CardHolderName, pa.EventName, pa.Email, pa.InvoiceID, pa.ProposalID, pa.DescriptionOfServices, pa.ProductPrice, "XXXXXXXXXXXX-" + pa.CreditCardNumber.Substring(12), transactionID, DbaName);
            EmailHelper.SendUserPaymentNotificationFirstData(pa.CardHolderName, pa.EventName, pa.Email, pa.InvoiceID, pa.ProposalID, this.hfCMS.Value, pa.Preparer, "XXXXXXXXXXXX-" + pa.CreditCardNumber.Substring(12), pa.DescriptionOfServices, pa.ProductPrice, transactionID);

            CelarFormFields();

            Response.Redirect("~/2repository/PaymentCompleted.html?transID=" + transactionID);
        }
        else
        {
            result = pa.MerchantAccount.ToString().Contains("PP") ? transactionID : result.Replace("Call", "") + ".";
            ClientScript.RegisterStartupScript(this.GetType(), "101", "alert('Payment authorization not successfully submitted! " + result + "')", true);

            using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
            {
                //Save Transaction to Log
                sw.WriteLine(pa.ProposalID + "-" + information + ", " + System.DateTime.Now.ToString() + ", PA ERROR");
            }
        }
    }

    protected void GetPaymentAuthorizationFirstData(ref PaymentAuthorizationFirstData pa)
    {
        pa.CardTypeID = int.Parse(this.ddlCardType.SelectedValue);
        pa.CreditCardNumber = this.tbCardNumber.Text;
        pa.Email = this.tbEmail.Text;
        pa.EventName = this.tbEventName.Text;
        pa.ExparationDate = DateTime.Parse(this.ddlMonth.SelectedValue.ToString() + "/" + "01" + "/" + this.ddlYear.SelectedValue.ToString());
        pa.InvoiceID = this.tbInvoiceNumber.Text;
        pa.ProposalID = this.tbProposalID.Text;
        pa.CardHolderName = this.tbCardHolderName.Text;
        pa.Preparer = this.hfPreparer.Value;
        pa.City = this.tbCity.Value;
        pa.Postal = this.tbZip.Value;
        pa.Address = this.tbAddress.Value;
        pa.Phone = this.tbPhone.Value;
        pa.Country = this.ddlCountry.Items[this.ddlCountry.SelectedIndex].Value;
        pa.Province = this.tbState.Value;
        pa.PaymentInstructions = this.textAreaInstruction.Value;
        pa.ProductPrice = this.tbPrice.Value.Trim().Replace("$", "");
        pa.Test = false;
        pa.CCV = MerchantAccount.ToString().Contains("PP") ? this.tbCVV.Text : this.ddlCardType.SelectedItem.Text; //this.tbCVV.Text;
        pa.CardType = this.ddlCardType.SelectedItem.Text;
        pa.ExactID = DatabaseConnection.GetGatewayID(MerchantAccount);
        pa.Password = DatabaseConnection.GetGatewayPassword(MerchantAccount);
        pa.ProductDescription = tbProposalID.Text + "|" + tbInvoiceNumber.Text;
        pa.Currency = "USD";
        pa.DescriptionOfServices = this.txtDescriptionOfServices.Value;
        pa.ConferenceID = DbaName;
        pa.GuestListID = IDGuestList;
        pa.ServiceItemID = IDServiceItem;
        pa.HotelCode = HotelCode;
        pa.MerchantAccount = MerchantAccount;
        pa.ProductCode = tbEventName.Text;
        pa.GuestName = GuestName;
        pa.GatewayID = Code;
    }

    protected void CelarFormFields()
    {
        this.tbCardHolderName.Text = "";
        this.tbCardNumber.Text = "";
        this.tbEventName.Text = "";
        this.tbInvoiceNumber.Text = "";
        this.tbProposalID.Text = "";
        this.ddlCardType.SelectedValue = "-1";
        this.ddlMonth.SelectedIndex = 0;
        this.ddlYear.SelectedIndex = 0;
        this.tbEmail.Text = "";
        this.hfCMS.Value = "";
        this.hfPreparer.Value = "";
        this.tbCity.Value = "";
        this.tbZip.Value = "";
        this.tbAddress.Value = "";
        this.tbPhone.Value = "";
        this.ddlCountry.SelectedIndex = 0;
        this.tbState.Value = "";
        this.tbCVV.Text = "";
        this.tbPrice.Value = "";
        this.txtDescriptionOfServices.Value = "";
    }
}