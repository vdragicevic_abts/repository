﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentAuthorizationFirstData.aspx.cs" Inherits="pages_PaymentAuthorizationFirstData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment Authorization</title>
    <script src="javascript/validation.js" type="text/javascript"></script>
    <link href="Visuals/css/mainFirstData.css" rel="stylesheet" type="text/css" />
    <link href="Visuals/css/pages/CreditCardFirstData.css" rel="stylesheet" type="text/css" />
    <script src="javascript/jquery.js" type="text/javascript"></script>
    <script src="javascript/jquery.price_format.2.0.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#cbAgree').removeAttr('checked');
            $('#btnOK').attr('disabled', 'disabled');
            $('#btnOK').fadeTo('fast', 0.5);
        });

        $(document).ready(function () {
            $('#tbPrice').priceFormat(
                {
                    prefix: '$ ',
                    suffix: '',
                    thousandsSeparator: ''
                });
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="ccFormWrapper" style="margin-left: 33%; margin-top: -6px;">
            <div align="center" style="font-weight: normal; margin-left: -20px;">
                <h1>
                    <asp:Label ID="lblLogo" runat="server" Text="Logo" Visible="false"></asp:Label></h1>
                <%--<asp:Image ID="imgLogo" runat="server" AlternateText="Conference Logo" Width="500" Height="80" />--%></div>
            <div align="center"><span>
                <h1>Credit Card Payment Form</h1>
            </span></div>
            <ul style="list-style-type: none; font-weight: normal; font-size: 10px; margin-left: -10px; padding: 10px;">
                <li>Please enter your credit card information in the fields below. Once submitted your credit card will be charged.</li>
                <br />
                <li>For your security, your credit card number is encrypted while in transit over the Internet.</li>
            </ul>
            <div class="float_left" id="ccFields1">
                <span class="field_label">Proposal ID: <span style="color: Red;"></span></span>
                <br />
                <div class="mediumText_field_wrapper mediumShortText_field_wrapper_vlajko">
                    <asp:TextBox CssClass="mediumText_field mediumShortText_field_vlajko" ID="tbProposalID" runat="server" message="*Enter ProposalID!" display="true" ReadOnly="true" BackColor="#cccccc" Style="color: #666;"></asp:TextBox>
                </div>
                <br />

                <span class="field_label">Event name: <span style="color: Red;"></span></span>
                <br />
                <div class="mediumText_field_wrapper mediumShortText_field_wrapper_vlajko">
                    <asp:TextBox CssClass="mediumText_field mediumShortText_field_vlajko" ID="tbEventName" runat="server" message="*Enter Event name!" display="true" ReadOnly="true" BackColor="#cccccc" Style="color: #666;"></asp:TextBox>
                </div>
                <br />

                <span class="field_label">Card Type: <span style="color: Red;">*</span></span><br />
                <div class="mediumText_field_wrapper mediumShortText_field_wrapper_vlajko">
                    <asp:DropDownList CssClass="mediumDDL_field mediumShortText_field_vlajko" ID="ddlCardType" runat="server" tip="option" message="*Select Card Type!" display="true"></asp:DropDownList>
                </div>
                <br />


                <div class="float_left" style="margin-right: 12px; height: 380px;">
                    <span class="field_label">Card Number: <span style="color: Red;">*</span></span><br />
                    <div class="mediumText_field_wrapper mediumShortText_field_wrapper_vlajko">
                        <asp:TextBox CssClass="mediumText_field mediumShortText_field_vlajko" ID="tbCardNumber" runat="server" message="*Enter creditcard number!" regularexpression="^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$"
                            regmessage="*Not a valid creditcard number!" display="true"></asp:TextBox>
                    </div>
                    <span class="field_label">
                        <br />
                        Billing Address: <span style="color: Red;">*</span></span>
                    <div class="longText_field_wrapper">
                        <input runat="server" maxlength="30" class="longText_field" type="text" id="tbAddress" message="*Enter billing address!" display="true" />
                    </div>
                    <span class="field_label">
                        <br />
                        Phone: <span style="color: Red;">*</span></span>
                    <div class="mediumText_field_wrapper">
                        <input runat="server" class="mediumText_field" type="text" id="tbPhone" message="*Enter phone number!" display="true" />
                    </div>
                    <span class="field_label">
                        <br />
                        Description of services: <span style="color: Red;"></span></span>
                    <div class="longText_field_wrapper">
                        <input runat="server" class="longText_field" type="text" id="txtDescriptionOfServices" message="*Enter description of services!" display="false" style="background-color: gainsboro;" readonly="readonly" />
                    </div>
                    <span class="field_label">
                        <br />
                        Payment Instructions/Comment: <span style="color: Red;"></span></span>
                    <div class="longTextArea_field_wrapper">
                        <textarea class="longText_field" runat="server" id="textAreaInstruction" rows="5" style="height: 80px;"></textarea>
                    </div>
                    <span class="field_label">
                        <br />
                        Amount: <span style="color: Red;">*</span></span>
                    <div class="mediumText_field_wrapper">
                        <input runat="server" class="mediumText_field" type="text" id="tbPrice" readonly="readonly" message="*Enter Price!" display="true" style="background-color: gainsboro;" />
                    </div>
                </div>

                <%-- <div class="float_left" style="display:none;">
                <span class="field_label">CCV: <span style="color:Red;"> *</span></span><br />
                <div class="shortText_field_wrapper" style="width:50px;">                   
                    <asp:TextBox CssClass="shortText_field" ID="tbCCV" runat="server"></asp:TextBox>
                </div>
            </div>--%>


                <div class="clear_float"></div>
                <br />
                <%--<div class="float_left" style="margin-right:10px;">
                <span class="field_label">Exp.Date: <span style="color:Red;"> *</span></span>
                <div class="shortDDL_field_Wrapper">
                    <asp:DropDownList CssClass="shortDDL_field" ID="ddlMonth" runat="server">
                            <asp:ListItem Text="01" Value="01"></asp:ListItem>
                            <asp:ListItem Text="02" Value="02"></asp:ListItem>
                            <asp:ListItem Text="03" Value="03"></asp:ListItem>
                            <asp:ListItem Text="04" Value="04"></asp:ListItem>
                            <asp:ListItem Text="05" Value="05"></asp:ListItem>
                            <asp:ListItem Text="06" Value="06"></asp:ListItem>
                            <asp:ListItem Text="07" Value="07"></asp:ListItem>
                            <asp:ListItem Text="08" Value="08"></asp:ListItem>
                            <asp:ListItem Text="09" Value="09"></asp:ListItem>
                            <asp:ListItem Text="10" Value="10"></asp:ListItem>
                            <asp:ListItem Text="11" Value="11"></asp:ListItem>
                            <asp:ListItem Text="12" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                 </div> 
             </div>   
             <div class="float_left">
                 <span class="field_label">/</span>
                 <div class="shortDDL_field_Wrapper">
                    <asp:DropDownList CssClass="shortDDL_field" ID="ddlYear" runat="server">
                            <asp:ListItem Text="2011" Value="11"></asp:ListItem>
                            <asp:ListItem Text="2012" Value="12"></asp:ListItem>
                            <asp:ListItem Text="2013" Value="13"></asp:ListItem>
                            <asp:ListItem Text="2014" Value="14"></asp:ListItem>
                            <asp:ListItem Text="2015" Value="15"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>--%>

                <%--<div class="clear_float"></div>--%>
            </div>


            <div class="float_left" id="ccFields2">

                <span class="field_label">Website Booking ID: </span>
                <br />
                <div class="text_field_wrapper">
                    <asp:TextBox CssClass="text_field" ID="tbInvoiceNumber" message="*Enter Invoice Number!" display="true" runat="server" ReadOnly="true" BackColor="#cccccc" Style="color: #666;"></asp:TextBox>
                </div>
                <br />

                <span class="field_label">Email:<span style="color: Red;"></span></span><br />
                <div class="longText_field_wrapper">
                    <asp:TextBox CssClass="longText_field" ID="tbEmail" message="*Enter Email!" display="true" runat="server" ReadOnly="true" BackColor="#cccccc" Style="color: #666;"></asp:TextBox>
                </div>
                <br />

                <span class="field_label">Card Holder Name:<span style="color: Red;"> *</span></span><br />
                <div class="longText_field_wrapper">
                    <asp:TextBox CssClass="longText_field" ID="tbCardHolderName" message="*Enter Card Holder Name!" display="true" runat="server"></asp:TextBox>
                </div>
                <br />

                <span class="field_label">CVV:<span style="color: Red;"> *</span></span><br />
                <div class="tinyText_field_wrapper">
                    <asp:TextBox CssClass="tinyText_field" ID="tbCVV" message="*Enter CVV!" display="true" runat="server" MaxLength="4"></asp:TextBox>
                </div>


                <div class="clear_float"></div>
                <br />
                <div class="float_left" style="margin-right: 10px;">
                    <span class="field_label">Exp.Date: <span style="color: Red;">*</span></span>
                    <div class="shortDDL_field_Wrapper">
                        <asp:DropDownList CssClass="shortDDL_field" ID="ddlMonth" runat="server">
                            <asp:ListItem Text="01" Value="01"></asp:ListItem>
                            <asp:ListItem Text="02" Value="02"></asp:ListItem>
                            <asp:ListItem Text="03" Value="03"></asp:ListItem>
                            <asp:ListItem Text="04" Value="04"></asp:ListItem>
                            <asp:ListItem Text="05" Value="05"></asp:ListItem>
                            <asp:ListItem Text="06" Value="06"></asp:ListItem>
                            <asp:ListItem Text="07" Value="07"></asp:ListItem>
                            <asp:ListItem Text="08" Value="08"></asp:ListItem>
                            <asp:ListItem Text="09" Value="09"></asp:ListItem>
                            <asp:ListItem Text="10" Value="10"></asp:ListItem>
                            <asp:ListItem Text="11" Value="11"></asp:ListItem>
                            <asp:ListItem Text="12" Value="12"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="float_left">
                    <span class="field_label">/</span>
                    <div class="shortDDL_field_Wrapper">
                        <asp:DropDownList CssClass="shortDDL_field" ID="ddlYear" runat="server">
                            <asp:ListItem Text="2016" Value="16"></asp:ListItem>
                            <asp:ListItem Text="2017" Value="17"></asp:ListItem>
                            <asp:ListItem Text="2018" Value="18"></asp:ListItem>
                            <asp:ListItem Text="2019" Value="19"></asp:ListItem>
                            <asp:ListItem Text="2020" Value="20"></asp:ListItem>
                            <asp:ListItem Text="2021" Value="21"></asp:ListItem>
                            <asp:ListItem Text="2022" Value="22"></asp:ListItem>
                            <asp:ListItem Text="2023" Value="23"></asp:ListItem>
                            <asp:ListItem Text="2024" Value="24"></asp:ListItem>
                            <asp:ListItem Text="2025" Value="25"></asp:ListItem>
                            <asp:ListItem Text="2026" Value="26"></asp:ListItem>
                            <asp:ListItem Text="2027" Value="27"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <%--<div style="height:1px; margin-top:5px; width:245px; border-top:1px dotted #ccc;"></div>--%>
            </div>
            <div class="float_left">
                <span class="field_label">
                    <br />
                    City: <span style="color: Red;">*</span></span>
                <div class="shortText_field_wrapper">
                    <input runat="server" type="text" id="tbCity" class="shortText_field" message="*Enter city!" display="true" />
                </div>
            </div>
            <div class="float_left" style="margin-left: 17px;">
                <span class="field_label">
                    <br />
                    Zip/Postal: <%--<span style="color: Red;">*</span>--%></span>
                <div class="shortText_field_wrapper">
                    <input runat="server" type="text" id="tbZip" class="shortText_field" message="*Enter zip!" display="false" />
                </div>
            </div>
            <div class="float_left">
                <span class="field_label">
                    <br />
                    Country: <span style="color: Red;">*</span>
                </span>
                <div class="longDDL_field_Wrapper">
                    <select runat="server" id="ddlCountry" class="longDDL_field" tip="option" message="*Select Country!" display="true" onchange="if(this.value == 226) { $('#tbState').attr('display', 'true'); } else { $('#tbState').attr('display', 'false'); };">
                        <option value="-1">Select</option>
                        <option value="AFG">Afghanistan</option>
                        <option value="ALA">Aland Islands</option>
                        <option value="ALB">Albania</option>
                        <option value="DZA">Algeria</option>
                        <option value="ASM">American Samoa</option>
                        <option value="AND">Andorra</option>
                        <option value="AGO">Angola</option>
                        <option value="AIA">Anguilla</option>
                        <option value="ATA">Antarctica</option>
                        <option value="ATG">Antigua and Barbuda</option>
                        <option value="ARG">Argentina</option>
                        <option value="ARM">Armenia</option>
                        <option value="ABW">Aruba</option>
                        <option value="AUS">Australia</option>
                        <option value="AUT">Austria</option>
                        <option value="AZE">Azerbaijan</option>
                        <option value="BHS">Bahamas</option>
                        <option value="BHR">Bahrain</option>
                        <option value="BGD">Bangladesh</option>
                        <option value="BRB">Barbados</option>
                        <option value="BLR">Belarus</option>
                        <option value="BEL">Belgium</option>
                        <option value="BLZ">Belize</option>
                        <option value="BEN">Benin</option>
                        <option value="BMU">Bermuda</option>
                        <option value="BTN">Bhutan</option>
                        <option value="BOL">Bolivia, Plurinational State of</option>
                        <option value="BES">Bonaire, Sint Eustatius and Saba</option>
                        <option value="BIH">Bosnia and Herzegovina</option>
                        <option value="BWA">Botswana</option>
                        <option value="BVT">Bouvet Island</option>
                        <option value="BRA">Brazil</option>
                        <option value="IOT">British Indian Ocean Territory</option>
                        <option value="BRN">Brunei Darussalam</option>
                        <option value="BGR">Bulgaria</option>
                        <option value="BFA">Burkina Faso</option>
                        <option value="BDI">Burundi</option>
                        <option value="KHM">Cambodia</option>
                        <option value="CMR">Cameroon</option>
                        <option value="CAN">Canada</option>
                        <option value="CPV">Cape Verde</option>
                        <option value="CYM">Cayman Islands</option>
                        <option value="CAF">Central African Republic</option>
                        <option value="TCD">Chad</option>
                        <option value="CHL">Chile</option>
                        <option value="CHN">China</option>
                        <option value="CXR">Christmas Island</option>
                        <option value="CCK">Cocos (Keeling) Islands</option>
                        <option value="COL">Colombia</option>
                        <option value="COM">Comoros</option>
                        <option value="COG">Congo</option>
                        <option value="COD">Congo, the Democratic Republic of the</option>
                        <option value="COK">Cook Islands</option>
                        <option value="CRI">Costa Rica</option>
                        <option value="HRV">Croatia</option>
                        <option value="CUB">Cuba</option>
                        <option value="CUW">Curaçao</option>
                        <option value="CYP">Cyprus</option>
                        <option value="CZE">Czech Republic</option>
                        <option value="CIV">Côte d'Ivoire</option>
                        <option value="DNK">Denmark</option>
                        <option value="DJI">Djibouti</option>
                        <option value="DMA">Dominica</option>
                        <option value="DOM">Dominican Republic</option>
                        <option value="ECU">Ecuador</option>
                        <option value="EGY">Egypt</option>
                        <option value="SLV">El Salvador</option>
                        <option value="GNQ">Equatorial Guinea</option>
                        <option value="ERI">Eritrea</option>
                        <option value="EST">Estonia</option>
                        <option value="ETH">Ethiopia</option>
                        <option value="FLK">Falkland Islands (Malvinas)</option>
                        <option value="FRO">Faroe Islands</option>
                        <option value="FJI">Fiji</option>
                        <option value="FIN">Finland</option>
                        <option value="FRA">France</option>
                        <option value="GUF">French Guiana</option>
                        <option value="PYF">French Polynesia</option>
                        <option value="ATF">French Southern Territories</option>
                        <option value="GAB">Gabon</option>
                        <option value="GMB">Gambia</option>
                        <option value="GEO">Georgia</option>
                        <option value="DEU">Germany</option>
                        <option value="GHA">Ghana</option>
                        <option value="GIB">Gibraltar</option>
                        <option value="GRC">Greece</option>
                        <option value="GRL">Greenland</option>
                        <option value="GRD">Grenada</option>
                        <option value="GLP">Guadeloupe</option>
                        <option value="GUM">Guam</option>
                        <option value="GTM">Guatemala</option>
                        <option value="GGY">Guernsey</option>
                        <option value="GIN">Guinea</option>
                        <option value="GNB">Guinea-Bissau</option>
                        <option value="GUY">Guyana</option>
                        <option value="HTI">Haiti</option>
                        <option value="HMD">Heard Island and McDonald Islands</option>
                        <option value="VAT">Holy See (Vatican City State)</option>
                        <option value="HND">Honduras</option>
                        <option value="HKG">Hong Kong</option>
                        <option value="HUN">Hungary</option>
                        <option value="ISL">Iceland</option>
                        <option value="IND">India</option>
                        <option value="IDN">Indonesia</option>
                        <option value="IRN">Iran, Islamic Republic of</option>
                        <option value="IRQ">Iraq</option>
                        <option value="IRL">Ireland</option>
                        <option value="IMN">Isle of Man</option>
                        <option value="ISR">Israel</option>
                        <option value="ITA">Italy</option>
                        <option value="JAM">Jamaica</option>
                        <option value="JPN">Japan</option>
                        <option value="JEY">Jersey</option>
                        <option value="JOR">Jordan</option>
                        <option value="KAZ">Kazakhstan</option>
                        <option value="KEN">Kenya</option>
                        <option value="KIR">Kiribati</option>
                        <option value="PRK">Korea, Democratic People's Republic of</option>
                        <option value="KOR">Korea, Republic of</option>
                        <option value="KWT">Kuwait</option>
                        <option value="KGZ">Kyrgyzstan</option>
                        <option value="LAO">Lao People's Democratic Republic</option>
                        <option value="LVA">Latvia</option>
                        <option value="LBN">Lebanon</option>
                        <option value="LSO">Lesotho</option>
                        <option value="LBR">Liberia</option>
                        <option value="LBY">Libya</option>
                        <option value="LIE">Liechtenstein</option>
                        <option value="LTU">Lithuania</option>
                        <option value="LUX">Luxembourg</option>
                        <option value="MAC">Macao</option>
                        <option value="MKD">Macedonia, the former Yugoslav Republic of</option>
                        <option value="MDG">Madagascar</option>
                        <option value="MWI">Malawi</option>
                        <option value="MYS">Malaysia</option>
                        <option value="MDV">Maldives</option>
                        <option value="MLI">Mali</option>
                        <option value="MLT">Malta</option>
                        <option value="MHL">Marshall Islands</option>
                        <option value="MTQ">Martinique</option>
                        <option value="MRT">Mauritania</option>
                        <option value="MUS">Mauritius</option>
                        <option value="MYT">Mayotte</option>
                        <option value="MEX">Mexico</option>
                        <option value="FSM">Micronesia, Federated States of</option>
                        <option value="MDA">Moldova, Republic of</option>
                        <option value="MCO">Monaco</option>
                        <option value="MNG">Mongolia</option>
                        <option value="MNE">Montenegro</option>
                        <option value="MSR">Montserrat</option>
                        <option value="MAR">Morocco</option>
                        <option value="MOZ">Mozambique</option>
                        <option value="MMR">Myanmar</option>
                        <option value="NAM">Namibia</option>
                        <option value="NRU">Nauru</option>
                        <option value="NPL">Nepal</option>
                        <option value="NLD">Netherlands</option>
                        <option value="NCL">New Caledonia</option>
                        <option value="NZL">New Zealand</option>
                        <option value="NIC">Nicaragua</option>
                        <option value="NER">Niger</option>
                        <option value="NGA">Nigeria</option>
                        <option value="NIU">Niue</option>
                        <option value="NFK">Norfolk Island</option>
                        <option value="MNP">Northern Mariana Islands</option>
                        <option value="NOR">Norway</option>
                        <option value="OMN">Oman</option>
                        <option value="PAK">Pakistan</option>
                        <option value="PLW">Palau</option>
                        <option value="PSE">Palestinian Territory, Occupied</option>
                        <option value="PAN">Panama</option>
                        <option value="PNG">Papua New Guinea</option>
                        <option value="PRY">Paraguay</option>
                        <option value="PER">Peru</option>
                        <option value="PHL">Philippines</option>
                        <option value="PCN">Pitcairn</option>
                        <option value="POL">Poland</option>
                        <option value="PRT">Portugal</option>
                        <option value="PRI">Puerto Rico</option>
                        <option value="QAT">Qatar</option>
                        <option value="ROU">Romania</option>
                        <option value="RUS">Russian Federation</option>
                        <option value="RWA">Rwanda</option>
                        <option value="REU">Réunion</option>
                        <option value="BLM">Saint Barthélemy</option>
                        <option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
                        <option value="KNA">Saint Kitts and Nevis</option>
                        <option value="LCA">Saint Lucia</option>
                        <option value="MAF">Saint Martin (French part)</option>
                        <option value="SPM">Saint Pierre and Miquelon</option>
                        <option value="VCT">Saint Vincent and the Grenadines</option>
                        <option value="WSM">Samoa</option>
                        <option value="SMR">San Marino</option>
                        <option value="STP">Sao Tome and Principe</option>
                        <option value="SAU">Saudi Arabia</option>
                        <option value="SEN">Senegal</option>
                        <option value="SRB">Serbia</option>
                        <option value="SYC">Seychelles</option>
                        <option value="SLE">Sierra Leone</option>
                        <option value="SGP">Singapore</option>
                        <option value="SXM">Sint Maarten (Dutch part)</option>
                        <option value="SVK">Slovakia</option>
                        <option value="SVN">Slovenia</option>
                        <option value="SLB">Solomon Islands</option>
                        <option value="SOM">Somalia</option>
                        <option value="ZAF">South Africa</option>
                        <option value="SGS">South Georgia and the South Sandwich Islands</option>
                        <option value="SSD">South Sudan</option>
                        <option value="ESP">Spain</option>
                        <option value="LKA">Sri Lanka</option>
                        <option value="SDN">Sudan</option>
                        <option value="SUR">Suriname</option>
                        <option value="SJM">Svalbard and Jan Mayen</option>
                        <option value="SWZ">Swaziland</option>
                        <option value="SWE">Sweden</option>
                        <option value="CHE">Switzerland</option>
                        <option value="SYR">Syrian Arab Republic</option>
                        <option value="TWN">Taiwan, Province of China</option>
                        <option value="TJK">Tajikistan</option>
                        <option value="TZA">Tanzania, United Republic of</option>
                        <option value="THA">Thailand</option>
                        <option value="TLS">Timor-Leste</option>
                        <option value="TGO">Togo</option>
                        <option value="TKL">Tokelau</option>
                        <option value="TON">Tonga</option>
                        <option value="TTO">Trinidad and Tobago</option>
                        <option value="TUN">Tunisia</option>
                        <option value="TUR">Turkey</option>
                        <option value="TKM">Turkmenistan</option>
                        <option value="TCA">Turks and Caicos Islands</option>
                        <option value="TUV">Tuvalu</option>
                        <option value="UGA">Uganda</option>
                        <option value="UKR">Ukraine</option>
                        <option value="ARE">United Arab Emirates</option>
                        <option value="GBR">United Kingdom</option>
                        <option value="UMI">United States Minor Outlying Islands</option>
                        <option value="USA">United States</option>
                        <option value="URY">Uruguay</option>
                        <option value="UZB">Uzbekistan</option>
                        <option value="VUT">Vanuatu</option>
                        <option value="VEN">Venezuela, Bolivarian Republic of</option>
                        <option value="VNM">Viet Nam</option>
                        <option value="VGB">Virgin Islands, British</option>
                        <option value="VIR">Virgin Islands, U.S.</option>
                        <option value="WLF">Wallis and Futuna</option>
                        <option value="ESH">Western Sahara</option>
                        <option value="YEM">Yemen</option>
                        <option value="ZMB">Zambia</option>
                        <option value="ZWE">Zimbabwe</option>
                    </select>
                </div>
                <span class="field_label">
                    <br />
                    State/Province: </span>
                <div class="shortText_field_wrapper">
                    <input runat="server" type="text" id="tbState" maxlength="2" class="shortText_field" message="*Enter state!" display="false" />
                </div>
            </div>
            <div class="clear_float">
            </div>
            <%--<div class="float_left" style="margin-right:500px; margin-top:15px;">
                <div class="button_wrapper1 vlajko_Wrapper">
                    <asp:Button CssClass="buttons1 vlajko" ID="btnOK" runat="server" Text="Submit" onclick="btnOK_Click" OnClientClick="return Validate(this.form);" style="background-color:#55A7D1;" />
                </div>
            </div>--%>

            <%-- <div class="float_left" style="margin-right:20px; margin-top:15px;">
                <div class="button_wrapper">
                    <asp:Button CssClass="buttons" ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click" />
                </div>
            </div>--%>


            <div class="float_left" style="margin-top: 5px; text-align: justify; margin-right: 20px;">
                <span style="color: Red;"></span>
                <ul style="list-style-type: none; font-weight: normal; font-size: 14px; margin-left: -40px; width: 500px;">
                    <li>You certify that all information is complete and accurate and that you are the authorized signer of the credit card listed.</li>
                    <br />
                    <%--<li>The group will be liable for any non-refundable deposit or cancellation penalty paid by <span id="cn1" runat="server"></span> due to service or Group cancellation. <span id="cn" runat="server"></span> will remit deposits or cancel services ONLY upon written authorization of the Group Representative.</li><br />
                <li>All ground services require prepayment 30 days prior group's arrival. <span id="cn2" runat="server"></span> will advise the Group Representative about any specific requirement and will confirm only upon written authorization of the Group Representative.</li><br />
                <li>Any remainder of Ground Services originally booked along with any other service not included in the original proposal and confirmed by the Group Representative will be due to <span id="cn3" runat="server"></span> no later than 30 days from the end of the conference and will be charged on the Credit Card herein provided.</li>--%>
                </ul>
                <div class="clear_float"></div>
                <div class="float_left" style="margin-right: 100px; margin-top: 5px;">
                    <div class="float_left">
                        <input type="checkbox" id="cbAgree" onchange=" if(this.checked) { $('#btnOK').removeAttr('disabled'); $('#btnOK').fadeTo('fast', 1); } else { $('#btnOK').attr('disabled', 'disabled'); $('#btnOK').fadeTo('fast', 0.5); }" />
                    </div>
                    <div class="float_left" style="margin-top: 3px; margin-left: 3px;">
                        <asp:Label ID="lblAgree" runat="server">I have read and agree to these terms, conditions and the <a runat="server" id="linkPrivacyPolicy" href="#" onclick="">Privacy Policy</a></asp:Label>
                    </div>
                </div>
                <div class="float_left" style="margin-right: 500px; margin-top: 15px;">
                    <div class="button_wrapper1 vlajko_Wrapper">
                        <asp:Button CssClass="buttons1 vlajko" ID="btnOK" runat="server" Text="Submit" OnClick="btnOK_Click" OnClientClick="if(Validate(this.form)) { var r = confirm('Your card will be charged: ' + $('#tbPrice').val() + '\nPlease press OK to confirm payment.'); if(r==true) { return true; } else { return false; } } else { return false; }" Style="background-color: #55A7D1;" />
                    </div>
                </div>
                <div class="clear_float"></div>
                <div class="float_right" style="margin-right: 15px; margin-top: -75px;">
                    <img src="https://seal.networksolutions.com/images/basicsqgreen.gif" />
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hfCMS" runat="server" />
        <asp:HiddenField ID="hfPreparer" runat="server" />
    </form>
</body>
</html>
