﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using FirstDataGlobalGatewayE4;
using System.Xml.Linq;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for EmailHelper
/// </summary>
public class EmailHelper
{
    public static bool SendClientEmail(string cardholderName, string eventName, string eMail, string invoiceID, string proposalID, string dcc = "", string dba = "")
    {
        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

        msg.To.Add(eMail);
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment Authorization Submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment Authorization Submitted";

        msg.IsBodyHtml = true;

        if(!string.IsNullOrEmpty(dba))
        {
            msg.Body = dba + "<br/>";
            msg.Body += "1666 KENNEDY CAUSEWY 702 <br/>";
            msg.Body += "NORTH BAY VILLAGE, FL 33141 <br/>";
            msg.Body += "United States <br/><br/>";
        }

        msg.Body += "Dear " + cardholderName + "," + "<br/><br/>Payment authorization was successfully submitted.<br/><br/>";
        msg.Body += "Payment details: <br/><br/> InvoiceID: " + invoiceID + " <br/> ProposalID: " + proposalID + "<br/> Email: " + eMail;

        if (dcc.Length > 0)
            msg.Body += "<br/><br/>Billing in local currency requested.<br/><br/>";

        msg.Body += "<br/><br/>No reply on this email"; 
        
        SmtpClient client = new SmtpClient(DatabaseConnection.GetSMTPServer());
        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //client.UseDefaultCredentials = false;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());          
        client.EnableSsl = true;
        client.Port = 587;
		//client.Port = 25;


        try
        {
            //client.Send(msg);            
            return true;
        }
        catch (Exception ex)
        {
            ((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);            
            return false;
        }
    }

    public static bool SendUserPaymentNotification(string cardholderName, string eventName, string eMail, string invoiceID, string proposalID, string CSM, string preparer, string creditCard, string descritpionOfServices, string dcc = "", string transactionStatus = "", string transactionResponse = "")
    {
        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

        msg.To.Add(CSM);
        msg.CC.Add(DatabaseConnection.GetPaymentAuthorizationEmail());
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment Authorization Submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment Authorization Submitted" + "(TOKENIZATION ENABLED)";

        msg.IsBodyHtml = true;
		
		if(transactionResponse.Length > 0)
            msg.Attachments.Add(new Attachment(transactionResponse));

        msg.Body = "User " + cardholderName + " submitted authorization payment" + ", <br/><br/> User payment details: <br/><br/> InvoiceID: " + invoiceID + " <br/> ProposalID: " + proposalID + "<br/> Email: " + eMail + "<br /><br /><br />Preparer: " + preparer + "<br /><br /><br />Credit card number: " + creditCard;
		
		msg.Body += "<br/><br/><br/>Description of services: " + descritpionOfServices;

        if (dcc.Length > 0)
            msg.Body += "<br/><br/>Billing in local currency requested.<br /><br />";
		
		if (transactionStatus.Length > 0)
            msg.Body += "<br/><br/>Transaction status " + transactionStatus + "<br/><br/>";

        SmtpClient client = new SmtpClient(DatabaseConnection.GetSMTPServer());        
        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //client.UseDefaultCredentials = false;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;
		//client.Port = 25;


        try
        {
            //client.Send(msg);            
            return true;
        }
        catch (Exception ex)
        {
            ((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);           
            return false;
        }

    }

    public static bool SendClientEmailFirstData(string cardholderName, string eventName, string eMail, string invoiceID, string proposalID, string descritpionOfServices, string totalCharge, string cardNumber, string transactionID, string dba = "")
    {
        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

        msg.To.Add(eMail);
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment successfully submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment successfully submitted";

        msg.IsBodyHtml = true;

        if (!string.IsNullOrEmpty(dba))
        {
            msg.Body = dba + "<br/>";
            msg.Body += "1666 KENNEDY CAUSEWY 702 <br/>";
            msg.Body += "NORTH BAY VILLAGE, FL 33141 <br/>";
            msg.Body += "United States <br/><br/>";
        }

        msg.Body += "Dear " + cardholderName + "," + "<br/><br/>Your payment was successfully submitted.<br/><br/>";
        msg.Body += "<br/><br/> Event: " + eventName + "<br/> TransactionID: " + transactionID + " <br/> Website Booking ID: " + invoiceID + " <br/> Proposal: " + proposalID + "<br/> Email: " + eMail + "<br /> Description of services: " + descritpionOfServices + "<br/><br/>";
        msg.Body += "Name on card: " + cardholderName + "<br/>Total charged today: " + totalCharge + "<br/> Card Number: " + cardNumber;
        msg.Body += "<br/><br/>Do not reply to this email";

        SmtpClient client = new SmtpClient(DatabaseConnection.GetSMTPServer());
        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //client.UseDefaultCredentials = false;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());          
        client.EnableSsl = true;
        client.Port = 587;
		//client.Port = 25;


        try
        {
            client.Send(msg);
            return true;
        }
        catch (Exception ex)
        {
            ((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);
            return false;
        }
    }

    public static bool SendUserPaymentNotificationFirstData(string cardholderName, string eventName, string eMail, string invoiceID, string proposalID, string CSM, string preparer, string creditCard, string descritpionOfServices, string totalCharge, string transactionID)
    {
        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

        //msg.To.Add(eMail);
        msg.To.Add("nnapolitano@abtscs.com");
        msg.CC.Add(DatabaseConnection.GetPaymentAuthorizationEmail());
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment successfully submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment successfully submitted" + "(TOKENIZATION ENABLED)";

        msg.IsBodyHtml = true;

        msg.Body = "Card was successfully charged." + " <br/><br/> Payment details: <br/><br/> Event: " + eventName + "<br/> TransactionID: " + transactionID + "<br/> Website Booking ID: " + invoiceID + " <br/> Proposal: " + proposalID + " <br/> Prepared by: " + preparer + "<br/> Email: " + eMail + "<br /> Description of services: " + descritpionOfServices + "<br /><br />";
        msg.Body += "Name on card: " + cardholderName + "<br/>Total charged today: " + totalCharge + "<br/> Card Number: " + creditCard;
        msg.Body += "<br/><br/>Do not reply to this email";
        SmtpClient client = new SmtpClient(DatabaseConnection.GetSMTPServer());
        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //client.UseDefaultCredentials = false;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;
		//client.Port = 25;


        try
        {
            client.Send(msg);
            return true;
        }
        catch (Exception ex)
        {
            ((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);
            return false;
        }

    }

    public static bool SendUserPaymentDetailsFirstData(TransactionResult result)
    {
        var msg = new System.Net.Mail.MailMessage();
        
        msg.CC.Add(DatabaseConnection.GetPaymentAuthorizationEmail());
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment submitted" + "(TOKENIZATION ENABLED)";
        msg.IsBodyHtml = true;

        msg.Body = result.MerchantName + "<br/>";
        msg.Body += result.MerchantAddress + "<br/>";
        msg.Body += result.MerchantCountry + "," + result.MerchantCity + "," + result.MerchantProvince + "," + result.MerchantPostal  + "<br/>";
        msg.Body += result.MerchantURL + "<br/><br/><br/>";

        msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        msg.Body += "Amount :" + result.DollarAmount + "<br/><br/><br/>";

        msg.Body += "Card Type:  " + result.CardType + "<br/>";
        msg.Body += "Card number:" + "XXXXXXXXXXXX-" + result.TransarmorToken.Substring(result.TransarmorToken.Length - 4) + "<br/>";
        msg.Body += "Card Holder :" + result.CardHoldersName + "<br/><br/>";
        
        msg.Body += "Customer reference :" + result.Customer_Ref + "<br/>";
        msg.Body += "Reference #3 :" + result.Reference_3 + "<br/>";
        msg.Body += "Reference number :" + result.Reference_No + "<br/><br/>";
       
        msg.Body += "Transaction Error :" + result.Transaction_Error + "<br/>";
        msg.Body += "Transaction ID :" + result.Transaction_Tag + "<br/>";
        msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        msg.Body += "Transaction Approved :" + result.Transaction_Approved + "<br/><br/>";

        msg.Body += "Bank Message :" + result.Bank_Message + "<br/>";
        msg.Body += "Bank Response Code :" + result.Bank_Resp_Code + "<br/>";
        msg.Body += "EXact Message :" + result.EXact_Message + "<br/>";
        msg.Body += "EXact Response Code :" + result.EXact_Resp_Code + "<br/>";
        //msg.Body += "Error Description :" + result.Error_Description + "<br/>";
        //msg.Body += "Error Number :" + result.Error_Number + "<br/>";

        msg.Body += "<br/><br/>Do not reply to this email";
        var client = new SmtpClient(DatabaseConnection.GetSMTPServer());

        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //client.UseDefaultCredentials = false;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;
		//client.Port = 25;


        try
        {
            client.Send(msg);
            return true;
        }
        catch (Exception ex)
        {
            //((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);
            return false;
        }

    }
	
	public static bool SendUserPaymentDetailsFirstDataChargeAdmin(TransactionResult result, string guestName = "", string websiteBookingID = "", string code = "")
    {
        var msg = new System.Net.Mail.MailMessage();
        
        msg.CC.Add(DatabaseConnection.GetPaymentAuthorizationEmail());
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment submitted" + "(TOKENIZATION ENABLED)";

        msg.IsBodyHtml = true;

        msg.Body = result.Bank_Message + "<br/>";
        msg.Body += "========== TRANSACTION RECORD ==========" + "<br/>";
        msg.Body += result.MerchantName + "<br/>";
        msg.Body += result.MerchantAddress + "<br/>";
        msg.Body += result.MerchantCity + "," + result.MerchantProvince + "," + result.MerchantPostal + "<br/>";
        msg.Body += result.MerchantCountry + "<br/>";
        msg.Body += result.MerchantURL + "<br/><br/>";

        msg.Body += "TYPE: Purchase<br/><br/>";

        msg.Body += "ACCT: " + result.CardType + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "$ " + result.DollarAmount + " " + result.Currency + "<br/><br/><br/>";

        //msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        //msg.Body += "Amount :" + result.DollarAmount + "<br/><br/><br/>";
        //msg.Body += "Card Type:  " + result.CardType + "<br/>";

        if (guestName.Length > 0)
            msg.Body += "GUEST NAME:" + guestName + "<br/>";

        msg.Body += "CARDHOLDER NAME:" + result.CardHoldersName + "<br/>";
        msg.Body += "CARD NUMBER:" + "############" + result.TransarmorToken.Substring(result.TransarmorToken.Length - 4) + "<br/>";
        msg.Body += "DATE/TIME:" + System.DateTime.Now.ToString("dd MMMM yy H:mm:ss") + "<br/>";
        msg.Body += "REFERENCE #:" + result.Retrieval_Ref_No + "<br/>";
        msg.Body += "AUTHOR. #:" + result.Authorization_Num + "<br/>";

        msg.Body += "CUSTOMER REFERENCE #:" + result.Customer_Ref + "<br/>";

        //if (result.Reference_3.Length > 0)
        //    msg.Body += "REFERENCE #3:" + result.Reference_3 + "<br/>";

        msg.Body += "REFERENCE NUMBER #:" + result.Reference_No + "<br/><br/>";
		msg.Body += "TRANSACTION ID #:" + result.Transaction_Tag + "<br/>";

		if (websiteBookingID.Length > 0)
            msg.Body += "WEBSITE BOOKING ID:" + websiteBookingID + "<br/><br/>";

        if (code.Length > 0)
            msg.Body += "CODE:" + code + "<br/><br/>";
		
        //msg.Body += "Transaction Error:" + result.Transaction_Error + "<br/>";
        //msg.Body += "Transaction ID :" + result.Transaction_Tag + "<br/>";
        //msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        //msg.Body += "Transaction Approved :" + result.Transaction_Approved + "<br/><br/>";

        //msg.Body += "Bank Message :" + result.Bank_Message + "<br/>";
        //msg.Body += "Bank Response Code :" + result.Bank_Resp_Code + "<br/>";
        //msg.Body += "EXact Message :" + result.EXact_Message + "<br/>";
        //msg.Body += "EXact Response Code :" + result.EXact_Resp_Code + "<br/>";
        //msg.Body += "Error Description :" + result.Error_Description + "<br/>";
        //msg.Body += "Error Number :" + result.Error_Number + "<br/>";

        msg.Body += result.Bank_Message + "-" + " Thank You " + result.Bank_Resp_Code + "<br/><br/>";

        //msg.Body += "<br/><br/>Do not reply to this email<br/><br/>";

        msg.Body += "Please retain this copy for your records<br/><br/>";
        msg.Body += "Cardholder will pay above amount to<br/>card issuer pursuant to cardholder<br/>agreement.<br/>";
        msg.Body += "========================================";

        //msg.Body = result.MerchantName + "<br/>";
        //msg.Body += result.MerchantAddress + "<br/>";
        //msg.Body += result.MerchantCountry + "," + result.MerchantCity + "," + result.MerchantProvince + "," + result.MerchantPostal  + "<br/>";
        //msg.Body += result.MerchantURL + "<br/><br/><br/>";

        //msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        //msg.Body += "Amount : $" + result.DollarAmount + " " + result.Currency + "<br/><br/>";

        //msg.Body += "Card Type:  " + result.CardType + "<br/>";
        //msg.Body += "Card number:" + "XXXXXXXXXXXX-" + result.TransarmorToken.Substring(result.TransarmorToken.Length - 4) + "<br/>";
        //msg.Body += "Card Holder :" + result.CardHoldersName + "<br/><br/>";

        //msg.Body += "Customer reference :" + result.Customer_Ref + "<br/>";
        //msg.Body += "Reference #3 :" + result.Reference_3 + "<br/>";
        //msg.Body += "Reference number :" + result.Reference_No + "<br/><br/>";

        //msg.Body += "Transaction Error :" + result.Transaction_Error + "<br/>";
        //msg.Body += "Transaction ID :" + result.Transaction_Tag + "<br/>";
        //msg.Body += "Transaction Type :" + result.Transaction_Type + "<br/>";
        //msg.Body += "Transaction Approved :" + result.Transaction_Approved + "<br/><br/>";

        //msg.Body += "Bank Message :" + result.Bank_Message + "<br/>";
        //msg.Body += "Bank Response Code :" + result.Bank_Resp_Code + "<br/>";
        //msg.Body += "EXact Message :" + result.EXact_Message + "<br/>";
        //msg.Body += "EXact Response Code :" + result.EXact_Resp_Code + "<br/>";
        ////msg.Body += "Error Description :" + result.Error_Description + "<br/>";
        ////msg.Body += "Error Number :" + result.Error_Number + "<br/>";
        //msg.Body += "<br/><br/>Do not reply to this email";

        var client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
        
        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;


        try
        {
            client.Send(msg);
            return true;
        }
        catch (Exception ex)
        {
            //((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);
            return false;
        }

    }

    public static string SendInternetSecureClientEmailSuccess(string useremail, string firstname, string lastname, string transactionID, string amount, string creditCard, string emailFrom, string emailTo, string msgSubject, string msgDisplayName, string userID)
    {
        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

        msg.To.Add(emailTo);
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress(emailFrom, msgDisplayName, System.Text.Encoding.UTF8);
        msg.Subject = msgSubject;

        msg.IsBodyHtml = true;
        msg.BodyEncoding = System.Text.Encoding.UTF8;
        msg.Body += "User " + firstname + " " + lastname + " (" + useremail + ") paid via InternetSecure payment. <br/><br/>Transaction ID: " + transactionID + "<br/ >Estimated Remaining Balance Amount: " + amount + " USD<br/>";
        msg.Body += "RegistrationID: " + userID;
        msg.Body += "<br/>Credit card: ############-" + creditCard.Substring(creditCard.Length - 4);
        System.Net.Mail.MailMessage msgTeam = new System.Net.Mail.MailMessage();

        SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;


        try
        {
            client.Send(msg);
            return "true";
        }
        catch (Exception ex)
        {
            return "false";
        }

    } 

    public static string SendInternetSecureRegistrantEmailSuccess(string useremail, string firstname, string lastname, string transactionID, string amount, string creditCard, string emailFrom, string emailTo, string msgSubject, string msgDisplayName, string msgFooter)
    {

        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

        msg.To.Add(useremail);
        msg.CC.Add(emailTo);
        msg.CC.Add("sysadmin@abtscs.com");


        msg.From = new MailAddress(emailFrom, msgDisplayName, System.Text.Encoding.UTF8);
        msg.Subject = msgSubject;

        msg.IsBodyHtml = true;
        msg.BodyEncoding = System.Text.Encoding.UTF8;
        msg.Body += "Dear " + firstname + " " + lastname + ",<br /><br />Your payment is now finalized.<br /><br />Transaction ID: " + transactionID + "<br/ >Estimated Remaining Balance Amount: " + amount + " USD<br/>Credit card: ############-" + creditCard.Substring(creditCard.Length - 4) + "<br /><br />";
        msg.Body += msgFooter;

        System.Net.Mail.MailMessage msgTeam = new System.Net.Mail.MailMessage();

        SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;


        try
        {
            client.Send(msg);
            return "true";
        }
        catch (Exception ex)
        {
            return "false";
        }
    }

    public static bool SendUserPaymentDetailsFirstDataDcc(TransactionResult result)
    {
        var msg = new System.Net.Mail.MailMessage();

        msg.CC.Add(DatabaseConnection.GetPaymentAuthorizationEmail());
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment submitted";

        msg.IsBodyHtml = false;

        msg.Body = result.CTR;
         
        //msg.Body = result.Bank_Message + "<br/>";
        //msg.Body += "========== TRANSACTION RECORD ==========" + "<br/>";
        //msg.Body += result.MerchantName + "<br/>";
        //msg.Body += result.MerchantAddress + "<br/>";
        //msg.Body += result.MerchantCity + "," + result.MerchantProvince + "," + result.MerchantPostal + "<br/>";
        //msg.Body += result.MerchantCountry + "<br/>";
        //msg.Body += result.MerchantURL + "<br/><br/>";

        //msg.Body += "TYPE: Purchase<br/><br/>";

        //msg.Body += "ACCT: " + result.CardType + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "BASE AMOUNT " + "$ " + result.DollarAmount + " " + result.Currency + "<br/>";
        //msg.Body += "FOREIGN AMOUNT " +  result.ForeignCurrencyDetails.ForeignAmount + "";
      
        //msg.Body += "CARDHOLDER NAME:" + result.CardHoldersName + "<br/>";
        //msg.Body += "CARD NUMBER:" + "############" + result.TransarmorToken.Substring(result.TransarmorToken.Length - 4) + "<br/>";
        //msg.Body += "DATE/TIME:" + System.DateTime.Now.ToString("dd MMMM yy H:mm:ss") + "<br/>";
        //msg.Body += "REFERENCE #:" + result.Retrieval_Ref_No + "<br/>";
        //msg.Body += "AUTHOR. #:" + result.Authorization_Num + "<br/>";
        //msg.Body += "CUSTOMER REFERENCE #:" + result.Customer_Ref + "<br/>";
        //msg.Body += "REFERENCE NUMBER #:" + result.Reference_No + "<br/><br/>";

        //msg.Body += result.Bank_Message + "-" + " Thank You " + result.Bank_Resp_Code + "<br/><br/>";

        //msg.Body += "Please retain this copy for your records<br/><br/>";
        //msg.Body += "Cardholder will pay above amount to<br/>card issuer pursuant to cardholder<br/>agreement.<br/>";
        //msg.Body += "========================================";

        SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;


        try
        {
            client.Send(msg);
            return true;
        }
        catch (Exception ex)
        {            
            return false;
        }

    }

    public static string SendPayPalClientEmailSuccess(string useremail, string firstname, string lastname, string transactionID, string amount, string creditCard)
    {
        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

		msg.To.Add("sysadmin@abtscs.com");
        //msg.To.Add("info@CardiovascularSurgeryConference.org");
        //msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("website@CardiovascularSurgeryConference.org", "STS/EACTS Latin America Cardiovascular Surgery Conference", System.Text.Encoding.UTF8);
        msg.Subject = "Live Testing STS/EACTS Latin America Cardiovascular Surgery Conference: Payment Successful";

        msg.IsBodyHtml = true;
        msg.BodyEncoding = System.Text.Encoding.UTF8;
        msg.Body += "User " + firstname + " " + lastname + " (" + useremail + ") paid via Paypal payment. <br/><br/>Transaction ID: " + transactionID + "<br/ >Estimated Remaining Balance Amount: " + amount + " USD";
        msg.Body += "<br/>Credit card: ############-" + creditCard.Substring(creditCard.Length - 4);
        System.Net.Mail.MailMessage msgTeam = new System.Net.Mail.MailMessage();

        SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;


        try
        {
            //client.Send(msg);
            return "true";
        }
        catch (Exception ex)
        {
            return "false";
        }

    }


    public static string SendPayPalRegistrantEmailSuccess(string useremail, string firstname, string lastname, string transactionID, string amount, string creditCard)
    {

        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

        msg.To.Add(useremail);
        //msg.CC.Add("info@CardiovascularSurgeryConference.org");
        msg.CC.Add("sysadmin@abtscs.com");


        msg.From = new MailAddress("website@CardiovascularSurgeryConference.org", "STS/EACTS Latin America Cardiovascular Surgery Conference", System.Text.Encoding.UTF8);
        msg.Subject = "Live Testing STS/EACTS Latin America Cardiovascular Surgery Conference: Estimated Remaining Balance Payment Successful";

        msg.IsBodyHtml = true;
        msg.BodyEncoding = System.Text.Encoding.UTF8;
        msg.Body += "Dear " + firstname + " " + lastname + ",<br /><br />Your payment is now finalized.<br /><br />Transaction ID: " + transactionID + "<br/ >Estimated Remaining Balance Amount: " + amount + " USD<br/>Credit card: ############-" + creditCard.Substring(creditCard.Length - 4) + "<br /><br /> For all other questions or comments, we invite you to email us directly at <a href='mailto:info@CardiovascularSurgeryConference.org'>info@CardiovascularSurgeryConference.org</a>.<br/><br/>We look forward to seeing you in Cartagena!<br /><br />Best regards,<br />STS/EACTS Latin America Cardiovascular Surgery Conference<br /><br />";

        System.Net.Mail.MailMessage msgTeam = new System.Net.Mail.MailMessage();

        SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;


        try
        {
            //client.Send(msg);
            return "true";
        }
        catch (Exception ex)
        {
            return "false";
        }
    }

    public static bool SendUserPaymentDetailsPlanetChargeAdmin(IEnumerable<XElement> responseData, Planet.Currency defaultcurrency, string cardType, string guestName = "", string websiteBookingID = "", string code = "", string Reference_No = "", string Reference_3 = "", string Customer_Ref = "", string responseMessage = "", Merchant merchant = Merchant.IgdInternationlGroup_PP)
    {
        string currency = responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "CURRENCY_CODE") == null ? defaultcurrency.ToString() : Enum.GetName(typeof(Planet.Currency), int.Parse(responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "CURRENCY_CODE").Value));

        var msg = new System.Net.Mail.MailMessage();

        
        msg.CC.Add(DatabaseConnection.GetPaymentAuthorizationEmail());
        msg.CC.Add("sysadmin@abtscs.com");

        msg.From = new MailAddress("info@internationalgroupsdepartment.com", "Payment submitted", System.Text.Encoding.UTF8);
        msg.Subject = "Payment submitted" + "(TOKENIZATION ENABLED)";

        msg.IsBodyHtml = true;
		
		if (responseMessage.Length > 0)
            msg.Attachments.Add(new Attachment(responseMessage));

        msg.Body = responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value + "<br/>";
        msg.Body += "========== TRANSACTION RECORD ==========" + "<br/>";
        msg.Body += Regex.Replace(((Merchant)merchant).ToString().Replace("_", ""), "([a-z])([A-Z])", "$1 $2").ToUpper() + "<br/>";
        msg.Body += DatabaseConnection.GetMerchantAddress() + "<br/>";
        msg.Body += DatabaseConnection.GetMerchantCity() + "," + DatabaseConnection.GetMerchantProvance() + "," + DatabaseConnection.GetMerchantPostal() + "<br/>";
        msg.Body += DatabaseConnection.GetMerchantCountry() + "<br/>";
        msg.Body += DatabaseConnection.GetMerchantURL() + "<br/><br/>";

        msg.Body += "TYPE: Purchase<br/><br/>";

        if (responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value != null && responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value != "1")
        {
            decimal amountInDefaultCurrency = Decimal.Parse(responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "AMOUNT").Value);
            decimal foreignCurrencyRate = Decimal.Parse(responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value);

            msg.Body += "ACCT: " + cardType + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Math.Round(amountInDefaultCurrency * foreignCurrencyRate, 2) + " " + DatabaseConnection.GetDefaultCurrency() + "<br/>";
            msg.Body += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                         responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "AMOUNT").Value + " " + currency + "<br/><br/><br/>";
            msg.Body += "Payment " + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "AMOUNT").Value + " " + currency + "<br/><br/><br/>";
        }
        else
        {
            msg.Body += "ACCT: " + cardType + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "AMOUNT").Value + " " + currency + "<br/><br/><br/>";
        }

        if (guestName != null && guestName.Length > 0)
            msg.Body += "GUEST NAME:" + guestName + "<br/>";

        msg.Body += "CARDHOLDER NAME:" + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "FIRST_NAME").Value + " " + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "LAST_NAME").Value + "<br/>";
        msg.Body += "CARD NUMBER:" + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "ACCOUNT_NUMBER").Value + "<br/>";
        msg.Body += "DATE/TIME:" + System.DateTime.Now.ToString("dd MMMM yy H:mm:ss") + "<br/>";
        msg.Body += "REFERENCE #:" + Reference_No + "<br/>";

        msg.Body += "CUSTOMER REFERENCE #:" + Customer_Ref + "<br/>";

        if (Reference_3.Length > 0)
            msg.Body += "REFERENCE #3:" + Reference_3 + "<br/>";

        //msg.Body += "REFERENCE NUMBER #:" + result.Reference_No + "<br/><br/>";
        msg.Body += "TRANSACTION ID #:" + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "TRANSACTION_ID").Value + "<br/>";

        if (websiteBookingID.Length > 0)
            msg.Body += "WEBSITE BOOKING ID:" + websiteBookingID + "<br/><br/>";

        if (code.Length > 0)
            msg.Body += "CODE:" + code + "<br/><br/>";

        msg.Body += responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "RESPONSE_TEXT").Value + "-" + " Thank You " + "<br/><br/>";

        //msg.Body += "<br/><br/>Do not reply to this email<br/><br/>";

        if (responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value != null && responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value != "1")
        {
            msg.Body += @"Currency Notes:<br/>
                         * Exchange Rate: 1 USD = " + responseData.FirstOrDefault(a => a.Attribute("KEY").Value == "EXCHANGE_RATE").Value + " " + currency + @"<br/>
                         * Rate includes 3.5 % margin<br/>
                         * Please debit my account for the total<br/>
                         amount of this Transaction in the<br/>
                         Transaction Currency shown above.<br/>
                         I acknowledge that I had a choice to pay<br/>
                         in USD and my currency choice is final.<br/>
                         Currency conversion is conducted by the<br/>
                         merchant. [X] Accepted.<br/><br/>";
        }

        msg.Body += "Please retain this copy for your records<br/><br/>";
        msg.Body += "Cardholder will pay above amount to<br/>card issuer pursuant to cardholder<br/>agreement.<br/>";
        msg.Body += "========================================";

        var client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());

        //client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPServerUserName"].ToString(), ConfigurationManager.AppSettings["SMTPServerPassword"].ToString());
        client.EnableSsl = true;
        client.Port = 587;


        try
        {
            client.Send(msg);
            return true;
        }
        catch (Exception ex)
        {
            //((Page)HttpContext.Current.Handler).ClientScript.RegisterStartupScript(((Page)HttpContext.Current.Handler).GetType(), "333", "alert('" + ex.Message + "');", true);
            return false;
        }
    }

    public EmailHelper()
    {
	//
	// TODO: Add constructor logic here
	//
    }
}