﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for PaymentAuthorizationExpanded
/// </summary>
[Serializable]
[XmlRoot(ElementName="PaymentAuthorizationExpanded")]
public class PaymentAuthorizationExpanded
{
    private string conferenceID;
    private string proposalID;
    private string invoiceID;
    private string eventName;
    private string email;
    private int cardTypeID;
    private string creditCardNumber;  
    private DateTime exparationDate;
    private string cardholderName;
    private string preparer;
    private string billingAddress;
    private string phone;
    private string city;
    private string zip;
    private string country;   
    private string state;
    private string paymentInstructions;
    private DateTime timestamp;
    private string descriptionOfServices;
    private string token;
    private string cardType;
    private bool dcc;
    private string rateResponseId;
    private string exchangeRate;
    private string foreignCurrencySymbolHtml;
    private int merchant;

    [XmlAttribute(AttributeName="ConferenceID")]
    public string ConferenceID
    {
        get
        {
            return conferenceID;
        }

        set
        {
            conferenceID = value;
        }        
    }

    [XmlAttribute(AttributeName="ProposalID")]
    public string ProposalID
    {
        get
        {
            return proposalID;
        }

        set
        {
            proposalID = value;
        }
    }

    [XmlAttribute(AttributeName="InvoiceID")]
    public string InvoiceID
    {
        get
        {
            return invoiceID;
        }

        set
        {
            invoiceID = value;
        }
    }

    [XmlAttribute(AttributeName = "EventName")]
    public string EventName
    {
        get
        {
            return eventName;
        }

        set
        {
            eventName = value;
        }
    }

    [XmlAttribute(AttributeName = "Email")]
    public string Email
    {
        get
        {
            return email;
        }

        set
        {
            email = value;
        }
    }

    [XmlElement(ElementName="CardTypeID")]
    public int CardTypeID
    {
        get
        {
            return cardTypeID;
        }

        set
        {
            cardTypeID = value;
        }
    }

    [XmlElement(ElementName = "CreditCardNumber")]
    public string CreditCardNumber
    {
        get
        {
            return creditCardNumber;
        }

        set
        {
            creditCardNumber = value;
        }
    }
   
    [XmlElement(ElementName = "ExparationDate")]
    public DateTime ExparationDate
    {
        get
        {
            return exparationDate;
        }

        set
        {
            exparationDate = value;
        }
    }

    [XmlElement(ElementName = "CardholderName")]
    public string CardholderName
    {
        get
        {
            return cardholderName;
        }

        set
        {
            cardholderName = value;
        }
    }

    [XmlElement(ElementName = "Preparer")]
    public string Preparer
    {
        get
        {
            return preparer;
        }

        set
        {
            preparer = value;
        }
    }

    [XmlAttribute(AttributeName = "BillingAddress")]
    public string BillingAddress
    {
        get
        {
            return billingAddress;
        }

        set
        {
            billingAddress = value;
        }
    }

    [XmlAttribute(AttributeName = "Phone")]
    public string Phone
    {
        get
        {
            return phone;
        }

        set
        {
            phone = value;
        }
    }

    [XmlAttribute(AttributeName = "City")]
    public string City
    {
        get
        {
            return city;
        }

        set
        {
            city = value;
        }
    }

    [XmlAttribute(AttributeName = "Zip")]
    public string Zip
    {
        get
        {
            return zip;
        }

        set
        {
            zip = value;
        }
    }

    [XmlAttribute(AttributeName = "Country")]
    public string Country
    {
        get
        {
            return country;
        }

        set
        {
            country = value;
        }
    }
    
    [XmlAttribute(AttributeName = "State")]
    public string State
    {
        get
        {
            return state;
        }

        set
        {
            state = value;
        }
    }

    [XmlAttribute(AttributeName = "PaymentInstructions")]
    public string PaymentInstructions
    {
        get
        {
            return paymentInstructions;
        }

        set
        {
            paymentInstructions = value;
        }
    }

    [XmlAttribute(AttributeName = "TimeStamp")]
    public DateTime TimeStamp
    {
        get
        {
            return timestamp;
        }

        set
        {
            timestamp = value;
        }
    }

    [XmlElement(ElementName = "DescriptionOfServices")]
    public string DescriptionOfServices
    {
        get
        {
            return descriptionOfServices;
        }

        set
        {
            descriptionOfServices = value;
        }
    }

    [XmlElement(ElementName = "Token")]
    public string Token
    {
        get
        {
            return token;
        }

        set
        {
            token = value;
        }
    }

    [XmlElement(ElementName = "CardType")]
    public string CardType
    {
        get
        {
            return cardType;
        }

        set
        {
            cardType = value;
        }
    }

    [XmlElement(ElementName = "DCC")]
    public bool DCC
    {
        get
        {
            return dcc;
        }

        set
        {
            dcc = value;
        }
    }

    [XmlAttribute(AttributeName = "RateResponseID")]
    public string RateResponseID
    {
        get
        {
            return rateResponseId;
        }

        set
        {
            rateResponseId = value;
        }
    }

    [XmlAttribute(AttributeName = "ExchangeRate")]
    public string ExchangeRate
    {
        get
        {
            return exchangeRate;
        }

        set
        {
            exchangeRate = value;
        }
    }

    [XmlAttribute(AttributeName = "ForeignCurrencySymbolHtml")]
    public string ForeignCurrencySymbolHtml
    {
        get
        {
            return foreignCurrencySymbolHtml;
        }

        set
        {
            foreignCurrencySymbolHtml = value;
        }
    }

    [XmlAttribute(AttributeName = "Merchant")]
    public int Merchant
    {
        get
        {
            return merchant;
        }

        set
        {
            merchant = value;
        }
    }

    public PaymentAuthorizationExpanded()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}