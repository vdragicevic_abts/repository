﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for HousingAgreementHelper
/// </summary>
public class HousingAgreementHelper : DatabaseConnection
{
    public static bool InsertHousingAgreement(HousingRegistrationAgreement hra)
    {
        bool success = false;
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertHousingAgreement";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", hra.ConferenceID));
            cmd.Parameters.Add(new SqlParameter("@HaID", hra.HaID));
            cmd.Parameters.Add(new SqlParameter("@CardTypeID", hra.CardTypeID));
            cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", StringHelpers.Encrypt(hra.CreditCardNumber)));

            if (hra.CCV != null && hra.CCV.Length > 0)
            {
                cmd.Parameters.Add(new SqlParameter("@CCV", hra.CCV));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@CCV", DBNull.Value));
            }

            cmd.Parameters.Add(new SqlParameter("@ExparationDate", hra.ExparationDate));

            cmd.Parameters.Add(new SqlParameter("@UserID", hra.UserID));

            if (hra.CardholderName != null)
            {
                cmd.Parameters.Add(new SqlParameter("@CardholderName", hra.CardholderName));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@CardholderName", DBNull.Value));
            }
           
            try
            {
                cmd.ExecuteNonQuery();

                trans.Commit();

                conn.Close();

                success = true;
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
            }
        }

        return success;
    }

    public static bool InsertHousingAgreementExpanded(HousingRegistrationAgreementExpanded hra)
    {
        bool success = false;
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertHousingAgreementExpanded";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", hra.ConferenceID));
            cmd.Parameters.Add(new SqlParameter("@HaID", hra.HaID));
            cmd.Parameters.Add(new SqlParameter("@CardTypeID", hra.CardTypeID));
            cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", StringHelpers.Encrypt(hra.CreditCardNumber)));

            if (hra.CCV != null && hra.CCV.Length > 0)
            {
                cmd.Parameters.Add(new SqlParameter("@CCV", hra.CCV));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@CCV", DBNull.Value));
            }

            cmd.Parameters.Add(new SqlParameter("@ExparationDate", hra.ExparationDate));

            cmd.Parameters.Add(new SqlParameter("@UserID", hra.UserID));

            if (hra.CardholderName != null)
            {
                cmd.Parameters.Add(new SqlParameter("@CardholderName", hra.CardholderName));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@CardholderName", DBNull.Value));
            }

            if (hra.BillingAddress != null)
            {
                cmd.Parameters.Add(new SqlParameter("@BillingAddress", hra.BillingAddress));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@BillingAddress", DBNull.Value));
            }

            if (hra.Phone != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Phone", hra.Phone));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Phone", DBNull.Value));
            }

            if (hra.City != null)
            {
                cmd.Parameters.Add(new SqlParameter("@City", hra.City));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@City", DBNull.Value));
            }

            if (hra.Zip != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Zip", hra.Zip));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Zip", DBNull.Value));
            }

            if (hra.Country != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Country", hra.Country));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Country", DBNull.Value));
            }

            if (hra.Email != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Email", hra.Email));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Email", DBNull.Value));
            }

            if (hra.State != null)
            {
                cmd.Parameters.Add(new SqlParameter("@State", hra.State));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@State", DBNull.Value));
            }

            try
            {
                cmd.ExecuteNonQuery();

                trans.Commit();

                conn.Close();

                success = true;
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
            }
        }

        return success;
    }

    public static HousingRegistrationAgreement SelectHousingAgreement(string conferenceID, int haID, int userID)
    {        
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "dbo.SelectHousingAgreement";


        cmd.Parameters.Add(new SqlParameter("@ConferenceID", conferenceID));
        cmd.Parameters.Add(new SqlParameter("@HaID", haID));
        cmd.Parameters.Add(new SqlParameter("@UserID", userID));

        DataTable dtHousingAgreement = new DataTable();
        HousingRegistrationAgreement hra = new HousingRegistrationAgreement();

        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dtHousingAgreement);

            foreach (DataRow dr in dtHousingAgreement.Rows)
            {
                hra.ConferenceID = dr["conferenceID"].ToString();
                hra.CardTypeID = int.Parse(dr["cardTypeID"].ToString());
                hra.HaID = int.Parse(dr["haID"].ToString());
                hra.CreditCardNumber = dr["creditCardNumber"].ToString();
                hra.ExparationDate = DateTime.Parse(dr["exparationDate"].ToString());
                hra.UserID = int.Parse(dr["userID"].ToString());
                hra.CCV = dr["ccv"].ToString();
                hra.CardholderName = dr["cardholderName"].ToString();
            }
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }           
        }

        return hra;
    }           
    
    
    public HousingAgreementHelper()
    {
	//
	// TODO: Add constructor logic here
	//
    }
}