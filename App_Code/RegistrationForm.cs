﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for RegistrationForm
/// </summary>
[Serializable]
[XmlRoot(ElementName = "RegistrationForm")]
public class RegistrationForm
{
    private string conferenceID;
    private int rfID;
    private int cardTypeID;
    private string creditCardNumber;
    private string ccv;
    private DateTime exparationDate;
    private int userID;
    private string cardholderName;
    private string address;
    private string city;
    private string country;
    private string postal;
    private string phone;
    private string email;
    private string state;
    private string proposalInfo;

    [XmlAttribute(AttributeName="ConferenceID")]
    public string ConferenceID
    {
        get
        {
            return conferenceID;
        }

        set
        {
            conferenceID = value;
        }        
    }

    [XmlAttribute(AttributeName="RegistrationFormID")]
    public int RfID
    {
        get
        {
            return rfID;
        }

        set
        {
            rfID = value;
        }
    }

    [XmlElement(ElementName="CardTypeID")]
    public int CardTypeID
    {
        get
        {
            return cardTypeID;
        }

        set
        {
            cardTypeID = value;
        }
    }

    [XmlElement(ElementName = "CreditCardNumber")]
    public string CreditCardNumber
    {
        get
        {
            return creditCardNumber;
        }

        set
        {
            creditCardNumber = value;
        }
    }

    [XmlElement(ElementName = "CCV")]
    public string CCV
    {
        get
        {
            return ccv;
        }

        set
        {
            ccv = value;
        }
    }

    [XmlElement(ElementName = "ExparationDate")]
    public DateTime ExparationDate
    {
        get
        {
            return exparationDate;
        }

        set
        {
            exparationDate = value;
        }
    }

    [XmlAttribute(AttributeName="UserID")]
    public int UserID
    {
        get
        {
            return userID;
        }

        set
        {
            userID = value;
        }
    }

    [XmlAttribute(AttributeName = "CardholderName")]
    public string CardholderName
    {
        get
        {
            return cardholderName;
        }

        set
        {
            cardholderName = value;
        }
    }

   [XmlAttribute(AttributeName = "Address")]
    public string Address
    {
        get
        {
            return address;
        }

        set
        {
            address = value;
        }
    }

    [XmlAttribute(AttributeName = "City")]
    public string City
    {
        get
        {
            return city;
        }

        set
        {
            city = value;
        }
    }

    [XmlAttribute(AttributeName = "Country")]
    public string Country
    {
        get
        {
            return country;
        }

        set
        {
            country = value;
        }
    }

    [XmlAttribute(AttributeName = "Postal")]
    public string Postal
    {
        get
        {
            return postal;
        }

        set
        {
            postal = value;
        }
    }

    [XmlAttribute(AttributeName = "Phone")]
    public string Phone
    {
        get
        {
            return phone;
        }

        set
        {
            phone = value;
        }
    }

    [XmlAttribute(AttributeName = "Email")]
    public string Email
    {
        get
        {
            return email;
        }

        set
        {
            email = value;
        }
    }

    [XmlAttribute(AttributeName = "State")]
    public string State
    {
        get
        {
            return state;
        }

        set
        {
            state = value;
        }
    }

    [XmlAttribute(AttributeName = "ProposalInfo")]
    public string ProposalInfo
    {
        get
        {
            return proposalInfo;
        }

        set
        {
            proposalInfo = value;
        }
    }


    public RegistrationForm()
	{
        this.ccv = String.Empty;		
	}
}