﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for HousingRegistrationAgreementExpanded
/// </summary>
[Serializable]
[XmlRoot(ElementName="HousingAgreement")]
public class HousingRegistrationAgreementExpanded
{
    private string conferenceID;
    private int haID;
    private int cardTypeID;
    private string creditCardNumber;
    private string ccv;
    private DateTime exparationDate;
    private int userID;
    private string cardholderName;
    private string billingAddress;
    private string phone;
    private string city;
    private string zip;
    private string country;
    private string email;
    private string state;

    [XmlAttribute(AttributeName="ConferenceID")]
    public string ConferenceID
    {
        get
        {
            return conferenceID;
        }

        set
        {
            conferenceID = value;
        }        
    }

    [XmlAttribute(AttributeName="HousingAgreementID")]
    public int HaID
    {
        get
        {
            return haID;
        }

        set
        {
            haID = value;
        }
    }

    [XmlElement(ElementName="CardTypeID")]
    public int CardTypeID
    {
        get
        {
            return cardTypeID;
        }

        set
        {
            cardTypeID = value;
        }
    }

    [XmlElement(ElementName = "CreditCardNumber")]
    public string CreditCardNumber
    {
        get
        {
            return creditCardNumber;
        }

        set
        {
            creditCardNumber = value;
        }
    }

    [XmlElement(ElementName = "CCV")]
    public string CCV
    {
        get
        {
            return ccv;
        }

        set
        {
            ccv = value;
        }
    }

    [XmlElement(ElementName = "ExparationDate")]
    public DateTime ExparationDate
    {
        get
        {
            return exparationDate;
        }

        set
        {
            exparationDate = value;
        }
    }

    [XmlAttribute(AttributeName="UserID")]
    public int UserID
    {
        get
        {
            return userID;
        }

        set
        {
            userID = value;
        }
    }

    [XmlAttribute(AttributeName = "CardholderName")]
    public string CardholderName
    {
        get
        {
            return cardholderName;
        }

        set
        {
            cardholderName = value;
        }
    }

    [XmlAttribute(AttributeName = "BillingAddress")]
    public string BillingAddress
    {
        get
        {
            return billingAddress;
        }

        set
        {
            billingAddress = value;
        }
    }

    [XmlAttribute(AttributeName = "Phone")]
    public string Phone
    {
        get
        {
            return phone;
        }

        set
        {
            phone = value;
        }
    }

    [XmlAttribute(AttributeName = "City")]
    public string City
    {
        get
        {
            return city;
        }

        set
        {
            city = value;
        }
    }

    [XmlAttribute(AttributeName = "Zip")]
    public string Zip
    {
        get
        {
            return zip;
        }

        set
        {
            zip = value;
        }
    }

    [XmlAttribute(AttributeName = "Country")]
    public string Country
    {
        get
        {
            return country;
        }

        set
        {
            country = value;
        }
    }

    [XmlAttribute(AttributeName = "Email")]
    public string Email
    {
        get
        {
            return email;
        }

        set
        {
            email = value;
        }
    }

    [XmlAttribute(AttributeName = "State")]
    public string State
    {
        get
        {
            return state;
        }

        set
        {
            state = value;
        }
    }


	public HousingRegistrationAgreementExpanded()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}