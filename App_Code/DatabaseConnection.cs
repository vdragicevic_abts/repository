﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

/// <summary>
/// Summary description for DatabaseConnection
/// </summary>
public class DatabaseConnection
{
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public static string GetConnectionString()
    {
        return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    }

    /// <summary>
    /// Get base URL
    /// </summary>
    /// <returns></returns>
    public static string GetBaseURL()
    {
        return ConfigurationManager.AppSettings["BaseURL"].ToString();
    }

    /// <summary>
    /// Get secure URL
    /// </summary>
    /// <returns></returns>
    public static string GetSecureURL()
    {
        return ConfigurationManager.AppSettings["SecureURL"].ToString();
    }

    /// <summary>
    /// GetPaymentAuthorizationEmail
    /// </summary>
    /// <returns></returns>
    public static string GetPaymentAuthorizationEmail()
    {
        return ConfigurationManager.AppSettings["PaymentAuthorizationEmail"].ToString();
    }

    /// <summary>
    /// GetSMTPServer
    /// </summary>
    /// <returns></returns>
    public static string GetSMTPServer()
    {
        return ConfigurationManager.AppSettings["SMTPServer"].ToString();
    }

    /// <summary>
    /// GetUserPaymentAuthorizationEmail
    /// </summary>
    /// <returns></returns>
    public static string GetUserPaymentAuthorizationEmail()
    {
        return ConfigurationManager.AppSettings["UserPaymentAuthorizationEmail"].ToString();
    }

    /// <summary>
    /// GetGatewayID
    /// </summary>
    /// <returns></returns>
    public static string GetGatewayID(Merchant merchant)
    {
        switch ((int)merchant)
        {
            case 1: return ConfigurationManager.AppSettings["GatewayID1"].ToString();
                
            case 2: return ConfigurationManager.AppSettings["GatewayID2"].ToString();
                
            case 3: return ConfigurationManager.AppSettings["GatewayID3"].ToString();
                
            case 4: return ConfigurationManager.AppSettings["GatewayID4"].ToString();
                
            case 5: return ConfigurationManager.AppSettings["GatewayID5"].ToString();
                
            default: return ConfigurationManager.AppSettings["GatewayID1"].ToString();                
        }
    }

    /// <summary>
    /// GetGatewayPassword
    /// </summary>
    /// <returns></returns>
    public static string GetGatewayPassword(Merchant merchant)
    {
        switch ((int)merchant)
        {
            case 1: return ConfigurationManager.AppSettings["GatewayPassword1"].ToString();
               
            case 2: return ConfigurationManager.AppSettings["GatewayPassword2"].ToString();
               
            case 3: return ConfigurationManager.AppSettings["GatewayPassword3"].ToString();
               
            case 4: return ConfigurationManager.AppSettings["GatewayPassword4"].ToString();
               
            case 5: return ConfigurationManager.AppSettings["GatewayPassword5"].ToString();
               
            default: return ConfigurationManager.AppSettings["GatewayPassword1"].ToString();               
        }
    }

    /// <summary>
    /// GetHashID
    /// </summary>
    /// <param name="merchant"></param>
    /// <returns></returns>
    public static string GetHashID(Merchant merchant)
    {
        switch ((int)merchant)
        {
            case 1: return ConfigurationManager.AppSettings["keyID1"].ToString();
               
            case 2: return ConfigurationManager.AppSettings["keyID2"].ToString();
               
            case 3: return ConfigurationManager.AppSettings["keyID3"].ToString();
               
            case 4: return ConfigurationManager.AppSettings["keyID4"].ToString();
               
            case 5: return ConfigurationManager.AppSettings["keyID5"].ToString();
               
            default: return ConfigurationManager.AppSettings["keyID1"].ToString();                 
        }
    }

    /// <summary>
    /// GetHashPassword
    /// </summary>
    /// <param name="merchant"></param>
    /// <returns></returns>
    public static string GetHashPassword(Merchant merchant)
    {
        switch ((int)merchant)
        {
            case 1: return ConfigurationManager.AppSettings["key1"].ToString();
               
            case 2: return ConfigurationManager.AppSettings["key2"].ToString();
               
            case 3: return ConfigurationManager.AppSettings["key3"].ToString();
               
            case 4: return ConfigurationManager.AppSettings["key4"].ToString();
               
            case 5: return ConfigurationManager.AppSettings["key5"].ToString();
               
            default: return ConfigurationManager.AppSettings["key1"].ToString();
               
        }
    }

    public static string GetMerchantName()
    {
        return ConfigurationManager.AppSettings["MerchantName"].ToString();
    }

    public static string GetMerchantAddress()
    {
        return ConfigurationManager.AppSettings["MerchantAddress"].ToString();
    }

    public static string GetMerchantCity()
    {
        return ConfigurationManager.AppSettings["MerchantCity"].ToString();
    }

    public static string GetMerchantProvance()
    {
        return ConfigurationManager.AppSettings["MerchantProvance"].ToString();
    }

    public static string GetMerchantPostal()
    {
        return ConfigurationManager.AppSettings["MerchantPostal"].ToString();
    }

    public static string GetMerchantCountry()
    {
        return ConfigurationManager.AppSettings["MerchantCountry"].ToString();
    }

    public static string GetMerchantURL()
    {
        return ConfigurationManager.AppSettings["MerchantURL"].ToString();
    }        

    public static string GetPlanetPaymentServerIP()
    {
        return ConfigurationManager.AppSettings["PlanetPaymentServerIP"].ToString();
    }

    public static int GetPlanetPaymentServerPort()
    {
        return int.Parse(ConfigurationManager.AppSettings["PlanetPaymentServerPort"].ToString());
    }    

    public static int GetPlanetPaymentTimeOut()
    {
        return int.Parse(ConfigurationManager.AppSettings["PlanetPaymentTimeOut"].ToString());
    }

    public static string GetPlanetPaymentCompanyKey(Merchant companyName)
    {
        var merch = ((Merchant)companyName).ToString();
        var company = ((Merchant)companyName).ToString().ToLower().Substring(0, 3);
        string amex = merch.Contains("Amex") ? "Amex" : "";

        switch (company)
        {
            case "igd": return ConfigurationManager.AppSettings["PlanetPaymentCompanyKeyIGD" + amex].ToString();
            case "cmr": return ConfigurationManager.AppSettings["PlanetPaymentCompanyKeyCMR" + amex].ToString();
            case "igh": return ConfigurationManager.AppSettings["PlanetPaymentCompanyKeyIGH" + amex].ToString();
            default: return "";                
        }

    }

    public static string GetPlanetPaymentTerminalId(Merchant merchant)
    {
        var merch = ((Merchant)merchant).ToString();
        var companyName = ((Merchant)merchant).ToString().ToLower().Substring(0, 3);
        string amex = merch.Contains("Amex") ? "Amex" : "";       

        switch (companyName)
        {
            case "igd": return ConfigurationManager.AppSettings["PlanetPaymentTerminalIdIGD" + amex].ToString();
            case "cmr": return ConfigurationManager.AppSettings["PlanetPaymentTerminalIdCMR" + amex].ToString();
            case "igh": return ConfigurationManager.AppSettings["PlanetPaymentTerminalIdIGH" + amex].ToString();
            default: return "";
        }
    }

    public static string GetPlanetPaymentEncryptionKey(Merchant companyName, PLanetPaymentEncryptionKeys keyNumber)
    {
        var merch = ((Merchant)companyName).ToString();
        var company = ((Merchant)companyName).ToString().ToLower().Substring(0, 3);  
        string amex = merch.Contains("Amex") ? "Amex" : "";

        switch (company)
        {
            case "igd": return ConfigurationManager.AppSettings["PlanetPaymentEncryptionKeyIGD" + amex + (int)keyNumber].ToString();
            case "cmr": return ConfigurationManager.AppSettings["PlanetPaymentEncryptionKeyCMR" + amex + (int)keyNumber].ToString();
            case "igh": return ConfigurationManager.AppSettings["PlanetPaymentEncryptionKeyIGH" + amex + (int)keyNumber].ToString();
            default: return "";
        }
    }

    public static string GetDefaultCurrency()
    {
        return ConfigurationManager.AppSettings["DefaultCurrency"].ToString();
    }

    public enum PLanetPaymentEncryptionKeys
    {
        Key1 = 1,
        Key2 = 2,
        Key3 = 3
    }
}