﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for DownloadDocument
/// </summary>
public class DownloadDocument
{
    public static void Excel(DataTable dt, string reportName)
    {
        string attachment = "attachment; filename=" + reportName + ".xls";
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.AddHeader("content-disposition", attachment);
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode; //System.Text.Encoding.Default; //System.Text.Encoding.GetEncoding("windows-1252");
        //HttpContext.Current.Response.Charset = ""; //System.Text.Encoding.GetEncoding("windows-1252").WebName; //System.Text.Encoding.Unicode.WebName;              
        HttpContext.Current.Response.ContentType = "application/ms-excel"; //"application/vnd.ms-excel";
        HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        string tab = "";
        foreach (DataColumn dc in dt.Columns)
        {
            HttpContext.Current.Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }

        HttpContext.Current.Response.Write("\n");
        
        int i;
        foreach (DataRow dr in dt.Rows)
        {
            tab = "";
            for (i = 0; i < dt.Columns.Count; i++)
            {

                HttpContext.Current.Response.Write(tab + dr[i].ToString().Replace("\n", "").Replace("\r", "").Replace("\t", ""));

                tab = "\t";
            }

            HttpContext.Current.Response.Write("\n");
        }

        HttpContext.Current.Response.End(); 
    }

    public static void Excel2(DataTable dt, string reportName)
    {
        string attachment = "attachment; filename=" + reportName + ".xls";
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.AddHeader("content-disposition", attachment);
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode; //System.Text.Encoding.Default; //System.Text.Encoding.GetEncoding("windows-1252");
        //HttpContext.Current.Response.Charset = ""; //System.Text.Encoding.GetEncoding("windows-1252").WebName; //System.Text.Encoding.Unicode.WebName;              
        HttpContext.Current.Response.ContentType = "application/ms-excel"; //"application/vnd.ms-excel";
        HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        // create a string writer
        using (System.IO.StringWriter sw = new System.IO.StringWriter(System.Globalization.CultureInfo.CurrentCulture))
        {
            using (HtmlTextWriter htw = new HtmlTextWriter(sw))
            {               
                DataGrid dg = new DataGrid();
                               
                dg.DataSource = dt;
                dg.GridLines = GridLines.Both;                            
                dg.HeaderStyle.Font.Bold = false;
                dg.DataBind();
                dg.RenderControl(htw);
                HttpContext.Current.Response.Write(sw);
                HttpContext.Current.Response.End();
            }
        }
    }


    public static void HtmlTableToExcel(HtmlTable table, string reportName)
    {
        string attachment = "attachment; filename=" + reportName + ".xls";
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.AddHeader("content-disposition", attachment);
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode; //System.Text.Encoding.Default; //System.Text.Encoding.GetEncoding("windows-1252");
        //HttpContext.Current.Response.Charset = ""; //System.Text.Encoding.GetEncoding("windows-1252").WebName; //System.Text.Encoding.Unicode.WebName;              
        HttpContext.Current.Response.ContentType = "application/ms-excel"; //"application/vnd.ms-excel";
        HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        // create a string writer
        using (System.IO.StringWriter sw = new System.IO.StringWriter(System.Globalization.CultureInfo.CurrentCulture))
        {
            using (HtmlTextWriter htw = new HtmlTextWriter(sw))
            {
                table.RenderControl(htw);
                HttpContext.Current.Response.Write(sw);
                HttpContext.Current.Response.End();
            }
        }
    }

    public static void Word(DataTable dt, string reportName)
    {
        string attachment = "attachment; filename=" + reportName + ".doc";
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.Buffer = true;
        //Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        HttpContext.Current.Response.ContentType = "application/vnd.ms-word";
        HttpContext.Current.Response.AddHeader("Content-Disposition", attachment);
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        //HttpContext.Current.Response.Charset = "";


        string tab = "";
        foreach (DataColumn dc in dt.Columns)
        {
            HttpContext.Current.Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }

        HttpContext.Current.Response.Write("\n");

        int i;
        foreach (DataRow dr in dt.Rows)
        {
            tab = "";
            for (i = 0; i < dt.Columns.Count; i++)
            {
                HttpContext.Current.Response.Write(tab + dr[i].ToString().Replace("\n", "").Replace("\r", "").Replace("\t", ""));

                tab = "\t";
            }

            HttpContext.Current.Response.Write("\n");
        }

        HttpContext.Current.Response.End();
    }    
}
