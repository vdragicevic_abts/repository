﻿using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace PassKeyAPI
{
    public class XmlHelper
    {
        public static bool Serialize<T>(T value, ref string serializeXml)
        {
            if (value == null)
            {
                return false;
            }
            try
            {
                var xmlserializer = new XmlSerializer(typeof(T));
                var stringWriter = new StringWriter();
                var writer = XmlWriter.Create(stringWriter, new XmlWriterSettings() { OmitXmlDeclaration = true });

                xmlserializer.Serialize(writer, value);

                serializeXml = stringWriter.ToString();

                writer.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static T Deserialize<T>(string serializedResults)
        {
            var serializer = new XmlSerializer(typeof (T));
            using (var stringReader = new StringReader(serializedResults))
                return (T) serializer.Deserialize(stringReader);
        }

        public static void WriteRawXmlOnPage(string bridge)
        {
            ((Page) HttpContext.Current.Handler).Response.Clear();
            XDocument doc = XDocument.Parse(bridge);
            ((Page) HttpContext.Current.Handler).Response.Write("<XMP>" + doc.ToString() + "<\\XMP>");
            ((Page) HttpContext.Current.Handler).Response.End();
        }
    }
}