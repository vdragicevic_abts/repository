﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for IndividualPaymentInternetSecure
/// </summary>
[Serializable]
[XmlRoot(ElementName = "IndividualPaymentInternetSecure")]
public class IndividualPaymentInternetSecure
{
    private string conferenceID;
    private int paymentDetailCCID;
    private int cardTypeID;
    private string creditCardNumber;
    private string ccv;
    private DateTime exparationDate;
    private int userID;
    private string cardHolderName;
    private string address;
    private string city;
    private string province;
    private string postal;
    private string country;
    private string phone;
    private string email;
    private string productCode;
    private string productDescription;
    private string productPrice;
    private int gatewayID;
    private string currency;
    private bool test;
    private string exactID;
    private string password;
    private Merchant merchant;
    private string guestName;
	private string websiteBookingID;
    private string cardType;
    private string code;
    private string state;
	private string stateName;

    [XmlAttribute(AttributeName = "ConferenceID")]
    public string ConferenceID
    {
        get
        {
            return conferenceID;
        }

        set
        {
            conferenceID = value;
        }
    }

    [XmlAttribute(AttributeName = "PaymentDetailCCID")]
    public int PaymentDetailCCID
    {
        get
        {
            return paymentDetailCCID;
        }

        set
        {
            paymentDetailCCID = value;
        }
    }

    [XmlElement(ElementName = "CardTypeID")]
    public int CardTypeID
    {
        get
        {
            return cardTypeID;
        }

        set
        {
            cardTypeID = value;
        }
    }
	
	[XmlElement(ElementName = "CardType")]
    public string CardType
    {
        get
        {
            return cardType;
        }

        set
        {
            cardType = value;
        }
    }

    [XmlElement(ElementName = "CreditCardNumber")]
    public string CreditCardNumber
    {
        get
        {
            return creditCardNumber;
        }

        set
        {
            creditCardNumber = value;
        }
    }

    [XmlElement(ElementName = "CCV")]
    public string CCV
    {
        get
        {
            return ccv;
        }

        set
        {
            ccv = value;
        }
    }

    [XmlElement(ElementName = "ExparationDate")]
    public DateTime ExparationDate
    {
        get
        {
            return exparationDate;
        }

        set
        {
            exparationDate = value;
        }
    }

    [XmlElement(ElementName = "UserID")]
    public int UserID
    {
        get
        {
            return userID;
        }

        set
        {
            userID = value;
        }
    }

    [XmlElement(ElementName = "CardHolderName")]
    public string CardHolderName
    {
        get
        {
            return cardHolderName;
        }

        set
        {
            cardHolderName = value;
        }
    }

    [XmlElement(ElementName = "Address")]
    public string Address
    {
        get
        {
            return address;
        }

        set
        {
            address = value;
        }
    }

    [XmlElement(ElementName = "City")]
    public string City
    {
        get
        {
            return city;
        }

        set
        {
            city = value;
        }
    }

    [XmlElement(ElementName = "Province")]
    public string Province
    {
        get
        {
            return province;
        }

        set
        {
            province = value;
        }
    }

    [XmlElement(ElementName = "State")]
    public string State
    {
        get
        {
            return state;
        }

        set
        {
            state = value;
        }
    }

    [XmlElement(ElementName = "Postal")]
    public string Postal
    {
        get
        {
            return postal;
        }

        set
        {
            postal = value;
        }
    }

    [XmlElement(ElementName = "Country")]
    public string Country
    {
        get
        {
            return country;
        }

        set
        {
            country = value;
        }
    }

    [XmlElement(ElementName = "Phone")]
    public string Phone
    {
        get
        {
            return phone;
        }

        set
        {
            phone = value;
        }
    }

    [XmlElement(ElementName = "Email")]
    public string Email
    {
        get
        {
            return email;
        }

        set
        {
            email = value;
        }
    }

    [XmlElement(ElementName = "ProductCode")]
    public string ProductCode
    {
        get
        {
            return productCode;
        }

        set
        {
            productCode = value;
        }
    }

    [XmlElement(ElementName = "ProductDescription")]
    public string ProductDescription
    {
        get
        {
            return productDescription;
        }

        set
        {
            productDescription = value;
        }
    }

    [XmlElement(ElementName = "ProductPrice")]
    public string ProductPrice
    {
        get
        {
            return productPrice;
        }

        set
        {
            productPrice = value;
        }
    }

    [XmlElement(ElementName = "GatewayID")]
    public int GatewayID
    {
        get
        {
            return gatewayID;
        }

        set
        {
            gatewayID = value;
        }
    }

    [XmlElement(ElementName = "Test")]
    public bool Test
    {
        get
        {
            return test;
        }

        set
        {
            test = value;
        }
    }

    [XmlElement(ElementName = "Currency")]
    public string Currency
    {
        get
        {
            return currency;
        }

        set
        {
            currency = value;
        }
    }

    [XmlElement(ElementName = "ExactID")]
    public string ExactID
    {
        get
        {
            return exactID;
        }

        set
        {
            exactID = value;
        }
    }

    [XmlElement(ElementName = "Password")]
    public string Password
    {
        get
        {
            return password;
        }

        set
        {
            password = value;
        }
    }

    [XmlElement(ElementName = "MerchantAccount")]
    public Merchant MerchantAccount
    {
        get
        {
            return merchant;
        }

        set
        {
            merchant = value;
        }
    }

    [XmlElement(ElementName = "GuestName")]
    public string GuestName
    {
        get
        {
            return guestName;
        }

        set
        {
            guestName = value;
        }
    }
	
	[XmlAttribute(AttributeName = "WebsiteBookingID")]
    public string WebsiteBookingID
    {
        get
        {
            return websiteBookingID;
        }

        set
        {
            websiteBookingID = value;
        }
    }

    [XmlElement(ElementName = "Code")]
    public string Code
    {
        get
        {
            return code;
        }

        set
        {
            code = value;
        }
    }
	
	[XmlAttribute(AttributeName = "StateName")]
    public string StateName
    {
        get
        {
            return stateName;
        }
        set
        {
            stateName = value;
        }
    }	
	

    public IndividualPaymentInternetSecure()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}