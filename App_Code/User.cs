﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;

/// <summary>
/// Summary description for User
/// </summary>
public class User:SoapHeader
{
    private string userID;
    private string password;

    public string UserID
    {
        get
        {
            return userID;
        }

        set
        {
            userID = value;
        }
    }

    public string Password
    {
        get
        {
            return password;
        }

        set
        {
            password = value;
        }
    }
}

public enum Merchant
{
    IgdInternationlGroup = 1,
    CmrGlobalGroupService = 2,
    AbtsConventionServices = 3,
    InternationalGroupHousing = 4,
    Swme = 5,
    IgdInternationlGroup_PP = 6,
    CmrGlobalGroupService_PP = 7,
    IghInternationalGroupHousing_PP = 8,
	CmrGlobalGroupService_PP_Amex = 9,
	IgdInternationlGroup_PP_Amex = 10,
    IghInternationalGroupHousing_PP_Amex = 11
}