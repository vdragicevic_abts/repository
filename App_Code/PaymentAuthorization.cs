﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for PaymentAuthorization
/// </summary>
[Serializable]
[XmlRoot(ElementName="PaymentAuthorization")]
public class PaymentAuthorization
{
    private string conferenceID;
    private string proposalID;
    private string invoiceID;
    private string eventName;
    private string email;
    private int cardTypeID;
    private string creditCardNumber;  
    private DateTime exparationDate;
    private string cardholderName;
    private string preparer;
    
    [XmlAttribute(AttributeName="ConferenceID")]
    public string ConferenceID
    {
        get
        {
            return conferenceID;
        }

        set
        {
            conferenceID = value;
        }        
    }

    [XmlAttribute(AttributeName="ProposalID")]
    public string ProposalID
    {
        get
        {
            return proposalID;
        }

        set
        {
            proposalID = value;
        }
    }

    [XmlAttribute(AttributeName="InvoiceID")]
    public string InvoiceID
    {
        get
        {
            return invoiceID;
        }

        set
        {
            invoiceID = value;
        }
    }

    [XmlAttribute(AttributeName = "EventName")]
    public string EventName
    {
        get
        {
            return eventName;
        }

        set
        {
            eventName = value;
        }
    }

    [XmlAttribute(AttributeName = "Email")]
    public string Email
    {
        get
        {
            return email;
        }

        set
        {
            email = value;
        }
    }

    [XmlElement(ElementName="CardTypeID")]
    public int CardTypeID
    {
        get
        {
            return cardTypeID;
        }

        set
        {
            cardTypeID = value;
        }
    }

    [XmlElement(ElementName = "CreditCardNumber")]
    public string CreditCardNumber
    {
        get
        {
            return creditCardNumber;
        }

        set
        {
            creditCardNumber = value;
        }
    }
   
    [XmlElement(ElementName = "ExparationDate")]
    public DateTime ExparationDate
    {
        get
        {
            return exparationDate;
        }

        set
        {
            exparationDate = value;
        }
    }

    [XmlElement(ElementName = "CardholderName")]
    public string CardholderName
    {
        get
        {
            return cardholderName;
        }

        set
        {
            cardholderName = value;
        }
    }

    [XmlElement(ElementName = "Preparer")]
    public string Preparer
    {
        get
        {
            return preparer;
        }

        set
        {
            preparer = value;
        }
    }

    

    public PaymentAuthorization()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}