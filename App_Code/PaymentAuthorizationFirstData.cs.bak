﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for PaymentAuthorizationFirstData
/// </summary>
public class PaymentAuthorizationFirstData
{
    private string conferenceID;
    private int paymentDetailCCID;
    private string proposalID;
    private string invoiceID;
    private string eventName;
    private int cardTypeID;
    private string creditCardNumber;
    private string ccv;
    private DateTime exparationDate;
    private int userID;
    private string cardHolderName;
    private string address;
    private string city;
    private string province;
    private string postal;
    private string country;
    private string phone;
    private string email;
    private string productCode;
    private string productDescription;
    private string productPrice;
    private string gatewayID;
    private string currency;
    private bool test;
    private string exactID;
    private string password;
    private string paymentInstructions;
    private string preparer;
    private string descriptionOfServices;
    private int guestListId;
    private int serviceItemId;
    private long hotelCode;
    private string token;
    private string cardType;
    private Merchant merchant;
    private string guestName;
    private bool dcc;
    private string rateResponseId;
    private string exchangeRate;
    private string foreignCurrencySymbolHtml;

    [XmlAttribute(AttributeName = "ConferenceID")]
    public string ConferenceID
    {
        get
        {
            return conferenceID;
        }

        set
        {
            conferenceID = value;
        }
    }

    [XmlAttribute(AttributeName = "PaymentDetailCCID")]
    public int PaymentDetailCCID
    {
        get
        {
            return paymentDetailCCID;
        }

        set
        {
            paymentDetailCCID = value;
        }
    }

    [XmlAttribute(AttributeName = "ProposalID")]
    public string ProposalID
    {
        get
        {
            return proposalID;
        }

        set
        {
            proposalID = value;
        }
    }

    [XmlAttribute(AttributeName = "InvoiceID")]
    public string InvoiceID
    {
        get
        {
            return invoiceID;
        }

        set
        {
            invoiceID = value;
        }
    }

    [XmlAttribute(AttributeName = "EventName")]
    public string EventName
    {
        get
        {
            return eventName;
        }

        set
        {
            eventName = value;
        }
    }

    [XmlElement(ElementName = "CardTypeID")]
    public int CardTypeID
    {
        get
        {
            return cardTypeID;
        }

        set
        {
            cardTypeID = value;
        }
    }

    [XmlElement(ElementName = "CreditCardNumber")]
    public string CreditCardNumber
    {
        get
        {
            return creditCardNumber;
        }

        set
        {
            creditCardNumber = value;
        }
    }

    [XmlElement(ElementName = "CCV")]
    public string CCV
    {
        get
        {
            return ccv;
        }

        set
        {
            ccv = value;
        }
    }

    [XmlElement(ElementName = "ExparationDate")]
    public DateTime ExparationDate
    {
        get
        {
            return exparationDate;
        }

        set
        {
            exparationDate = value;
        }
    }

    [XmlElement(ElementName = "UserID")]
    public int UserID
    {
        get
        {
            return userID;
        }

        set
        {
            userID = value;
        }
    }

    [XmlElement(ElementName = "CardHolderName")]
    public string CardHolderName
    {
        get
        {
            return cardHolderName;
        }

        set
        {
            cardHolderName = value;
        }
    }

    [XmlElement(ElementName = "Address")]
    public string Address
    {
        get
        {
            return address;
        }

        set
        {
            address = value;
        }
    }

    [XmlElement(ElementName = "City")]
    public string City
    {
        get
        {
            return city;
        }

        set
        {
            city = value;
        }
    }

    [XmlElement(ElementName = "Province")]
    public string Province
    {
        get
        {
            return province;
        }

        set
        {
            province = value;
        }
    }

    [XmlElement(ElementName = "Postal")]
    public string Postal
    {
        get
        {
            return postal;
        }

        set
        {
            postal = value;
        }
    }

    [XmlElement(ElementName = "Country")]
    public string Country
    {
        get
        {
            return country;
        }

        set
        {
            country = value;
        }
    }

    [XmlElement(ElementName = "Phone")]
    public string Phone
    {
        get
        {
            return phone;
        }

        set
        {
            phone = value;
        }
    }

    [XmlElement(ElementName = "Email")]
    public string Email
    {
        get
        {
            return email;
        }

        set
        {
            email = value;
        }
    }

    [XmlElement(ElementName = "ProductCode")]
    public string ProductCode
    {
        get
        {
            return productCode;
        }

        set
        {
            productCode = value;
        }
    }

    [XmlElement(ElementName = "ProductDescription")]
    public string ProductDescription
    {
        get
        {
            return productDescription;
        }

        set
        {
            productDescription = value;
        }
    }

    [XmlElement(ElementName = "ProductPrice")]
    public string ProductPrice
    {
        get
        {
            return productPrice;
        }

        set
        {
            productPrice = value;
        }
    }

    [XmlElement(ElementName = "GatewayID")]
    public string GatewayID
    {
        get
        {
            return gatewayID;
        }

        set
        {
            gatewayID = value;
        }
    }

    [XmlElement(ElementName = "Test")]
    public bool Test
    {
        get
        {
            return test;
        }

        set
        {
            test = value;
        }
    }

    [XmlElement(ElementName = "Currency")]
    public string Currency
    {
        get
        {
            return currency;
        }

        set
        {
            currency = value;
        }
    }

    [XmlElement(ElementName = "ExactID")]
    public string ExactID
    {
        get
        {
            return exactID;
        }

        set
        {
            exactID = value;
        }
    }

    [XmlElement(ElementName = "Password")]
    public string Password
    {
        get
        {
            return password;
        }

        set
        {
            password = value;
        }
    }

    [XmlAttribute(AttributeName = "PaymentInstructions")]
    public string PaymentInstructions
    {
        get
        {
            return paymentInstructions;
        }

        set
        {
            paymentInstructions = value;
        }
    }

    [XmlElement(ElementName = "Preparer")]
    public string Preparer
    {
        get
        {
            return preparer;
        }

        set
        {
            preparer = value;
        }
    }

    [XmlElement(ElementName = "DescriptionOfServices")]
    public string DescriptionOfServices
    {
        get
        {
            return descriptionOfServices;
        }

        set
        {
            descriptionOfServices = value;
        }
    }

    [XmlElement(ElementName = "GuestListID")]
    public int GuestListID
    {
        get
        {
            return guestListId;
        }

        set
        {
            guestListId = value;
        }
    }

    [XmlElement(ElementName = "ServiceItemID")]
    public int ServiceItemID
    {
        get
        {
            return serviceItemId;
        }

        set
        {
            serviceItemId = value;
        }
    }

    [XmlElement(ElementName = "HotelCode")]
    public long HotelCode
    {
        get
        {
            return hotelCode;
        }

        set
        {
            hotelCode = value;
        }
    }

    [XmlElement(ElementName = "Token")]
    public string Token
    {
        get
        {
            return token;
        }

        set
        {
            token = value;
        }
    }

    [XmlElement(ElementName = "CardType")]
    public string CardType
    {
        get
        {
            return cardType;
        }

        set
        {
            cardType = value;
        }
    }

    [XmlElement(ElementName = "MerchantAccount")]
    public Merchant MerchantAccount
    {
        get
        {
            return merchant;
        }

        set
        {
            merchant = value;
        }
    }

    [XmlAttribute(AttributeName = "GuestName")]
    public string GuestName
    {
        get
        {
            return guestName;
        }

        set
        {
            guestName = value;
        }
    }

    [XmlElement(ElementName = "DCC")]
    public bool DCC
    {
        get
        {
            return dcc;
        }

        set
        {
            dcc = value;
        }
    }

    [XmlAttribute(AttributeName = "RateResponseID")]
    public string RateResponseID
    {
        get
        {
            return rateResponseId;
        }

        set
        {
            rateResponseId = value;
        }
    }

    [XmlAttribute(AttributeName = "ExchangeRate")]
    public string ExchangeRate
    {
        get
        {
            return exchangeRate;
        }

        set
        {
            exchangeRate = value;
        }
    }

    [XmlAttribute(AttributeName = "ForeignCurrencySymbolHtml")]
    public string ForeignCurrencySymbolHtml
    {
        get
        {
            return foreignCurrencySymbolHtml;
        }

        set
        {
            foreignCurrencySymbolHtml = value;
        }
    }




    public PaymentAuthorizationFirstData()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}