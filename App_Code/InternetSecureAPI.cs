﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Text;
using System.Net;
using System.Xml;
using FirstDataGlobalGatewayE4;
using PassKeyAPI;

/// <summary>
/// Summary description for InternetSecureAPI
/// </summary>
public class InternetSecureAPI
{
    /// <summary>
    /// 
    /// </summary>
    public static string Conferences = "";

    /// <summary>
    /// SendUserInformation2Evalon
    /// </summary>
    /// <param name="gatewayID"></param>
    /// <param name="productPrice"></param>
    /// <param name="productID"></param>
    /// <param name="productName"></param>
    /// <param name="name"></param>
    /// <param name="company"></param>
    /// <param name="address"></param>
    /// <param name="city"></param>
    /// <param name="state"></param>
    /// <param name="zipCode"></param>
    /// <param name="country"></param>
    /// <param name="phone"></param>
    /// <param name="email"></param>
    /// <param name="cardNumber"></param>
    /// <param name="ccMonth"></param>
    /// <param name="ccYear"></param>
    /// <param name="cvv"></param>
    /// <returns></returns>
    public static string SendUserInformation2Evalon(string gatewayID, string productPrice, string productID, string productName, string name, string company, string address, string city, string state, string zipCode, string country, string phone, string email, string cardNumber, string ccMonth, string ccYear, string cvv, string currency, bool test)
    {
        string xml = "<?xml version='1.0' encoding='UTF-8'?>" +
                     "<TranxRequest>" +
                         "<GatewayID>" + gatewayID + "</GatewayID>" +
                         "<Products>" + productPrice + "::1::" + productID + "::" + productName + "::{Test}</Products>" + //{RB amount=100.00 startmonth=+1 frequency=monthly duration=1 email=1}
                         "<xxxName>" + name + "</xxxName>" +
                         "<xxxCompany>" + company + "</xxxCompany>" +
                         "<xxxAddress>" + address + "</xxxAddress>" +
                         "<xxxCity>" + city + "</xxxCity>" +
                         "<xxxState>" + state + "</xxxState>" +
                         "<xxxZipCode>" + zipCode + "</xxxZipCode>" +
                         "<xxxCountry>" + country + "</xxxCountry>" +
                         "<xxxPhone>" + phone + "</xxxPhone>" +
                         "<xxxEmail>" + email + "</xxxEmail>" +
                         "<xxxCard_Number>" + cardNumber + "</xxxCard_Number>" +
                         "<xxxCCMonth>" + ccMonth + "</xxxCCMonth>" +
                         "<xxxCCYear>" + ccYear + "</xxxCCYear>" +
                         "<CVV2>" + cvv + "</CVV2>" +
                         "<CVV2Indicator>1</CVV2Indicator>" +
                         "<xxxTransType>00</xxxTransType>" +
                         "<xxxCustomerDB>1</xxxCustomerDB>" +
                         "<xxxTransactionCurrency>" + currency + "</xxxTransactionCurrency>" +
                     "</TranxRequest>";

        string testURL = "https://test.internetsecure.com/process.cgi?xxxRequestMode=X&xxxRequestData=";
        string liveURL = "https://secure.internetsecure.com/process.cgi?xxxRequestMode=X&xxxRequestData=";
    
        if (!test) xml = xml.Replace("Test", currency);

        string success = test ? PostXml(testURL, xml) : PostXml(liveURL, xml);

        return success;
    }


    /// <summary>
    /// PostXml
    /// </summary>
    /// <param name="url"></param>
    /// <param name="xml"></param>
    /// <returns></returns>
    private static string PostXml(string url, string xml)
    {
        string result = "";

        Encoding encoding = Encoding.UTF8;

        string __xml = xml;
        string _xml = HttpUtility.UrlEncode(__xml, encoding);
        string _url = url + _xml;

        byte[] bytes = encoding.GetBytes(_url);

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_url);
        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";

        try
        {
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                    throw new ApplicationException(message);

                }
                else
                {
                    XmlDocument responseXML = new XmlDocument();
                    responseXML.Load(response.GetResponseStream());

                    string _request = "true";
                    string _card = "false";
                    string _cvv = "false";
                    string _avsAddress = "false";
                    string _avsZip = "false";

                    string status = response.StatusCode.ToString();
                    string page = ((XmlNodeList)responseXML.GetElementsByTagName("Page"))[0].InnerText; //Transaction Response Code
                    string receiptNumber = ((XmlNodeList)responseXML.GetElementsByTagName("ReceiptNumber"))[0].InnerText; // Transaction Receipt Number
                    string avsResponse = ((XmlNodeList)responseXML.GetElementsByTagName("AVSResponseCode"))[0].InnerText; // AVS Response Code
                    string cvvResponse = ((XmlNodeList)responseXML.GetElementsByTagName("CVV2ResponseCode"))[0].InnerText; // Card Verification Result

                    //result += "Page=" + page + "<br/>";
                    //result += "ReceiptNumber=" + receiptNumber + "<br/>";
                    //result += "AVSResponseCode=" + avsResponse + "<br/>";
                    //result += "CVV2ResponseCode=" + cvvResponse + "<br/>";

                    if (page != "90000")
                        _request = "false";
                    else if (receiptNumber.Length == 0)
                        _request = "false";
                    else if (page.Substring(0, 2) == "99")
                        _request = "false";
                    else
                        _request = "true";

                    if (page.Substring(1, 3) != "000")
                        _card = "false";
                    else
                        _card = "true";

                    if (cvvResponse == "M")
                        _cvv = "true";
                    else if (cvvResponse == "N")
                        _cvv = "false";
                    else
                        _cvv = "Empty";

                    switch (avsResponse)
                    {
                        case "A": { _avsAddress = "true"; _avsZip = "false"; }
                            break;
                        case "B": { _avsAddress = "true"; _avsZip = "false"; }
                            break;
                        case "C": { _avsAddress = "false"; _avsZip = "false"; }
                            break;
                        case "D": { _avsAddress = "true"; _avsZip = "true"; }
                            break;
                        case "E": { _avsAddress = "Empty"; _avsZip = "Empty"; }
                            break;
                        case "G": { _avsAddress = "Empty"; _avsZip = "Empty"; }
                            break;
                        case "I": { _avsAddress = "Empty"; _avsZip = "Empty"; }
                            break;
                        case "M": { _avsAddress = "true"; _avsZip = "true"; }
                            break;
                        case "N": { _avsAddress = "false"; _avsZip = "false"; }
                            break;
                        case "P": { _avsAddress = "false"; _avsZip = "true"; }
                            break;
                        case "R": { _avsAddress = "empty"; _avsZip = "empty"; }
                            break;
                        case "S": { _avsAddress = "empty"; _avsZip = "empty"; }
                            break;
                        case "U": { _avsAddress = "empty"; _avsZip = "empty"; }
                            break;
                        case "W": { _avsAddress = "false"; _avsZip = "true"; }
                            break;
                        case "X": { _avsAddress = "true"; _avsZip = "true"; }
                            break;
                        case "Y": { _avsAddress = "true"; _avsZip = "true"; }
                            break;
                        case "Z": { _avsAddress = "false"; _avsZip = "true"; }
                            break;
                    }

                    //result += "<br/>***********************************************************<br/>";
                    //result += "Results Analysis:<br><br>";
                    //result += "Request=" + _request + "<br/>";
                    //result += "Card=" + _card + "<br/>";
                    //result += "avsAddress=" + _avsAddress + "<br/>";
                    //result += "avsZip=" + _avsZip + "<br/>";
                    //result += "cvv=" + _cvv + "<br/>";

                    if (_request == "true")
                    {
                        result = receiptNumber;
                    }
                    else
                    {
                        result = page;
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }

        return result;
    }

    /// <summary>
    /// SendUserInformation2BOA
    /// </summary>   
    /// <param name="productPrice"></param>
    /// <param name="productID"></param>
    /// <param name="productName"></param>
    /// <param name="name"></param>
    /// <param name="company"></param>
    /// <param name="address"></param>
    /// <param name="city"></param>
    /// <param name="state"></param>
    /// <param name="zipCode"></param>
    /// <param name="country"></param>
    /// <param name="phone"></param>
    /// <param name="email"></param>
    /// <param name="cardNumber"></param>
    /// <param name="ccMonth"></param>
    /// <param name="ccYear"></param>
    /// <param name="cvv"></param>
    /// <param name="test"></param>
    /// <param name="exactID"></param>
    /// <param name="password"></param>
    /// <param name="currency"></param>
    /// <returns></returns>
    public static string SendUserInformation2BOA(string productPrice, string productID, string productName, string name, string company, string address, string city, string state, string zipCode, string country, string phone, string email, string cardNumber, string ccMonth, string ccYear, string cvv, string currency, bool test, string exactID, string password)
    {
        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
        //ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;

        string success = string.Empty;
              
        Transaction txn = new Transaction();
        Service srvc = new Service();
    
        srvc.Url = test == true ? "https://api.demo.globalgatewaye4.firstdata.com/transaction/v8" : "https://api.globalgatewaye4.firstdata.com/transaction/v8";
        
        txn.ExactID = exactID;
        txn.Password = password;
        txn.Transaction_Type = "00";
        txn.Card_Number = cardNumber;
        txn.CardHoldersName = name;
        txn.DollarAmount = productPrice;
        txn.CVD_Presence_Ind = "1";
        //txn.VerificationStr2 = cvv;
        //txn.VerificationStr1 = address + "|" + zipCode + "|" + city + "|" + state + "|" + country + "|" + "W" + phone;
        txn.Expiry_Date = ccMonth + ccYear;
        txn.Client_Email = email;
        txn.Reference_No = productPrice + "::" + "1::" + productID;
        txn.Reference_3 = company;
        txn.Currency = currency;
        txn.Customer_Ref = productName;

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        
        TransactionResult result = srvc.SendAndCommit(txn);

        if (result.EXact_Resp_Code == "00")
        {
            if (result.Bank_Resp_Code == "100")
            {
                success = result.Transaction_Tag;
            }
            else
            {
                success = result.Bank_Resp_Code;
            }
        }
        else
        {
            success = result.EXact_Resp_Code;
        }

        EmailHelper.SendUserPaymentDetailsFirstData(result);

        return success;
    }

    /// <summary>
    /// First Data Response Codes
    /// </summary>
    public enum FirstData
    {
        TransactionNormal = 0,
        Cvv2DataNotVerified = 8,
        InvalidCreditCardNumber = 22,
        InvalidExpiryDate = 25,
        InvalidAmount = 26,
        InvalidCardHolder = 27,
        InvalidAuthorizationNo = 28,
        InvalidVerificationString = 31,
        InvalidTransactionCode = 32,
		PaymentTypeNotSupportedByMerchant = 37,
        InvalidReferenceNo = 57,
        InvalidAvsString = 58,
        InvalidCustomerReferenceNumber = 60,
        InvalidDuplicate = 63,
        InvalidRefund = 64,
        RestrictedCardNumber = 68,
        InavlidTransactionTag = 69,
        DataWithinTheTransactionIsIncorrect = 72,
        InvalidTransarmorToken = 89,
        InvalidCurrencyRequested = 93,
        InvalidAuthorizationNumberEntered = 95,
        TransactionFailure = 10,
        Approved = 100,
        Validated = 101,
        Verified = 102,
        PreNoted = 103,
        NoReasonToDecline = 104,
        ReceivedAndStored = 105,
        ProvidedAuth = 106,
        RequestReceived = 107,
        ApprovedForActivation = 108,
        PreviouslyRocessedTransaction = 109,
        BinAlert = 110,
        ApprovedForPartial = 111,
        ConditionalApproval = 164,
        InvalidCcNumber = 201,
        BadAmount = 202,
        ZeroAmount = 203,
        OtherError = 204,
        BadTotalAuthAmount = 205,
        InvalidSkuNumber = 218,
        InvalidCreditPlan = 219,
        InvalidStoreNumber = 220,
        InvalidFieldData = 225,
        MissingCompanionData = 227,
        PercentsDoNotTotal100 = 229,
        PaymentsDoNotTotal100 = 230,
        InvalidDivisionNumber = 231,
        DoesNotMatchMop = 233,
        DuplicateOrderNumber = 234,
        FpoLocked = 235,
        AuthRecycleHostSystemDown = 236,
        FpoNotApproved = 237,
        InvalidCurrency = 238,
        InvalidMopForDivision = 239,
        AuthAmountForDivision = 240,
        IllegalAction = 241,
        InvalidPurchaseLevel3 = 243,
        InvalidEncryptionFormat = 244,
        MissingOrInvalidSecurePaymentData = 245,
        MerchantNotMasterCardSecureCodeEnabled = 246,
        CheckConversionDataError = 247,
        BlanksNotPassedInReservedField = 248,
        InvalidMcc = 249,
        InvalidStartDate = 251,
        InvalidIssueNumber = 252,
        InvalidTranType = 253,
        MissingCustServicePhone = 257,
        NotAuthorizedToSendRecord = 258,
        SoftAvs = 260,
        AccountNotEligibleForDivisionSetup = 261,
        AuthorizationCodeResponseDateInvalid = 262,
        PartialAuthorizationNotAllowedOrPartialAuthorizationRequestNoteValid = 263,
        DuplicateDepositTransaction = 264,
        MissingQhpAmount = 265,
        InvalidQhpAmount = 266,
        TransactionNotSupported = 274,
        IssuerUnavailable = 301,
        CreditFloor = 302,
        ProcessorDecline = 303,
        NotOnFile = 304,
        AlreadyReversed = 305,
        AmountMismatch = 306,
        AuthorizationNotFound = 307,
        TransArmorServiceUnavailable = 351,
        ExpiredLock = 352,
        TransArmorInvalidTokenOrPan = 353,
        TransArmorInvalidResult = 354,
        BadRequest = 400,
        Call = 401,
        DefaultCall = 402,
        Pickup = 501,
        LostStolen = 502,
        FraudSecurityViolation = 503,
        NegativeFile = 505,
        ExcessivePinTry = 508,
        OverTheLimit = 509,
        OverLimitFrequency = 510,
        OnNegativeFile = 519,
        InsufficientFunds = 521,
        CardIsExpired = 522,
        AlteredData = 524,
        DoNotHonor = 530,
        Cvv2VakFailure = 531,
        DoNotHonorHighFraud = 534,
        StopPaymentOrderOneTimeRecurring = 570,
        RevocationOfAuthorizationForAllRecurring = 571,
        RevocationOfAllAuthorizations = 572,
        AccountPreviouslyActivated = 580,
        UnableToVoid = 581,
        BlockActivationFailed = 582,
        BlockActivationFailedMin = 583,
        IssuanceDoesNotMeetMinimumAmount = 584,
        NoOriginalAuthorizationFound = 585,
        OutstandingAuthorizationFundsOnHold = 586,
        ActivationAmountIncorrect = 587,
        BlockActivationFailedFutureUse = 588,
        MaximumRedemptionLimitMet = 590,
        InvalidCcNumberProblem = 591,
        BadAmountZero = 592,
        OtherErrorUnidentifiable = 594,
        NewCardIssued = 595,
        SuspectedFraud = 596,
        RefundNotAllowed = 599,
        InvalidInstitutionCode = 602,
        InvalidInstitution = 603,
        InvalidExpirationDate = 605,
        InvalidTransactionType = 606,
        InvalidAmountIssue = 607,
        BinBlock = 610,
        FpoAccepted = 704,
        MatchFailed = 740,
        ValidationFailed = 741,
        InvalidTransitRoutingNumber = 750,
        TransitRoutingNumberUnknown = 751,
        MissingName = 752,
        InvalidAccountType = 753,
        AccountClosed = 754,
        SubscriberNumberDoesNotExist = 755,
        SubscriberNumberNotActive = 758,
        AchNonParticipant = 760,
        DuplicateCheck = 776,
        OriginalTransactionWasNotApproved = 777,
        RejectedCode3 = 787,
        RefundRrPartialAmountIsGreaterThenOriginalSaleAmount = 788,
        PositiveID = 802,
        Restraint = 806,
        InvalidSecurityCode = 811,
        InvalidPin = 813,
        NoAccount = 825,
        InvalidMerchant = 833,
        UnauthorizedUser = 834,
        ProcessUnavailable = 902,
        InvalidExpiration = 903,
        InvalidEffective = 904,
        AcquirerError = 997,
        DccUnavailable = 998
    }

    /// <summary>
    /// 
    /// </summary>
    public InternetSecureAPI()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary>
    /// SendUserInformation2BOARestService
    /// </summary>
    /// <param name="productPrice"></param>
    /// <param name="productID"></param>
    /// <param name="productName"></param>
    /// <param name="name"></param>
    /// <param name="company"></param>
    /// <param name="address"></param>
    /// <param name="city"></param>
    /// <param name="state"></param>
    /// <param name="zipCode"></param>
    /// <param name="country"></param>
    /// <param name="phone"></param>
    /// <param name="email"></param>
    /// <param name="cardNumber"></param>
    /// <param name="ccMonth"></param>
    /// <param name="ccYear"></param>
    /// <param name="cvv"></param>
    /// <param name="currency"></param>
    /// <param name="test"></param>
    /// <param name="exactID"></param>
    /// <param name="password"></param>
    /// <param name="token"></param>
    /// <param name="merchant"></param>
    /// <returns></returns>
    public static string SendUserInformation2BoaRestService(string productPrice, string productID, string productName, string name, string company, string address, string city, string state, string zipCode, string country, string phone, string email, string cardNumber, string ccMonth, string ccYear, string cvv, string currency, bool test, string exactID, string password, ref string token, Merchant merchant, string websiteBookingID = "", string code = "")
    {
		ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		
        var xml_string = String.Empty;
        var success = string.Empty;
        var result = new TransactionResult();
        var trans = new Transaction();

        trans.ExactID = exactID;
        trans.Password = password;
        trans.Transaction_Type = "00";
        trans.Card_Number = cardNumber;
        trans.CardHoldersName = name;
        trans.DollarAmount = productPrice;

        trans.Expiry_Date = ccMonth + ccYear;
        trans.Client_Email = email;
        trans.Reference_No = productID;
        trans.Reference_3 = company;
        trans.Currency = currency;
        trans.Customer_Ref = productName;

        if (!cvv.ToLower().Contains("american"))
        {
            trans.StoredCredentials = new StoredCredentialsType()
            {
                Indicator = cvv.ToLower().Contains("master") ? "S" : "1",
                Initiation = "M",
                Schedule = "S",
                TransactionId = "new"
            };
        }

        XmlHelper.Serialize(trans, ref xml_string);

        # region OldCode
        //var string_builder = new StringBuilder();
        //using (var string_writer = new StringWriter(string_builder))
        //{
        //    using (var xml_writer = new XmlTextWriter(string_writer))
        //    {
        //        //build XML string 
        //        xml_writer.Formatting = Formatting.Indented;
        //        xml_writer.WriteStartElement("Transaction");
        //        xml_writer.WriteElementString("ExactID", "OB5066-26");//Gateway ID
        //        xml_writer.WriteElementString("Password", "jh9rht2q685m01bzfn63b96yz4q1y30y");//Password
        //        xml_writer.WriteElementString("Transaction_Type", "00");
        //        xml_writer.WriteElementString("DollarAmount", productPrice);
        //        xml_writer.WriteElementString("Expiry_Date", ccMonth + ccYear);
        //        xml_writer.WriteElementString("CardHoldersName", name);
        //        xml_writer.WriteElementString("Client_Email", email);
        //        xml_writer.WriteElementString("Reference_No", productPrice + "::" + "1::" + productID);
        //        xml_writer.WriteElementString("Reference_3", company);
        //        xml_writer.WriteElementString("Currency", currency);
        //        xml_writer.WriteElementString("Customer_Ref", productName);
        //        xml_writer.WriteElementString("Card_Number", cardNumber);
        //        xml_writer.WriteEndElement();
        //    }
        //}

        //string xml_string = string_builder.ToString();
        # endregion 

        //SHA1 hash on XML string
        ASCIIEncoding encoder = new ASCIIEncoding();
        byte[] xml_byte = encoder.GetBytes(xml_string);
        SHA1CryptoServiceProvider sha1_crypto = new SHA1CryptoServiceProvider();
        string hash = BitConverter.ToString(sha1_crypto.ComputeHash(xml_byte)).Replace("-", "");
        string hashed_content = hash.ToLower();

        //assign values to hashing and header variables
        string keyID = DatabaseConnection.GetHashID(merchant); //key ID
        string key = DatabaseConnection.GetHashPassword(merchant); //Hmac key
        string method = "POST\n";
        string type = "application/xml";//REST XML
        string time = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
        string uri = "/transaction/v27";
        string hash_data = method + type + "\n" + hashed_content + "\n" + time + "\n" + uri;
        //hmac sha1 hash with key + hash_data
        HMAC hmac_sha1 = new HMACSHA1(Encoding.UTF8.GetBytes(key)); //key
        byte[] hmac_data = hmac_sha1.ComputeHash(Encoding.UTF8.GetBytes(hash_data)); //data
        //base64 encode on hmac_data
        string base64_hash = Convert.ToBase64String(hmac_data);
        string url = test ? "https://api.demo.globalgatewaye4.firstdata.com" + uri : "https://api.globalgatewaye4.firstdata.com" + uri; //DEMO or LIVE Endpoint

        //begin HttpWebRequest 
        HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(url);
        web_request.Method = "POST";
        web_request.ContentType = type;
        web_request.Accept = "*/*";
        web_request.Headers.Add("x-gge4-date", time);
        web_request.Headers.Add("x-gge4-content-sha1", hashed_content);
        web_request.Headers.Add("Authorization", "GGE4_API " + keyID + ":" + base64_hash);
        web_request.ContentLength = xml_string.Length;

        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

        // write and send request data 
        /* using (StreamWriter stream_writer = new StreamWriter(web_request.GetRequestStream()))
        {
            stream_writer.Write(xml_string);
        } */
        
		// write and send request data  (new way which eliminate problem with special characters
		using (Stream stream_writer = web_request.GetRequestStream())
		{
			stream_writer.Write(xml_byte, 0, xml_byte.Length);				
		}

        //get response and read into string
        string response_string = String.Empty;
        try
        {
            using (var web_response = (HttpWebResponse)web_request.GetResponse())
            {
                using (var response_stream = new StreamReader(web_response.GetResponseStream()))
                {
                    response_string = response_stream.ReadToEnd();
                }

                //load xml
                //XmlDocument xmldoc = new XmlDocument();
                //xmldoc.LoadXml(response_string);

                result = XmlHelper.Deserialize<TransactionResult>(response_string);

                if (result.EXact_Resp_Code == "00")
                {
                    if (result.Bank_Resp_Code == "100")
                    {
                        success = result.Transaction_Tag;
                        token = result.TransarmorToken;
                    }
                    else
                    {
                        success = result.Bank_Resp_Code;
                    }
					
					EmailHelper.SendUserPaymentDetailsFirstDataChargeAdmin(result, state, websiteBookingID, code);
					
                }
                else
                {
                    success = result.EXact_Resp_Code;
                }

                //EmailHelper.SendUserPaymentDetailsFirstDataChargeAdmin(result, state);
                
                //output raw XML for debugging
                //var request_label = "<b>Request</b><br />" + web_request.Headers.ToString() + System.Web.HttpUtility.HtmlEncode(xml_string);
                //var response_label = "<b>Response</b><br />" + web_response.Headers.ToString() + System.Web.HttpUtility.HtmlEncode(response_string);
            }
        }
        //read stream for remote error response
        catch (WebException ex)
        {
            if (ex.Response != null)
            {
                using (var error_response = (HttpWebResponse)ex.Response)
                {
                    using (var reader = new StreamReader(error_response.GetResponseStream()))
                    {
                        string remote_ex = reader.ReadToEnd();
                        var codeError = (int)error_response.StatusCode;
                        success = codeError.ToString();
                    }
                }
            }
        }

        return success;
    }

    /// <summary>
    /// CheckCreditCardAndGetToken
    /// </summary>
    /// <param name="productPrice"></param>
    /// <param name="name"></param>
    /// <param name="cardNumber"></param>
    /// <param name="cardType"></param>
    /// <param name="ccMonth"></param>
    /// <param name="ccYear"></param>
    /// <param name="test"></param>
    /// <param name="exactID"></param>
    /// <param name="password"></param>
    /// <param name="merchant"></param>
    /// <returns></returns>
    public static string CheckCreditCardAndGetToken(string productPrice, string name, string cardNumber, string cardType, string ccMonth, string ccYear, bool test, string exactID, string password, Merchant merchant)
    {
		ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		
        var xml_string = String.Empty;
        var success = string.Empty;

        var string_builder = new StringBuilder();
        using (var string_writer = new StringWriter(string_builder))
        {
            using (var xml_writer = new XmlTextWriter(string_writer))
            {
                //build XML string 
                xml_writer.Formatting = Formatting.Indented;
                xml_writer.WriteStartElement("Transaction");
                xml_writer.WriteElementString("ExactID", exactID);//Gateway ID
                xml_writer.WriteElementString("Password", password);//Password
                xml_writer.WriteElementString("Transaction_Type", "05");
                xml_writer.WriteElementString("DollarAmount", "0");
                xml_writer.WriteElementString("Expiry_Date", ccMonth + ccYear);
                xml_writer.WriteElementString("CardHoldersName", name);
                xml_writer.WriteElementString("Card_Number", cardNumber);
                xml_writer.WriteElementString("TransarmorToken", "");
                xml_writer.WriteElementString("CardType", cardType);
                xml_writer.WriteEndElement();
            }
        }

        xml_string = string_builder.ToString();

       

        //SHA1 hash on XML string
        ASCIIEncoding encoder = new ASCIIEncoding();
        byte[] xml_byte = encoder.GetBytes(xml_string);
        SHA1CryptoServiceProvider sha1_crypto = new SHA1CryptoServiceProvider();
        string hash = BitConverter.ToString(sha1_crypto.ComputeHash(xml_byte)).Replace("-", "");
        string hashed_content = hash.ToLower();

        //assign values to hashing and header variables
        string keyID = DatabaseConnection.GetHashID(merchant); //key ID
        string key = DatabaseConnection.GetHashPassword(merchant); //Hmac key
        string method = "POST\n";
        string type = "application/xml";//REST XML
        string time = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
        string uri = "/transaction/v27";
        string hash_data = method + type + "\n" + hashed_content + "\n" + time + "\n" + uri;
        //hmac sha1 hash with key + hash_data
        HMAC hmac_sha1 = new HMACSHA1(Encoding.UTF8.GetBytes(key)); //key
        byte[] hmac_data = hmac_sha1.ComputeHash(Encoding.UTF8.GetBytes(hash_data)); //data
        //base64 encode on hmac_data
        string base64_hash = Convert.ToBase64String(hmac_data);
        string url = test ? "https://api.demo.globalgatewaye4.firstdata.com" + uri : "https://api.globalgatewaye4.firstdata.com" + uri; //DEMO or LIVE Endpoint

        //begin HttpWebRequest 
        HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(url);
        web_request.Method = "POST";
        web_request.ContentType = type;
        web_request.Accept = "*/*";
        web_request.Headers.Add("x-gge4-date", time);
        web_request.Headers.Add("x-gge4-content-sha1", hashed_content);
        web_request.Headers.Add("Authorization", "GGE4_API " + keyID + ":" + base64_hash);
        web_request.ContentLength = xml_string.Length;
		//web_request.Timeout = 10;		
		
        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

        try
        {
            // write and send request data 
            /* using (StreamWriter stream_writer = new StreamWriter(web_request.GetRequestStream()))
            {
                stream_writer.Write(xml_string);
            } */
			
			// write and send request data  (new way which eliminate problem with special characters
			using (Stream stream_writer = web_request.GetRequestStream())
			{
				stream_writer.Write(xml_byte, 0, xml_byte.Length);				
			}
        }
        catch(Exception)
        {
            // write and send request data 
            using (StreamWriter stream_writer = new StreamWriter(web_request.GetRequestStream()))
            {
                stream_writer.Write(xml_string);
            }
        }

        //get response and read into string
        string response_string = String.Empty;
        try
        {
            using (var web_response = (HttpWebResponse)web_request.GetResponse())
            {
                using (var response_stream = new StreamReader(web_response.GetResponseStream()))
                {
                    response_string = response_stream.ReadToEnd();
                }

                //load xml
                //XmlDocument xmldoc = new XmlDocument();
                //xmldoc.LoadXml(response_string);

                var result = XmlHelper.Deserialize<TransactionResult>(response_string);

                if (result.EXact_Resp_Code == "00")
                {
                    if (result.Bank_Resp_Code == "100")
                    {
                        success = result.TransarmorToken;
                    }
                    else
                    {
                        success = result.Bank_Resp_Code;
                    }
                }
                else
                {
                    success = result.EXact_Resp_Code;
                }

                //output raw XML for debugging
                //var request_label = "<b>Request</b><br />" + web_request.Headers.ToString() + System.Web.HttpUtility.HtmlEncode(xml_string);
                //var response_label = "<b>Response</b><br />" + web_response.Headers.ToString() + System.Web.HttpUtility.HtmlEncode(response_string);
            }
        }
        //read stream for remote error response
        catch (WebException ex)
        {
            if (ex.Response != null)
            {   
                using (var error_response = (HttpWebResponse)ex.Response)
                {
                    using (var reader = new StreamReader(error_response.GetResponseStream()))
                    {
                        string remote_ex = reader.ReadToEnd();
                        var code = (int)error_response.StatusCode;
                        success = code.ToString();
						
						using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
                        {
                            //Save Transaction to Log
                            sw.WriteLine(success + ", " + System.DateTime.Now.ToString() + ", PA ERROR" );
                        }	
                    }
                }
            }
        }

        return success;
    }

    /// <summary>
    /// SendUserInformation2BoaRestServiceTokenVersion
    /// </summary>
    /// <param name="productPrice"></param>
    /// <param name="name"></param>
    /// <param name="cardNumber"></param>
    /// <param name="cardType"></param>
    /// <param name="ccMonth"></param>
    /// <param name="ccYear"></param>
    /// <param name="test"></param>
    /// <param name="exactID"></param>
    /// <param name="password"></param>
    /// <param name="merchant"></param>
    /// <returns></returns>
    public static string SendUserInformation2BoaRestServiceTokenVersion(string productID, string productName, string productPrice, string name, string cardNumber, string cardType, string ccMonth, string ccYear, bool test, string exactID, string password, Merchant merchant)
    {
        var xml_string = String.Empty;
        var success = string.Empty;

        var string_builder = new StringBuilder();
        using (var string_writer = new StringWriter(string_builder))
        {
            using (var xml_writer = new XmlTextWriter(string_writer))
            {
                //build XML string 
                xml_writer.Formatting = Formatting.Indented;
                xml_writer.WriteStartElement("Transaction");
                xml_writer.WriteElementString("ExactID", exactID);//Gateway ID
                xml_writer.WriteElementString("Password", password);//Password
                xml_writer.WriteElementString("Transaction_Type", "00");
                xml_writer.WriteElementString("DollarAmount", productPrice);
                xml_writer.WriteElementString("Expiry_Date", ccMonth + ccYear);
                xml_writer.WriteElementString("CardHoldersName", name);
                xml_writer.WriteElementString("TransarmorToken", cardNumber);
                xml_writer.WriteElementString("CardType", cardType);
                xml_writer.WriteElementString("Reference_No", productID);
                xml_writer.WriteElementString("Customer_Ref", productName);

                if (!cardType.ToLower().Contains("american"))
                {
                    xml_writer.WriteStartElement("StoredCredentials");
                    xml_writer.WriteElementString("Indicator", cardType.ToLower().Contains("master") ? "S" : "1");
                    xml_writer.WriteElementString("Initiation", "M");
                    xml_writer.WriteElementString("Schedule", "S");
                    xml_writer.WriteElementString("TransactionId", "new");
                    xml_writer.WriteEndElement();
                }

                xml_writer.WriteEndElement();
            }
        }

        xml_string = string_builder.ToString();

        //SHA1 hash on XML string
        ASCIIEncoding encoder = new ASCIIEncoding();
        byte[] xml_byte = encoder.GetBytes(xml_string);
        SHA1CryptoServiceProvider sha1_crypto = new SHA1CryptoServiceProvider();
        string hash = BitConverter.ToString(sha1_crypto.ComputeHash(xml_byte)).Replace("-", "");
        string hashed_content = hash.ToLower();

        //assign values to hashing and header variables
        string keyID = DatabaseConnection.GetHashID(merchant); //key ID
        string key = DatabaseConnection.GetHashPassword(merchant); //Hmac key
        string method = "POST\n";
        string type = "application/xml";//REST XML
        string time = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
        string uri = "/transaction/v27";
        string hash_data = method + type + "\n" + hashed_content + "\n" + time + "\n" + uri;
        //hmac sha1 hash with key + hash_data
        HMAC hmac_sha1 = new HMACSHA1(Encoding.UTF8.GetBytes(key)); //key
        byte[] hmac_data = hmac_sha1.ComputeHash(Encoding.UTF8.GetBytes(hash_data)); //data
        //base64 encode on hmac_data
        string base64_hash = Convert.ToBase64String(hmac_data);
        string url = test ? "https://api.demo.globalgatewaye4.firstdata.com" + uri : "https://api.globalgatewaye4.firstdata.com" + uri; //DEMO or LIVE Endpoint

        //begin HttpWebRequest 
        HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(url);
        web_request.Method = "POST";
        web_request.ContentType = type;
        web_request.Accept = "*/*";
        web_request.Headers.Add("x-gge4-date", time);
        web_request.Headers.Add("x-gge4-content-sha1", hashed_content);
        web_request.Headers.Add("Authorization", "GGE4_API " + keyID + ":" + base64_hash);
        web_request.ContentLength = xml_string.Length;

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

        // write and send request data 
        /* using (StreamWriter stream_writer = new StreamWriter(web_request.GetRequestStream()))
        {
            stream_writer.Write(xml_string);
        } */
		
		// write and send request data  (new way which eliminate problem with special characters
		using (Stream stream_writer = web_request.GetRequestStream())
		{
			stream_writer.Write(xml_byte, 0, xml_byte.Length);				
		}

        //get response and read into string
        string response_string = String.Empty;
        try
        {
            using (var web_response = (HttpWebResponse)web_request.GetResponse())
            {
                using (var response_stream = new StreamReader(web_response.GetResponseStream()))
                {
                    response_string = response_stream.ReadToEnd();
                }

                //load xml
                //XmlDocument xmldoc = new XmlDocument();
                //xmldoc.LoadXml(response_string);

                var result = XmlHelper.Deserialize<TransactionResult>(response_string);

                if (result.EXact_Resp_Code == "00")
                {
                    if (result.Bank_Resp_Code == "100")
                    {
                        success = result.Transaction_Tag;
                    }
                    else
                    {
                        success = result.Bank_Resp_Code;
                    }
					
					EmailHelper.SendUserPaymentDetailsFirstDataChargeAdmin(result);
                }
                else
                {
                    success = result.EXact_Resp_Code;
                }

				//EmailHelper.SendUserPaymentDetailsFirstDataChargeAdmin(result);
                //EmailHelper.SendUserPaymentDetailsFirstData(result);
            }
        }
        //read stream for remote error response
        catch (WebException ex)
        {
            if (ex.Response != null)
            {
                using (var error_response = (HttpWebResponse)ex.Response)
                {
                    using (var reader = new StreamReader(error_response.GetResponseStream()))
                    {
                        string remote_ex = reader.ReadToEnd();
                        var code = (int)error_response.StatusCode;
                        success = code.ToString();                        
                    }
                }
            }
        }

        return success;
    }


    /// <summary>
    /// SendUserInformation2PayeezyRestServiceTokenVersionDcc
    /// </summary>
    /// <param name="productPrice"></param>
    /// <param name="productID"></param>
    /// <param name="productName"></param>
    /// <param name="name"></param>
    /// <param name="cardType"></param>
    /// <param name="ccMonth"></param>
    /// <param name="ccYear"></param>
    /// <param name="test"></param>
    /// <param name="exactID"></param>
    /// <param name="password"></param>
    /// <param name="token"></param>
    /// <param name="merchant"></param>
    /// <returns></returns>
    public static string SendUserInformation2PayeezyRestServiceTokenVersionDcc(string productID, string productName, string productPrice, string name, string rateResponseID, string token, string cardType, string ccMonth, string ccYear, bool test, string exactID, string password, Merchant merchant)
    {
        var xml_string = String.Empty;
        var success = string.Empty;
        var result = new TransactionResult();
        var trans = new Transaction();

        trans.ExactID = exactID;
        trans.Password = password;
        trans.Transaction_Type = "00";
        trans.TransarmorToken = token;
        trans.CardType = cardType;
        trans.CardHoldersName = name;
        trans.DollarAmount = productPrice;
        trans.Expiry_Date = ccMonth + ccYear;
        trans.Reference_No = productID + "/" + productName;
        trans.Customer_Ref = productName;

        if (!cardType.ToLower().Contains("american"))
        {
            trans.StoredCredentials = new StoredCredentialsType()
            {
                Indicator = cardType.ToLower().Contains("master") ? "S" : "1",
                Initiation = "M",
                Schedule = "S",
                TransactionId = "new"
            };
        }

        var dc = new DynamicCurrency();
        dc.OptedIn = "true";
        dc.RateResponseSignature = rateResponseID;
        trans.DynamicCurrency = dc;

        XmlHelper.Serialize(trans, ref xml_string);

        //SHA1 hash on XML string
        ASCIIEncoding encoder = new ASCIIEncoding();
        byte[] xml_byte = encoder.GetBytes(xml_string);
        SHA1CryptoServiceProvider sha1_crypto = new SHA1CryptoServiceProvider();
        string hash = BitConverter.ToString(sha1_crypto.ComputeHash(xml_byte)).Replace("-", "");
        string hashed_content = hash.ToLower();

        //assign values to hashing and header variables
        string keyID = DatabaseConnection.GetHashID(merchant); //key ID
        string key = DatabaseConnection.GetHashPassword(merchant); //Hmac key
        string method = "POST\n";
        string type = "application/xml"; //REST XML
        string time = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
        string uri = "/transaction/v27";
        string hash_data = method + type + "\n" + hashed_content + "\n" + time + "\n" + uri;
        //hmac sha1 hash with key + hash_data
        HMAC hmac_sha1 = new HMACSHA1(Encoding.UTF8.GetBytes(key)); //key
        byte[] hmac_data = hmac_sha1.ComputeHash(Encoding.UTF8.GetBytes(hash_data)); //data
        //base64 encode on hmac_data
        string base64_hash = Convert.ToBase64String(hmac_data);
        string url = test ? "https://api.demo.globalgatewaye4.firstdata.com" + uri : "https://api.globalgatewaye4.firstdata.com" + uri; //DEMO or LIVE Endpoint

        //begin HttpWebRequest 
        HttpWebRequest web_request = (HttpWebRequest) WebRequest.Create(url);
        web_request.Method = "POST";
        web_request.ContentType = type;
        web_request.Accept = "*/*";
        web_request.Headers.Add("x-gge4-date", time);
        web_request.Headers.Add("x-gge4-content-sha1", hashed_content);
        web_request.Headers.Add("Authorization", "GGE4_API " + keyID + ":" + base64_hash);
        web_request.ContentLength = xml_string.Length;

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

        // write and send request data 
        /* using (StreamWriter stream_writer = new StreamWriter(web_request.GetRequestStream()))
        {
            stream_writer.Write(xml_string);
        } */

		// write and send request data  (new way which eliminate problem with special characters
		using (Stream stream_writer = web_request.GetRequestStream())
		{
			stream_writer.Write(xml_byte, 0, xml_byte.Length);				
		}
		
        //get response and read into string
        string response_string = String.Empty;
        try
        {
            using (var web_response = (HttpWebResponse) web_request.GetResponse())
            {
                using (var response_stream = new StreamReader(web_response.GetResponseStream()))
                {
                    response_string = response_stream.ReadToEnd();
                }

                result = XmlHelper.Deserialize<TransactionResult>(response_string);

                if (result.EXact_Resp_Code == "00")
                {
                    if (result.Bank_Resp_Code == "100")
                    {
                        success = result.Transaction_Tag;
                    }
                    else
                    {
                        success = result.Bank_Resp_Code;
                    }
					
					EmailHelper.SendUserPaymentDetailsFirstDataDcc(result);
					
                }
                else
                {
                    success = result.EXact_Resp_Code;
                }

                //EmailHelper.SendUserPaymentDetailsFirstDataDcc(result);

            }
        }
            //read stream for remote error response
        catch (WebException ex)
        {
            if (ex.Response != null)
            {
                using (var error_response = (HttpWebResponse) ex.Response)
                {
                    using (var reader = new StreamReader(error_response.GetResponseStream()))
                    {
                        string remote_ex = reader.ReadToEnd();
                        success = "10";
                    }
                }
            }
        }

        return success;
    }

    public static CardRate PayeezyDccCardRateRequest(bool test, Merchant merchant, string cc6D, string amount)
    {
        String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(DatabaseConnection.GetGatewayID(merchant) + ":" + DatabaseConnection.GetGatewayPassword(merchant)));

        string url = test ? "https://api.demo.globalgatewaye4.firstdata.com/exchange_rates/v1/card_rate" : "https://api.globalgatewaye4.firstdata.com/exchange_rates/v1/card_rate"; //DEMO or LIVE Endpoint


        var string_builder = new StringBuilder();

        using (var string_writer = new StringWriter(string_builder))
        {
            using (var xml_writer = new XmlTextWriter(string_writer))
            {
                //build XML string 
                xml_writer.Formatting = Formatting.Indented;
                xml_writer.WriteStartElement("CardRate");
                xml_writer.WriteElementString("Bin", cc6D);

                if (amount.Length > 0)
                {
                    xml_writer.WriteElementString("Amount", amount);
                }

                xml_writer.WriteEndElement();
            }
        }
        
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        byte[] bytes;
        bytes = System.Text.Encoding.UTF8.GetBytes(string_builder.ToString());
        request.ContentType = "application/xml";
        request.ContentLength = bytes.Length;
        request.Method = "POST";
        request.Headers.Add("Authorization", "Basic " + encoded);
        //request.Accept = "application/xml";
        
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();

        HttpWebResponse response;
        response = (HttpWebResponse)request.GetResponse();
        if (response.StatusCode == HttpStatusCode.OK)
        {
            Stream responseStream = response.GetResponseStream();
            if (responseStream != null)
            {
                string responseStr = new StreamReader(responseStream).ReadToEnd();

                var cr = XmlHelper.Deserialize<CardRate>(responseStr);

                return cr;
            }
        }
        else if(response.StatusCode == HttpStatusCode.Created)
        {
            Stream responseStream = response.GetResponseStream();
            if (responseStream != null)
            {
                string responseStr = new StreamReader(responseStream).ReadToEnd();

                var crError = XmlHelper.Deserialize<CardRateError.CardRate>(responseStr);
                var cr = new CardRate { BinQualifies = crError.BinQualifies == "true" ? true : false, DccOffered = crError.DccOffered == "true" ? true : false };

                return cr;
            }
        }

        return null;
    }
}