﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for HousingRegistrationAgreement
/// </summary>
[Serializable]
[XmlRoot(ElementName = "IndividualPayment")]
public class IndividualPayment
{
    private string conferenceID;
    private int paymentDetailCCID;
    private int cardTypeID;
    private string creditCardNumber;
    private string ccv;
    private DateTime exparationDate;
    private int userID;
    private string cardHolderName;

    [XmlAttribute(AttributeName = "ConferenceID")]
    public string ConferenceID
    {
        get
        {
            return conferenceID;
        }

        set
        {
            conferenceID = value;
        }
    }

    [XmlAttribute(AttributeName = "PaymentDetailCCID")]
    public int PaymentDetailCCID
    {
        get
        {
            return paymentDetailCCID;
        }

        set
        {
            paymentDetailCCID = value;
        }
    }

    [XmlElement(ElementName = "CardTypeID")]
    public int CardTypeID
    {
        get
        {
            return cardTypeID;
        }

        set
        {
            cardTypeID = value;
        }
    }

    [XmlElement(ElementName = "CreditCardNumber")]
    public string CreditCardNumber
    {
        get
        {
            return creditCardNumber;
        }

        set
        {
            creditCardNumber = value;
        }
    }

    [XmlElement(ElementName = "CCV")]
    public string CCV
    {
        get
        {
            return ccv;
        }

        set
        {
            ccv = value;
        }
    }

    [XmlElement(ElementName = "ExparationDate")]
    public DateTime ExparationDate
    {
        get
        {
            return exparationDate;
        }

        set
        {
            exparationDate = value;
        }
    }

    [XmlElement(ElementName = "UserID")]
    public int UserID
    {
        get
        {
            return userID;
        }

        set
        {
            userID = value;
        }
    }

    [XmlElement(ElementName = "CardHolderName")]
    public string CardHolderName
    {
        get
        {
            return cardHolderName;
        }

        set
        {
            cardHolderName = value;
        }
    }

    public IndividualPayment()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}