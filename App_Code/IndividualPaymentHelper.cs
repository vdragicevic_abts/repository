﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for HousingAgreementHelper
/// </summary>
public class IndividualPaymentHelper : DatabaseConnection
{
    public static bool InsertIndividualPayment(IndividualPayment ip)
    {
        bool success = false;
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertIndividualPayment";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", ip.ConferenceID));
            cmd.Parameters.Add(new SqlParameter("@PaymentDetailCCID", ip.PaymentDetailCCID));
            cmd.Parameters.Add(new SqlParameter("@CardTypeID", ip.CardTypeID));
            cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", StringHelpers.Encrypt(ip.CreditCardNumber)));
            cmd.Parameters.Add(new SqlParameter("@CCV", ip.CCV));
            cmd.Parameters.Add(new SqlParameter("@ExparationDate", ip.ExparationDate));
            cmd.Parameters.Add(new SqlParameter("@UserID", ip.UserID));
            cmd.Parameters.Add(new SqlParameter("@CardHolderName", ip.CardHolderName));

            try
            {
                cmd.ExecuteNonQuery();

                trans.Commit();

                conn.Close();

                success = true;
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
            }
        }

        return success;
    }

    public static string InsertIndividualPaymentInternetSecure(IndividualPaymentInternetSecure ip)
    {
        bool success = false;
        string transactionID = string.Empty;

        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertIndividualPayment";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", ip.ConferenceID));
            cmd.Parameters.Add(new SqlParameter("@PaymentDetailCCID", ip.PaymentDetailCCID));
            cmd.Parameters.Add(new SqlParameter("@CardTypeID", ip.CardTypeID));
            cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", StringHelpers.Encrypt(ip.CreditCardNumber)));
            cmd.Parameters.Add(new SqlParameter("@CCV", ip.CCV));
            cmd.Parameters.Add(new SqlParameter("@ExparationDate", ip.ExparationDate));
            cmd.Parameters.Add(new SqlParameter("@UserID", ip.UserID));
            cmd.Parameters.Add(new SqlParameter("@CardHolderName", ip.CardHolderName));


            try
            {
                cmd.ExecuteNonQuery();

                transactionID = InternetSecureAPI.SendUserInformation2Evalon(ip.GatewayID.ToString(), ip.ProductPrice, ip.ProductCode, ip.ProductDescription, ip.CardHolderName, ip.Province, ip.Address, ip.City, "", ip.Postal, ip.Country, ip.Phone, ip.Email, ip.CreditCardNumber, ip.ExparationDate.Month.ToString(), ip.ExparationDate.Year.ToString(), ip.CCV, ip.Currency, ip.Test);


                if (!transactionID.Contains("900"))
                {
                    trans.Commit();
                    conn.Close();

                    success = true;
                }
                else
                {
                    trans.Rollback();
                    conn.Close();

                    success = false;
                }

            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
            }
        }

        //if (success)
        //{
        //    transactionID = InternetSecureAPI.SendUserInformation2Evalon(ip.GatewayID.ToString(), ip.ProductPrice, ip.ProductCode, ip.ProductDescription, ip.CardHolderName, ip.Province, ip.Address, ip.City, "", ip.Postal, ip.Country, ip.Phone, ip.Email, ip.CreditCardNumber, ip.ExparationDate.Month.ToString(), ip.ExparationDate.Year.ToString(), ip.CCV, ip.Currency, ip.Test);
        //}

        return transactionID;
    }

    public static string InsertIndividualPaymentBOA(IndividualPaymentInternetSecure ip)
    {
        bool success = false;

        string transactionID = string.Empty;

        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertIndividualPayment";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", ip.ConferenceID));
            cmd.Parameters.Add(new SqlParameter("@PaymentDetailCCID", ip.PaymentDetailCCID));
            cmd.Parameters.Add(new SqlParameter("@CardTypeID", ip.CardTypeID));
            cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", StringHelpers.Encrypt("000000000000" + ip.CreditCardNumber.Substring(ip.CreditCardNumber.Length - 4)))); //ip.CardTypeID != 3 ? StringHelpers.Encrypt("000000000000" + ip.CreditCardNumber.Substring(ip.CreditCardNumber.Length - 4)) : StringHelpers.Encrypt("00000000000" + ip.CreditCardNumber.Substring(ip.CreditCardNumber.Length - 4)))); //StringHelpers.Encrypt(ip.CreditCardNumber)));
            cmd.Parameters.Add(new SqlParameter("@CCV", DBNull.Value));
            cmd.Parameters.Add(new SqlParameter("@ExparationDate", ip.ExparationDate));
            cmd.Parameters.Add(new SqlParameter("@UserID", ip.UserID));
            cmd.Parameters.Add(new SqlParameter("@CardHolderName", ip.CardHolderName));
			
			//Dodati Email u svakom slucaju kao vid evidencije
            cmd.Parameters.Add(new SqlParameter("@Email", ip.Email));
            //Dodati Address u svakom slucaju kao vid evidencije
            cmd.Parameters.Add(new SqlParameter("@Address", ip.Address));

            var pID = new SqlParameter("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(pID);


            try
            {
                cmd.ExecuteNonQuery();

                string id = pID.Value.ToString();

                string token = String.Empty;

                if (!ip.MerchantAccount.ToString().Contains("PP"))
                {
                    transactionID = InternetSecureAPI.SendUserInformation2BoaRestService(ip.ProductPrice, ip.Province, ip.ProductDescription, ip.CardHolderName, ip.Province, ip.Address, ip.City, ip.GuestName, ip.Postal, ip.Country, ip.Phone, ip.Email, ip.CreditCardNumber, ip.ExparationDate.Month.ToString().PadLeft(2, '0'), ip.ExparationDate.ToString("yy"), ip.CCV, ip.Currency, ip.Test, ip.ExactID, ip.Password, ref token, ip.MerchantAccount, ip.WebsiteBookingID, ip.Code);
                }
                else
                {
					ip.MerchantAccount = (ip.MerchantAccount.ToString().Contains("PP") && ip.CardType.ToLower().Contains("american express")) ? (Merchant)Enum.Parse(typeof(Merchant), ip.MerchantAccount.ToString() + "_Amex") : ip.MerchantAccount;
                    
					//Create Wallet Account
                    transactionID = new Planet.PlanetAPI(ip.MerchantAccount).CheckCreditCardAndGetTokenPlanetServiceIndividual(ip.CardHolderName, ip.Address, ip.Country, ip.City, ip.StateName, ip.Postal, ip.CreditCardNumber, Planet.Currency.USD, ip.ExparationDate.Month.ToString().PadLeft(2, '0'), ip.ExparationDate.ToString("yy"), ip.CCV, ip.Test, ref token, ip.MerchantAccount);

                    if (!transactionID.Contains("Error"))
                    {
					    //Sleep 3-5 sec
                        System.Threading.Thread.Sleep(3000);
						
                        token = transactionID;
                        //Charge Account using Wallet AccountID
                        transactionID = new Planet.PlanetAPI(ip.MerchantAccount).SendUserInformation2PlanetTokenVersion(token, ip.ProductPrice, ip.ProductCode, ip.ProductDescription, Planet.Currency.USD, ip.Test, ip.MerchantAccount, ref token, Planet.CurrencyIndicator.DomesticTransaction, ip.CardType, ip.GuestName, ip.WebsiteBookingID, ip.Code, ip.Province, ip.ProductDescription, ip.Province);
                    }
                }

                if (transactionID.Length > 3 && !transactionID.Contains("Error"))
                {
                    trans.Commit();
                    conn.Close();

                    UpdateIndividualPaymentBoa(id, transactionID, token);

                    success = true;
                }
                else
                {
                    trans.Rollback();
                    conn.Close();

                    success = false;

                    //transactionID = "900";
                }

            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
				
				using (System.IO.StreamWriter sw = System.IO.File.AppendText("C:\\CustomLogs\\Log.txt"))
                {
                    //Save Transaction to Log
                    sw.WriteLine(ip.ConferenceID + ", " + ip.CardHolderName + ", " + System.DateTime.Now.ToString() + ", IP ERROR " + ex.Message);
                }

                transactionID = "40";
            }
        }

        return transactionID;
    }

    public static IndividualPayment SelectIndividualPayment(string conferenceID, int paymentDetailCCID, int userID)
    {
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "dbo.SelectIndividualPayment";


        cmd.Parameters.Add(new SqlParameter("@ConferenceID", conferenceID));
        cmd.Parameters.Add(new SqlParameter("@PaymentDetailCCID", paymentDetailCCID));
        cmd.Parameters.Add(new SqlParameter("@UserID", userID));

        DataTable dtIndividualPayment = new DataTable();
        IndividualPayment ip = new IndividualPayment();

        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dtIndividualPayment);

            foreach (DataRow dr in dtIndividualPayment.Rows)
            {
                ip.ConferenceID = dr["conferenceID"].ToString();
                ip.CardTypeID = int.Parse(dr["cardTypeID"].ToString());
                ip.PaymentDetailCCID = int.Parse(dr["paymentDetailCCID"].ToString());
                ip.CreditCardNumber = dr["creditCardNumber"].ToString();
                ip.ExparationDate = DateTime.Parse(dr["exparationDate"].ToString());
                ip.CCV = dr["ccv"].ToString();
                ip.CardHolderName = dr["cardHolderName"].ToString();
                ip.UserID = int.Parse(dr["userID"].ToString());
            }
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }

        return ip;
    }

    public static string InsertSWMEPaymentBOA(IndividualPaymentInternetSecure ip)
    {
        bool success = false;

        string transactionID = string.Empty;

        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertSWMEPayment";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", ip.ConferenceID));
            cmd.Parameters.Add(new SqlParameter("@PaymentDetailCCID", ip.PaymentDetailCCID));
            cmd.Parameters.Add(new SqlParameter("@CardTypeID", ip.CardTypeID));
            cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", DBNull.Value));
            cmd.Parameters.Add(new SqlParameter("@CCV", DBNull.Value));
            cmd.Parameters.Add(new SqlParameter("@ExparationDate", ip.ExparationDate));
            cmd.Parameters.Add(new SqlParameter("@CardHolderName", ip.CardHolderName));
            cmd.Parameters.Add(new SqlParameter("@Email", ip.Email));
            cmd.Parameters.Add(new SqlParameter("@SessionID", ip.ProductDescription));
            cmd.Parameters.Add(new SqlParameter("@Amount", Decimal.Parse(ip.ProductPrice)));

            var pID = new SqlParameter("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(pID);

            try
            {
                cmd.ExecuteNonQuery();

                string id = pID.Value.ToString();

                transactionID = InternetSecureAPI.SendUserInformation2BOA(ip.ProductPrice, ip.ProductCode, ip.ProductDescription, ip.CardHolderName, ip.Province, ip.Address, ip.City, "", ip.Postal, ip.Country, ip.Phone, ip.Email, ip.CreditCardNumber, ip.ExparationDate.Month.ToString().PadLeft(2, '0'), ip.ExparationDate.ToString("yy"), ip.CCV, ip.Currency, ip.Test, ip.ExactID, ip.Password);

                if (transactionID.Length > 2)
                {
                    trans.Commit();
                    conn.Close();

                    UpdateSwmeRegistrationPaymentBoa(id, transactionID);

                    success = true;
                }
                else
                {
                    trans.Rollback();
                    conn.Close();

                    success = false;
                }

            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
            }
        }

        return transactionID;
    }

    public static void UpdateSwmeRegistrationPaymentBoa(string id, string transactionID)
    {
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = @"Update SWMEPayment
                          Set transactionID = @TransactionID
                          Where id = @ID";

        cmd.Parameters.Add(new SqlParameter("@TransactionID", transactionID));
        cmd.Parameters.Add(new SqlParameter("@ID", id));


        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
    }

    public static void UpdateIndividualPaymentBoa(string id, string transactionID, string token)
    {
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = @"Update IndividualPayment
                          Set transactionID = @TransactionID, token = @Token --, creditCardNumber = @CreditCardNumber
                          Where id = @ID";

        cmd.Parameters.Add(new SqlParameter("@TransactionID", transactionID));
        cmd.Parameters.Add(new SqlParameter("@ID", id));
        cmd.Parameters.Add(new SqlParameter("@Token", token));
        //Updating credit card number with the token - PCI Compliance
        //cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", StringHelpers.Encrypt(token)));
        
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
    }


    public IndividualPaymentHelper()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}