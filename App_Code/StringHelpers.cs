﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using System.Web.UI;



public class StringHelpers
{
    #region Constants

    private const char QUERY_STRING_DELIMITER = '&';

    #endregion Constants

    #region Members

    private static RijndaelManaged _cryptoProvider;
    //128 bit encyption: DO NOT CHANGE    
    private static readonly byte[] Key = {18, 19, 8, 24, 36, 22, 4, 22, 17, 5, 11, 9, 13, 15, 06, 23};
    private static readonly byte[] IV = {14, 2, 16, 7, 5, 9, 17, 8, 4, 47, 16, 12, 1, 32, 25, 18};

    #endregion Members

    #region Constructor

    static StringHelpers()
    {
        _cryptoProvider = new RijndaelManaged();
        _cryptoProvider.Mode = CipherMode.CBC;
        _cryptoProvider.Padding = PaddingMode.PKCS7;
    }

    #endregion Constructor

    #region Methods

    /// <summary>
    /// DecodeFrom64
    /// </summary>
    /// <param name="encodedData"></param>
    /// <returns></returns>
    public static string DecodeFrom64(string encodedData)
    {

        byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);

        string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

        return returnValue;

    }

    /// <summary>
    /// DecodeTo64
    /// </summary>
    /// <param name="decodeData"></param>
    /// <returns></returns>
    public static string DecodeTo64(string decodeData)
    {
        byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(decodeData);

        string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);

        return returnValue;
    }

    /// <summary>
    /// Encrypts a given string.
    /// </summary>
    /// <param name="unencryptedString">Unencrypted string</param>
    /// <returns>Returns an encrypted string</returns>
    public static string Encrypt(string unencryptedString)
    {
        byte[] bytIn = ASCIIEncoding.ASCII.GetBytes(unencryptedString);

        // Create a MemoryStream
        MemoryStream ms = new MemoryStream();

        // Create Crypto Stream that encrypts a stream
        CryptoStream cs = new CryptoStream(ms,
            _cryptoProvider.CreateEncryptor(Key, IV),
            CryptoStreamMode.Write);

        // Write content into MemoryStream
        cs.Write(bytIn, 0, bytIn.Length);
        cs.FlushFinalBlock();

        byte[] bytOut = ms.ToArray();
        return Convert.ToBase64String(bytOut);
    }

    /// <summary>
    /// Decrypts a given string.
    /// </summary>
    /// <param name="encryptedString">Encrypted string</param>
    /// <returns>Returns a decrypted string</returns>
    public static string Decrypt(string encryptedString)
    {
        if (encryptedString.Trim().Length != 0)
        {
            //Dodato zbog problema sa praznim poljem prilikom konverzije u Base64 to String
            encryptedString = encryptedString.Replace(" ", "+");

            // Convert from Base64 to binary
            byte[] bytIn = Convert.FromBase64String(encryptedString);

            // Create a MemoryStream
            MemoryStream ms = new MemoryStream(bytIn, 0, bytIn.Length);

            // Create a CryptoStream that decrypts the data
            CryptoStream cs = new CryptoStream(ms,
                _cryptoProvider.CreateDecryptor(Key, IV),
                CryptoStreamMode.Read);

            // Read the Crypto Stream
            StreamReader sr = new StreamReader(cs);

            return sr.ReadToEnd();
        }
        else
        {
            return "";
        }
    }

    public static NameValueCollection DecryptQueryString(string queryString)
    {
        if (queryString.Length != 0)
        {
            //Decode the string
            string decodedQueryString = HttpUtility.UrlDecode(queryString);

            //Decrypt the string
            string decryptedQueryString = StringHelpers.Decrypt(decodedQueryString);

            //Now split the string based on each parameter
            string[] actionQueryString = decryptedQueryString.Split(new char[] {QUERY_STRING_DELIMITER});

            NameValueCollection newQueryString = new NameValueCollection();

            //loop around for each name value pair.
            for (int index = 0; index < actionQueryString.Length; index++)
            {
                string[] queryStringItem = actionQueryString[index].Split(new char[] {'='});
                newQueryString.Add(queryStringItem[0], queryStringItem[1]);
            }

            return newQueryString;
        }
        else
        {
            //No query string was passed in.
            return null;
        }
    }

    public static string EncryptQueryString(NameValueCollection queryString)
    {
        //create a string for each value in the query string passed in.
        string tempQueryString = "";

        for (int index = 0; index < queryString.Count; index++)
        {
            tempQueryString += queryString.GetKey(index) + "=" + queryString[index];
            if (index != queryString.Count - 1)
            {
                tempQueryString += QUERY_STRING_DELIMITER;
            }
        }

        return EncryptQueryString(tempQueryString);
    }

    /// <summary>
    /// You must pass in a string that uses the QueryStringHelper.DELIMITER as the delimiter.
    /// This will also append the "?" to the beginning of the query string.
    /// </summary>
    /// <param name="queryString"></param>
    /// <returns></returns>
    public static string EncryptQueryString(string queryString)
    {
        return "?" + HttpUtility.UrlEncode(StringHelpers.Encrypt(queryString));
    }

    /// <summary>
    /// Generate token for security purpose
    /// It expires in a certain time period and should be renewed
    /// </summary>
    /// <returns></returns>
    public static string GetWebServiceAuthKey()
    {
        string guid = Guid.NewGuid().ToString();

        string connectionString = DatabaseConnection.GetConnectionString();
        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString);

        try
        {
            conn.Open();

            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "InsertWebServiceAuthKey";
            cmd.Parameters.AddWithValue("@AuthKey", guid);
            cmd.ExecuteNonQuery();

            conn.Close();

            return StringHelpers.Encrypt(guid);
        }
        catch (Exception ex)
        {
            HttpContext.Current.Session["ERROR"] = ex.Message;

            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }

            return "400";
        }

    }

    public static bool CheckWebServiceAuthKeyValidity(string token)
    {        
        string connectionString = DatabaseConnection.GetConnectionString();
        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString);

        try
        {
            string _token = String.Empty;
            DateTime? _expDate = null;
            DateTime? _currentDate = null;

            conn.Open();

            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select AuthKey, DateExpired, GETUTCDATE() Date " +
                              "From WebServiceAuthKeys " +
                              "Where AuthKey='" + token + "' " +
                              "And IsActive=1 and (DateExpired >= GETUTCDATE())";

            System.Data.SqlClient.SqlDataReader MyReader;
            MyReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            while (MyReader.Read())
            {
                _token = MyReader["AuthKey"].ToString();
                _expDate = DateTime.Parse(MyReader["DateExpired"].ToString());
                _currentDate = DateTime.Parse(MyReader["Date"].ToString());
            }

            if(_token.Length > 0 && _expDate != null)
            {
                if(_expDate >= _currentDate)
                {
                    return true;
                }
                else
                {
                    return false;
                } 
            }
            else
            {
                return false;
            }                         
        }
        catch (Exception ex)
        {
            HttpContext.Current.Session["ERROR"] = ex.Message;

            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }

            return false;
        }

    }
    #endregion Methods
}
