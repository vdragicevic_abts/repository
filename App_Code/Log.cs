﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Web.UI;

/// <summary>
/// Summary description for Log
/// </summary>
public static class Log
{
    public static void InsertLog4Groups(string conferenceID, int housingAgreementID, int userID, string action)
    {
        Thread threadLog = new Thread(() => InsertAction2Log4Groups(conferenceID, housingAgreementID, userID, action));
        threadLog.Start();
    }

    private static void InsertAction2Log4Groups(string conferenceID, int housingAgreementID, int userID, string action)
    {

        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertLog";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", conferenceID));
            cmd.Parameters.Add(new SqlParameter("@HaID", housingAgreementID));
            cmd.Parameters.Add(new SqlParameter("@PDetailCCID", DBNull.Value));
            cmd.Parameters.Add(new SqlParameter("@UserID", userID));
            cmd.Parameters.Add(new SqlParameter("@Action", action));
           
            try
            {
                cmd.ExecuteNonQuery();

                trans.Commit();

                conn.Close();               
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
            }
        }                                      
    }

    public static void InsertLog4Individuals(string conferenceID, int paymentDetailCCID, string action)
    {
        Thread threadLog = new Thread(() => InsertAction2Log4Individuals(conferenceID, paymentDetailCCID, action));
        threadLog.Start();
    }

    private static void InsertAction2Log4Individuals(string conferenceID, int paymentDetailCCID, string action)
    {
        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertLog";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", conferenceID));
            cmd.Parameters.Add(new SqlParameter("@HaID", DBNull.Value));
            cmd.Parameters.Add(new SqlParameter("@PDetailCCID", paymentDetailCCID));
            cmd.Parameters.Add(new SqlParameter("@UserID", DBNull.Value));
            cmd.Parameters.Add(new SqlParameter("@Action", action));

            try
            {
                cmd.ExecuteNonQuery();

                trans.Commit();

                conn.Close();
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
            }
        }      
    }
}