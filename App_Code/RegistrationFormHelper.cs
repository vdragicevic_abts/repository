﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for HousingAgreementHelper
/// </summary>
public class RegistrationFormHelper : DatabaseConnection
{
    public static bool InsertRegistrationForm(RegistrationForm rf)
    {
        bool success = false;
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertRegistrationFormExpanded";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", rf.ConferenceID));
            cmd.Parameters.Add(new SqlParameter("@RfID", rf.RfID));
            cmd.Parameters.Add(new SqlParameter("@CardTypeID", rf.CardTypeID));
            cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", "############-" + rf.CreditCardNumber.Substring(rf.CreditCardNumber.Length - 4)));

            cmd.Parameters.Add(new SqlParameter("@CCV", rf.State));  

            if (rf.CardholderName != null)
            {
                cmd.Parameters.Add(new SqlParameter("@CardholderName", rf.CardholderName));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@CardholderName", DBNull.Value));
            }

            cmd.Parameters.Add(new SqlParameter("@ExparationDate", rf.ExparationDate));
            cmd.Parameters.Add(new SqlParameter("@UserID", rf.UserID));
            cmd.Parameters.Add(new SqlParameter("@Address", rf.Address));
            cmd.Parameters.Add(new SqlParameter("@Postal", rf.Postal));
            cmd.Parameters.Add(new SqlParameter("@Phone", rf.Phone));
            cmd.Parameters.Add(new SqlParameter("@City", rf.City));
            cmd.Parameters.Add(new SqlParameter("@Country", rf.Country));
            cmd.Parameters.Add(new SqlParameter("@Province", rf.ProposalInfo));
           
            try
            {
                cmd.ExecuteNonQuery();

                trans.Commit();

                conn.Close();

                success = true;
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
            }
        }

        return success;
    }

    public static RegistrationForm SelectRegistrationForm(string conferenceID, int rfID, int userID)
    {        
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "dbo.SelectRegistrationForm";


        cmd.Parameters.Add(new SqlParameter("@ConferenceID", conferenceID));
        cmd.Parameters.Add(new SqlParameter("@RfID", rfID));
        cmd.Parameters.Add(new SqlParameter("@UserID", userID));

        DataTable dtRegistrationForm = new DataTable();
        RegistrationForm rf = new RegistrationForm();

        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dtRegistrationForm);

            foreach (DataRow dr in dtRegistrationForm.Rows)
            {
                rf.ConferenceID = dr["conferenceID"].ToString();
                rf.CardTypeID = int.Parse(dr["cardTypeID"].ToString());
                rf.RfID = int.Parse(dr["rfID"].ToString());
                rf.CreditCardNumber = dr["creditCardNumber"].ToString();
                rf.ExparationDate = DateTime.Parse(dr["exparationDate"].ToString());
                rf.UserID = int.Parse(dr["userID"].ToString());
                rf.CCV = dr["ccv"].ToString();
                rf.CardholderName = dr["cardholderName"].ToString();
            }
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }           
        }

        return rf;
    }           
    
    
    public RegistrationFormHelper()
    {
	//
	// TODO: Add constructor logic here
	//
    }
}