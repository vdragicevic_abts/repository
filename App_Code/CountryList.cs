﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

public class CountryList
{
    private readonly CultureInfo[] cinfo;
    private readonly List<RegionInfo> reginfo;

    public CountryList()
    {
        cinfo = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
        reginfo = new List<RegionInfo>();        
    }

    private void GetCountryList()
    {
        reginfo.Clear();

        foreach (CultureInfo c in cinfo)
        {
            RegionInfo r = new RegionInfo(c.LCID);
            reginfo.Add(r);
        }        
    }

    public RegionInfo GetRegionInfoByName(string name)
    {
        name = name.ToLower();
        GetCountryList();
        return reginfo.Where(r => r.EnglishName.ToLower().Contains(name)).FirstOrDefault();
    }   
}