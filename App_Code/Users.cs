﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Net;

/// <summary>
/// Summary description for MyWebService
/// </summary>
[System.Web.Script.Services.ScriptService()]
[ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
[WebService(Namespace = "http://www.abtscs.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ToolboxItem(false)]
public class Users : System.Web.Services.WebService
{

    public User CurrentUser;

    public Users()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public bool SendHAToRepositoryRQ(HousingRegistrationAgreement hra)
    {

        bool success = false;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Save HousingRegistrationAgreement data
            HousingAgreementHelper.InsertHousingAgreement(hra);

            success = true;
        }

        return success;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public bool SendHAToRepositoryExpandedRQ(HousingRegistrationAgreementExpanded hra)
    {

        bool success = false;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Save HousingRegistrationAgreement data
            HousingAgreementHelper.InsertHousingAgreementExpanded(hra);

            success = true;
        }

        return success;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public HousingRegistrationAgreement SendHAToRepositoryRS(string conferenceID, int housingAgreementID, int userID)
    {
        HousingRegistrationAgreement hra = null;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Load HousingRegistrationAgreement data
            hra = HousingAgreementHelper.SelectHousingAgreement(conferenceID, housingAgreementID, userID);

            Log.InsertLog4Groups(conferenceID, housingAgreementID, userID, "SendHAToRepositoryRS");
        }

        return hra;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public bool SendIPToRepositoryRQ(IndividualPayment ip)
    {
        bool success = false;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Save Individual Booking data
            success = IndividualPaymentHelper.InsertIndividualPayment(ip);
        }

        return success;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public string SendIPToRepositoryInternetSecureRQ(IndividualPaymentInternetSecure ip)
    {
        string transactionID = string.Empty;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Save Individual Booking data
            transactionID = IndividualPaymentHelper.InsertIndividualPaymentInternetSecure(ip);
        }

        return transactionID;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public string SendIPToRepositoryBOARQ(IndividualPaymentInternetSecure ip)
    {
        string transactionID = string.Empty;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Save Individual Booking data
            transactionID = IndividualPaymentHelper.InsertIndividualPaymentBOA(ip);
        }

        return transactionID;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public IndividualPayment SendIPToRepositoryRS(string conferenceID, int paymentDetailCCID, int userID)
    {

        IndividualPayment ip = null;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Load Individual Booking data
            ip = IndividualPaymentHelper.SelectIndividualPayment(conferenceID, paymentDetailCCID, userID);

            Log.InsertLog4Individuals(conferenceID, paymentDetailCCID, "SendHAToRepositoryRS");
        }

        return ip;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public DataTable GetConferencesRS()
    {
        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Select distinct conferenceID from HousingAgreement";

        DataTable dtConferences = new DataTable();

        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        try
        {
            adapter.Fill(dtConferences);

            return dtConferences;
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }

            return dtConferences;
        }
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public DataTable GetVisaTypesRS()
    {

        string connectionString = DatabaseConnection.GetConnectionString();

        SqlConnection conn = new SqlConnection(connectionString);

        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Select id, Name from CardType"; //where Name not like 'American%'";
        cmd.Connection = conn;

        SqlDataAdapter adapter = new SqlDataAdapter(cmd);

        DataTable dtCardTypes = new DataTable();
        adapter.Fill(dtCardTypes);

        try
        {
            return dtCardTypes;
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }

            return dtCardTypes;
        }
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public bool SendPAToRepositoryRQ(PaymentAuthorization pa)
    {
        bool success = false;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Save Payment Authorization data
            success = PaymentAuthorizationHelper.InsertPaymentAuthorization(pa);
        }

        return success;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public bool SendPAToRepositoryExpandedRQ(PaymentAuthorizationExpanded pa)
    {
        bool success = false;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Save Payment Authorization data
            success = PaymentAuthorizationHelper.InsertPaymentAuthorizationExpanded(pa);
        }

        return success;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public string SendPAToRepositoryFirstDataRQ(PaymentAuthorizationFirstData pa)
    {
        bool success = false;
        string transactionID = "";

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Save Payment Authorization data
            transactionID = PaymentAuthorizationHelper.InsertPaymentAuthorizationFirstData(pa);
        }

        return transactionID;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public PaymentAuthorization SendPAToRepositoryRS(string invoiceID)
    {
        PaymentAuthorization pa = null;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Load Payment Authorization data
            pa = PaymentAuthorizationHelper.SelectPaymentAuthorization(invoiceID);

            //Log.InsertLog4Individuals(conferenceID, paymentDetailCCID, "SendHAToRepositoryRS");
        }

        return pa;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public PaymentAuthorizationExpanded SendPAToRepositoryExpandedRS(string invoiceID)
    {
        PaymentAuthorizationExpanded pa = null;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Load Payment Authorization data
            pa = PaymentAuthorizationHelper.SelectPaymentAuthorizationExpanded(invoiceID);

            //Log.InsertLog4Individuals(conferenceID, paymentDetailCCID, "SendHAToRepositoryRS");
        }

        return pa;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public List<PaymentAuthorizationExpanded> SendPAToRepositoryExpandedRSList(string invoiceID)
    {
        List<PaymentAuthorizationExpanded> pa = null;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Load Payment Authorization data
            pa = PaymentAuthorizationHelper.SelectPaymentAuthorizationExpandedList(invoiceID);

            //Log.InsertLog4Individuals(conferenceID, paymentDetailCCID, "SendHAToRepositoryRS");
        }

        return pa;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public DataTable repositoryHistoryReport(DateTime dateFrom, DateTime dateTo, string formType)
    {
        DataTable dtRepositoryHistoryReport = null;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Load Payment Authorization data
            dtRepositoryHistoryReport = PaymentAuthorizationHelper.RepositoryHistoryReport(dateFrom, dateTo, formType);

            //Log.InsertLog4Individuals(conferenceID, paymentDetailCCID, "SendHAToRepositoryRS");
        }

        return dtRepositoryHistoryReport;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public bool SendRFToRepositoryRQ(RegistrationForm rf)
    {

        bool success = false;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Save RegistrationForm data
            RegistrationFormHelper.InsertRegistrationForm(rf);

            success = true;
        }

        return success;
    }

    [WebMethod]
    [SoapHeader("CurrentUser", Direction = SoapHeaderDirection.In)]
    public RegistrationForm SendRFToRepositoryRS(string conferenceID, int registrationFormID, int userID)
    {
        RegistrationForm rf = null;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Load RegistrationForm data
            rf = RegistrationFormHelper.SelectRegistrationForm(conferenceID, registrationFormID, userID);

            Log.InsertLog4Groups(conferenceID, registrationFormID, userID, "SendRFToRepositoryRS");
        }

        return rf;
    }

    public string SendSWMEToRepositoryBOARQ(IndividualPaymentInternetSecure ip)
    {
        string transactionID = string.Empty;

        if (CurrentUser == null)
        {
            throw new SoapHeaderException("Authentication deatils not found!", SoapException.ClientFaultCode);
        }

        if (CurrentUser.UserID == "Admin" && CurrentUser.Password == "password")
        {
            //Save Individual Booking data
            transactionID = IndividualPaymentHelper.InsertSWMEPaymentBOA(ip);
        }

        return transactionID;
    }
	
    public Merchant? GetMerchantIDFromABTSolute(string conference)
    {
        string conferenceName = String.Empty;

        if (conference.Contains("Overflow"))
        {
            conferenceName = "2019 AAN-Overflow";
        }
		else if(conference == "2020_SBDIA")
        {
            conferenceName = "DIA 2020 SB";
        }
		else if(conference.Substring(0,4).All(char.IsDigit))
        {
            conferenceName = conference;
        }				        
        else
        {
            conferenceName = conference.Substring(conference.Length - 4) + " " + conference.Substring(0, conference.Length - 4).Replace("_", " ");
        }
        
        ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        Merchant merchant;
        //Get Merchant Terminal Info From ABTSolute
        HotelRooms.ABTSoluteWebServices ser = new HotelRooms.ABTSoluteWebServices();
        var merchantInfo = ser.GetMerchantAccountInfoByConferenceName(conferenceName, "9DD873E8-3465-4426-B498-38EA9589090E");

        int merchantID = merchantInfo != null && merchantInfo.Rows.Count > 0 ? int.Parse(merchantInfo.Rows[0]["merchantID"].ToString()) : 0;

        if (merchantID > 0)
        {
            merchant = (Merchant)merchantID;

            return merchant;
        }
        else
        {
            return null;
        }
    }
}
