﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using iTextSharp.text.pdf;

/// <summary>
/// Summary description for HousingAgreementHelper
/// </summary>
public class PaymentAuthorizationHelper : DatabaseConnection
{
    public static bool InsertPaymentAuthorization(PaymentAuthorization pa)
    {
        bool success = false;
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertPaymentAuthorization";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", DBNull.Value));
            cmd.Parameters.Add(new SqlParameter("@InvoiceID", pa.InvoiceID));
            cmd.Parameters.Add(new SqlParameter("@EventName", pa.EventName));
            cmd.Parameters.Add(new SqlParameter("@Email", pa.Email));
            cmd.Parameters.Add(new SqlParameter("@CardTypeID", pa.CardTypeID));
            cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", StringHelpers.Encrypt(pa.CreditCardNumber)));
            cmd.Parameters.Add(new SqlParameter("@ProposalID", pa.ProposalID));
            cmd.Parameters.Add(new SqlParameter("@ExparationDate", pa.ExparationDate));
            cmd.Parameters.Add(new SqlParameter("@CardholderName", pa.CardholderName));
            cmd.Parameters.Add(new SqlParameter("@Preparer", pa.Preparer));
                       
            try
            {
                cmd.ExecuteNonQuery();

                trans.Commit();

                conn.Close();

                success = true;
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
            }
        }

        return success;
    }

    public static bool InsertPaymentAuthorizationExpanded(PaymentAuthorizationExpanded pa)
    {
        bool success = false;
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertPaymentAuthorizationExpanded";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", DBNull.Value));
            cmd.Parameters.Add(new SqlParameter("@InvoiceID", pa.InvoiceID));
            cmd.Parameters.Add(new SqlParameter("@EventName", pa.EventName));
            cmd.Parameters.Add(new SqlParameter("@Email", pa.Email));
            cmd.Parameters.Add(new SqlParameter("@CardTypeID", pa.CardTypeID));

            //Save Token instead of the credit card number
            cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", pa.Merchant >= 6 ? StringHelpers.Encrypt("000000000000" + pa.CreditCardNumber.Substring(pa.CreditCardNumber.Length - 4)) : StringHelpers.Encrypt(pa.CreditCardNumber.Substring(0, 6) + "0000000000")));  //StringHelpers.Encrypt(pa.CreditCardNumber.Substring(0, 6) + "0000000000"))); // pa.DCC ? StringHelpers.Encrypt(pa.CreditCardNumber.Substring(0, 6) + "0000000000") : StringHelpers.Encrypt(pa.Token)));
            cmd.Parameters.Add(new SqlParameter("@ProposalID", pa.ProposalID));
            cmd.Parameters.Add(new SqlParameter("@ExparationDate", pa.ExparationDate));
            cmd.Parameters.Add(new SqlParameter("@CardholderName", pa.CardholderName));
            cmd.Parameters.Add(new SqlParameter("@Preparer", pa.Preparer));
            
            if (pa.BillingAddress != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Address", pa.BillingAddress));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Address", DBNull.Value));
            }

            if (pa.Phone != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Phone", pa.Phone));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Phone", DBNull.Value));
            }

            if (pa.City != null)
            {
                cmd.Parameters.Add(new SqlParameter("@City", pa.City));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@City", DBNull.Value));
            }

            if (pa.Zip != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Zip", pa.Zip));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Zip", DBNull.Value));
            }

            if (pa.Country != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Country", pa.Country));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Country", DBNull.Value));
            }

            if (pa.State != null)
            {
                cmd.Parameters.Add(new SqlParameter("@State", pa.State));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@State", DBNull.Value));
            }

            if (pa.PaymentInstructions != null)
            {
                cmd.Parameters.Add(new SqlParameter("@PaymentInstructions", pa.PaymentInstructions));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@PaymentInstructions", DBNull.Value));
            }

            try
            {
                string cc = pa.CreditCardNumber.Length < 16 ? pa.CreditCardNumber.Substring(11) : pa.CreditCardNumber.Substring(12);
                cmd.Parameters.Add(new SqlParameter("@CC", cc));
            }
            catch (Exception ex)
            {
                cmd.Parameters.Add(new SqlParameter("@CC", DBNull.Value));
            }

            if (pa.DescriptionOfServices != null)
            {
                cmd.Parameters.Add(new SqlParameter("@DescriptionOfServices", pa.DescriptionOfServices));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@DescriptionOfServices", DBNull.Value));
            }

            if (pa.Token != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Token", StringHelpers.Encrypt(pa.Token)));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Token", DBNull.Value));
            }

            if (pa.DCC)
            {
                cmd.Parameters.Add(new SqlParameter("@Dcc", pa.DCC));
            }

            if (pa.ExchangeRate != null)
            {
                cmd.Parameters.Add(new SqlParameter("@ExchangeRate", pa.ExchangeRate));
            }

            if (pa.ForeignCurrencySymbolHtml != null)
            {
                cmd.Parameters.Add(new SqlParameter("@ForeignCurrencySymbolHtml", pa.ForeignCurrencySymbolHtml));
            }

            if (pa.RateResponseID != null)
            {
                cmd.Parameters.Add(new SqlParameter("@RateResponseID", pa.RateResponseID));
            }

            if (pa.Merchant >= 0)
            {
                cmd.Parameters.Add(new SqlParameter("@Merchant", pa.Merchant));
            }

            try
            {
                cmd.ExecuteNonQuery();

                trans.Commit();

                conn.Close();

                success = true;
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
            }
        }

        return success;
    }

    public static string InsertPaymentAuthorizationFirstData(PaymentAuthorizationFirstData pa)
    {
        bool success = false;
        string transactionID = string.Empty;

        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();

        using (SqlTransaction trans = conn.BeginTransaction())
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.InsertPaymentAuthorizationFirstData";
            cmd.Transaction = trans;

            cmd.Parameters.Add(new SqlParameter("@ConferenceID", DBNull.Value));
            cmd.Parameters.Add(new SqlParameter("@InvoiceID", pa.InvoiceID));
            cmd.Parameters.Add(new SqlParameter("@EventName", pa.EventName));
            cmd.Parameters.Add(new SqlParameter("@Email", pa.Email));
            cmd.Parameters.Add(new SqlParameter("@CardTypeID", pa.CardTypeID));
            //Save only last four digits and later update with the token - PCI Compliance
            cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", StringHelpers.Encrypt("000000000000" + pa.CreditCardNumber.Substring(pa.CreditCardNumber.Length - 4)))); //StringHelpers.Encrypt("000000000000" + pa.CreditCardNumber.Substring(pa.CreditCardNumber.Length - 4)))); //pa.CardTypeID != 3 ? StringHelpers.Encrypt("000000000000" + pa.CreditCardNumber.Substring(pa.CreditCardNumber.Length - 4)) : StringHelpers.Encrypt("00000000000" + pa.CreditCardNumber.Substring(pa.CreditCardNumber.Length - 4)))); //StringHelpers.Encrypt(pa.CreditCardNumber)));
            cmd.Parameters.Add(new SqlParameter("@ProposalID", pa.ProposalID));
            cmd.Parameters.Add(new SqlParameter("@ExparationDate", pa.ExparationDate));
            cmd.Parameters.Add(new SqlParameter("@CardholderName", pa.CardHolderName));
            cmd.Parameters.Add(new SqlParameter("@Preparer", pa.Preparer));

            if (pa.Address != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Address", pa.Address));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Address", DBNull.Value));
            }

            if (pa.Phone != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Phone", pa.Phone));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Phone", DBNull.Value));
            }

            if (pa.City != null)
            {
                cmd.Parameters.Add(new SqlParameter("@City", pa.City));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@City", DBNull.Value));
            }

            if (pa.Postal != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Zip", pa.Postal));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Zip", DBNull.Value));
            }

            if (pa.Country != null)
            {
                cmd.Parameters.Add(new SqlParameter("@Country", pa.Country));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Country", DBNull.Value));
            }

            if (pa.Province != null)
            {
                cmd.Parameters.Add(new SqlParameter("@State", pa.Province));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@State", DBNull.Value));
            }

            if (pa.PaymentInstructions != null)
            {
                cmd.Parameters.Add(new SqlParameter("@PaymentInstructions", pa.PaymentInstructions));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@PaymentInstructions", DBNull.Value));
            }

            if (pa.DescriptionOfServices != null)
            {
                cmd.Parameters.Add(new SqlParameter("@DescriptionOfServices", pa.DescriptionOfServices));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@DescriptionOfServices", DBNull.Value));
            }

            try
            {
                string cc = pa.CreditCardNumber.Length < 16 ? pa.CreditCardNumber.Substring(11) : pa.CreditCardNumber.Substring(12);
                cmd.Parameters.Add(new SqlParameter("@CC", cc));
            }
            catch (Exception ex)
            {
                cmd.Parameters.Add(new SqlParameter("@CC", DBNull.Value));
            }  
            
            if(pa.MerchantAccount > 0)
            {
                cmd.Parameters.Add(new SqlParameter("@Merchant", pa.MerchantAccount));
            }

            SqlParameter pID = new SqlParameter("@ID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(pID);

            try
            {
                cmd.ExecuteNonQuery();

                string id = pID.Value.ToString();

                Log.InsertLog4Individuals(pa.EventName, pa.PaymentDetailCCID, "Price: " + pa.ProductPrice + " ProductCode: " + pa.ProductCode + " CardholderName: " + pa.CardHolderName + " Email:" + pa.Email + " Phone: " + pa.Phone + " ProposalID: " + pa.ProposalID + " Preparer: " + pa.Preparer);
                
                string token = String.Empty;

                if (pa.MerchantAccount.ToString().Contains("PP"))
                {
                    transactionID = new Planet.PlanetAPI(pa.MerchantAccount).SendUserInformation2PlanetRestService(pa.ProductPrice, pa.ProductCode, pa.ProductDescription, pa.CardHolderName, pa.ConferenceID, pa.Address, pa.City, pa.Province, pa.Postal, pa.Country, pa.Phone, pa.Email, pa.CreditCardNumber, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), pa.CCV, pa.CardType, Planet.Currency.USD, pa.Test, ref token, pa.MerchantAccount, "", pa.GatewayID);

                    Log.InsertLog4Individuals(pa.EventName, pa.PaymentDetailCCID, "TransactionID: " + transactionID);

                    if (token.Length > 2)
                    {
                        trans.Commit();
                        conn.Close();

                        UpdatePaymentAuthorizationFirstData(id, token, pa.ProductPrice, StringHelpers.Encrypt(token));
                        //transactionID = token;
                    }
                    else
                    {
                        trans.Rollback();
                        conn.Close();
						
						transactionID = "Error " + transactionID;
                        //transactionID = "Transaction Error - Invalid " + transactionID;
                    }
                }
                else
                {
                    transactionID = InternetSecureAPI.SendUserInformation2BoaRestService(pa.ProductPrice, pa.ProductCode, pa.ProductDescription, pa.CardHolderName, pa.Province, pa.Address, pa.City, pa.GuestName, pa.Postal, pa.Country, pa.Phone, pa.Email, pa.CreditCardNumber, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), pa.CCV, pa.Currency, pa.Test, pa.ExactID, pa.Password, ref token, pa.MerchantAccount, "", pa.GatewayID);

                    Log.InsertLog4Individuals(pa.EventName, pa.PaymentDetailCCID, "TransactionID: " + transactionID);

                    if (transactionID.Length > 3)
                    {
                        trans.Commit();
                        conn.Close();

                        UpdatePaymentAuthorizationFirstData(id, transactionID, pa.ProductPrice, StringHelpers.Encrypt(token));
                    }
                    else
                    {
                        trans.Rollback();
                        conn.Close();
                    }

                }                              
            }
            catch (Exception ex)
            {
                if (conn.State == ConnectionState.Open)
                {
                    trans.Rollback();
                    conn.Close();
                }
            }
        }

        return transactionID;
    }

    public static void UpdatePaymentAuthorizationFirstData(string id, string transactionID, string amount, string token)
    {
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        

        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = @"Update PaymentAuthorization
                            Set transactionID = @TransactionID, token = @Token, amount = @Amount --, creditCardNumber = @CreditCardNumber
                            Where id = @ID";

        cmd.Parameters.Add(new SqlParameter("@TransactionID", transactionID));
        cmd.Parameters.Add(new SqlParameter("@ID", id));
        cmd.Parameters.Add(new SqlParameter("@Token", token));
        cmd.Parameters.Add(new SqlParameter("@Amount", amount));
        //Updating credit card number with the token - PCI Compliance
        //cmd.Parameters.Add(new SqlParameter("@CreditCardNumber", token));

        

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
    }

    public static void UpdatePaymentAuthorizationWithAmountFirstData(string id, string transactionID, string amount, string token, Merchant? merchant, string exchangeRate = null, string foreignCurrencySymbolHtml = null, string rateResponseId = null)
    {
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = @"Update PaymentAuthorization
                            Set transactionID = @TransactionID
                                , amount = @Amount
                                , token = @Token
                                , merchant = @Merchant
                                --, creditCardNumber = @Token
                                , exchangeRate = @ExchangeRate
                                , foreignCurrencySymbolHtml = @ForeignCurrencySymbolHtml
                                , rateResponseId = @RateResponseID 
                          Where id = @ID";

        cmd.Parameters.Add(new SqlParameter("@TransactionID", transactionID));
        cmd.Parameters.Add(new SqlParameter("@Amount", amount));
        cmd.Parameters.Add(new SqlParameter("@ID", id));

        if (token.Length > 0)
        {
            cmd.Parameters.Add(new SqlParameter("@Token", StringHelpers.Encrypt(token)));
        }
        else
        {
            cmd.Parameters.Add(new SqlParameter("@Token", DBNull.Value));
        }

        if (merchant != null)
        {
            cmd.Parameters.Add(new SqlParameter("@Merchant", (int)merchant));
        }
        else
        {
            cmd.Parameters.Add(new SqlParameter("@Merchant", DBNull.Value));
        }

        if (exchangeRate != null)
        {
            cmd.Parameters.Add(new SqlParameter("@ExchangeRate", exchangeRate));
        }
        else
        {
            cmd.Parameters.Add(new SqlParameter("@ExchangeRate", DBNull.Value));
        }

        if (foreignCurrencySymbolHtml != null)
        {
            cmd.Parameters.Add(new SqlParameter("@ForeignCurrencySymbolHtml", foreignCurrencySymbolHtml));
        }
        else
        {
            cmd.Parameters.Add(new SqlParameter("@ForeignCurrencySymbolHtml", DBNull.Value));
        }

        if (rateResponseId != null)
        {
            cmd.Parameters.Add(new SqlParameter("@RateResponseID", rateResponseId));
        }
        else
        {
            cmd.Parameters.Add(new SqlParameter("@RateResponseID", DBNull.Value));
        }
        

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
    }

    public static PaymentAuthorization SelectPaymentAuthorization(string invoiceID)
    {        
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "dbo.SelectPaymentAuthorization";
       
        cmd.Parameters.Add(new SqlParameter("@InvoiceID", invoiceID));
        
        DataTable dtHousingAgreement = new DataTable();
        PaymentAuthorization pa = new PaymentAuthorization();

        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dtHousingAgreement);

            foreach (DataRow dr in dtHousingAgreement.Rows)
            {
                pa.ConferenceID = dr["conferenceID"].ToString();
                pa.CardTypeID = int.Parse(dr["cardTypeID"].ToString());
                pa.EventName = dr["eventName"].ToString();
                pa.Email = dr["email"].ToString();
                pa.InvoiceID = dr["invoiceID"].ToString();
                pa.CreditCardNumber = dr["creditCardNumber"].ToString();
                pa.ExparationDate = DateTime.Parse(dr["exparationDate"].ToString());
                pa.ProposalID = dr["proposalID"].ToString();
                pa.CardholderName = dr["cardholderName"].ToString();
                pa.Preparer = dr["preparer"].ToString();
            }
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }           
        }

        return pa;
    }

    public static PaymentAuthorizationExpanded SelectPaymentAuthorizationExpanded(string invoiceID)
    {
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "dbo.SelectPaymentAuthorizationExpanded";

        cmd.Parameters.Add(new SqlParameter("@InvoiceID", invoiceID));

        DataTable dtHousingAgreement = new DataTable();
        PaymentAuthorizationExpanded pa = new PaymentAuthorizationExpanded();

        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dtHousingAgreement);

            foreach (DataRow dr in dtHousingAgreement.Rows)
            {
                pa.ConferenceID = dr["conferenceID"].ToString();
                pa.CardTypeID = int.Parse(dr["cardTypeID"].ToString());
                pa.EventName = dr["eventName"].ToString();
                pa.Email = dr["email"].ToString();
                pa.InvoiceID = dr["invoiceID"].ToString();
                pa.CreditCardNumber = dr["creditCardNumber"].ToString();
                pa.ExparationDate = DateTime.Parse(dr["exparationDate"].ToString());
                pa.ProposalID = dr["proposalID"].ToString();
                pa.CardholderName = dr["cardholderName"].ToString();
                pa.Preparer = dr["preparer"].ToString();
                pa.BillingAddress = dr["billingAddress"].ToString();
                pa.Phone = dr["phone"].ToString();
                pa.City = dr["city"].ToString();
                pa.Zip = dr["zip"].ToString();
                pa.Country = dr["country"].ToString();
                pa.State = dr["state"].ToString();
                pa.PaymentInstructions = dr["paymentInstructions"].ToString();
            }
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }

        return pa;
    }

    public static DataTable RepositoryHistoryReport(DateTime dateFrom, DateTime dateTo, string formType)
    {
        string connectionString = GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();


        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = @"Select * from 
                           (Select 'IP' formType, ip.conferenceID eventName, 'N/A' invoiceID, 'N/A' proposalID, ct.Name cardTypeID, ip.creditCardNumber, ip.exparationDate, ip.cardHolderName, 'N/A' preparer, ip.[timestamp], '1' merchant, ISNULL(ip.transactionID, 'N/A') DepositTransactionID, 'N/A' FinalBalanceTransactionID, CONVERT(nvarchar, ip.paymentDetailCCID) + REPLACE(ip.conferenceID, '20', '') + CONVERT(nvarchar,ip.userID) Code    
                            from dbo.IndividualPayment ip inner join dbo.CardType ct on ip.cardTypeID = ct.id

                            UNION

                            Select 'PA' formType, pa.eventName conferenceID, pa.invoiceID, pa.proposalID, ct.Name cardTypeID, pa.creditCardNumber, pa.exparationDate, isNull(pa.cardholderName, 'n/a') cardHolderName, isNull(pa.preparer, 'n/a') preparer, pa.[timestamp], ISNULL(pa.merchant, -1) merchant, 'N/A' DepositTransactionID, pa.transactionID FinalBalanceTransactionID, pa.proposalID + '/' + pa.eventName Code
                            From dbo.PaymentAuthorization pa inner join dbo.CardType ct on pa.cardTypeID = ct.id) Test

                            Where timestamp >= @DateFrom and timestamp <= @DateTo  
                            Order by timestamp";

        cmd.Parameters.Add(new SqlParameter("@DateFrom", dateFrom));
        cmd.Parameters.Add(new SqlParameter("@DateTo", dateTo));

        DataTable dtRepositoryHistoryReport = new DataTable();
      
        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dtRepositoryHistoryReport);           

            if (formType == "0" || formType == "3" || formType == "4")
            {
                LoadSubmittedHousingAgreements(dateFrom, dateTo, ref dtRepositoryHistoryReport);
            }
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }

        return dtRepositoryHistoryReport;
    }

    public static List<PaymentAuthorizationExpanded> SelectPaymentAuthorizationExpandedList(string invoiceID)
    {
        string connectionString = GetConnectionString();
        var conn = new SqlConnection(connectionString);
        conn.Open();


        var cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "dbo.SelectPaymentAuthorizationExpanded";

        cmd.Parameters.Add(new SqlParameter("@InvoiceID", invoiceID));

        DataTable dtHousingAgreement = new DataTable();
        var paList = new List<PaymentAuthorizationExpanded>();

        try
        {
            var adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dtHousingAgreement);

            foreach (DataRow dr in dtHousingAgreement.Rows)
            {
                var pa = new PaymentAuthorizationExpanded();

                pa.ConferenceID = dr["conferenceID"].ToString();
                pa.CardTypeID = int.Parse(dr["cardTypeID"].ToString());
                pa.EventName = dr["eventName"].ToString();
                pa.Email = dr["email"].ToString();
                pa.InvoiceID = dr["invoiceID"].ToString();
                pa.CreditCardNumber = dr["creditCardNumber"].ToString();
                pa.ExparationDate = DateTime.Parse(dr["exparationDate"].ToString());
                pa.ProposalID = dr["proposalID"].ToString();
                pa.CardholderName = dr["cardholderName"].ToString();
                pa.Preparer = dr["preparer"].ToString();
                pa.BillingAddress = dr["billingAddress"].ToString();
                pa.Phone = dr["phone"].ToString();
                pa.City = dr["city"].ToString();
                pa.Zip = dr["zip"].ToString();
                pa.Country = dr["country"].ToString();
                pa.State = dr["state"].ToString();
                pa.PaymentInstructions = dr["paymentInstructions"].ToString();
                pa.TimeStamp = DateTime.Parse(dr["timestamp"].ToString());

                paList.Add(pa);
            }
        }
        catch (Exception ex)
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }

        return paList;
    }

    public static DataTable LoadSubmittedHousingAgreements(DateTime dateFrom, DateTime dateTo, ref DataTable dtRepositoryHistoryReport)
    {
        string[] strFiles = Directory.GetFiles(@"E:\agreements", "*.pdf", SearchOption.AllDirectories); 

        foreach (string file in strFiles)
        {
            DataRow row = dtRepositoryHistoryReport.NewRow();

            FileInfo fi = new FileInfo(file);
            string name = fi.Name;

            if (fi.Name.Contains("View_"))
                continue;

            DateTime creationDate = fi.LastWriteTime;  //fi.LastWriteTime; //fi.CreationTime; 
            string code = name.Substring(name.LastIndexOf("_") + 1).Replace(".pdf", "");
            string conferenceID = fi.DirectoryName.Substring(fi.DirectoryName.LastIndexOf('\\') + 1);

            row["formType"] = "HA";
            row["timestamp"] = creationDate;
            row["eventName"] = conferenceID;
            row["invoiceID"] = "N/A";
            row["proposalID"] = "N/A";
            row["preparer"] = "N/A";
            row["code"] = code;

            if (creationDate >= dateFrom && creationDate <= dateTo)
            {
                ExtractHousingAgreementContext(code, conferenceID, name, ref row, fi.FullName);

                if (row["exparationDate"].ToString().Length > 0 && row["creditCardNumber"].ToString().Length > 0)
                {
                    dtRepositoryHistoryReport.Rows.Add(row);
                }


            }
        }

        return dtRepositoryHistoryReport;
    }

    public static void ExtractHousingAgreementContext(string code, string conferenceID, string fileName, ref DataRow row, string path)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            PdfReader reader = new PdfReader(path);
            PdfStamper stamper = new PdfStamper(reader, ms);

            if (!path.Contains("View_"))
            {
                try
                {
                    AcroFields pdfFormFields = stamper.AcroFields;
                    AcroFields.Item item = pdfFormFields.GetFieldItem("txtDepositTransactionID");
                    AcroFields.Item item1 = pdfFormFields.GetFieldItem("txtFinalBalanceTransactionID");
                    //AcroFields.Item item3 = pdfFormFields.GetFieldItem("txtToken");
                    AcroFields.Item item4 = pdfFormFields.GetFieldItem("txtMerchant");

                    bool sidenights = bool.Parse(pdfFormFields.GetField("txtSidenights"));

                    if (item == null)
                    {
                        row["DepositTransactionID"] = "N/A";

                    }
                    else
                    {
                        row["DepositTransactionID"] = pdfFormFields.GetField("txtDepositTransactionID") == "0" ? "N/A" : pdfFormFields.GetField("txtDepositTransactionID");
                    }

                    if (item1 == null)
                    {
                        row["FinalBalanceTransactionID"] = "N/A";

                    }
                    else
                    {
                        row["FinalBalanceTransactionID"] = pdfFormFields.GetField("txtFinalBalanceTransactionID") == "0" ? "N/A" : pdfFormFields.GetField("txtFinalBalanceTransactionID");
                    }

                    //else
                    //{
                    //    if (!sidenights)
                    //    {
                    //        row["DepositTransactionID"] = pdfFormFields.GetField("txtDepositTransactionID") == "0" ? "N/A" : pdfFormFields.GetField("txtDepositTransactionID");
                    //        row["DepositAmount"] = pdfFormFields.GetField("txtTotalSD");
                    //    }
                    //    else
                    //    {
                    //        row["DepositTransactionID"] = "N/A";
                    //        row["DepositAmount"] = "0.00";
                    //    }
                    //}

                    //if (item1 == null)
                    //{
                    //    row["FinalBalanceTransactionID"] = "N/A";
                    //    row["FinalBalanceAmount"] = "N/A";
                    //}
                    //else
                    //{
                    //    if (!sidenights)
                    //    {
                    //        row["FinalBalanceTransactionID"] = pdfFormFields.GetField("txtFinalBalanceTransactionID") == "0" ? "N/A" : pdfFormFields.GetField("txtFinalBalanceTransactionID");
                    //        row["FinalBalanceAmount"] = pdfFormFields.GetField("txtTotalPrice");
                    //    }
                    //    else
                    //    {
                    //        row["FinalBalanceTransactionID"] = pdfFormFields.GetField("txtFinalBalanceTransactionID") == "0" ? "N/A" : pdfFormFields.GetField("txtFinalBalanceTransactionID");
                    //        row["FinalBalanceAmount"] = pdfFormFields.GetField("txtTotalAmount");
                    //    }
                    //}

                    row["cardHolderName"] = pdfFormFields.GetField("txtCardholderName");

                    if (pdfFormFields.GetField("txtCardholderName").Length > 0)
                    {
                        row["creditCardNumber"] = pdfFormFields.GetField("txtCardNumber"); //pdfFormFields.GetField("txtCardNumber").Length > 16 ? StringHelpers.Decrypt(pdfFormFields.GetField("txtCardNumber")) : pdfFormFields.GetField("txtCardNumber");
                    }
                    else
                    {
                        row["creditCardNumber"] = "000000000000ERR";
                    }


                    if (!sidenights)
                    {
                        row["formType"] = "HA(Picknights)";
                        //string exparationDtae = (Convert.ToInt32(pdfFormFields.GetField("txtExpMoYr")[0])) <= 9 && (!pdfFormFields.GetField("txtExpMoYr").StartsWith("0")) ? "0" + pdfFormFields.GetField("txtExpMoYr").Split('/')[0] + pdfFormFields.GetField("txtExpMoYr").Split('/')[1] : pdfFormFields.GetField("txtExpMoYr").Split('/')[0] + pdfFormFields.GetField("txtExpMoYr").Split('/')[1];

                        if (pdfFormFields.GetField("txtExpMoYr").Length > 0)
                        {
                            row["exparationDate"] = new DateTime(int.Parse(pdfFormFields.GetField("txtExpMoYr").Split('/')[1].Contains("20") ? pdfFormFields.GetField("txtExpMoYr").Split('/')[1] : "20" + pdfFormFields.GetField("txtExpMoYr").Split('/')[1]), int.Parse(pdfFormFields.GetField("txtExpMoYr").Split('/')[0]), 1);
                            //Convert.ToInt32(pdfFormFields.GetField("txtExpMoYr").Split('/')[0]) <= 9 ? "0" + pdfFormFields.GetField("txtExpMoYr").Split('/')[0] + pdfFormFields.GetField("txtExpMoYr").Split('/')[1] : pdfFormFields.GetField("txtExpMoYr").Split('/')[0] + pdfFormFields.GetField("txtExpMoYr").Split('/')[1];
                        }
                        else
                        {
                            row["exparationDate"] = DateTime.MinValue;
                        }

                        if (reader.AcroFields.GetField("rbCreditCard").Contains("rb"))
                        {
                            row["cardTypeID"] = reader.AcroFields.GetField("rbCreditCard").Replace("rb", "");
                        }
                        else
                        {
                            int cardType = reader.AcroFields.GetField("rbCreditCard").Length > 0 ? Convert.ToInt32(reader.AcroFields.GetField("rbCreditCard")) : 1;
                            //int cardType = Convert.ToInt32(reader.AcroFields.GetField("rbCreditCard"));

                            switch (cardType)
                            {
                                case 1:
                                    row["cardTypeID"] = "Visa";
                                    break;
                                case 2:
                                    row["cardTypeID"] = "MasterCard";
                                    break;
                                case 3:
                                    row["cardTypeID"] = "AmericaExpress";
                                    break;
                                default:
                                    row["cardTypeID"] = "Visa";
                                    break;
                            }
                        }
                    }
                    else
                    {
                        row["formType"] = "HA(Sidenights)";
                        //string exparationDtae = (Convert.ToInt32(pdfFormFields.GetField("txtExpDate")[0])) <= 9 && (!pdfFormFields.GetField("txtExpDate").StartsWith("0")) ? "0" + pdfFormFields.GetField("txtExpDate").Split('/')[0] + pdfFormFields.GetField("txtExpDate").Split('/')[1] : pdfFormFields.GetField("txtExpDate").Split('/')[0] + pdfFormFields.GetField("txtExpDate").Split('/')[1];
                        row["exparationDate"] = new DateTime(int.Parse(pdfFormFields.GetField("txtExpDate").Split('/')[1].Contains("20") ? pdfFormFields.GetField("txtExpDate").Split('/')[1] : "20" + pdfFormFields.GetField("txtExpDate").Split('/')[1]), int.Parse(pdfFormFields.GetField("txtExpDate").Split('/')[0]), 1);
                        //row["Expiry_Date"] = Convert.ToInt32(pdfFormFields.GetField("txtExpDate").Split('/')[0]) <= 9 ? "0" + pdfFormFields.GetField("txtExpDate").Split('/')[0] + pdfFormFields.GetField("txtExpDate").Split('/')[1] : pdfFormFields.GetField("txtExpDate").Split('/')[0] + pdfFormFields.GetField("txtExpDate").Split('/')[1];


                        if (reader.AcroFields.GetField("Group12").Contains("rb"))
                        {
                            row["cardTypeID"] = reader.AcroFields.GetField("Group12").Replace("rb", "");
                        }
                        else
                        {
                            int cardType = reader.AcroFields.GetField("Group12").Length > 0 ? Convert.ToInt32(reader.AcroFields.GetField("Group12")) : 1;
                            //int cardType = Convert.ToInt32(reader.AcroFields.GetField("Group12"));

                            switch (cardType)
                            {
                                case 1:
                                    row["cardTypeID"] = "Visa";
                                    break;
                                case 2:
                                    row["cardTypeID"] = "MasterCard";
                                    break;
                                case 3:
                                    row["cardTypeID"] = "AmericaExpress";
                                    break;
                                default:
                                    row["cardTypeID"] = "Visa";
                                    break;
                            }
                        }
                    }

                    //if (item3 != null)
                    //{
                    //    row["Token"] = pdfFormFields.GetField("txtToken");
                    //}
                    //else
                    //{
                    //    row["Token"] = "N/A";
                    //}

                    if (item4 != null)
                    {
                        if (row["DepositTransactionID"].ToString() != "N/A" || row["FinalBalanceTransactionID"].ToString() != "N/A")
                        {
                            row["merchant"] = pdfFormFields.GetField("txtMerchant");
                        }
                        else
                        {
                            row["merchant"] = "-1";
                        }
                    }
                    else
                    {
                        row["merchant"] = "-1";
                    }
                }
                catch (Exception ex)
                {
                    stamper.Close();
                    return;
                }
            }

            stamper.Close();
        }
    }


    public PaymentAuthorizationHelper()
    {
	    //
	    // TODO: Add constructor logic here
	    //
    }
}