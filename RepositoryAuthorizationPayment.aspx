﻿<%@ Page Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="RepositoryAuthorizationPayment.aspx.cs" Inherits="RepositoryAuthorizationPayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style type="text/css">
      .floatLeft {float:left; margin-left:15px;}
    </style> 
    <div class="floatLeft" style="width:600px; min-height:610px; float:left; margin-top:10px; margin-left:40px; font-size:10px; color:Black;">
        <div>
           <asp:GridView runat="server" ID="gvRAP" EnableModelValidation="True" AutoGenerateColumns="False">
               <Columns>
                   <asp:BoundField DataField="proposalID" HeaderText="proposalID" />
                   <asp:BoundField DataField="email" HeaderText="email" />
                   <asp:BoundField DataField="cardholderName" HeaderText="cardholderName" />
                   <asp:BoundField DataField="preparer" HeaderText="preparer" />
                   <asp:BoundField DataField="billingAddress" HeaderText="billingAddress" />
                   <asp:BoundField DataField="phone" HeaderText="phone" />
                   <asp:BoundField DataField="city" HeaderText="city" />
                   <asp:BoundField DataField="zip" HeaderText="zip" />
                   <asp:BoundField DataField="country" HeaderText="country" />
                   <asp:BoundField DataField="state" HeaderText="state" />
                   <asp:BoundField DataField="paymentInstructions" HeaderText="paymentInstructions" />
                   <asp:BoundField DataField="timestamp" HeaderText="Date" />
               </Columns>
            </asp:GridView> 
        </div>
        <br/>
        <div>
            <span>Invoice Number: </span>
            <br />
            <asp:TextBox ID="tbInvoiceNumber" runat="server"></asp:TextBox><br />            
            <br />            
            <asp:Button ID="btnGet" runat="server" Text="Get Data" OnClick="btnGet_Click" />
        </div>
        <br />
        <br />
        <div>
            <span>ProposalID: </span><br />
            <asp:TextBox ID="tbProposalID" runat="server"></asp:TextBox><br /><br />
            <span>Email: </span><br />
            <asp:TextBox ID="tbEmail" runat="server"></asp:TextBox><br /><br />
            <span>CreditCardType</span><br />
            <asp:DropDownList ID="ddlCreditCard" runat="server">
            </asp:DropDownList>
            <br /><br />
            <span>Credit card number: </span>
            <br />
            <asp:TextBox ID="tbCreditCardNumber" runat="server"></asp:TextBox><br /><br />
            <span>Exparation date: </span>
            <br />
            <div style="float:left;"><asp:TextBox ID="tbExparationMonth" runat="server" Width="25px"></asp:TextBox></div><div style="float:left;">/</div>
            <div style="float:left;"><asp:TextBox ID="tbExparationYear" runat="server" Width="39px"></asp:TextBox></div>
            <div style="clear:both;"></div>  
            <br />      
            <span>Cardholder name: </span><br />
            <asp:TextBox ID="tbCardHolderName" runat="server"></asp:TextBox>
            <br /><br />
            <span>Preparer: </span><br />
            <asp:TextBox ID="tbPreparer" runat="server"></asp:TextBox>
            <br /><br />
            <span>Billing address: </span><br />
            <asp:TextBox ID="tbBillingAddress" runat="server"></asp:TextBox>
            <br /><br />
            <span>City: </span><br />
            <asp:TextBox ID="tbCity" runat="server"></asp:TextBox>
            <br /><br />
            <span>Zip: </span><br />
            <asp:TextBox ID="tbZip" runat="server"></asp:TextBox>
            <br /><br />
            <span>Phone: </span><br />
            <asp:TextBox ID="tbPhone" runat="server"></asp:TextBox>
            <br /><br />
            <span>Country: </span><br />
            <asp:TextBox ID="tbCountry" runat="server"></asp:TextBox>
            <br /><br />
            <span>State: </span><br />
            <asp:TextBox ID="tbState" runat="server"></asp:TextBox>
            <br /><br />
            <span>Payment Instructions: </span><br/>
            <asp:TextBox runat="server" TextMode="MultiLine" ID="txtPaymentInstructions" Rows="5" Width="300px" ></asp:TextBox>
            <br />
        </div>
    </div>
</asp:Content>