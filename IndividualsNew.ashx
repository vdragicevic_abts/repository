﻿<%@ WebHandler Language="C#" Class="IndividualsNew" %>

using System;
using System.Web;

public class IndividualsNew : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        try
        {
            string cardholder = context.Request["cardholder"];
            string address = context.Request["address"];
            string phone = context.Request["phone"];
            string email = context.Request["email"];
            string city = context.Request["city"];
            string postal = context.Request["postal"];
            string country = context.Request["country"];
            string cardType = context.Request["cardType"];
            string expireMonth = context.Request["expireMonth"];
            string expireYear = context.Request["expireYear"];
            string cardnumber = context.Request["cardnumber"];
            string ccv = context.Request["ccv"];
            string amount = context.Request["amountcc"];
            string code = context.Request["code"];
            string gatewayID = context.Request["gatewayID"];
            string paymentDetailCCID = String.Empty;
            string conferenceID = String.Empty;
            string userID = String.Empty;
            string callback = context.Request["callback"];
            string currency = context.Request["currency"];
            string test = context.Request["test"];
            string conferenceName = context.Request["conferenceName"];           
            
            ParseCode(code, ref paymentDetailCCID, ref conferenceID, ref userID);

            string conference = conferenceName.Remove(conferenceName.Length - 4);
            conference = conference == "AAO" ? conference += "HSNF" : conference;
            
            string url = GetConferenceDetails(conference);
            string post = Security_Post(url, "paymentDetailCCID=" + paymentDetailCCID + "&userID=" + userID);

            if (post == "true")
            {
                string transactionID = SendPayPalPayment2Repository(int.Parse(paymentDetailCCID), int.Parse(userID), int.Parse(cardType), cardnumber, ccv, cardholder, new DateTime(int.Parse("20" + expireYear), int.Parse(expireMonth), 1), conferenceID, address, city, country, email, phone, postal, "Individual Booking Payment " + conferenceID, amount, conferenceID + userID + ", " + cardholder + ", " + email, gatewayID, conferenceName, Boolean.Parse(test), currency);

                context.Response.ContentType = "text/plain";
                context.Response.Write(callback + "({ \"transactionID\": \"" + transactionID + "\" })");
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write(callback + "({ \"transactionID\": \"" + "900" + "\" })");
            }
        }
        catch (Exception ex)
        {
            string callback = context.Request["callback"];
            context.Response.Write(callback + "({ \"transactionID\": \"" + "900" + "\" })");
        }                               
    }

    protected void ParseCode(string _code, ref string paymentDetailCCID, ref string conferenceID, ref string userID)
    {
        string code = _code.Trim();

        char[] c = code.ToCharArray();

        paymentDetailCCID = String.Empty;
        conferenceID = String.Empty;
        userID = String.Empty;

        bool firstNumAfterLetter = false;
        int yearCount = 0;

        foreach (char chr in c)
        {
            if (char.IsDigit(chr))
            {
                if (yearCount == 2)
                {
                    firstNumAfterLetter = false;
                    userID += chr;
                }

                if (firstNumAfterLetter)
                {
                    conferenceID += chr;
                    yearCount += 1;
                }
                else
                {
                    if (yearCount == 0)
                    {
                        paymentDetailCCID += chr;
                    }
                }
            }

            if (char.IsLetter(chr))
            {
                conferenceID += chr;
                firstNumAfterLetter = true;
            }
        }
    }

    private string GetConferenceDetails(string conferenceID)
    {
        string url = String.Empty;
        string connectionString = DatabaseConnection.GetConnectionString();
        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString);
        conn.Open();

        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.Connection = conn;
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.CommandText = "Select 'http://' + url + '/' + webPage from Conferences where conferenceID=@conferenceID";

        cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ConferenceID", conferenceID));

        try
        {
            url = (string)cmd.ExecuteScalar();

            conn.Close();
        }
        catch (Exception ex)
        {
            if (conn.State == System.Data.ConnectionState.Open)
            {
                conn.Close();
            }
        }

        return url;
    }

    private string Security_Post(string strPage, string strBuffer)
    {
        //Our postvars
        byte[] buffer = System.Text.Encoding.ASCII.GetBytes(strBuffer);
        //Initialization
        System.Net.HttpWebRequest WebReq = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(strPage);
        //Our method is post, otherwise the buffer (postvars) would be useless
        WebReq.Method = "POST";
        //We use form contentType, for the postvars.
        WebReq.ContentType = "application/x-www-form-urlencoded";
        //The length of the buffer (postvars) is used as contentlength.
        WebReq.ContentLength = buffer.Length;
        //We open a stream for writing the postvars
        System.IO.Stream PostData = WebReq.GetRequestStream();
        //Now we write, and afterwards, we close. Closing is always important!
        PostData.Write(buffer, 0, buffer.Length);
        PostData.Close();
        //Get the response handle, we have no true response yet!
        System.Net.HttpWebResponse WebResp = (System.Net.HttpWebResponse)WebReq.GetResponse();

        //Let's show some information about the response
        //Console.WriteLine(WebResp.StatusCode);
        //Console.WriteLine(WebResp.Server);

        if (WebResp.StatusCode == System.Net.HttpStatusCode.OK)
        {
            System.IO.Stream Answer = WebResp.GetResponseStream();
            System.IO.StreamReader _Answer = new System.IO.StreamReader(Answer);
            return _Answer.ReadToEnd();
        }
        else
        {
            return "";
        }

        //Now, we read the response (the string), and output it.          
        //Console.WriteLine(_Answer.ReadToEnd());
    }   

    protected string SendPayPalPayment2Repository(int id, int userID, int cardType, string cardNumber, string ccv, string cardHolder, DateTime exparationDate, string conferenceID, string address, string city, string country, string email, string phone, string postal, string productDescription, string price, string province, string gatewayID, string conferenceName, bool test, string currency)
    {

        string fullName = string.Empty;
        string userEmail = string.Empty;
        string transactionID = string.Empty;

        User us = new User();
        us.UserID = "Admin";
        us.Password = "password";

        Users users = new Users();
        users.CurrentUser = us;

        IndividualPaymentInternetSecure ip = new IndividualPaymentInternetSecure();

        ip.CardHolderName = cardHolder;
        ip.CardTypeID = cardType;
        ip.CCV = ccv;
        ip.ConferenceID = conferenceName;
        ip.CreditCardNumber = cardNumber;
        ip.ExparationDate = exparationDate;
        ip.PaymentDetailCCID = id;
        ip.UserID = userID;
        ip.Address = address;
        ip.City = city;
        ip.Country = country;
        ip.Email = email;
        ip.Phone = phone;
        ip.Postal = postal;
        ip.ProductCode = id.ToString();
        ip.ProductDescription = productDescription;        
        ip.ProductPrice = price;
        ip.Province = id.ToString() + conferenceID + userID.ToString() + ", " + cardHolder + ", " + email;
        ip.GatewayID = int.Parse(gatewayID);
        ip.Currency = currency;
        ip.Test = test;
        
        try
        {
            transactionID = users.SendIPToRepositoryInternetSecureRQ(ip);
        }
        catch (Exception ex)
        {

        }

        return transactionID;
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}