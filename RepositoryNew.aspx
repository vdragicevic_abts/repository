﻿<%@ Page Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="RepositoryNew.aspx.cs" Inherits="Repository" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <style type="text/css">
      .floatLeft {float:left; margin-left:15px;}
    </style> 
    <div class="floatLeft" style="width:600px; min-height:610px; float:left; margin-top:10px; margin-left:40px; font-size:12px; color:Black;">
        <div>
            <span>Code: </span>
            <br />            
            <asp:TextBox ID="tbCode" runat="server"></asp:TextBox><br />
            <br />
            <asp:Button ID="btnGet" runat="server" Text="Get Data" OnClick="btnGet_Click" />
        </div>
        <br />
        <br />
        <div>
            <span>CreditCardType</span><br />
            <asp:DropDownList ID="ddlCreditCard" runat="server">
            </asp:DropDownList>
            <br /><br />
            <span>Credit card number: </span>
            <br />
            <asp:TextBox ID="tbCreditCardNumber" runat="server"></asp:TextBox> <br /><br />
            <span>Exparation date: </span>
            <br />
            <div style="float:left;"><asp:TextBox ID="tbExparationMonth" runat="server" Width="25px"></asp:TextBox></div><div style="float:left;">/</div>
            <div style="float:left;"><asp:TextBox ID="tbExparationYear" runat="server" Width="39px"></asp:TextBox></div>
            <br /><br /><br />
            <span>Cardholder name: </span>
            <br />
            <asp:TextBox ID="tbCardholderName" runat="server"></asp:TextBox>
            <br />
        </div>
    </div>
</asp:Content>