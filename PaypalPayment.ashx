﻿<%@ WebHandler Language="C#" Class="PaypalPayment" %>

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using PayPal.Payments.Common;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;

public class PaypalPayment : IHttpHandler {

    public void ProcessRequest (HttpContext context) {

        try
        {
            string cardholder = context.Request["cardholder"];
            string address = context.Request["address"];
            string phone = context.Request["phone"];
            string email = context.Request["email"];
            string city = context.Request["city"];
            string postal = context.Request["postal"];
            string country = context.Request["country"];
            string cardType = context.Request["cardType"];
            string expireMonth = context.Request["expireMonth"];
            string expireYear = context.Request["expireYear"];
            string cardnumber = context.Request["cardnumber"];
            string ccv = context.Request["ccv"];
            string amount = context.Request["amountcc"];
            string paymentDetailCCID = String.Empty;
            string conferenceID = String.Empty;
            string userID = context.Request["userID"];
            string callback = context.Request["callback"];
            string currency = context.Request["currency"];
            string test = context.Request["test"];
            string conferenceName = context.Request["conferenceName"];
			string proposalInfo = "";
			
			if (context.Request["proposalID"] != null && context.Request["invoiceNumber"] != null)
            {
                proposalInfo = context.Request["proposalID"] + "/" + context.Request["invoiceNumber"];
            }

            string Messages = String.Empty;

            string transactionID = DoPayPalPaymentInstant(ref Messages, amount, cardholder, address, city, proposalInfo,  postal, phone, email, expireMonth, expireYear, cardnumber, ccv, currency, userID);
			
			SendRegistrationFormToRepository(GetCardTypeID(cardType), ccv, conferenceName, cardnumber, new DateTime(int.Parse("20" + expireYear), int.Parse(expireMonth), 1), int.Parse(userID), int.Parse(userID), cardholder, address, phone, city, postal, country, email, proposalInfo, transactionID);

            context.Response.ContentType = "text/plain";
            context.Response.Write(callback + "({ \"transactionID\": \"" + transactionID + "\" })");
        }
        catch (Exception ex)
        {
            string callback = context.Request["callback"];
            context.Response.Write(callback + "({ \"transactionID\": \"" + "40" + "\" })");
        }
    }
	
	private int GetCardTypeID(string cardType)
    {
        int cardTypeID = 1;

        switch (cardType)
        {
            case "Visa":
                cardTypeID = 1;
                break;
            case "Master/Euro Card":
                cardTypeID = 2;
                break;
            case "American Express":
                cardTypeID = 3;
                break;
            default:
                cardTypeID = 1;
                break;
        }

        return cardTypeID;
    }

    private string DoPayPalPaymentInstant(ref string Messages, string amountcc, string cardholder, string billingAddress, string city, string state, string cityCode, string phone, string email, string expMonth, string expYear, string cardNumber, string cvv, string currency, string userID)
    {
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

        string retCode = String.Empty;

        Random rnd = new Random();
        int number = rnd.Next(9999999);

        string CCPaymentErrorNote = "";

        //UserInfo UI = new UserInfo("vdragicevic", "vdragicevic", "PayPal", "Dvladh321"); //Testing Account credentials // ove parametre pokupis sa PP web sajta za merchant info ili iz sandbox accounta. Ovo ovde sto vec stoji su live parametri.
        //UserInfo UI = new UserInfo("vdragicevic", "vdragicevic", "PayPal", "Dvladh321"); //Testing Credentials

        UserInfo UI = new UserInfo("Agraham", "STSABTSCC", "PayPal", "Password1"); //Live Credentials

        String strRequestID = PayflowUtility.RequestId;
        PayflowConnectionData Connection = new PayflowConnectionData();
        Invoice Inv = new Invoice();

        Currency Amt = new Currency(new decimal(double.Parse(amountcc)), currency);
        //Currency Amt = new Currency(new decimal(double.Parse(amountccd.Value)), "USD"); // svi paymenti su u dolarima. alternativno ovo moze da bude izbor iz drop down liste, mada ABTS-u trebaju samo dolari
        Inv.Amt = Amt;

        // ovde kreiras invoice zapis za PayPal, odnosno transaction record u nasoj bazi

        DataTable DTPO = new DataTable();

        //DataTable DTPO = DBC.GetData("SELECT isnull(MAX(IDPayment),0)+1 FROM PaymentDetailCC");

        Inv.PoNum = state.Length > 0 ? state.Split('/')[0] : "PO" + number.ToString();
        Inv.InvNum = state.Length > 0 ? state.Split('/')[1] : "INV" + number.ToString(); // INVADA09 je proizvoljan prefiks da bi se znalo sa koje je konferencije 
        Inv.CustRef = state.Length > 0 ? state : "RG" + userID + "#INV" + number.ToString(); // isto – IDCustomer+#<konferencija>. Treba da bude unique sa strane PP-a, kao I prethodno
        Inv.MerchDescr = "Payment";
        Inv.MerchSvc = "ABTS";
        Inv.Comment1 = "Online Payment via PayPal";
        Inv.Comment2 = "Amount: " + amountcc;

        BillTo Bill = new BillTo();

        string[] name = cardholder.Split(' ');

        Bill.FirstName = name[0].Trim();
        Bill.LastName = name.Length > 1 ? name[1].Trim() : "";
        Bill.CompanyName = "";
        Bill.Street = billingAddress;
        Bill.City = city;
        Bill.State = state;
        Bill.Zip = cityCode;

        //Bill.BillToCountry = CCODE.Rows[0]["Code"].ToString(); // ISO kod zemlje (sa PP sajta)
        Bill.PhoneNum = phone;
        Bill.Email = email;

        Inv.BillTo = Bill;

        ShipTo Ship = new ShipTo();
        Ship = Bill.Copy();

        Inv.ShipTo = Ship;

        // ovde kreiras unique CustCode .... ja sam stavio da je C#<ime_konferencije>#<IDUser>

        CustomerInfo CustInfo = new CustomerInfo();
        CustInfo.CustCode = number.ToString();
        CustInfo.CustId = number.ToString(); // ID customera (iz sesije); IDC = ID Customer

        string _ccExpDate = expMonth + expYear;
        CreditCard CC = new CreditCard(cardNumber, _ccExpDate); // cardnumber i expiration date kako je uneto na formi
        CC.Cvv2 = cvv;
        CC.Name = cardholder; // card holder name kako je uneto na formi

        CardTender Card = new CardTender(CC);

        // make payment

        SaleTransaction Trans = new SaleTransaction(UI, Connection, Inv, Card, strRequestID);
        Trans.Verbosity = "FULL";

        Response Resp = Trans.SubmitTransaction();

        // i sad pratis response pokusanog paymenta...

        if (Resp != null)
        {
            //CCPaymentErrorNote += "There was response....";
            TransactionResponse TrxnResponse = Resp.TransactionResponse;

            if (TrxnResponse != null)
            {

                if (TrxnResponse.Result != 0)
                {
                    CCPaymentErrorNote += "Sorry, your payment request failed. ";
                }
                else
                {
                    CCPaymentErrorNote += "Thank you, your payment request was successful.";
                }
            }

            // ako je transakcija pala iz bezbednosnih razloga

            FraudResponse FraudResp = Resp.FraudResponse;

            if (FraudResp != null)
            {
                //CCPaymentErrorNote += "Fraud Note: " + FraudResp.PreFpsMsg + ", " + FraudResp.PostFpsMsg + ". ";
            }

            // ovo je ako u toku paymenta dok ne stigne krajnji rezultat, customer refreshuje stranu i resenduje podatke
            if (TrxnResponse.Duplicate == "1")
            {
                CCPaymentErrorNote += "DUPLICATE TRANSACTION!";
            }
            else
            {
                //CCPaymentErrorNote += "Unique transaction. ";

            }

            // a sada rezultati samog procesiranja...

            if (TrxnResponse.Result < 0)
            {
                // ne mora da im ide tacan opis greske, ali ovaj TrxnResponse.Result onda mora da se loguje negde
                CCPaymentErrorNote += "There was an error processing your transaction. Please contact Customer Service." + Environment.NewLine + "Error: " + TrxnResponse.Result.ToString();
            }
            else if (TrxnResponse.Result == 1 || TrxnResponse.Result == 26)
            {

                // Nisu dobri parametri primaoca uplate (user, pass, vendor, partner)

                CCPaymentErrorNote += "Account configuration issue.  Please verify your login credentials.";
            }
            else if (TrxnResponse.Result == 0)
            {
                retCode = TrxnResponse.Pnref;

                CCPaymentErrorNote += " Your credit card has been charged in the amount of USD $ " + amountcc + ". A confirmation email containing your reservation details has been sent to your registered email address.";
                CCPaymentErrorNote += "\\n";
                //CCPaymentErrorNote += "\\nYour credit card has successfully been charged in the amount of USD " + amountcc.Value.ToString();
                ////CCPaymentErrorNote += "Your credit card has successfully been charged in the amount of USD " + amountccd.Value.ToString();
                //CCPaymentErrorNote += "\\nA confirmation has been sent to your registered email address.";
                //CCPaymentErrorNote += "\\nThank you";
                //CCPaymentErrorNote += "\\n";
            }
            else if (TrxnResponse.Result == 12)
            {
                // Banka odbila (nema para ili ko zna sta drugo)
                CCPaymentErrorNote += "Your transaction was declined.";
            }
            else if (TrxnResponse.Result == 13)
            {
                // sumljiva transakcija. Banka ne moze da utvrdi poreklo payment sredstva, itd
                CCPaymentErrorNote += "Your Transaction is pending. Contact Customer Service to complete your order.";
            }
            else if (TrxnResponse.Result == 23 || TrxnResponse.Result == 24)
            {
                // Ne valja broj kartice ili expiration date
                CCPaymentErrorNote += "Invalid credit card information. Please re-enter.";
            }
            else if (TrxnResponse.Result == 125)
            {
                // Ovo je odbio PayPal
                CCPaymentErrorNote += "Your Transactions has been declined. Contact Customer Service.";
            }
            else if (TrxnResponse.Result == 126)
            {
                // Nije prosao identifikaciju, odbo PP, fraud
                if ((TrxnResponse.AVSAddr != "Y" | TrxnResponse.AVSZip != "Y"))
                {
                    CCPaymentErrorNote += "Your billing information does not match. Please re-enter.";
                }
                else
                {
                    CCPaymentErrorNote += "Your Transaction is Under Review. We will notify you via e-mail if accepted.";
                }
            }

            // odbio PP, fraud
            else if (TrxnResponse.Result == 127)
            {
                CCPaymentErrorNote += "Your Transaction is Under Review. We will notify you via e-mail if accepted.";
            }
            else
            {
                // ovo je nehendlovana greska
                CCPaymentErrorNote += TrxnResponse.RespMsg;
            }

            CCPaymentErrorNote += PayflowUtility.GetStatus(Resp);

            Context TransCtx = Resp.TransactionContext;
            if (TransCtx != null && TransCtx.getErrorCount() > 0)
            {
                CCPaymentErrorNote = TransCtx.ToString();
            }
        }
        else
        {
            CCPaymentErrorNote = "No response";
        }

        Messages = CCPaymentErrorNote;

        EmailHelper.SendPayPalClientEmailSuccess(email, Bill.FirstName, Bill.LastName, retCode, amountcc, cardNumber);
        EmailHelper.SendPayPalRegistrantEmailSuccess(email, Bill.FirstName, Bill.LastName, retCode, amountcc, cardNumber);

        return retCode;

    }
	
	public bool SendRegistrationFormToRepository(int cardTypeID, string ccv, string conferenceID, string creditCardNumber, DateTime exparationDate, int registrationFormID, int userID, string cardholderName, string billingAddress, string phone, string city, string zip, string country, string email, string state, string transactionID)
    {
        RegistrationForm rf = new RegistrationForm();

        rf.CardTypeID = cardTypeID;
        rf.CCV = ccv;
        rf.ConferenceID = conferenceID;
        rf.CreditCardNumber = creditCardNumber;
        rf.ExparationDate = exparationDate;
        rf.RfID = registrationFormID;
        rf.UserID = userID;
        rf.CardholderName = cardholderName;
        rf.Address = billingAddress;
        rf.City = city;
        rf.Country = country;
        rf.Postal = zip;
        rf.Phone = phone;
        rf.Email = email;
        rf.State = transactionID;
		rf.ProposalInfo = state;

        User us = new User();
        us.UserID = "Admin";
        us.Password = "password";

        Users users = new Users();
        users.CurrentUser = us;

        return users.SendRFToRepositoryRQ(rf);
    }



    public bool IsReusable {
        get {
            return false;
        }
    }

}