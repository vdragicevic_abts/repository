﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class RepositoryIndividuals : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin"] == null)
        {
            Response.Redirect("./2repository/Login.aspx");
        }
      
        if (!IsPostBack)
        {           
            FillDDLCardTypes();
        }
    }   

    protected void btnGet_Click(object sender, EventArgs e)
    {
        string code = this.tbCode.Text.Trim();

        char[] c = code.ToCharArray();

        string paymentDetailCCID = String.Empty;
        string conferenceID = String.Empty;
        string userID = String.Empty;

        bool firstNumAfterLetter = false;
        int yearCount = 0;

        foreach (char chr in c)
        {
            if (char.IsDigit(chr))
            {
                if (yearCount == 2)
                {
                    firstNumAfterLetter = false;
                    userID += chr;
                }

                if (firstNumAfterLetter)
                {
                    conferenceID += chr;
                    yearCount += 1;
                }
                else
                {
                    if (yearCount == 0)
                    {
                        paymentDetailCCID += chr;
                    }
                }
            }

            if(char.IsLetter(chr))
            {
                conferenceID += chr;
                firstNumAfterLetter = true;
            }
        }

        char[] confID = conferenceID.ToCharArray();
        bool injected = false;
        conferenceID = string.Empty;

        foreach (char conf in confID)
        {
            if(char.IsLetter(conf))
            {
                conferenceID += conf;
            }

            if (char.IsDigit(conf))
            {
                if (!injected)
                {
                    conferenceID += "20" + conf;
                    injected = true;
                }
                else
                {
                    conferenceID += conf;
                }
            }
        }


        IndividualPayment ip = IndividualPaymentHelper.SelectIndividualPayment(conferenceID, int.Parse(paymentDetailCCID), int.Parse(userID));

        Log.InsertLog4Groups(conferenceID, int.Parse(paymentDetailCCID), int.Parse(userID), "SendIPToRepositoryRS");

        if (ip.CreditCardNumber != null)
        {
            this.tbCreditCardNumber.Text = StringHelpers.Decrypt(ip.CreditCardNumber);
            this.ddlCreditCard.SelectedValue = ip.CardTypeID.ToString();
            this.tbExparationMonth.Text = ip.ExparationDate.Month.ToString();
            this.tbExparationYear.Text = ip.ExparationDate.Year.ToString();
            this.tbCardholderName.Text = ip.CardHolderName;
        }
    }    

    protected void FillDDLCardTypes()
    {
        Users usr = new Users();
        DataTable dtCardTypes = usr.GetVisaTypesRS();

        if (dtCardTypes.Rows.Count > 0)
        {
            ddlCreditCard.DataTextField = "Name";
            ddlCreditCard.DataValueField = "id";
            ddlCreditCard.DataSource = dtCardTypes;
            ddlCreditCard.DataBind();

            ListItem li = new ListItem("None", "-1");
            ddlCreditCard.Items.Insert(0, li);
        }
    }

}