﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="RepositoryHistoryReport.aspx.cs" Inherits="RepositoryHistoryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdminPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        div.ui-datepicker, .ui-datepicker td {
            font-size: 10px;
        }
    </style>
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {

            SetPickers();
        });

        function SetPickers() {
            $('.DateTimePickerFrom').datepicker();
            $('.DateTimePickerTo').datepicker();
        }

    </script>
    <div style="clear: both"></div>
    <div style="background-color: Gray; border: 2px; height: 70px; padding-top: 20px; padding-left: 10px; width: 840px;">
        <div style="float: left; margin-left: 10px;">
            <span>Date From: </span>
            <asp:TextBox ID="tbDateFrom" runat="server" CssClass="DateTimePickerFrom"></asp:TextBox>
        </div>
        <div style="float: left; margin-left: 10px;">
            <span>Date To: </span>
            <asp:TextBox ID="tbDateTo" runat="server" CssClass="DateTimePickerTo"></asp:TextBox>
        </div>
        <div style="float: left; margin-left: 10px;">
            <span>Merchant: </span>
            <asp:DropDownList runat="server" ID="ddlMerchant" Width="100">
                <asp:ListItem Text="All" Value="0" Selected="True" />
                <asp:ListItem Text="NOT CHARGED" Value="-1" Selected="False" />
                <asp:ListItem Text="IGD INTERNATIONAL GROUP" Value="1" Selected="False" />
                <asp:ListItem Text="CMR GLOBAL GROUP SERVICE" Value="2" Selected="False" />
                <asp:ListItem Text="ABTS CONVENTION SERVICES" Value="3" Selected="False" />
                <asp:ListItem Text="INTERNATIONAL GROUP HOUSING" Value="4" Selected="False" />
                <asp:ListItem Text="SWME" Value="5" Selected="False" />
            </asp:DropDownList>
        </div>
        <div style="float: right; margin-right: 40px;">
            <span>Form Type: </span>
            <asp:DropDownList runat="server" ID="ddlFormType">
                <asp:ListItem Text="All" Value="0" Selected="True" />
                <asp:ListItem Text="PA" Value="1" Selected="False" />
                <asp:ListItem Text="IP" Value="2" Selected="False" />
                <asp:ListItem Text="HA(Picknights)" Value="3" Selected="False" />
                <asp:ListItem Text="HA(Sidenights)" Value="4" Selected="False" />
            </asp:DropDownList>
        </div>
        <br />
        <br />
        <div align="center" style="padding-top: 5px;">
            <asp:Button ID="btnSearch" runat="server" Text="Search" Height="23px"
                OnClick="btnSearch_Click" />
        </div>
    </div>
    <br />
    <div align="center">
        <asp:GridView ID="gvRepositoryHistoryReport" runat="server" CellPadding="4"
            EnableModelValidation="True" ForeColor="#333333" GridLines="None"
            OnRowDataBound="gvRepositoryHistoryReport_RowDataBound">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        </asp:GridView>
    </div>
    <br />

</asp:Content>

