﻿<%@ WebHandler Language="C#" Class="Groups" %>

using System;
using System.Web;

public class Groups : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        try
        {
            string cardTypeID = context.Request["cardTypeID"];
            string ccv = context.Request["ccv"];
            string conferenceName = context.Request["conferenceName"];
            string cardnumber = context.Request["cardnumber"];
            string expireMonth = context.Request["expireMonth"];
            string expireYear = context.Request["expireYear"];
            string haID = context.Request["haID"];
            string userID = context.Request["userID"];
            string cardholder = context.Request["cardholder"];
            string billingAddress = context.Request["billingAddress"];
            string phone = context.Request["phone"];
            string city = context.Request["city"];
            string zip = context.Request["zip"];
            string country = context.Request["country"];
            string email = context.Request["email"];
            string state = context.Request["state"];
            
            bool success = false;
            string callback = context.Request["callback"];

            success = SendHAToRepository(int.Parse(cardTypeID), ccv, conferenceName, cardnumber, new DateTime(int.Parse(expireYear), int.Parse(expireMonth), 1), int.Parse(haID), int.Parse(userID), cardholder, billingAddress, phone, city, zip, country, email, state);

            context.Response.ContentType = "text/plain";
            context.Response.Write(callback + "({ \"Success\": \"" + success + "\" })");
        }
        catch (Exception ex)
        {
            string callback = context.Request["callback"];
            context.Response.Write(callback + "({ \"Success\": \"" + "false" + "\" })");
        }          
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public bool SendHAToRepository(int cardTypeID, string ccv, string conferenceID, string creditCardNumber, DateTime exparationDate, int housingAgreementID, int userID, string cardholderName, string billingAddress, string phone, string city, string zip, string country, string email, string state)
    {       
        HousingRegistrationAgreementExpanded hra = new HousingRegistrationAgreementExpanded();

        hra.CardTypeID = cardTypeID;
        hra.CCV = ccv;
        hra.ConferenceID = conferenceID;
        hra.CreditCardNumber = creditCardNumber;
        hra.ExparationDate = exparationDate;
        hra.HaID = housingAgreementID;
        hra.UserID = userID;
        hra.CardholderName = cardholderName;
        hra.BillingAddress = billingAddress;
        hra.City = city;
        hra.Country = country;
        hra.Zip = zip;
        hra.Phone = phone;
        hra.Email = email;
        hra.State = state;

        User us = new User();
        us.UserID = "Admin";
        us.Password = "password";

        Users users = new Users();
        users.CurrentUser = us;
       
        return users.SendHAToRepositoryExpandedRQ(hra);
    }

}