﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;

public partial class RepositoryAuthorizationPayment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Admin"] == null)
        {
            Response.Redirect("./2repository/Login.aspx");
        }
   
        if (!IsPostBack)
        {
            gvRAP.Visible = false;
            FillDDLCardTypes();
        }
    }   

    protected void btnGet_Click(object sender, EventArgs e)
    {
        string invoiceID = this.tbInvoiceNumber.Text;

        User us = new User();
        us.UserID = "Admin";
        us.Password = "password";

        Users usr = new Users();
        usr.CurrentUser = us;

        List<PaymentAuthorizationExpanded> paList = usr.SendPAToRepositoryExpandedRSList(invoiceID);

        //Log.InsertLog4Groups(ddlConference.SelectedValue, int.Parse(tbHAID.Text), int.Parse(tbUserID.Text), "SendHAToRepositoryRS");

        if (paList.Count > 1)
        {
            gvRAP.DataSource = paList;
            gvRAP.DataBind();
            gvRAP.Visible = true;
            gvRAP.Rows[gvRAP.Rows.Count - 1].Visible = false;
        }
        else
        {
            gvRAP.Visible = false;
            gvRAP.DataSource = null;
            gvRAP.DataBind();
        }

        PaymentAuthorizationExpanded pa = null;

        if (paList.Count > 0)
            pa = paList.Last();

        if (pa != null && pa.CreditCardNumber != null)
        {
            this.tbProposalID.Text = pa.ProposalID;
            this.tbEmail.Text = pa.Email;
            this.tbCreditCardNumber.Text = StringHelpers.Decrypt(pa.CreditCardNumber);
            this.ddlCreditCard.SelectedValue = pa.CardTypeID.ToString();
            this.tbExparationMonth.Text = pa.ExparationDate.Month.ToString();
            this.tbExparationYear.Text = pa.ExparationDate.Year.ToString();
            this.tbCardHolderName.Text = pa.CardholderName;
            this.tbPreparer.Text = pa.Preparer;
            this.tbBillingAddress.Text = pa.BillingAddress;
            this.tbPhone.Text = pa.Phone;
            this.tbCity.Text = pa.City;
            this.tbCountry.Text = pa.Country;
            this.tbZip.Text = pa.Zip;
            this.txtPaymentInstructions.Text = pa.PaymentInstructions;
            this.tbState.Text = pa.State;

			using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
			//using (StreamWriter sw = File.AppendText("C:\\Windows\\System32\\winevt\\Logs\\PaymentLog.txt"))
            {
                sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "Viewed (" + "Invoice: " + invoiceID + ")");
            }
        }
        else
        {
            ClearData();
        }
    }    

    protected void FillDDLCardTypes()
    {
        Users usr = new Users();
        DataTable dtCardTypes = usr.GetVisaTypesRS();

        if (dtCardTypes.Rows.Count > 0)
        {
            ddlCreditCard.DataTextField = "Name";
            ddlCreditCard.DataValueField = "id";
            ddlCreditCard.DataSource = dtCardTypes;
            ddlCreditCard.DataBind();

            ListItem li = new ListItem("None", "-1");
            ddlCreditCard.Items.Insert(0, li);
        }
    }

    protected void ClearData()
    {
        this.tbProposalID.Text = "";
        this.tbEmail.Text = "";
        this.tbCreditCardNumber.Text = "";
        this.ddlCreditCard.SelectedIndex = 0;
        this.tbExparationMonth.Text = "";
        this.tbExparationYear.Text = "";
        this.tbCardHolderName.Text = "";
        this.tbPreparer.Text = "";
        this.tbBillingAddress.Text = "";
        this.tbPhone.Text = "";
        this.tbCity.Text = "";
        this.tbCountry.Text = "";
        this.tbZip.Text = "";
        this.txtPaymentInstructions.Text = "";
        this.tbState.Text = "";
    }

}