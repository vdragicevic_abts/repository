﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Net;
using System.IO;

public partial class pages_PaymentAuthorizationDcc : System.Web.UI.Page
{
    public Merchant MerchantAccount
    {
        get { return (Merchant)ViewState["MerchantAccount"]; }
        set { ViewState["MerchantAccount"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //string test = "ProposalID=232&InvoiceNumber=3232&Email=dvladh@gmail.com&EventName=ASBMR2012";
        //test = HttpUtility.UrlEncode(StringHelpers.DecodeTo64(test));

        if (!IsPostBack)
        {
            MerchantAccount = Merchant.IgdInternationlGroup;
            FillDDLCardTypes();
        }

        if (Request.QueryString.Count > 0)
        {
            string str = Request.QueryString.Get(0);
            str = HttpUtility.UrlDecode(str);

            string d = StringHelpers.DecodeFrom64(str);
            string[] query = d.Split('&');

            foreach (string s in query)
            {
                string[] q = s.Split('=');

                switch (q[0])
                {
                    case "ProposalID":
                        tbProposalID.Text = q[1];
                        break;
                    case "InvoiceNumber":
                        tbInvoiceNumber.Text = q[1];
                        break;
                    case "Email":
                        tbEmail.Text = q[1];
                        break;
                    case "EventName":
                        tbEventName.Text = q[1]; //this.imgLogo.ImageUrl = "../repository/Visuals/img/" + q[1] + ".jpg";
                        break;
                    //this.imgLogo.ImageUrl = "../repository/Visuals/img/" + q[1] + ".jpg";
                    case "CompanyLogo":
                        lblLogo.Text = q[1]; this.cn.InnerText = q[1]; this.cn1.InnerText = q[1]; this.cn2.InnerText = q[1]; this.cn3.InnerText = q[1];
                        break;
                    case "CSM":
                        hfCMS.Value = q[1];
                        break;
                    case "Preparer":
                        hfPreparer.Value = q[1];
                        break;
                    case "Note":
                        txtDescriptionOfServices.Value = q[1];
                        break;
					case "Merchant":
                        MerchantAccount = (Merchant)Enum.Parse(typeof(Merchant), q[1]);
                        break;
                }
            }

            //Set Privacy Policy link depending on DBA name
            linkPrivacyPolicy.Attributes["onClick"] = lblLogo.Text.ToLower().Contains("cmr") ? "window.open('" + ConfigurationManager.AppSettings["PrivacyPolicyCMR"] + "','targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=800, height=1000'); return false;" : "window.open('" + ConfigurationManager.AppSettings["PrivacyPolicyIGD"] + "','targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=800, height=1000'); return false;";
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        SavePaymentAuthorization();
    }


    protected void FillDDLCardTypes()
    {
        Users usr = new Users();
        DataTable dtCardTypes = usr.GetVisaTypesRS();

        if (dtCardTypes.Rows.Count > 0)
        {
            ddlCardType.DataTextField = "Name";
            ddlCardType.DataValueField = "id";
            ddlCardType.DataSource = dtCardTypes;
            ddlCardType.DataBind();

            ListItem li = new ListItem("None", "-1");
            ddlCardType.Items.Insert(0, li);

            //ddlCardType.Items.FindByText("American Express").Enabled = false;

        }
    }

    protected void SavePaymentAuthorization()
    {
        User us = new User();
        us.UserID = "Admin";
        us.Password = "password";

        Users usr = new Users();
        usr.CurrentUser = us;

        //Used when DCC Requested
        CardRate cRate = new CardRate();
        cRate.Details = new CardRateDetails();
		
		Merchant? merchantTerminal = new Users().GetMerchantIDFromABTSolute(tbEventName.Text.Trim());

        if (merchantTerminal == null)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "002", "alert('Merchant account not found!')", true);
            return;
        }
        else
        {
            MerchantAccount = (Merchant)merchantTerminal;
        }

        PaymentAuthorizationExpanded pa = new PaymentAuthorizationExpanded();
        GetPaymentAuthorizationDataExpanded(ref pa);

        //var merchant = MerchantAccount;
		var merchant = (MerchantAccount.ToString().Contains("PP") && pa.CardType.ToLower().Contains("american express")) ? (Merchant)Enum.Parse(typeof(Merchant), MerchantAccount.ToString() + "_Amex") : MerchantAccount;
        //merchant = lblLogo.Text.ToLower().Trim().Contains("swme") ? Merchant.Swme : Merchant.IgdInternationlGroup;

        if (!merchant.ToString().Contains("PP"))
        {
            //Check credit card using FirstData and get a token
            string response = InternetSecureAPI.CheckCreditCardAndGetToken("0", pa.CardholderName, pa.CreditCardNumber, pa.CardType, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), false, DatabaseConnection.GetGatewayID(merchant), DatabaseConnection.GetGatewayPassword(merchant), merchant);

            if (response.Length >= 15)
            {
                //Save token for further transaction
                pa.Token = response;
                //Leave only last four digits of credit card number
                //pa.CreditCardNumber = pa.CardTypeID != 3 ? "000000000000" + pa.CreditCardNumber.Substring(pa.CreditCardNumber.Length - 4) : "00000000000" + pa.CreditCardNumber.Substring(pa.CreditCardNumber.Length - 4);

                if (pa.DCC)
                {
                    cRate = InternetSecureAPI.PayeezyDccCardRateRequest(false, merchant, pa.CreditCardNumber.Substring(0, 6), "");

                    if (cRate.BinQualifies && cRate.DccOffered)
                    {
                        pa.ExchangeRate = cRate.Details.ExchangeRate;
                        pa.ForeignCurrencySymbolHtml = cRate.Details.ForeignCurrencySymbolHtml;
                        //pa.RateResponseID = cRate.Details.RateResponseId;
                    }
					else
                    {
                        if(!cRate.DccOffered)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "191", "alert('" + "Your credit card is not enabled for charging in local currency. Payment authorization not successfully submitted!" + "')", true);
                            cbDCC.Checked = false;
							return;
                        }
                    }
                }

                bool success = usr.SendPAToRepositoryExpandedRQ(pa);

                string information = success ? "Payment authorization successfully submitted!" : "Payment authorization not successfully submitted!";

                ClientScript.RegisterStartupScript(this.GetType(), "001", "alert('" + information + "')", true);

                if (success)
                {
                    var dcc = pa.DCC ? "DCC: Requested" : "";

                    try
                    {
                        EmailHelper.SendClientEmail(pa.CardholderName, pa.EventName, pa.Email, pa.InvoiceID, pa.ProposalID, dcc, lblLogo.Text);
                        EmailHelper.SendUserPaymentNotification(pa.CardholderName, pa.EventName, pa.Email, pa.InvoiceID, pa.ProposalID, this.hfCMS.Value, pa.Preparer, "XXXXXXXXXXXX-" + pa.Token.Substring(12), dcc);
                    }
                    catch (Exception ex)
                    {
                    }

                    CelarFormFields();
                }
            }
            else
            {
                var firstData = ((InternetSecureAPI.FirstData)Int64.Parse(response));
                string result = firstData.ToString();
                var res = new string((result.SelectMany((c, i) => i != 0 && char.IsUpper(c) && !char.IsUpper(result[i - 1]) ? new char[] { ' ', c } : new char[] { c })).ToArray());
                string information = "Payment authorization not successfully submitted! " + res;

                ClientScript.RegisterStartupScript(this.GetType(), "002", "alert('" + information + "')", true);

                using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
                {
                    //Save Transaction to Log
                    sw.WriteLine(pa.ProposalID + "-" + information + ", " + System.DateTime.Now.ToString() + ", PA ERROR");
                }
            }
        }
        else
        {
            pa.Merchant = (int)merchant;
            
            string success = String.Empty;
            string accountID = String.Empty;
            string token = String.Empty;
			string rate = String.Empty;
            Planet.Currency curr = Planet.Currency.USD;
			string transactionStatus = String.Empty;
            string transactionResponse = String.Empty;
			
			
            if (pa.DCC)
            {
                //Get currency rate by Country
                curr = (Planet.Currency)Enum.Parse(typeof(Planet.Currency), new CountryList().GetRegionInfoByName(pa.Country).ISOCurrencySymbol, true);
                string currencyCode = Convert.ToString((int)curr);

                rate = new Planet.PlanetAPI(merchant).SendUserInformation2PlanetRestServiceCurrencyRateRequestQuery(pa.Country, currencyCode, merchant, false, ref success);

                if (rate.Length > 2)
                {
                    token = new Planet.PlanetAPI(merchant).CheckCreditCardAndGetTokenPlanetService(pa.CardholderName, pa.BillingAddress, curr.ToString(), pa.City, pa.State, pa.Zip, pa.CreditCardNumber, curr, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), "", false, ref accountID, merchant, ref transactionStatus, ref transactionResponse);
                }
				else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "192", "alert('" + "Your credit card is not enabled for charging in local currency. Payment authorization not successfully submitted!" + "')", true);
                    cbDCC.Checked = false;
					return;
                }
            }
            else
            {
                token = new Planet.PlanetAPI(merchant).CheckCreditCardAndGetTokenPlanetService(pa.CardholderName, pa.BillingAddress, pa.Country, pa.City, pa.State, pa.Zip, pa.CreditCardNumber, curr, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), "", false, ref accountID, merchant, ref transactionStatus, ref transactionResponse);
            }

            if (accountID.Length > 2)
            {
                pa.Token = accountID;
                pa.ExchangeRate = pa.DCC ? rate : null;
                pa.ForeignCurrencySymbolHtml = pa.DCC ? curr.ToString() : null;
                
				var succ = usr.SendPAToRepositoryExpandedRQ(pa);

                string information = succ ? "Payment authorization successfully submitted!" : "Payment authorization not successfully submitted!";

                ClientScript.RegisterStartupScript(this.GetType(), "004", "alert('" + information + "')", true);

                if (succ)
                {
                    var dcc = pa.DCC ? "DCC: Requested" : "";

                    try
                    {
                        EmailHelper.SendClientEmail(pa.CardholderName, pa.EventName, pa.Email, pa.InvoiceID, pa.ProposalID, dcc, lblLogo.Text);
                        EmailHelper.SendUserPaymentNotification(pa.CardholderName, pa.EventName, pa.Email, pa.InvoiceID, pa.ProposalID, this.hfCMS.Value, pa.Preparer, "XXXXXXXXXXXX-" + pa.CreditCardNumber.Substring(12), pa.DescriptionOfServices, dcc, transactionStatus, transactionResponse);
                    }
                    catch (Exception ex)
                    {
                    }

                    CelarFormFields();
                }
            }
            else
            {
                string information = "Payment authorization not successfully submitted! " + transactionStatus;
                ClientScript.RegisterStartupScript(this.GetType(), "003", "alert('" + information + "')", true);
            }
        }
    }

    protected void GetPaymentAuthorizationData(ref PaymentAuthorization pa)
    {
        pa.CardTypeID = int.Parse(this.ddlCardType.SelectedValue);
        pa.CreditCardNumber = this.tbCardNumber.Text;
        pa.Email = this.tbEmail.Text;
        pa.EventName = this.tbEventName.Text;
        pa.ExparationDate = DateTime.Parse(this.ddlMonth.SelectedValue.ToString() + "/" + "01" + "/" + this.ddlYear.SelectedValue.ToString());
        pa.InvoiceID = this.tbInvoiceNumber.Text;
        pa.ProposalID = this.tbProposalID.Text;
        pa.CardholderName = this.tbCardHolderName.Text;
        pa.Preparer = this.hfPreparer.Value;

    }

    protected void GetPaymentAuthorizationDataExpanded(ref PaymentAuthorizationExpanded pa)
    {
        pa.CardTypeID = int.Parse(this.ddlCardType.SelectedValue);
        pa.CreditCardNumber = this.tbCardNumber.Text;
        pa.Email = this.tbEmail.Text;
        pa.EventName = this.tbEventName.Text;
        pa.ExparationDate = DateTime.Parse(this.ddlMonth.SelectedValue.ToString() + "/" + "01" + "/" + this.ddlYear.SelectedValue.ToString());
        pa.InvoiceID = this.tbInvoiceNumber.Text;
        pa.ProposalID = this.tbProposalID.Text;
        pa.CardholderName = this.tbCardHolderName.Text;
        pa.Preparer = this.hfPreparer.Value;
        pa.City = this.tbCity.Value;
        pa.Zip = this.tbZip.Value;
        pa.BillingAddress = this.tbAddress.Value;
        pa.Phone = this.tbPhone.Value;
        pa.Country = this.cbDCC.Checked ? this.ddlCountry.Items[this.ddlCountry.SelectedIndex].Text : this.ddlCountry.Items[this.ddlCountry.SelectedIndex].Value;
        pa.State = this.tbState.Value;
        pa.PaymentInstructions = this.textAreaInstruction.Value;
        pa.DescriptionOfServices = this.txtDescriptionOfServices.Value;
        pa.CardType = this.ddlCardType.SelectedItem.Text;
        pa.DCC = this.cbDCC.Checked;
        pa.Merchant = (int)MerchantAccount;
    }

    protected void CelarFormFields()
    {
        this.tbCardHolderName.Text = "";
        this.tbCardNumber.Text = "";
        this.tbEventName.Text = "";
        this.tbInvoiceNumber.Text = "";
        this.tbProposalID.Text = "";
        this.ddlCardType.SelectedValue = "-1";
        this.ddlMonth.SelectedIndex = 0;
        this.ddlYear.SelectedIndex = 0;
        this.tbEmail.Text = "";
        this.hfCMS.Value = "";
        this.hfPreparer.Value = "";
        this.tbCity.Value = "";
        this.tbZip.Value = "";
        this.tbAddress.Value = "";
        this.tbPhone.Value = "";
        this.ddlCountry.SelectedIndex = 0;
        this.tbState.Value = "";
        this.textAreaInstruction.Value = "";
        this.txtDescriptionOfServices.Value = "";
        this.cbDCC.Checked = false;
    }
}