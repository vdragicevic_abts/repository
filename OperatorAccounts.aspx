﻿<%@ Page Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="OperatorAccounts.aspx.cs" Inherits="Admin_OperatorAccounts" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <style type="text/css">
    .floatLeft {float:left; margin-right:15px;}
    </style>
    
    <div style="width:600px; min-height:610px; float:left; margin-top:10px; margin-left:40px; font-size:12px; color:Black;">
		
		<p style="font-size:16px; color:#990033; font-weight:bold; margin-bottom:20px;">
            Create account</p>
    	
		<asp:Panel ID="Register" runat="server" Visible="true">
		
		    <div class="floatLeft">
		        <asp:Label ID="lblCompanyName" runat="server" Text="Company Name: "></asp:Label><br />
			    <asp:TextBox ID="tbCompanyName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
				    ID="rfvCompanyName" runat="server" ErrorMessage="*Enter company name!" Text="*" ControlToValidate="tbCompanyName" Display="Dynamic"></asp:RequiredFieldValidator><br /><br />
			</div>
			<div class="floatLeft">
			    <asp:Label ID="lblFirstName" runat="server" Text="First Name: "></asp:Label><br />
			    <asp:TextBox ID="tbFirstName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
				    ID="rfvFirstName" runat="server" ErrorMessage="*Enter first name!" Text="*" ControlToValidate="tbFirstname" Display="Dynamic"></asp:RequiredFieldValidator><br /><br />
			</div>
			<br />	
			<div class="floatLeft">
			    <asp:Label ID="lblLastName" runat="server" Text="Last/Family Name:"></asp:Label><br />
			    <asp:TextBox ID="tbLastName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
				    ID="rfvLastName" runat="server" ErrorMessage="*Enter last name!" Text="*" ControlToValidate="tbLastName" Display="Dynamic"></asp:RequiredFieldValidator><br /><br />
			</div>
			<div class="floatLeft">
			    <asp:Label ID="lblAddress" runat="server" Text="Street Address: "></asp:Label><br />
			    <asp:TextBox ID="tbAddress" runat="server"></asp:TextBox><asp:RequiredFieldValidator 
			        ID="rfvAddress" runat="server" ErrorMessage="*Enter address!" Text="*" ControlToValidate="tbAddress" Display="Dynamic"></asp:RequiredFieldValidator><br /><br />	
			</div>
			<br />
			<div class="floatLeft">
			    <asp:Label ID="lblCity" runat="server" Text="City: "></asp:Label><br />
			    <asp:TextBox ID="tbCity" runat="server"></asp:TextBox><asp:RequiredFieldValidator 
			        ID="rfvCity" runat="server" ErrorMessage="*Enter city!" Text="*" ControlToValidate="tbCity" Display="Dynamic"></asp:RequiredFieldValidator><br /><br />	
			</div>
			<div class="floatLeft">    
			    <asp:Label ID="lblZip" runat="server" Text="Zip/Postal Code: "></asp:Label><br />
			    <asp:TextBox ID="tbZip" runat="server"></asp:TextBox><asp:RequiredFieldValidator 
			        ID="rfvZip" runat="server" ErrorMessage="*Enter zip code!" Text="*" ControlToValidate="tbZip" Display="Dynamic"></asp:RequiredFieldValidator><br /><br />	    
			</div>
			<br />
            <br /><br />            
			<div class="floatLeft">
			    <asp:Label ID="lblCountry" runat="server" Text="Country: "></asp:Label><br />
			    <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlCountry_SelectedIndexChanged">
			    </asp:DropDownList><asp:Label ID="lblState" runat="server" Visible="false" Text="State: " ><br /></asp:Label>
                <asp:DropDownList ID="ddlState" runat="server" Visible="false"></asp:DropDownList> 
			</div>
			<br />
            <br />
            <br />
			<br /><br /><br /><br />
			<div class="floatLeft">    
			    <asp:Label ID="lblEmail" runat="server" 
                    Text="E-mail address:"></asp:Label><br />
			    <asp:TextBox ID="tbEmail" runat="server"></asp:TextBox><asp:RequiredFieldValidator
				    ID="rfvEmail" runat="server" ErrorMessage="*Enter email!" Text="*" ControlToValidate="tbEmail" Display="Dynamic"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
					    ID="revEmail" runat="server" ErrorMessage="*Not a valid email!!" Text="*" ControlToValidate="tbEmail" Display="Dynamic"
					    ValidationExpression="^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$"></asp:RegularExpressionValidator>
			</div>
			<div class="floatLeft">
			    <asp:Label ID="lblPhone" runat="server" Text="Phone number: "></asp:Label><br />
			    <asp:TextBox ID="tbPhone" runat="server"></asp:TextBox>
			    <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ErrorMessage="*Enter phone" Text="*" ControlToValidate="tbPhone" Display="Dynamic"></asp:RequiredFieldValidator><br /><br />
			</div>	
									
			<div class="floatLeft">		
			<asp:Label ID="lblUserName" runat="server" Text="Desired username: "></asp:Label><br />
			<asp:TextBox ID="tbUserName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
				ID="rfvUserName" runat="server" ErrorMessage="*Enter username!" Text="*" ControlToValidate="tbUserName" Display="Dynamic"></asp:RequiredFieldValidator><br /><br />
			</div>
			<div class="floatLeft">	
			    <asp:Label ID="lbPassword" runat="server" Text="Desired Password: "></asp:Label><br />
			    <asp:TextBox ID="tbPassword" runat="server" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator
				    ID="rfvPassword" runat="server" ErrorMessage="*Enter password" Text="*" ControlToValidate="tbPassword" Display="Dynamic"></asp:RequiredFieldValidator><br /><br />
			</div>			
			<br />
			<br />
			<br />
			<br />
			<div class="floatLeft">
			<asp:Button ID="Submit" Text="Submit" runat="server" OnClick="Submit_Click" 
                Enabled="true" />
            </div>

		</asp:Panel>
		<asp:Panel ID="pnUserAccounts" runat="server">
		<p style="font-size:16px; color:#990033; font-weight:bold; margin-bottom:20px;">Existing accounts</p>
		<asp:DataGrid ID="dgrAccounts" runat="server" AutoGenerateColumns="False" 
                BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
                CellPadding="3" Width="536px">
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" 
                Mode="NumericPages" />
            <ItemStyle ForeColor="#000066" />
            <Columns>
                <asp:BoundColumn DataField="FullName" HeaderText="Full Name"></asp:BoundColumn>
                <asp:BoundColumn DataField="UserName" HeaderText="User Name"></asp:BoundColumn>
            </Columns>
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            </asp:DataGrid> 
		
		</asp:Panel>
		</div> 
</asp:Content>

