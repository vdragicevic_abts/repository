﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="RepositoryAuthorizationPaymentReport.aspx.cs" Inherits="RepositoryAuthorizationPaymentReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TitleHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AdminPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
    div.ui-datepicker, .ui-datepicker td {
        font-size: 10px;
    }

    .DropDownStyle {
        background-color: palegoldenrod;
    }
</style>
<script language="javascript" type="text/javascript">

    $(document).ready(function () {

        SetPickers();
    });

    function SetPickers() {
        $('.DateTimePickerFrom').datepicker();
        $('.DateTimePickerTo').datepicker();
    }

</script>
<script type="text/javascript">

    $("[id*='text']").live('keypress', function (e) {
        return isNumberKey(e);
    });
    
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }

    


    $("[id*=chkHeader]").live("click", function () {
        var chkHeader = $(this);
        var grid = $(this).closest("table");
        $("input[type=checkbox]", grid).each(function () {
            if (chkHeader.is(":checked")) {
                if (!$(this).is(":disabled")) {
                    $(this).attr("checked", "checked");
                    $("td", $(this).closest("tr")).addClass("selected");
                }
            } else {
                if (!$(this).is(":disabled")) {
                    $(this).removeAttr("checked");
                    $("td", $(this).closest("tr")).removeClass("selected");
                }
            }
        });
    });
    
    $("[id*=chkRow]").live("click", function () {
        var grid = $(this).closest("table");
        var chkHeader = $("[id*=chkHeader]", grid);
        if (!$(this).is(":checked")) {
            $("td", $(this).closest("tr")).removeClass("selected");
            chkHeader.removeAttr("checked");
        } else {
            $("td", $(this).closest("tr")).addClass("selected");
            if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                chkHeader.attr("checked", "checked");
            }
        }
    });
    
    function CheckAmounts() {
        var b = 0;
        var grid = $('#<%=gvAuthorizationPayment.ClientID %>').closest("table");
        $("input[type*='text']", grid).each(function() {
            if (($(this).val() == "0.00")) {
                if (!$(this).is('[readonly]')) {

                    var a = $(this).parent("td").closest('tr').find($("input[type*='checkbox']")).is(":checked");
                    
                    if (a) {
                        $(this).css('background-color', 'palegoldenrod');
                        b++;
                        
                        var ddl = $(this).parent("td").closest('tr').find('select');
                        if (ddl.val() == '0') {
                            $(ddl).addClass('DropDownStyle');
                            b++;
                        }
                    }
                }
            } else {
                
                if ($(this).val() != "0.00") {
                    $(this).css('background-color', 'white');
                }
                
                var a = $(this).parent("td").closest('tr').find($("input[type*='checkbox']")).is(":checked");

                if (a) {

                    var ddl = $(this).parent("td").closest('tr').find('select');
                    if (ddl.val() == '0') {
                        $(ddl).addClass('DropDownStyle');
                        b++;
                    } else {
                        $(ddl).removeClass('DropDownStyle');
                    }
                }
            }
        });
        
        if (b > 0) {
            return false;
        } else {
            
            if (confirm('Are you sure you want to charge selected transactions?')) {
                return true;
            } else {
                return false;
            }
            
        }
    }    

</script>


    <div style="clear: both"></div>
    <div style="border-style: none; border-color: inherit; border-width: 2px; background-color: Gray; height: 80px; padding-top: 15px; padding-left: 10px; width: 911px;">
        <div style="float:left;">
        <span>Date From: </span><br/>
        <asp:TextBox ID="tbDateFrom" runat="server" CssClass="DateTimePickerFrom"></asp:TextBox>
        </div>
        <div style="float:left; margin-left:10px;">
        <span>Date To: </span><br/>
        <asp:TextBox ID="tbDateTo" runat="server" CssClass="DateTimePickerTo"></asp:TextBox>
        </div>
        <div style="float:left; margin-left: 10px;">
        <span>CC Number: </span><br/>
        <asp:TextBox ID="tbCCNumber" runat="server" MaxLength="4" Width="60px"></asp:TextBox>
        </div>
        <div style="float:left; margin-left:10px;">
        <span>Cardholder name: </span><br/>
        <asp:TextBox ID="tbCardHolderName" runat="server" Width="240px"></asp:TextBox>
        </div>
        <div style="float:left; margin-left: 10px;">
        <span>Merchant</span><br/>
        <asp:DropDownList runat="server" ID="ddlMerchantSearch" >
             <asp:listitem text="All" value="0" selected="True" />
             <asp:listitem text="IGD INTERNATIONAL GROUP" value="1" selected="False" />
             <asp:listitem text="CMR GLOBAL GROUP SERVICE" value="2" selected="False" />
             <asp:listitem text="ABTS CONVENTION SERVICES" value="3" selected="False" />
             <asp:listitem text="INTERNATIONAL GROUP HOUSING" value="4" selected="False" />
             <asp:listitem text="SWME" value="5" selected="False" />
             <asp:listitem text="IGD INTERNATIONAL GROUP PP" value="6" selected="False" />
             <asp:listitem text="CMR GLOBAL GROUP SERVICE PP" value="7" selected="False" />             
             <asp:listitem text="IGH INTERNATIONAL GROUP HOUSING PP" value="8" selected="False" />
			 <asp:listitem text="CMR GLOBAL GROUP SERVICE AMEX PP" value="9" selected="False" />
			 <asp:listitem text="IGD INTERNATIONAL GROUP AMEX PP" value="10" selected="False" />
			 <asp:listitem text="IGH INTERNATIONAL GROUP HOUSING AMEX PP" value="11" selected="False" />
        </asp:DropDownList>
        </div>
        <br/><br/><br/>
        <div align="center" style="float:right; margin-right:18px; margin-top:5px;">           
            <asp:Button ID="btnSearch" runat="server" Text="Search" Height="23px"
                OnClick="btnSearch_Click" />
        </div>
    </div>
    <br/>
    <div>
        <asp:GridView runat="server" ID="gvAuthorizationPayment" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowDataBound="gvAuthorizationPayment_RowDataBound">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="invoiceID" HeaderText="InvoiceID" />
                <asp:BoundField DataField="proposalID" HeaderText="ProposalID" />
                <asp:BoundField DataField="email" HeaderText="Email" />
                <asp:BoundField DataField="cardTypeID" HeaderText="CardType" />
                <asp:BoundField DataField="creditCardNumber" HeaderText="Credit Card Number" />
                <asp:BoundField DataField="exparationDate" HeaderText="Exparation Date" />
                <asp:BoundField DataField="cardholderName" HeaderText="Cardholder Name" />
                <asp:BoundField DataField="timestamp" HeaderText="Date" />
                <asp:BoundField DataField="transactionID" HeaderText="TransactionID" NullDisplayText=" " />
                <asp:TemplateField HeaderText="Amount">
                    <ItemTemplate>
                        <span>$</span>
                        <asp:TextBox Width="70px" ID="textBox" runat="server" Text='<%# Eval("amount") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Merchant">
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlMerchant" runat="server" Width="150" AppendDataBoundItems="true" DataValueField="merchant" SelectedValue='<%# Eval("merchant") %>'>
                             <asp:listitem text="Select" value="0" selected="True" />
                             <asp:listitem text="IGD INTERNATIONAL GROUP" value="1" selected="False" />
                             <asp:listitem text="CMR GLOBAL GROUP SERVICE" value="2" selected="False" />
                             <asp:listitem text="ABTS CONVENTION SERVICES" value="3" selected="False" />
                             <asp:listitem text="INTERNATIONAL GROUP HOUSING" value="4" selected="False" />
                             <asp:listitem text="SWME" value="5" selected="False" />
                             <asp:listitem text="IGD INTERNATIONAL GROUP PP" value="6" selected="False" />
                             <asp:listitem text="CMR GLOBAL GROUP SERVICE PP" value="7" selected="False" />             
                             <asp:listitem text="IGH INTERNATIONAL GROUP HOUSING PP" value="8" selected="False" />
							 <asp:listitem text="CMR GLOBAL GROUP SERVICE AMEX PP" value="9" selected="False" />
							 <asp:listitem text="IGD INTERNATIONAL GROUP AMEX PP" value="10" selected="False" />
							 <asp:listitem text="IGH INTERNATIONAL GROUP HOUSING AMEX PP" value="11" selected="False" />
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkHeader" runat="server" />                        
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkRow" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="id" HeaderText="ID" />
                <asp:BoundField DataField="paymentInstructions" HeaderText="Pay&lt;br/&gt;Instr" HtmlEncode="False" />
                <asp:BoundField DataField="dcc" HeaderText="DCC" />
                <asp:BoundField DataField="token"></asp:BoundField>
                <asp:BoundField DataField="exchangeRate" HeaderText="Exchange&lt;br/&gt;Rate" HtmlEncode="False" />
                <asp:BoundField DataField="rateResponseId" />
                <asp:BoundField DataField="descriptionOfServices" HeaderText="Description&lt;br/&gt;of service" HtmlEncode="False" />
            </Columns>
            <EditRowStyle BackColor="#999999" />            
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        </asp:GridView>
    </div>
    <div align="right" style="margin-top:10px; margin-bottom: 10px;">
        <asp:Button Width="130" runat="server" ID="btnCharge" Text="Charge Selected" OnClientClick="return CheckAmounts()" OnClick="btnCharge_Click"/>
    </div>
</asp:Content>

