﻿<%@ WebHandler Language="C#" Class="Individuals" %>

using System;
using System.Web;

public class Individuals : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        try
        {
            string cardholder = context.Request["cardholder"];
            string address = context.Request["address"];
            string phone = context.Request["phone"];
            string email = context.Request["email"];
            string city = context.Request["city"];
            string postal = context.Request["postal"];
            string country = context.Request["country"];
            string cardType = context.Request["cardType"];
            string expireMonth = context.Request["expireMonth"];
            string expireYear = context.Request["expireYear"];
            string cardnumber = context.Request["cardnumber"];
            string ccv = context.Request["ccv"];
            string amount = context.Request["amountcc"];
            string code = context.Request["code"];
            string gatewayID = context.Request["gatewayID"];
            string paymentDetailCCID = String.Empty;
            string conferenceID = String.Empty;
            string userID = String.Empty;
            string callback = context.Request["callback"];
            string currency = context.Request["currency"];
            string test = context.Request["test"];
            string conferenceName = context.Request["conferenceName"];
           
            
            ParseCode(code, ref paymentDetailCCID, ref conferenceID, ref userID);

            string transactionID = SendPayPalPayment2Repository(int.Parse(paymentDetailCCID), int.Parse(userID), int.Parse(cardType), cardnumber, ccv, cardholder, new DateTime(int.Parse("20" + expireYear), int.Parse(expireMonth), 1), conferenceID, address, city, country, email, phone, postal, "Individual Booking Payment " + conferenceID, amount, conferenceID + userID + ", " + cardholder + ", " + email, gatewayID, conferenceName, Boolean.Parse(test), currency);

            context.Response.ContentType = "text/plain";
            context.Response.Write(callback + "({ \"transactionID\": \"" + transactionID + "\" })");
        }
        catch (Exception ex)
        {
            string callback = context.Request["callback"];
            context.Response.Write(callback + "({ \"transactionID\": \"" + "0" + "\" })");
        }                               
    }

    protected void ParseCode(string _code, ref string paymentDetailCCID, ref string conferenceID, ref string userID)
    {
        string code = _code.Trim();

        char[] c = code.ToCharArray();

        paymentDetailCCID = String.Empty;
        conferenceID = String.Empty;
        userID = String.Empty;

        bool firstNumAfterLetter = false;
        int yearCount = 0;

        foreach (char chr in c)
        {
            if (char.IsDigit(chr))
            {
                if (yearCount == 2)
                {
                    firstNumAfterLetter = false;
                    userID += chr;
                }

                if (firstNumAfterLetter)
                {
                    conferenceID += chr;
                    yearCount += 1;
                }
                else
                {
                    if (yearCount == 0)
                    {
                        paymentDetailCCID += chr;
                    }
                }
            }

            if (char.IsLetter(chr))
            {
                conferenceID += chr;
                firstNumAfterLetter = true;
            }
        }
    }


    protected string SendPayPalPayment2Repository(int id, int userID, int cardType, string cardNumber, string ccv, string cardHolder, DateTime exparationDate, string conferenceID, string address, string city, string country, string email, string phone, string postal, string productDescription, string price, string province, string gatewayID, string conferenceName, bool test, string currency)
    {

        string fullName = string.Empty;
        string userEmail = string.Empty;
        string transactionID = string.Empty;

        User us = new User();
        us.UserID = "Admin";
        us.Password = "password";

        Users users = new Users();
        users.CurrentUser = us;

        IndividualPaymentInternetSecure ip = new IndividualPaymentInternetSecure();

        ip.CardHolderName = cardHolder;
        ip.CardTypeID = cardType;
        ip.CCV = ccv;
        ip.ConferenceID = conferenceID;
        ip.CreditCardNumber = cardNumber;
        ip.ExparationDate = exparationDate;
        ip.PaymentDetailCCID = id;
        ip.UserID = userID;
        ip.Address = address;
        ip.City = city;
        ip.Country = country;
        ip.Email = email;
        ip.Phone = phone;
        ip.Postal = postal;
        ip.ProductCode = id.ToString();
        ip.ProductDescription = productDescription;        
        ip.ProductPrice = price;
        ip.Province = id.ToString() + conferenceID + userID.ToString() + ", " + cardHolder + ", " + email;
        ip.GatewayID = int.Parse(gatewayID);
        ip.Currency = currency;
        ip.Test = test;

        try
        {
            transactionID = users.SendIPToRepositoryInternetSecureRQ(ip);
        }
        catch (Exception ex)
        {

        }

        return transactionID;
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}