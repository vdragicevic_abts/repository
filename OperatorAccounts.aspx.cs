﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Xml;
using System.IO;

public partial class Admin_OperatorAccounts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {            
            FillDDLCountries();
            FillDDLStates(); 

            GetAllAccounts();
        }
    }

    protected void FillDDLCountries()
    {       
        DataSet dsCountries = new DataSet();

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath(".\\") + "\\" + "App_Data\\Countries.xml");

        dsCountries.ReadXml(reader);
      
        ListItem item = new ListItem("Select", "-1");

        ddlCountry.DataTextField = "Country";
        ddlCountry.DataValueField = "id";
        ddlCountry.DataSource = dsCountries.Tables[0];
        ddlCountry.DataBind();

        ddlCountry.Items.Insert(0, item);

    }

    protected void FillDDLStates()
    {
        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath(".\\") + "\\" + "App_Data\\States.xml");
        
        DataSet dsStates = new DataSet();

        dsStates.ReadXml(reader);
    
        ListItem item = new ListItem("Select", "-1");
 
        ddlState.DataTextField = "Name";
        ddlState.DataValueField = "IDState";
        ddlState.DataSource = dsStates.Tables[0];
        ddlState.DataBind();

        ddlState.Items.Insert(0, item);

    }

    protected void GetAllAccounts()
    {       
        DataTable dtUsers = new DataTable("Users");
        DataColumn dcFullName = new DataColumn("FullName", typeof(System.String));
        DataColumn dcUserName = new DataColumn("UserName", typeof(System.String));
        dtUsers.Columns.Add(dcFullName);
        dtUsers.Columns.Add(dcUserName);

        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(Server.MapPath(".\\") + "\\" + "App_Data\\Users.xml");

        DataSet dsUsers = new DataSet("Users");
        dsUsers.ReadXml(reader);

        foreach (DataRow dr in dsUsers.Tables[0].Rows)
        {
            DataRow drUsers = dtUsers.NewRow();

            drUsers["FullName"] = dr["FirstName"].ToString() + ' ' + dr["LastName"].ToString();
            drUsers["UserName"] = dr["UserName"].ToString();

            dtUsers.Rows.Add(drUsers);
        }   
               
       dgrAccounts.DataSource = dtUsers;
       dgrAccounts.DataBind();

       reader.Close();
     
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        string companyName = this.tbCompanyName.Text.Trim();
        string firstName = this.tbFirstName.Text.Trim();
        string lastName = this.tbLastName.Text.Trim();
        string eMail = this.tbEmail.Text.Trim();
        string username = this.tbUserName.Text.Trim();
        string password = this.tbPassword.Text.Trim();
        string phone = this.tbPhone.Text.Trim();
        string country = this.ddlCountry.SelectedValue;
        string address = this.tbAddress.Text;
        string zip = this.tbZip.Text.Trim();
        string city = this.tbCity.Text.Trim();
        string id = String.Empty;
        string state = this.ddlState.Visible ? this.ddlState.SelectedValue : "";

        XmlDocument doc = new XmlDocument();

        doc.Load(Server.MapPath(".\\") + "\\" + "App_Data\\Users.xml");

        int _id = int.Parse(doc.DocumentElement.LastChild.Attributes.Item(0).Value) + 1; 

        XmlElement _user = doc.CreateElement("user");
        XmlElement _firstname = doc.CreateElement("firstname");
        XmlElement _lastname = doc.CreateElement("lastname");
        XmlElement _email = doc.CreateElement("email");
        XmlElement _userName = doc.CreateElement("username");
        XmlElement _password = doc.CreateElement("password");
        XmlElement _phone = doc.CreateElement("phone");
        XmlElement _country = doc.CreateElement("country");
        XmlElement _address = doc.CreateElement("address");
        XmlElement _city = doc.CreateElement("city");
        XmlElement _state = doc.CreateElement("state");
        XmlElement _zip = doc.CreateElement("zip");

        XmlAttribute _userid = doc.CreateAttribute("userid");
        _userid.Value = _id.ToString();

        XmlText _firstnametext = doc.CreateTextNode(firstName);
        XmlText _lastnametext = doc.CreateTextNode(lastName);
        XmlText _emailtext = doc.CreateTextNode(eMail);
        XmlText _usernametext = doc.CreateTextNode(username);
        XmlText _passwordtext = doc.CreateTextNode(StringHelpers.Encrypt(password));
        XmlText _phonetext = doc.CreateTextNode(phone);
        XmlText _countrytext = doc.CreateTextNode(country);
        XmlText _addresstext = doc.CreateTextNode(address);
        XmlText _ziptext = doc.CreateTextNode(zip);
        XmlText _citytext = doc.CreateTextNode(city);
        XmlText _statetext = doc.CreateTextNode(state);

        _user.Attributes.Append(_userid);
        _user.AppendChild(_firstname);
        _user.AppendChild(_lastname);
        _user.AppendChild(_email);
        _user.AppendChild(_userName);
        _user.AppendChild(_password);
        _user.AppendChild(_phone);
        _user.AppendChild(_country);
        _user.AppendChild(_address);
        _user.AppendChild(_city);
        _user.AppendChild(_state);
        _user.AppendChild(_zip);

        _firstname.AppendChild(_firstnametext);
        _lastname.AppendChild(_lastnametext);
        _email.AppendChild(_emailtext);
        _userName.AppendChild(_usernametext);
        _password.AppendChild(_passwordtext);
        _phone.AppendChild(_phonetext);
        _country.AppendChild(_countrytext);
        _address.AppendChild(_addresstext);
        _city.AppendChild(_citytext);
        _state.AppendChild(_statetext);
        _zip.AppendChild(_ziptext);


        doc.DocumentElement.AppendChild(_user);
        doc.Save(Server.MapPath(".\\") + "\\" + "App_Data\\Users.xml");

	using (StreamWriter sw = File.AppendText(Server.MapPath("/Login.txt")))
	//using (StreamWriter sw = File.AppendText("C:\\Windows\\System32\\winevt\\Logs\\PaymentLog.txt"))
        {
            sw.WriteLine(username + ", " + System.DateTime.Now.ToString() + ", " + "Created Account (" + username + ")");
        }

        GetAllAccounts();

        ClearControls();        
    }

    protected void ClearControls()
    {
        this.tbCompanyName.Text = "";
        this.tbFirstName.Text = "";
        this.tbLastName.Text = "";
        this.tbEmail.Text = "";
        this.tbUserName.Text = "";
        this.tbPassword.Text = "";
        this.tbPhone.Text = "";
        this.ddlCountry.SelectedIndex = 0;
        this.tbAddress.Text = "";
        this.tbCity.Text = "";
        this.tbZip.Text = "";
    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCountry.SelectedValue == "226")
        {
            this.ddlState.Visible = true;
            this.lblState.Visible = true;
        }
        else
        {
            this.ddlState.Visible = false;
            this.lblState.Visible = false;
        }
    }
}
