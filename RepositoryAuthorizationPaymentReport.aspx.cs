﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;

public partial class RepositoryAuthorizationPaymentReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e) 
    {
        if (Session["Admin"] == null)
        {
            Response.Redirect("./2repository/Login.aspx");
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string connectionString = DatabaseConnection.GetConnectionString();
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = conn;
        command.CommandType = CommandType.Text;

        var sql = "Select conferenceID, " +
                              "id, " +
                              "invoiceID, " +
                              "proposalID, " +
                              "eventName, " +
                              "email, " +
                              "cardTypeID, " +
                              "creditCardNumber, " +
                              "exparationDate, " +
                              "[timestamp], " +
                              "billingAddress, " +
                              "phone, " +
                              "city, " +
                              "zip, " +
                              "country, " +
                              "[state], " +
                              "paymentInstructions, " +
                              "cardholderName, " +
                              "preparer, " +
                              "transactionID, " +
                              "descriptionOfServices, " +
                              "ISNULL(amount, '0.00') amount, " +
                              "Token, " +
                              "Case cardTypeID when 1 then 'Visa' when 2 then 'MasterCard' when 3 then 'American Express' end CardType, " +
                              "ISNULL(merchant, 0) merchant, " +
                              "ISNULL(dcc, 0) dcc, " +
                              "ISNULL(foreignCurrencySymbolHtml, '') + ' ' + CONVERT(nvarchar, ISNULL(exchangeRate, 0)) exchangeRate, " +
                              "ISNULL(rateResponseId, 'n/a') rateResponseId, " +
                              "ISNULL(foreignCurrencySymbolHtml, '') foreignCurrencySymbolHtml " +
                              "From dbo.PaymentAuthorization " +
                              "Where 1=1";
        
        String dateFrom = String.Empty;
        String dateTo = String.Empty;
        String ccNumber = String.Empty;
        String cardHolerName = String.Empty;

        if (this.tbDateFrom.Text.Length > 0 && tbDateTo.Text.Length > 0)
        {
            dateFrom = DateTime.Parse(this.tbDateFrom.Text).ToShortDateString();
            dateTo = DateTime.Parse(this.tbDateTo.Text).AddDays(1).ToShortDateString();

            sql += " AND timestamp >= @DateFrom and timestamp <= @DateTo";

            command.Parameters.Add(new SqlParameter("@DateFrom", dateFrom));
            command.Parameters.Add(new SqlParameter("@DateTo", dateTo));

        }

        if (tbCCNumber.Text.Length > 0)
        {
            ccNumber = tbCCNumber.Text;

            sql += " AND cc = @CC";

            command.Parameters.Add(new SqlParameter("@CC", ccNumber));
        }

       
        if (tbCardHolderName.Text.Length > 0)
        {
            cardHolerName = tbCardHolderName.Text;

            sql += " AND cardholderName like '%' + @CardHolerName + '%'";

            command.Parameters.Add(new SqlParameter("@CardHolerName", cardHolerName));
        }


        if (ddlMerchantSearch.SelectedValue != "0")
        {
            int merchant = int.Parse(ddlMerchantSearch.SelectedValue);
            sql += " AND merchant = @Merchant";
            command.Parameters.Add(new SqlParameter("@Merchant", merchant));
        }

        command.CommandText = sql;

        SqlDataAdapter sda = new SqlDataAdapter(command);

        DataTable dtAuthorizationPayment = new DataTable();

        try
        {
            sda.Fill(dtAuthorizationPayment);
        }
        catch (Exception ex)
        {
            
        }

        if (dtAuthorizationPayment.Rows.Count > 0)
        {
            gvAuthorizationPayment.DataSource = dtAuthorizationPayment;
            gvAuthorizationPayment.DataBind();

            ViewState["dtAuthorizationPayment"] = dtAuthorizationPayment;
        }
    }

    protected void gvAuthorizationPayment_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[15].Visible = false;
            e.Row.Cells[17].Visible = false;
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string authentication = StringHelpers.DecodeFrom64(Session["Authentication"].ToString());

            //if (e.Row.Cells[4].Text.Length > 16)
            //{
            //    e.Row.Cells[4].Text = StringHelpers.Decrypt(e.Row.Cells[4].Text).Length == 16 ? "XXXXXXXXXXXX-" + StringHelpers.Decrypt(e.Row.Cells[4].Text).Substring(12) : "XXXXXXXXXXX-" + StringHelpers.Decrypt(e.Row.Cells[4].Text).Substring(11);
            //    //e.Row.Cells[4].Text = StringHelpers.Decrypt(e.Row.Cells[4].Text);

            //    e.Row.Cells[5].Text = "XX/XX/XXXX";
            //}
            //else
            //{
            //    e.Row.Cells[5].Text = "XX/XX/XXXX";
            //}

            if (authentication.Contains("akostka") || authentication.Contains("jbrescia"))
            {
                //e.Row.Cells[4].Text = StringHelpers.Decrypt(e.Row.Cells[15].Text);
                e.Row.Cells[4].Text = StringHelpers.Decrypt(e.Row.Cells[4].Text);
                e.Row.Cells[5].Text = DateTime.Parse(e.Row.Cells[5].Text).ToShortDateString();
            }
            else
            {
                //e.Row.Cells[4].Text = StringHelpers.Decrypt(e.Row.Cells[15].Text).Length == 16 ? "XXXXXXXXXXXX-" + StringHelpers.Decrypt(e.Row.Cells[15].Text).Substring(12) : "XXXXXXXXXXX-" + StringHelpers.Decrypt(e.Row.Cells[15].Text).Substring(11);
                e.Row.Cells[4].Text = StringHelpers.Decrypt(e.Row.Cells[4].Text).Length == 16 ? "XXXXXXXXXXXX-" + StringHelpers.Decrypt(e.Row.Cells[4].Text).Substring(12) : "XXXXXXXXXXX-" + StringHelpers.Decrypt(e.Row.Cells[4].Text).Substring(11);
                e.Row.Cells[5].Text = "XX/XX/XXXX";
            }

            //Show tooltip
            e.Row.Cells[13].ToolTip = e.Row.Cells[13].Text;

            //Show only 3 letters of Payment Instructions
            e.Row.Cells[13].Text = e.Row.Cells[13].Text.Length > 3 ? e.Row.Cells[13].Text.Substring(0, 3) + "..." : e.Row.Cells[13].Text;

            
            //Show 6 digits of credit card number if dcc otherwise 'n/a'
            if (e.Row.Cells[14].Text == "True")
            {
                e.Row.Cells[14].Text = e.Row.Cells[4].Text.Substring(0, 6);
            }
            else
            {
                e.Row.Cells[14].Text = "n/a";
            }

            //e.Row.Cells[7].Text = DateTime.Parse(e.Row.Cells[7].Text).ToShortDateString();

            switch (e.Row.Cells[3].Text)
            {
                case "1": e.Row.Cells[3].Text = "Visa";
                    break;
                case "2": e.Row.Cells[3].Text = "Master/Euro Card";
                    break;
                case "3": e.Row.Cells[3].Text = "American Express";
                    break;
                default: e.Row.Cells[3].Text = "n/a";
                    break;
            }

            if (e.Row.Cells[8].Text.Trim().Length > 3)
            {
                ((CheckBox) e.Row.Cells[11].Controls[1]).Enabled = false;
                ((TextBox) e.Row.Cells[9].Controls[1]).ReadOnly = true;
                ((TextBox) e.Row.Cells[9].Controls[1]).BackColor = Color.Gainsboro;
                ((TextBox) e.Row.Cells[9].Controls[1]).BorderStyle = BorderStyle.Groove;
                ((TextBox) e.Row.Cells[9].Controls[1]).ForeColor = Color.DimGray;
                ((DropDownList)e.Row.Cells[10].Controls[1]).Enabled = false;
                e.Row.Cells[14].Text = "n/a";
                
                //e.Row.Cells[10].Controls[1].Visible = false;
                //e.Row.Cells[10].Controls[0].Visible = false;
            }
			
			if (e.Row.Cells[15].Text.Length > 0)
            {
                var token = StringHelpers.Decrypt(e.Row.Cells[15].Text);
                if (token.Length <= 3 || token.Contains("Error"))
                {
                    ((CheckBox)e.Row.Cells[11].Controls[1]).Enabled = false;
                    ((TextBox)e.Row.Cells[9].Controls[1]).ReadOnly = true;
                    ((TextBox)e.Row.Cells[9].Controls[1]).BackColor = Color.Gainsboro;
                    ((TextBox)e.Row.Cells[9].Controls[1]).BorderStyle = BorderStyle.Groove;
                    ((TextBox)e.Row.Cells[9].Controls[1]).ForeColor = Color.DimGray;
                    ((DropDownList)e.Row.Cells[10].Controls[1]).Enabled = false;

                    //Show the error
                    e.Row.Cells[8].Text = token;
                }
            }
            else
            {
                ((CheckBox)e.Row.Cells[11].Controls[1]).Enabled = false;
                ((TextBox)e.Row.Cells[9].Controls[1]).ReadOnly = true;
                ((TextBox)e.Row.Cells[9].Controls[1]).BackColor = Color.Gainsboro;
                ((TextBox)e.Row.Cells[9].Controls[1]).BorderStyle = BorderStyle.Groove;
                ((TextBox)e.Row.Cells[9].Controls[1]).ForeColor = Color.DimGray;
                ((DropDownList)e.Row.Cells[10].Controls[1]).Enabled = false;
            }

            //Show token instead of credit card
            //e.Row.Cells[4].Text = StringHelpers.Decrypt(e.Row.Cells[15].Text);
			
            //Make token column hidden
            e.Row.Cells[15].Visible = false;

            //Make rateRsponseId column hidden
            e.Row.Cells[17].Visible = false;

            //Show tooltip
            e.Row.Cells[18].ToolTip = e.Row.Cells[18].Text;
            //Show only 3 letters of Description of Services
            e.Row.Cells[18].Text = e.Row.Cells[18].Text.Length > 3 ? e.Row.Cells[18].Text.Substring(0, 3) + "..." : e.Row.Cells[18].Text;            
        }
    }

    protected void btnCharge_Click(object sender, EventArgs e)
    {
		ServicePointManager.Expect100Continue = true;
        ServicePointManager.DefaultConnectionLimit = 9999;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		
        var us = new User();
        us.UserID = "Admin";
        us.Password = "password";
        var usr = new Users();
        usr.CurrentUser = us;
        string message = "'";

        foreach (GridViewRow gvr in gvAuthorizationPayment.Rows)
        {
            if (gvr.RowType == DataControlRowType.DataRow)
            {
                if (((CheckBox) gvr.Cells[11].Controls[1]).Checked)
                {
					
                    string merchantID = ((DropDownList) gvr.Cells[10].Controls[1]).SelectedValue;

                    Merchant merchant = (Merchant)Enum.Parse(typeof (Merchant), merchantID);

                    var itemIndex = gvr.DataItemIndex;

                    var dtAuthPayment =  (DataTable)ViewState["dtAuthorizationPayment"];
                    DataRow dr = dtAuthPayment.Rows[itemIndex];
                    var pa = new PaymentAuthorizationFirstData();
                    pa.ProductPrice = ((TextBox) gvr.Cells[9].Controls[1]).Text;
                    GetPaymentAuthorizationFirstData(ref pa, dr, merchant);

                    string token = String.Empty;
                    var transactionID = String.Empty;
                    var cRate = new CardRate();
                    cRate.Details = new CardRateDetails();

                    string dccMessage = String.Empty;

                    if (!merchant.ToString().Contains("PP"))
                    {
                        if (pa.Token.Length > 0)
                        {
                            if (!pa.DCC)
                            {
                                transactionID = InternetSecureAPI.SendUserInformation2BoaRestServiceTokenVersion(pa.InvoiceID, pa.ProposalID, pa.ProductPrice, pa.CardHolderName, pa.Token, pa.CardType, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), false, pa.ExactID, pa.Password, merchant);
                            }
                            else
                            {
                                if (pa.RateResponseID == "n/a")
                                {
                                    cRate = InternetSecureAPI.PayeezyDccCardRateRequest(pa.Test, merchant, pa.CreditCardNumber.Substring(0, 6), pa.ProductPrice);
                                }
                                else
                                {
                                    cRate.Details.RateResponseId = pa.RateResponseID;
                                    cRate.BinQualifies = true;
                                    cRate.DccOffered = true;
                                }

                                if (cRate.BinQualifies && cRate.DccOffered)
                                {
                                    transactionID = InternetSecureAPI.SendUserInformation2PayeezyRestServiceTokenVersionDcc(pa.InvoiceID, pa.ProposalID, pa.ProductPrice, pa.CardHolderName, cRate.Details.RateResponseId, pa.Token, pa.CardType, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), false, pa.ExactID, pa.Password, merchant);
                                }
                                else
                                {
                                    transactionID = "998";
                                }
                            }
                        }
                        else
                        {
                            transactionID = InternetSecureAPI.SendUserInformation2BoaRestService(pa.ProductPrice, pa.ProductCode, pa.ProductDescription, pa.CardHolderName, pa.Province, pa.Address, pa.City, "", pa.Postal, pa.Country, pa.Phone, pa.Email, pa.CreditCardNumber, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), pa.CCV, pa.Currency, pa.Test, pa.ExactID, pa.Password, ref token, merchant);
                        }

                        //InternetSecureAPI.SendUserInformation2BoaRestService(pa.ProductPrice, pa.ProductCode, pa.ProductDescription, pa.CardHolderName, pa.Province, pa.Address, pa.City, "", pa.Postal, pa.Country, pa.Phone, pa.Email, pa.CreditCardNumber, pa.ExparationDate.Month.ToString().PadLeft(2, '0'), pa.ExparationDate.ToString("yy"), pa.CCV, pa.Currency, pa.Test, pa.ExactID, pa.Password);

                        token = pa.Token.Length > 0 ? pa.Token : token;

                        if (transactionID.Length > 3)
                        {
                            if (pa.DCC)
                            {
                                dccMessage = ", ForeignAmount: " + cRate.Details.ForeignCurrencySymbolHtml + " " + cRate.Details.ForeignAmount + ", ExchangeRate: " + cRate.Details.ExchangeRate;
                            }

                            message += "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + dccMessage + ", Status: " + "Sucessfull\\n";
                            PaymentAuthorizationHelper.UpdatePaymentAuthorizationWithAmountFirstData(pa.PaymentDetailCCID.ToString(), transactionID, pa.ProductPrice, token, merchant, cRate.Details.ExchangeRate, cRate.Details.ForeignCurrencySymbolHtml, cRate.Details.RateResponseId);

                            using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
                            {
                                //Save Transaction to Log
                                sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "Sucessfully Charged: " + "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Merchant: " + merchant.ToString());
                            }
                        }
                        else
                        {
                            string res = String.Empty;

                            try
                            {
                                var firstData = ((InternetSecureAPI.FirstData)Int64.Parse(transactionID));
                                string result = firstData.ToString();
                                res = new string((result.SelectMany((c, i) => i != 0 && char.IsUpper(c) && !char.IsUpper(result[i - 1]) ? new char[] { ' ', c } : new char[] { c })).ToArray());
                            }
                            catch (Exception)
                            {

                                res = "Invalid Transaction";
                            }

                            if (pa.DCC && (transactionID == "998"))
                            {
                                dccMessage = "CreditCard DCC Unavailable";
                                res = "Invalid Transaction";
                            }

                            message += "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", " + dccMessage + ", Status: " + res + "\\n";
                            PaymentAuthorizationHelper.UpdatePaymentAuthorizationWithAmountFirstData(pa.PaymentDetailCCID.ToString(), transactionID, "0.00", token, null, pa.ExchangeRate, pa.ForeignCurrencySymbolHtml, pa.RateResponseID);

                            using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
                            {
                                //Save Transaction to Log
                                sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "Not Sucessfully Charged: " + "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Merchant: " + merchant.ToString());
                            }
                        }
                    }
                    else
                    {
                        if (pa.Token.Length > 0)
                        {
							pa.MerchantAccount = (pa.MerchantAccount.ToString().Contains("PP") && pa.CardType.ToLower().Contains("american express")) ? (Merchant)Enum.Parse(typeof(Merchant), pa.MerchantAccount.ToString()) : pa.MerchantAccount;
                            
							if (!pa.DCC)
                            {

                                string responseText = string.Empty;
                                transactionID = new Planet.PlanetAPI(pa.MerchantAccount).SendUserInformation2PlanetTokenVersion(pa.Token, pa.ProductPrice, pa.ProductCode, pa.ProductDescription, Planet.Currency.USD, false, pa.MerchantAccount, ref responseText, Planet.CurrencyIndicator.DomesticTransaction, pa.CardType, pa.GuestName, "", "", pa.InvoiceID, "", pa.ProposalID);

                                token = pa.Token.Length > 0 ? pa.Token : token;

                                if (transactionID.Length > 3)
                                {
                                    message += "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + dccMessage + ", Status: " + "Sucessfull\\n";
                                    PaymentAuthorizationHelper.UpdatePaymentAuthorizationWithAmountFirstData(pa.PaymentDetailCCID.ToString(), transactionID, pa.ProductPrice, token, merchant, cRate.Details.ExchangeRate, cRate.Details.ForeignCurrencySymbolHtml, cRate.Details.RateResponseId);

                                    using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
                                    {
                                        //Save Transaction to Log
                                        sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "Sucessfully Charged: " + "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Merchant: " + merchant.ToString());
                                    }
                                }
                                else
                                {

                                    string res = "Invalid Transaction " + responseText;
                                    message += "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", " + dccMessage + ", Status: " + res + "\\n";
                                    PaymentAuthorizationHelper.UpdatePaymentAuthorizationWithAmountFirstData(pa.PaymentDetailCCID.ToString(), transactionID, "0.00", token, null, pa.ExchangeRate, pa.ForeignCurrencySymbolHtml, pa.RateResponseID);

                                }
                            }
                            else
                            {
                                //Get currency rate by Country
                                var curr = (Planet.Currency)Enum.Parse(typeof(Planet.Currency), new CountryList().GetRegionInfoByName(pa.Country).ISOCurrencySymbol, true);
                                string currencyCode = Convert.ToString((int)curr);

                                string success = String.Empty;
                                string rate = new Planet.PlanetAPI(merchant).SendUserInformation2PlanetRestServiceCurrencyRateRequestQuery(pa.Country, currencyCode, merchant, false, ref success);


                                if (rate.Length > 3)
                                {
                                    pa.ExchangeRate = rate;
                                    pa.RateResponseID = pa.ForeignCurrencySymbolHtml + " " + pa.ExchangeRate;

                                    Decimal calculatePriceByRate = Decimal.Parse(new Planet.PlanetAPI(merchant).CalculateAmountByCurrencyRate(pa.ProductPrice, rate));

                                    string responseText = String.Empty;
                                    var transID = new Planet.PlanetAPI(merchant).SendUserInformation2PlanetTokenVersion(pa.Token, Math.Round(calculatePriceByRate, 2).ToString(), pa.ProductCode, pa.ProductDescription, curr, pa.Test, pa.MerchantAccount, ref responseText, Planet.CurrencyIndicator.McpTransaction, pa.CardType, pa.GuestName, "", "", pa.InvoiceID, "", pa.ProposalID);

                                    if (transID.Length > 3)
                                    {
                                        if (pa.DCC)
                                        {
                                            dccMessage = ", ForeignAmount: " + pa.ForeignCurrencySymbolHtml + " " + Math.Round(calculatePriceByRate, 2) + ", ExchangeRate: " + pa.ExchangeRate;
                                        }

                                        message += "Proposal: " + pa.ProposalID + " , TransactionID: " + transID + ", Amount: " + "$ " + pa.ProductPrice + dccMessage + ", Status: " + "Sucessfull\\n";
                                        PaymentAuthorizationHelper.UpdatePaymentAuthorizationWithAmountFirstData(pa.PaymentDetailCCID.ToString(), transID, pa.ProductPrice, pa.Token, merchant, pa.ExchangeRate, pa.ForeignCurrencySymbolHtml, pa.RateResponseID);

                                        using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
                                        {
                                            //Save Transaction to Log
                                            sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "Sucessfully Charged: " + "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Merchant: " + merchant.ToString());
                                        }
                                    }
                                    else
                                    {
                                        string res = String.Empty;

                                        res = "Invalid Transaction";
                                        dccMessage = responseText;

                                        message += "Proposal: " + pa.ProposalID + " , TransactionID: " + transID + ", Amount: " + "$ " + pa.ProductPrice + ", " + dccMessage + ", Status: " + res + "\\n";
                                        PaymentAuthorizationHelper.UpdatePaymentAuthorizationWithAmountFirstData(pa.PaymentDetailCCID.ToString(), transID, "0.00", pa.Token, null, pa.ExchangeRate, pa.ForeignCurrencySymbolHtml, pa.RateResponseID);

                                        using (StreamWriter sw = File.AppendText("C:\\CustomLogs\\Log.txt"))
                                        {
                                            //Save Transaction to Log
                                            sw.WriteLine(Session["username"].ToString() + ", " + System.DateTime.Now.ToString() + ", " + "Not Sucessfully Charged: " + "Proposal: " + pa.ProposalID + " , TransactionID: " + transactionID + ", Amount: " + "$ " + pa.ProductPrice + ", Merchant: " + merchant.ToString());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        message = message + "'";

        ClientScript.RegisterStartupScript(this.GetType(), "000", "alert("+ message + ");", true);

        btnSearch_Click(this, null);
    }

    protected void GetPaymentAuthorizationFirstData(ref PaymentAuthorizationFirstData pa, DataRow dr, Merchant merchant)
    {
        pa.PaymentDetailCCID = int.Parse(dr["id"].ToString());
        pa.CardTypeID = int.Parse(dr["cardTypeID"].ToString());
        pa.CreditCardNumber = dr["creditCardNumber"].ToString().Length > 16 ? StringHelpers.Decrypt(dr["creditCardNumber"].ToString()) : dr["creditCardNumber"].ToString();
        pa.Email = dr["email"].ToString();
        pa.EventName = dr["eventName"].ToString();
        pa.ExparationDate = DateTime.Parse(dr["exparationDate"].ToString());
        pa.InvoiceID = dr["invoiceID"].ToString();
        pa.ProposalID = dr["proposalID"].ToString();
        pa.CardHolderName = dr["cardholderName"].ToString();
        pa.Preparer = dr["preparer"].ToString();
        pa.City = dr["city"].ToString();
        pa.Postal = dr["zip"].ToString();
        pa.Address = dr["billingAddress"].ToString();
        pa.Phone = dr["phone"].ToString();
        pa.Country = dr["country"].ToString();
        pa.Province = dr["state"].ToString();
        pa.PaymentInstructions = dr["paymentInstructions"].ToString();
        pa.Test = false;
        pa.CCV = ""; //this.tbCVV.Text;
        pa.ExactID = DatabaseConnection.GetGatewayID(merchant);
        pa.Password = DatabaseConnection.GetGatewayPassword(merchant);
        pa.ProductDescription = dr["proposalID"].ToString() + dr["eventName"].ToString() + dr["invoiceID"].ToString();
        pa.Currency = "USD";
        pa.DescriptionOfServices = dr["descriptionOfServices"].ToString();
        pa.ConferenceID = dr["eventName"].ToString();
        pa.Token = dr["Token"].ToString().Length > 0 ? StringHelpers.Decrypt(dr["Token"].ToString()) : "";
        pa.CardType = dr["CardType"].ToString();
        pa.DCC = Boolean.Parse(dr["dcc"].ToString()); //pa.CreditCardNumber.Substring(6) == "0000000000" ? true : false;
        pa.RateResponseID = dr["rateResponseId"].ToString();
        pa.ForeignCurrencySymbolHtml = dr["foreignCurrencySymbolHtml"].ToString();
        pa.ExchangeRate = dr["exchangeRate"].ToString();
        pa.MerchantAccount = merchant;
    }
}