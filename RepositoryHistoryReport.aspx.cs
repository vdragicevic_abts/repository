﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class RepositoryHistoryReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (this.tbDateFrom.Text.Length > 0 && tbDateTo.Text.Length > 0)
        {
            DateTime dateFrom = DateTime.Parse(this.tbDateFrom.Text);
            DateTime dateTo = DateTime.Parse(this.tbDateTo.Text).AddDays(1);
            Users users = new Users();
            User usr = new User();
            usr.UserID = "Admin";
            usr.Password = "password";

            users.CurrentUser = usr;


            DataTable dtRepositoryHistoryReport = users.repositoryHistoryReport(dateFrom, dateTo, ddlFormType.SelectedValue);

            DataView dvRepo = new DataView();


            if (ddlMerchant.SelectedValue == "0" && ddlFormType.SelectedValue == "0")
            {
                dvRepo = (from s in dtRepositoryHistoryReport.AsEnumerable()
                          select s).AsDataView();
            }
            else if (ddlMerchant.SelectedValue != "0" && ddlFormType.SelectedValue == "0")
            {
                dvRepo = (from s in dtRepositoryHistoryReport.AsEnumerable()
                          where s.Field<Int32>("merchant") == int.Parse(ddlMerchant.SelectedValue)
                          select s).AsDataView();
            }
            else if (ddlMerchant.SelectedValue != "0" && ddlFormType.SelectedValue != "0")
            {
                dvRepo = (from s in dtRepositoryHistoryReport.AsEnumerable()
                          where s.Field<Int32>("merchant") == int.Parse(ddlMerchant.SelectedValue) && s.Field<string>("formType") == ddlFormType.SelectedItem.Text
                          select s).AsDataView();
            }
            else if (ddlMerchant.SelectedValue == "0" && ddlFormType.SelectedValue != "0")
            {
                dvRepo = (from s in dtRepositoryHistoryReport.AsEnumerable()
                          where s.Field<string>("formType") == ddlFormType.SelectedItem.Text
                          select s).AsDataView();
            }

            DataView dv = dtRepositoryHistoryReport.AsDataView();
            dv.Sort = "formType, timestamp DESC";

            if (dtRepositoryHistoryReport != null && dtRepositoryHistoryReport.Rows.Count > 0)
            {
                dvRepo.Sort = "formType, timestamp DESC";
                gvRepositoryHistoryReport.DataSource = dvRepo;
                gvRepositoryHistoryReport.DataBind();
            }

            //DataTable dtRepositoryHistoryReport = users.repositoryHistoryReport(dateFrom, dateTo);

            //if (dtRepositoryHistoryReport != null && dtRepositoryHistoryReport.Rows.Count > 0)
            //{
            //    gvRepositoryHistoryReport.DataSource = dtRepositoryHistoryReport;
            //    gvRepositoryHistoryReport.DataBind();
            //}
        }
    }

    protected void gvRepositoryHistoryReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string authentication = StringHelpers.DecodeFrom64(Session["Authentication"].ToString());          

            if (!authentication.Contains("cconrad") && !authentication.Contains("jbrescia") && !authentication.Contains("mfleitas") && !authentication.Contains("akostka"))
            {
                if (e.Row.Cells[5].Text.Length > 16)
                {
                    e.Row.Cells[5].Text = StringHelpers.Decrypt(e.Row.Cells[5].Text).Length == 16 ? "XXXXXXXXXXXX-" + StringHelpers.Decrypt(e.Row.Cells[5].Text).Substring(12) : "XXXXXXXXXXX-" + StringHelpers.Decrypt(e.Row.Cells[5].Text).Substring(11);
                }
                else
                {
                    e.Row.Cells[5].Text = e.Row.Cells[5].Text.Length == 16 ? "XXXXXXXXXXXX-" + e.Row.Cells[5].Text.Substring(12) : "XXXXXXXXXXX-" + e.Row.Cells[5].Text.Substring(11);
                }

                //e.Row.Cells[5].Text = StringHelpers.Decrypt(e.Row.Cells[5].Text).Length == 16 ? "XXXXXXXXXXXX-" + StringHelpers.Decrypt(e.Row.Cells[5].Text).Substring(12) : "XXXXXXXXXXX-" + StringHelpers.Decrypt(e.Row.Cells[5].Text).Substring(11); //StringHelpers.Decrypt(e.Row.Cells[5].Text);
                e.Row.Cells[6].Text = "XX/XX/XXXX"; //DateTime.Parse(e.Row.Cells[6].Text).ToString("MM/dd/yyyy");             
            }
            else
            {
                e.Row.Cells[5].Text = e.Row.Cells[5].Text.Length > 16 ? StringHelpers.Decrypt(e.Row.Cells[5].Text) : e.Row.Cells[5].Text;
                e.Row.Cells[6].Text = DateTime.Parse(e.Row.Cells[6].Text).ToString("MM/dd/yyyy");
            }

            var merchant = GetMercnahts();
            merchant.Width = 150;            
            merchant.SelectedValue = e.Row.Cells[10].Text;
            merchant.ToolTip = merchant.SelectedItem.Text;
            e.Row.Cells[10].Text = "";

            if (merchant.SelectedValue.Length > 0)
                e.Row.Cells[10].Controls.Add(merchant);


        }

        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[11].Text = e.Row.Cells[11].Text.Split('T')[0] + "<br/>" + "T" + e.Row.Cells[11].Text.Split('T')[1];
            e.Row.Cells[12].Text = e.Row.Cells[12].Text.Split('T')[0] + "<br/>" + "T" + e.Row.Cells[12].Text.Split('T')[1];            
        }
    }

    private DropDownList GetMercnahts()
    {
        var ddlMerchants = new DropDownList();

        var li0 = new ListItem { Text = "", Value = "" };
        var li1 = new ListItem { Text = "IGD INTERNATIONAL GROUP", Value = "1" };
        var li2 = new ListItem { Text = "CMR GLOBAL GROUP SERVICE", Value = "2" };
        var li3 = new ListItem { Text = "ABTS CONVENTION SERVICES", Value = "3" };
        var li4 = new ListItem { Text = "INTERNATIONAL GROUP HOUSING", Value = "4" };
        var li5 = new ListItem { Text = "SWME", Value = "5" };
                         

        ddlMerchants.Items.Add(li0);
        ddlMerchants.Items.Add(li1);
        ddlMerchants.Items.Add(li2);
        ddlMerchants.Items.Add(li3);
        ddlMerchants.Items.Add(li4);
        ddlMerchants.Items.Add(li5);

        ddlMerchants.Enabled = false;

        return ddlMerchants;
    }
}