﻿<%@ WebHandler Language="C#" Class="SwmeBoa" %>

using System;
using System.Web;

public class SwmeBoa : IHttpHandler
{
    public void ProcessRequest (HttpContext context) {

        try
        {
            string cardholder = context.Request["cardholder"];
            string address = context.Request["address"];
            string phone = context.Request["phone"];
            string email = context.Request["email"];
            string city = context.Request["city"];
            string postal = context.Request["postal"];
            string country = context.Request["country"];
            string cardType = context.Request["cardType"];
            string expireMonth = context.Request["expireMonth"];
            string expireYear = context.Request["expireYear"];
            string cardnumber = context.Request["cardnumber"];
            string ccv = context.Request["ccv"];
            string amount = context.Request["amountcc"];
            string code = context.Request["code"];
            string exactID = context.Request["gatewayID"];
            string password = context.Request["gatewayPassword"];
            string conferenceID = String.Empty;
            string userID = String.Empty;
            string callback = context.Request["callback"];
            string currency = context.Request["currency"];
            string test = context.Request["test"];
            string conferenceName = context.Request["conferenceName"];
            string sessionID = context.Request["sessionID"];
            string regPayment = context.Request["regPayment"];           
            
            string transactionID = SendPayPalPayment2Repository(int.Parse(code), sessionID, regPayment , int.Parse(cardType), cardnumber, ccv, cardholder, new DateTime(int.Parse("20" + expireYear), int.Parse(expireMonth), 1), conferenceID, address, city, country, email, phone, postal, "Registration Payment " + conferenceID, amount, conferenceID + userID + ", " + cardholder + ", " + email, exactID, password, conferenceName, Boolean.Parse(test), currency);

            context.Response.ContentType = "text/plain";
            context.Response.Write(callback + "({ \"transactionID\": \"" + transactionID + "\" })");
        }
        catch (Exception ex)
        {
            string callback = context.Request["callback"];
            context.Response.Write(callback + "({ \"transactionID\": \"" + "40" + "\" })");
        }                               
    }

    protected void ParseCode(string _code, ref string paymentDetailCCID, ref string conferenceID, ref string userID)
    {
        string code = _code.Trim();

        char[] c = code.ToCharArray();

        paymentDetailCCID = String.Empty;
        conferenceID = String.Empty;
        userID = String.Empty;

        bool firstNumAfterLetter = false;
        int yearCount = 0;

        foreach (char chr in c)
        {
            if (char.IsDigit(chr))
            {
                if (yearCount == 2)
                {
                    firstNumAfterLetter = false;
                    userID += chr;
                }

                if (firstNumAfterLetter)
                {
                    conferenceID += chr;
                    yearCount += 1;
                }
                else
                {
                    if (yearCount == 0)
                    {
                        paymentDetailCCID += chr;
                    }
                }
            }

            if (char.IsLetter(chr))
            {
                conferenceID += chr;
                firstNumAfterLetter = true;
            }
        }
    }


    protected string SendPayPalPayment2Repository(int id, string sessionID, string regPayment, int cardType, string cardNumber, string ccv, string cardHolder, DateTime exparationDate, string conferenceID, string address, string city, string country, string email, string phone, string postal, string productDescription, string price, string province, string exactID, string password, string conferenceName, bool test, string currency)
    {

        string fullName = string.Empty;
        string userEmail = string.Empty;
        string transactionID = string.Empty;

        User us = new User();
        us.UserID = "Admin";
        us.Password = "password";

        Users users = new Users();
        users.CurrentUser = us;

        IndividualPaymentInternetSecure ip = new IndividualPaymentInternetSecure();

        ip.CardHolderName = cardHolder;
        ip.CardTypeID = cardType;
        ip.CCV = ccv;
        ip.ConferenceID = conferenceName; //conferenceID;
        ip.CreditCardNumber = cardNumber;
        ip.ExparationDate = exparationDate;
        ip.PaymentDetailCCID = id;
        ip.Address = address;
        ip.City = city;
        ip.Country = country;
        ip.Email = email;
        ip.Phone = phone;
        ip.Postal = postal;
        ip.ProductCode = id.ToString();
        ip.ProductDescription = sessionID;        
        ip.ProductPrice = price;
        //ip.Province = id.ToString() + conferenceID + userID.ToString() + ", " + cardHolder + ", " + email;
        ip.Province = id.ToString() + ", " + regPayment + ", " + cardHolder + ", " + email;         
        ip.ExactID = exactID;
        ip.Password = password;       
        ip.Currency = currency;
        ip.Test = test;
        
        try
        {          
            transactionID = users.SendSWMEToRepositoryBOARQ(ip);
        }
        catch (Exception ex)
        {

        }

        return transactionID;
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}